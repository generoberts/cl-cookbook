---
title: Home
---

> Cookbook, n.
> a book containing recipes and other information about the preparation and cooking of food.

*now with extra Lisp*


# Content

<p id="two-cols"></p>

* [License](license.html)
* [Getting started](getting-started.html)
* [Editor support](editor-support.html)
* [Functions](functions.html)
* [Data Structures](data-structures.html)
* [Strings](strings.html)
* [Numbers](numbers.html)
* [Loops, iteration, mapping](iteration.html)
* [Multidimensional Arrays](arrays.html)
* [Dates and Times](dates_and_times.html)
* [Pattern Matching](pattern_matching.html)
* [Regular Expressions](regexp.html)
* [Input/Output](io.html)
* [Files and Directories](files.html)
* [Error and condition handling](error_handling.html)
* [Packages](packages.html)
* [Macros and Backquote](macros.html)
* [CLOS (the Common Lisp Object System)](clos.html)
* [Type System](type.html)
* [Sockets](sockets.html)
* [Interfacing with your OS](os.html)
* [Foreign Function Interfaces](ffi.html)
* [Threads](process.html)
* [Defining Systems](systems.html)
* [Using the Win32 API](win32.html)
* [Debugging](debugging.html)
* [Performance Tuning](performance.html)
* [Scripting. Building executables](scripting.html)
* [Testing and Continuous Integration](testing.html)
* [Databases](databases.html)
* [Web development](web.html)
* [Web Scraping](web-scraping.html)
* [WebSockets](websockets.html)
* [Experimental CL21](cl21.html)
* [Miscellaneous](misc.html)

## Other CL Resources

* [lisp-lang.org](http://lisp-lang.org/)
* The [Awesome-cl](https://github.com/CodyReichert/awesome-cl) list
* [The Common Lisp HyperSpec](http://www.lispworks.com/documentation/HyperSpec/Front/index.htm) by Kent M. Pitman (also available in [Dash](https://kapeli.com/dash), [Zeal](https://zealdocs.org/) and [Velocity](https://velocity.silverlakesoftware.com/))
* [The Common Lisp UltraSpec](http://phoe.tymoon.eu/clus/doku.php)
* [Practical Common Lisp](http://www.gigamonkeys.com/book/) by Peter Seibel
* [Common Lisp Recipes](http://weitz.de/cl-recipes/) by Edmund Weitz, published in 2016,
* [Cliki](http://www.cliki.net/), Common Lisp's wiki
* [Articulate Common Lisp](http://articulate-lisp.com/), an initiation manual for the uninitiated
* [Common Lisp](https://en.wikibooks.org/wiki/Common_Lisp) on Wikibooks
* [The old comp.lang.lisp FAQ](https://common-lisp.net/project/lispfaq/FAQ/) by Mark Kantrowitz, Barry Margolin, and Christophe Rhodes
* [Common Lisp: A Gentle Introduction to Symbolic Computation](http://www-2.cs.cmu.edu/~dst/LispBook/) by David S. Touretzky
* [Successful Lisp: How to Understand and Use Common Lisp](https://successful-lisp.blogspot.com/p/httpsdrive.html) by David B. Lamkins
* [On Lisp](http://www.paulgraham.com/onlisptext.html) by Paul Graham
* [Common Lisp the Language, 2nd Edition](http://www-2.cs.cmu.edu/Groups/AI/html/cltl/cltl2.html) by Guy L. Steele
* [Common Lisp Hints](http://www.n-a-n-o.com/lisp/cmucl-tutorials/LISP-tutorial.html) by Geoffrey J. Gordon
* [Common Lisp Pitfalls](https://raw.githubusercontent.com/ebzzry/cl-pitfalls/master/cl-pitfalls.txt) by Jeff Dalton
* [A Tutorial on Good Lisp Style](https://www.cs.umd.edu/%7Enau/cmsc421/norvig-lisp-style.pdf) by Peter Norvig and Kent Pitman
* [Lisp and Elements of Style](http://web.archive.org/web/20190316190256/https://www.nicklevine.org/declarative/lectures/) by Nick Levine
* Pascal Costanza's [Highly Opinionated Guide to Lisp](http://www.p-cos.net/lisp/guide.html)
* [Loving Lisp - the Savy Programmer's Secret Weapon](https://leanpub.com/lovinglisp/) by Mark Watson
* [FranzInc](https://franz.com/), a company selling Common Lisp and Graph Database solutions.

## Further remarks

This is a collaborative project that aims to provide for Common Lisp something
similar to the [Perl Cookbook][perl] published by O'Reilly. More details about
what it is and what it isn't can be found in this [thread][thread] from
[comp.lang.lisp][cll].

If you want to contribute to the CL Cookbook, please send a pull request in or
file a ticket!

Yes, we're talking to you! We need contributors - write a chapter that's missing
and add it, find an open question and provide an answer, find bugs and report
them, (If you have no idea what might be missing but would like to help, take a
look at the [table of contents][toc] of the Perl Cookbook.) Don't worry about
the formatting, just send plain text if you like - we'll take care about that
later.

Thanks in advance for your help!

The pages here on Github are kept up to date. You can also download a
[up to date zip file][zip] for offline browsing. More info can be found at the
[Github project page][gh].

<div style="text-align: center">
    <img src="orly-cover.png"/>
</div>

[cll]: news:comp.lang.lisp
[perl]: http://www.oreilly.com/catalog/cookbook/
[thread]: http://groups.google.com/groups?threadm=m3it9soz3m.fsf%40bird.agharta.de
[toc]: http://www.oreilly.com/catalog/cookbook/
[zip]: https://github.com/LispCookbook/cl-cookbook/archive/master.zip
[gh]: https://github.com/LispCookbook/cl-cookbook

---
title: Getting started
---

Easy steps to install a development environment and start a project.

Want a 2-clicks install ? Then get
[Portacle](https://shinmera.github.io/portacle/), *a portable and
multi-platform* Common Lisp environment. It ships Emacs25, SBCL (the
implementation), Quicklisp (package manager), SLIME (IDE) and
Git. It's the most straightforward way to get going !

## Install an implementation

### With your package manager

TLDR;

    apt-get install sbcl

Common Lisp has been standardized via an ANSI document, so it can be
implemented in different ways: see
[Wikipedia's list of implementations](https://en.wikipedia.org/wiki/Common_Lisp#Implementations).

The following are packaged for Debian and probably for your distro:

* [SBCL](http://www.sbcl.org/)
* [ECL](https://gitlab.com/embeddable-common-lisp/ecl/)
  (compiles to C)
* [CMUCL](https://gitlab.common-lisp.net/cmucl/cmucl)
* [GCL](https://en.wikipedia.org/wiki/GNU_Common_Lisp)
* [CLISP](https://clisp.sourceforge.io/)

In doubt, just get SBCL.

See also

* [ABCL](http://abcl.org/) (for the JVM),
* [ClozureCL](https://ccl.clozure.com/)
* [CLASP](https://github.com/drmeister/clasp) (C++ and LLVM)
* [AllegroCL](https://franz.com/products/allegrocl/) (proprietary)
* [LispWorks](http://www.lispworks.com/) (proprietary)

and  this [Debian package for Clozure CL](http://mr.gy/blog/clozure-cl-deb.html).

### With Roswell

[Roswell](https://github.com/roswell/roswell/wiki) is:

* an implementation manager: it makes it easy to install a Common Lisp
  implementation (`ros install ecl`), an exact version of an
  implementation (`ros install sbcl/1.2.0`), to change the default one
  being used (`ros use ecl`),
* a scripting environment (helps to run Lisp from the shell, to get
  the command line arguments,…),
* a script installer,
* a testing environment (to run tests, including on popular Continuous
  Integration platforms),
* a building utility (to build images and executables in a portable way).

You'll find several ways of installation on its wiki (Debian package,
Windows installer, Brew/Linux Brew,…).


### With Docker

If you already know [Docker](https://docs.docker.com), you can get
started with Common Lisp pretty quickly. The
[daewok/lisp-devel-docker](https://github.com/daewok/lisp-devel-docker)
image comes with recent versions of SBCL, CCL, ECL and ABCL, plus
Quicklisp installed in the home (`/home/lisp`) so than we can
`ql:quickload` libraries straight away.

It works on GNU/Linux, Mac and Windows.

The following command will download the required image (around 400MB), put your
local sources inside the Docker image where indicated and drop you
into an SBCL REPL:

    docker run --rm -it -v /path/to/local/code:/usr/local/share/common-lisp/source daewok/lisp-devel:base sbcl

But we still want to develop from our Emacs and SLIME, so we need to
connect SLIME to the Lisp inside Docker. See
[slime-docker](https://github.com/daewok/slime-docker) for a library
that helps on setting that up.


## Start a REPL

Just launch the implementation executable on the command line to enter
the REPL (Read Eval Print Loop), i.e. the interactive
interpreter.

Quit with `(quit)` or `ctr-d` (on some implementations).

Here is a sample session:

```
user@debian:~$ sbcl
This is SBCL 1.3.14.debian, an implementation of ANSI Common Lisp.
More information about SBCL is available at <http://www.sbcl.org/>.

SBCL is free software, provided as is, with absolutely no warranty.
It is mostly in the public domain; some portions are provided under
BSD-style licenses.  See the CREDITS and COPYING files in the
distribution for more information.
* (+ 1 2)

3
* (quit)
user@debian:~$
```

You can slightly enhance the REPL (the arrow keys do not work,
it has no history,…) with `rlwrap`:

    apt-get install rlwrap

and:

    rlwrap sbcl

But we'll setup our editor to offer a better experience instead of
working in this REPL. See [editor-support](editor-support.html).

Lisp is interactive by nature, so in case of an error we enter the
debugger. This can be annoying in certain cases so you might want to
use SBCL's `--disable-debugger` option.

_Tip: the CLISP implementation has a better default REPL for the
terminal (readline capabilities, completion of symbols). You can even
use `clisp -on-error abort` to have error messages without the
debugger. It's handy to try things out, but we recommend to set-up
one's editor and to use SBCL or CCL for production._


## Libraries

Common Lisp has hundreds of libraries available under a free software license. See:

* [Quickdocs](http://quickdocs.org/) - the library documentation hosting for CL.
* the [Awesome-cl](https://github.com/CodyReichert/awesome-cl) list, a
  curated list of libraries.
* [Cliki](http://www.cliki.net/), the Common Lisp wiki.

### Some terminology

* In the Common Lisp world, a **package** is a way of grouping symbols
together and of providing encapsulation. It is similar to a C++
namespace, a Python module or a Java package.

* A **system** is a collection of CL source files bundled with an .asd
  file which tells how to compile and load them. There is often a
  one-to-one relationship between systems and packages, but this is in
  no way mandatory. A system may declare a dependency on other
  systems. Systems are managed by [ASDF](https://common-lisp.net/project/asdf/asdf.html) (Another System Definition
  Facility), which offers functionalities similar to those of `make` and
  `ld.so`, and has become a de facto standard.

* A Common Lisp library or project typically consists of one or
  several ASDF systems (and is distributed as one Quicklisp project).

### Install Quicklisp

[Quicklisp](https://www.quicklisp.org/beta/) is more than a package
manager, it is also a central repository (a *dist*) that ensures that
all libraries build together.

It provides its own *dist* but it is also possible to build our own.

To install it, we can either:

1- run this command, anywhere:

    curl -O https://beta.quicklisp.org/quicklisp.lisp

and enter a Lisp REPL and load this file:

    sbcl --load quicklisp.lisp

or

2- install the Debian package:

    apt-get install cl-quicklisp

and load it, from a REPL:

~~~lisp
(load "/usr/share/cl-quicklisp/quicklisp.lisp")
~~~

Then, in both cases, still from the REPL:

~~~lisp
(quicklisp-quickstart:install)
~~~

This will create the `~/quicklisp/` directory, where Quicklisp will
maintain its state and downloaded projects.

If you wish, you can install Quicklisp to a different location.  For instance,
to install it to a hidden folder on Unix systems:

~~~lisp
(quicklisp-quickstart:install :path "~/.quicklisp)
~~~

If you want Quicklisp to always be loaded in your Lisp sessions, run
`(ql:add-to-init-file)`: this adds the right stuff to the init file of
your CL implementation. Otherwise, you have to run `(load
"~/quicklisp/setup.lisp")` in every session if you want to use
Quicklisp or any of the libraries installed through it.

It adds the following in your (for example) `~/.sbclrc`:

~~~lisp
#-quicklisp
  (let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                         (user-homedir-pathname))))
    (when (probe-file quicklisp-init)
      (load quicklisp-init)))
~~~

### Install libraries

In the REPL:

~~~lisp
(ql:quickload "package-name")
~~~

and voilà. See Quicklisp's documentation for more commands.


Note also that dozens of Common Lisp libraries are packaged in
Debian. The package names usually begin with the cl- prefix (use
`apt-cache search --names-only "^cl-.*"` to list them all).

For example, in order to use the CL-PPCRE library (for regular
expressions), one should first install the `cl-ppcre` package.

Then, in SBCL and ECL, it can be used with:

~~~lisp
(require "asdf")
(require "cl-ppcre")
(cl-ppcre:regex-replace "fo+" "foo bar" "frob")
~~~

See more: https://wiki.debian.org/CommonLisp

### Advanced dependencies management

You can drop Common Lisp projects into any of those folders:

- `~/common-lisp`,
- `~/.local/share/common-lisp/source`,
- `~/quicklisp/local-projects`

For a complete list, see

~~~lisp
(asdf/source-registry:default-user-source-registry)
~~~

and
~~~lisp
asdf:*central-registry*
~~~

A library installed here is automatically available for every project.

#### Providing our own version of a library. Cloning projects.

Given the property above, we can clone any library into the
local-projects directory and it will be found by ASDF (and Quicklisp) and
available right-away:

~~~lisp
(asdf:load-system "system")
~~~
or
~~~lisp
(ql:quickload "system")
~~~

The practical different between the two is that `ql:quickload` first tries to
fetch the system from the Internet if it is not already installed.

#### How to work with local versions of libraries

If we need libraries to be installed locally, for only one project, or
in order to easily ship a list of dependencies with an application, we
can use [Qlot](https://github.com/fukamachi/qlot).

Quicklisp also provides
[Quicklisp bundles](https://www.quicklisp.org/beta/bundles.html). They
are self-contained sets of systems that are exported from Quicklisp
and loadable without involving Quicklisp.

At last, there's
[Quicklisp controller](https://github.com/quicklisp/quicklisp-controller)
to help us build *dists*.

## Working with projects

Now that we have Quicklisp and our editor ready, we can start writing
Lisp code in a file and interacting with the REPL.

But what if we want to work with an existing project or create a new
one, how do we proceed, what's the right sequence of `defpackage`,
what to put in the `.asd` file, how to load the project into the REPL ?

### Creating a new project

Some project builders help to scaffold the project structure. We like
[cl-project](https://github.com/fukamachi/cl-project) that also sets
up a tests skeleton.

In short:

~~~lisp
(ql:quickload "cl-project")
(cl-project:make-project #P"./path-to-project/root/")
~~~

it will create a directory structure like this:

```
|-- my-project.asd
|-- my-project-test.asd
|-- README.markdown
|-- README.org
|-- src
|   `-- my-project.lisp
`-- tests
    `-- my-project.lisp
```

Where `my-project.asd` resembles this:

~~~lisp
(defsystem "my-project"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ()  ;; <== list of Quicklisp dependencies
  :components ((:module "src"
                :components
                ((:file "my-project"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "my-project-test"))))
~~~

and `src/my-project.lisp` this:

~~~lisp
(defpackage footest
  (:use :cl))
(in-package :footest)
~~~

- ASDF documentation: [defining a system with defsystem](https://common-lisp.net/project/asdf/asdf.html#Defining-systems-with-defsystem)

### How to load an existing project

You have created a new project, or you have an existing one, and you
want to work with it on the REPL, but Quicklisp doesn't know it. How
can you do ?

Well first, if you create it or clone it into
one of `~/common-lisp`, `~/.local/share/common-lisp/source/` or
`~/quicklisp/local-projects`, you'll be able to `(ql:quickload …)` it with no
further ado.

Otherwise you'll need to compile and load its system definition
(`.asd`) first. In SLIME with the `slime-asdf` contrib loaded, type `C-c C-k`
(*slime-compile-and-load-file*) in the `.asd`, then you can
`(ql:quickload …)` it.

Usually you want to "enter" the system in the REPL at this stage:

~~~lisp
(use-package :my-project)
~~~

Lastly, you can compile or eval the sources (`my-project.lisp`) with
`C-c C-k` or `C-c C-c` (*slime-compile-defun*) in a form, and see its
result in the REPL.

Another solution is to use ASDF's list of known projects:

~~~lisp
(pushnew "~/path-to-project/root/" asdf:*central-registry* :test #'equal)
~~~

and since ASDF is integrated into Quicklisp, we can `quickload` our project.

Happy hacking !


## More settings

You might want to set SBCL's default encoding format to utf-8:

    (setf sb-impl::*default-external-format* :utf-8)

You can add this to your `~/.sbclrc`.

If you dislike the REPL to print all symbols upcase, add this:

    (setf *print-case* :downcase)

Beware that this might break some packages like
[Mito](https://github.com/fukamachi/mito/issues/45).  Avoid doing this in
production.


## Read more

* Source code organization, libraries and packages:  [https://lispmethods.com/libraries.html](https://lispmethods.com/libraries.html)

## Credits

* [https://wiki.debian.org/CommonLisp](https://wiki.debian.org/CommonLisp)
* [http://articulate-lisp.com/project/new-project.html](http://articulate-lisp.com/project/new-project.html)

---
title: Editor support
---

The editor of choice is still Emacs, but it is not the only one.

## Emacs

[SLIME](https://github.com/slime/slime/) is the Superior Lisp
Interaction Mode for Emacs. It has support for interacting with a
running Common Lisp process for compilation, debugging, documentation
lookup, cross-references, and so on. It works with many implementations.

[Portacle](https://shinmera.github.io/portacle/) is a portable and
multi-platform Common Lisp environment. It ships Emacs25, SBCL,
Quicklisp, SLIME and Git.

<img src="assets/portacle.png"
     style="width: 800px"/>

### Installing SLIME

SLIME is in the official GNU ELPA repository of Emacs Lisp packages
(in Emacs24 and forward). Install with:

    M-x package-install RET slime RET

Since SLIME is heavily modular and the defaults only do the bare minimum (not
even the SLIME REPL), you might want to enable more features with

~~~lisp
(slime-setup '(slime-fancy slime-quicklisp slime-asdf))
~~~

For more details, consult the
[documentation](https://common-lisp.net/project/slime/doc/html/) (also available
as an Info page).

Now you can run SLIME with `M-x slime` and/or `M-x slime-connect`.

See also:

* [https://wikemacs.org/wiki/SLIME](https://wikemacs.org/wiki/SLIME) - configuration examples and extensions.


### Using Emacs as an IDE

See ["Using Emacs as an IDE"](emacs-ide.html).

### Setting up Emacs on Windows or Mac

See ["Setting up Emacs on Windows or Mac"](windows.html).


## Vim & Neovim

[Slimv](https://www.vim.org/scripts/script.php?script_id=2531) is a full-blown
environment for Common Lisp inside of Vim.

[Vlime](https://github.com/l04m33/vlime) is a Common Lisp dev
environment for Vim (and Neovim), similar to SLIME for Emacs and SLIMV
for Vim.

<img src="assets/slimv.jpg"
     style="width: 800px"/>

[cl-neovim](https://github.com/adolenc/cl-neovim/) makes it possible to write
Neovim plugins in Common Lisp.

[quicklisp.nvim](https://gitlab.com/HiPhish/quicklisp.nvim) is a Neovim
frontend for Quicklisp.

[Slimv_box](https://github.com/justin2004/slimv_box) brings Vim, SBCL
and tmux in a Docker container for a quick installation.


## Eclipse

[Dandelion](https://github.com/Ragnaroek/dandelion) is a plugin for the
Eclipse IDE.

Available for Windows, Mac and Linux, built-in SBCL and CLISP support
and possibility to connect other environments, interactive debugger
with restarts, macro-expansion, parenthesis matching,…

<img src="dandelion.png" style="width: 800px"/>

## Lem

Lem is an editor tailored for Common Lisp development. Once you
install it, you can start developing. Its interface resembles Emacs
and SLIME (same shortcuts). It comes with an ncurses and an Electron
frontend, and other programming modes: Python, Go, Rust, JS, Nim,
Scheme, HTML, CSS, directory mode, a vim layer, and more.

<img src="https://github.com/cxxxr/lem/raw/master/screenshots/terminal.png"
     style="width: 800px"/>


## Atom

See [SLIMA](https://github.com/neil-lindquist/slima). This package
allows you to interactively develop Common Lisp code, turning
Atom into a pretty good Lisp IDE.

<img src="assets/atom-slime.png"
     style="width: 800px"/>

## Sublime Text

[Sublime Text](http://www.sublimetext.com/3) supports running a Lisp
REPL and evaluating code in it.

You need to install the "SublimeREPL" package and then see the options
in Tools/SublimeREPL to choose your CL implementation, and `eval` what
you want.

<img src="assets/editor-sublime.png"
     style="width: 800px"/>

## Notebooks

[cl-jupyter](https://github.com/fredokun/cl-jupyter) is a Common Lisp
kernel for Jupyter notebooks.


[Darkmatter](https://github.com/tamamu/darkmatter) is a notebook-style
Common Lisp environment.


<img src="https://github.com/tamamu/darkmatter/raw/master/screenshots/screenshot.png"
     style="width: 800px"/>


## REPLs

[cl-repl](https://github.com/koji-kojiro/cl-repl) is an ipython-like REPL. It supports symbol completion, magic and shell commands, editing command in a file and a simple debugger.

<img src="assets/cl-repl.png"
     style="width: 500px"/>


## Others

For reviews of plugins for more editors, including free versions of
proprietary ones (LispWorks, Allegro), see
[Articulate Common Lisp](http://articulate-lisp.com/ides/summary.html).

---
title: Functions
---

<a name="return"></a>

## Named functions: `defun`

Creating named functions is done with the `defun` keyword. It follows this model:

~~~lisp
(defun <name> (list of arguments)
  "docstring"
  (function body))
~~~

The return value is the value returned by the last expression of the body
(see below for more). There is no "return xx" statement.

So, for example:

~~~lisp
(defun hello-world ()
  ;;               ^^ no arguments
  (print "hello world!"))
~~~

Call it:

~~~lisp
(hello-world)
;; "hello world!"  <-- output
;; "hello world!"  <-- a string is returned.
~~~

## Arguments

### Base case: required arguments

Add in arguments like this:

~~~lisp
(defun hello (name)
  "Say hello to `name'."
  (format t "hello ~a !~&" name))
;; HELLO
~~~

(where `~a` is the most used `format` directive to print a variable
*aesthetically* and `~&` prints a newline)

Call the function:

~~~lisp
(hello "me")
;; hello me !  <-- this is printed by `format`
;; NIL         <-- return value: `format t` prints a string to standard output and returns nil.
~~~

If you don't specify the right amount of arguments, you'll be trapped
into the interactive debugger with an explicit error message:

   (hello)

> invalid number of arguments: 0

### Optional arguments: `&optional`

Optional arguments are declared after the `&optional` keyword in the
lambda list. They are ordered, they must appear one after another.

This function:

~~~lisp
(defun hello (name &optional age gender) …)
~~~

must be called like this:

~~~lisp
(hello "me") ;; a value for the required argument, zero optional arguments
(hello "me" "7")  ;; a value for age
(hello "me" 7 :h) ;; a value for age and gender
~~~

### Named parameters: `&key`

It is not always convenient to remember the order of the arguments. It
is thus possible to supply arguments by name: we declare them using
`&key <name>`, we set them with `:name <value>` in the function call,
and we use `name` as a regular variable in the function body. They are
`nil` by default.

~~~lisp
(defun hello (name &key happy)
  "If `happy' is `t', print a smiley"
  (format t "hello ~a " name)
  (when happy
    (format t ":)~&"))
~~~

The following calls are possible:

    (hello "me")
    (hello "me" :happy t)
    (hello "me" :happy nil) ;; useless, equivalent to (hello "me")

and this is not valid: `(hello "me" :happy)`:

> odd number of &KEY arguments

A similar example of a function declaration, with several key parameters:

~~~lisp
(defun hello (name &key happy lisper cookbook-contributor-p) …)
~~~

it can be called with zero or more key parameters, in any order:

~~~lisp
(hello "me" :lisper t)
(hello "me" :lisper t :happy t)
(hello "me" :cookbook-contributor-p t :happy t)
~~~

#### Mixing optional and key parameters

It is generally a style warning, but it is possible.

~~~lisp
(defun hello (&optional name &key happy)
  (format t "hello ~a " name)
  (when happy
    (format t ":)~&")))
~~~

In SBCL, this yields:

~~~lisp
; in: DEFUN HELLO
;     (SB-INT:NAMED-LAMBDA HELLO
;         (&OPTIONAL NAME &KEY HAPPY)
;       (BLOCK HELLO (FORMAT T "hello ~a " NAME) (WHEN HAPPY (FORMAT T ":)~&"))))
;
; caught STYLE-WARNING:
;   &OPTIONAL and &KEY found in the same lambda list: (&OPTIONAL (NAME "John") &KEY
;                                                      HAPPY)
;
; compilation unit finished
;   caught 1 STYLE-WARNING condition
~~~

We can call it:

~~~lisp
(hello "me" :happy t)
;; hello me :)
;; NIL
~~~

### Default values

In the lambda list, use pairs to give a default value to an optional or a key argument, like `(happy t)` below:

~~~lisp
(defun hello (name &key (happy t))
~~~

Now `happy` is true by default.

### Variable number of arguments: `&rest`

Sometimes you want a function to accept a variable number of
arguments. Use `&rest <variable>`, where `<variable>` will be a list.

~~~lisp
(defun mean (x &rest numbers)
    (/ (apply #'+ x numbers)
       (1+ (length numbers))))
~~~

~~~lisp
(mean 1)
(mean 1 2)
(mean 1 2 3 4 5)
~~~

### `&allow-other-keys`

Observe:

~~~lisp
(defun hello (name &key happy)
  (format t "hello ~a~&" name))

(hello "me" :lisper t)
;; => Error: unknown keyword argument
~~~

whereas

~~~lisp
(defun hello (name &key happy &allow-other-keys)
  (format t "hello ~a~&" name))

(hello "me" :lisper t)
;; hello me
~~~

We might need `&allow-other-keys` when passing around arguments or
with higher level manipulation of functions.


## Return values

The return value of the function is the value returned by the last
executed form of the body.

There are ways for non-local exits (`return-from <function name> <value>`), but they are usually not needed.

Common Lisp has also the concept of multiple return values.

### Multiple return values: `values`, `multiple-value-bind` and `nth-value`

Returning multiple values is *not* like returning a tuple or a list of
results ;) This is a common misconception.

Multiple values are specially useful and powerful because a change in
them needs little to no refactoring.

~~~lisp
(defun foo (a b c)
  a)
~~~

This function returns `a`.

~~~lisp
(defvar *res* (foo :a :b :c))
;; :A
~~~

We use `values` to return multiple values:

~~~lisp
(defun foo (a b c)
  (values a b c))
~~~

~~~lisp
(setf *res* (foo :a :b :c))
;; :A
~~~

Observe here that `*res*` *is still `:A`*.

All functions that use the return value of `foo` need *not* to change, they
still work. If we had returned a list or an array, this would be
different.

**multiple-value-bind**

We destructure multiple values with `multiple-value-bind` (or
`mvb`+TAB in Slime for short) and we can get one given its position
with `nth-value`:

~~~lisp
(multiple-value-bind (res1 res2 res3)
    (foo :a :b :c)
  (format t "res1 is ~a, res2 is ~a, res2 is ~a~&" res1 res2 res3))
;; res1 is A, res2 is B, res2 is C
;; NIL
~~~

Its general form is

~~~lisp
(multiple-value-bind (var-1 .. var-n) expr
  body)
~~~

The variables `var-n` are not available outside the scope of `multiple-value-bind`.

With **nth-value**:

~~~lisp
(nth-value 0 (values :a :b :c))  ;; => :A
(nth-value 2 (values :a :b :c))  ;; => :C
(nth-value 9 (values :a :b :c))  ;; => NIL
~~~

Look here too that `values` is different from a list:

~~~lisp
(nth-value 0 '(:a :b :c)) ;; => (:A :B :C)
(nth-value 1 '(:a :b :c)) ;; => NIL
~~~

Note that `(values)` with no values returns… no values at all.

**multiple-value-list**

While we are at it: [multiple-value-list](http://www.lispworks.com/documentation/HyperSpec/Body/m_mult_1.htm) turns multiple values to a list:

~~~lisp
(multiple-value-list (values 1 2 3))
;; (1 2 3)
~~~

The reverse is **values-list**, it turns a list to multiple values:

~~~lisp
(values-list '(1 2 3))
;; 1
;; 2
;; 3
~~~


## Anonymous functions: `lambda`

Anonymous functions are created with `lambda`:

~~~lisp
(lambda (x) (print x))
~~~

We can call a lambda with `funcall` or `apply` (see below).

If the first element of an unquoted list is a lambda expression, the
lambda is called:

~~~lisp
((lambda (x) (print x)) "hello")
;; hello
~~~

## Calling functions programmatically: `funcall` and `apply`

`funcall` is to be used with a known number of arguments, when `apply`
can be used on a list, for example from `&rest`:

~~~lisp
(funcall #'+ 1 2)
(apply #'+ '(1 2))
~~~

## Higher order functions: functions that return functions

Writing functions that return functions is simple enough:

~~~lisp
(defun adder (n)
  (lambda (x) (+ x n)))
;; ADDER
~~~

Here we have defined the function `adder` which returns an _object_ of _type_ [`function`](http://www.lispworks.com/documentation/HyperSpec/Body/t_fn.htm).

To call the resulting function, we must use `funcall` or `apply`:

~~~lisp
(adder 5)
;; #<CLOSURE (LAMBDA (X) :IN ADDER) {100994ACDB}>
(funcall (adder 5) 3)
;; 8
~~~

Trying to call it right away leads to an illegal function call:

~~~lisp
((adder 3) 5)
In: (ADDER 3) 5
    ((ADDER 3) 5)
Error: Illegal function call.
~~~

Indeed, CL has different _namespaces_ for functions and variables, i.e. the same _name_ can refer to different things depending on its position in a form that's evaluated.

~~~lisp
;; The symbol foo is bound to nothing:
CL-USER> (boundp 'foo)
NIL
CL-USER> (fboundp 'foo)
NIL
;; We create a variable:
CL-USER> (defparameter foo 42)
FOO
* foo
42
;; Now foo is "bound":
CL-USER> (boundp 'foo)
T
;; but still not as a function:
CL-USER> (fboundp 'foo)
NIL
;; So let's define a function:
CL-USER> (defun foo (x) (* x x))
FOO
;; Now the symbol foo is bound as a function too:
CL-USER> (fboundp 'foo)
T
;; Get the function:
CL-USER> (function foo)
#<FUNCTION FOO>
;; and the shorthand notation:
* #'foo
#<FUNCTION FOO>
;; We call it:
(funcall (function adder) 5)
#<CLOSURE (LAMBDA (X) :IN ADDER) {100991761B}>
;; and call the lambda:
(funcall (funcall (function adder) 5) 3)
8
~~~

To simplify a bit, you can think of each symbol in CL having (at least) two "cells" in which information is stored. One cell - sometimes referred to as its _value cell_ - can hold a value that is _bound_ to this symbol, and you can use [`boundp`](http://www.lispworks.com/documentation/HyperSpec/Body/f_boundp.htm) to test whether the symbol is bound to a value (in the global environment). You can access the value cell of a symbol with [`symbol-value`](http://www.lispworks.com/documentation/HyperSpec/Body/f_symb_5.htm).


The other cell - sometimes referred to as its _function cell_ - can hold the definition of the symbol's (global) function binding. In this case, the symbol is said to be _fbound_ to this definition. You can use [`fboundp`](http://www.lispworks.com/documentation/HyperSpec/Body/f_fbound.htm) to test whether a symbol is fbound. You can access the function cell of a symbol (in the global environment) with [`symbol-function`](http://www.lispworks.com/documentation/HyperSpec/Body/f_symb_1.htm).


Now, if a _symbol_ is evaluated, it is treated as a _variable_ in that its value cell is returned (just `foo`). If a _compound form_, i.e. a _cons_, is evaluated and its _car_ is a symbol, then the function cell of this symbol is used (as in `(foo 3)`).


In Common Lisp, as opposed to Scheme, it is _not_ possible that the car of the compound form to be evaluated is an arbitrary form. If it is not a symbol, it _must_ be a _lambda expression_, which looks like `(lambda `_lambda-list_ _form*_`)`.

This explains the error message we got above - `(adder 3)` is neither a symbol nor a lambda expression.

If we want to be able to use the symbol `*my-fun*` in the car of a compound form, we have to explicitly store something in its _function cell_ (which is normally done for us by the macro [`defun`](http://www.lispworks.com/documentation/HyperSpec/Body/m_defun.htm)):

~~~lisp
;;; continued from above
CL-USER> (fboundp '*my-fun*)
NIL
CL-USER> (setf (symbol-function '*my-fun*) (adder 3))
#<CLOSURE (LAMBDA (X) :IN ADDER) {10099A5EFB}>
CL-USER> (fboundp '*my-fun*)
T
CL-USER> (*my-fun* 5)
8
~~~

Read the CLHS section about [form evaluation](http://www.lispworks.com/documentation/HyperSpec/Body/03_aba.htm) for more.

## Closures

Closures allow to capture lexical bindings:

~~~lisp
(let ((limit 3)
      (counter -1))
    (defun my-counter ()
      (if (< counter limit)
          (incf counter)
          (setf counter 0))))

(my-counter)
0
(my-counter)
1
(my-counter)
2
(my-counter)
3
(my-counter)
0
~~~

Or similarly:

~~~lisp
(defun repeater (n)
  (let ((counter -1))
     (lambda ()
       (if (< counter n)
         (incf counter)
         (setf counter 0)))))

(defparameter *my-repeater* (repeater 3))
;; *MY-REPEATER*
(funcall *my-repeater*)
0
(funcall *my-repeater*)
1
(funcall *my-repeater*)
2
(funcall *my-repeater*)
3
(funcall *my-repeater*)
0
~~~


See more on [Practical Common Lisp](http://www.gigamonkeys.com/book/variables.html).

## `setf` functions

A function name can also be a list of two symbols with `setf` as the
first one, and where the first argument is the new value:

~~~lisp
(defun (setf <name>) (new-value <other arguments>)
  body)
~~~

This mechanism is particularly used for CLOS methods.

A silly example:

~~~lisp
(defparameter *current-name* ""
  "A global name.")

(defun hello (name)
  (format t "hello ~a~&" name))

(defun (setf hello) (new-value)
  (hello new-value)
  (setf *current-name* new-value)
  (format t "current name is now ~a~&" new-value))

(setf (hello) "Alice")
;; hello Alice
;; current name is now Alice
;; NIL
~~~

<a name="curry"></a>

## Currying

### Concept

A related concept is that of _[currying](https://en.wikipedia.org/wiki/Currying)_ which you might be familiar with if you're coming from a functional language. After we've read the last section that's rather easy to implement:

~~~lisp
CL-USER> (defun curry (function &rest args)
           (lambda (&rest more-args)
	           (apply function (append args more-args))))
CURRY
CL-USER> (funcall (curry #'+ 3) 5)
8
CL-USER> (funcall (curry #'+ 3) 6)
9
CL-USER> (setf (symbol-function 'power-of-ten) (curry #'expt 10))
#<Interpreted Function "LAMBDA (FUNCTION &REST ARGS)" {482DB969}>
CL-USER> (power-of-ten 3)
1000
~~~

### With the Alexandria library

Now that you know how to do it, you may appreciate using the
implementation of the
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html#Data-and-Control-Flow)
library (in Quicklisp).

~~~lisp
(ql:quickload :alexandria)

(defun adder (foo bar)
  "Add the two arguments."
  (+ foo bar))

(defvar add-one (alexandria:curry #'adder 1) "Add 1 to the argument.")

(funcall add-one 10)  ;; => 11

(setf (symbol-function 'add-one) add-one)
(add-one 10)  ;; => 11
~~~

## Documentation

- functions: http://www.lispworks.com/documentation/HyperSpec/Body/t_fn.htm#function
- ordinary lambda lists: http://www.lispworks.com/documentation/HyperSpec/Body/03_da.htm
- multiple-value-bind: http://clhs.lisp.se/Body/m_multip.htm

---
title: Data structures
---

We hope to give here a clear reference of the common data
structures. To really learn the language, you should take the time to
read other resources. The following resources, which we relied upon,
also have many more details:

- [Practical CL](http://gigamonkeys.com/book/they-called-it-lisp-for-a-reason-list-processing.html), by Peter Seibel
- [CL Recipes](http://weitz.de/cl-recipes/), by E. Weitz, full of explanations and tips,
- the
  [CL standard](http://cvberry.com/tech_writings/notes/common_lisp_standard_draft.html)
  with a nice TOC, functions reference, extensive descriptions, more
  examples and warnings (i.e: everything).
- a [Common Lisp quick reference](http://clqr.boundp.org/)

Don't miss the appendix and when you need more data structures, have a
look at the
[awesome-cl](https://github.com/CodyReichert/awesome-cl#data-structures)
list and [Quickdocs](http://quickdocs.org/search?q=data+structure).

## Lists

### Building lists. Cons cells, lists.

_A list is also a sequence, so we can use the functions shown below._

The list basic element is the cons cell. We build lists by assembling
cons cells.

~~~lisp
(cons 1 2)
;; => (1 . 2) ;; representation with a point, a dotted pair.
~~~

It looks like this:

```
[o|o]--- 2
 |
 1
```

If the `cdr` of the first cell is another cons cell, and if the `cdr` of
this last one is `nil`, we build a list:

~~~lisp
(cons 1 (cons 2 nil))
;; => (1 2)
~~~

It looks like this:

```
[o|o]---[o|/]
 |       |
 1       2
```
(ascii art by [draw-cons-tree](https://github.com/cbaggers/draw-cons-tree)).

See that the representation is not a dotted pair ? The Lisp printer
understands the convention.

Finally we can simply build a literal list with `list`:

~~~lisp
(list 1 2)
;; => (1 2)
~~~

or by calling quote:

~~~lisp
'(1 2)
;; => (1 2)
~~~

which is shorthand notation for the function call `(quote (1 2))`.

### Circular lists

A cons cell car or cdr can refer to other objects, including itself or
other cells in the same list. They can therefore be used to define
self-referential structures such as circular lists.

Before working with circular lists, tell the printer to recognise them
and not try to print the whole list by setting
[\*print-circle\*](http://clhs.lisp.se/Body/v_pr_cir.htm)
to `T`:
~~~lisp
(setf *print-circle* t)
~~~

A function which modifies a list, so that the last `cdr` points to the
start of the list is:
~~~lisp
(defun circular! (items)
  "Modifies the last cdr of list ITEMS, returning a circular list"
  (setf (cdr (last items)) items))

(circular! (list 1 2 3))
;; => #1=(1 2 3 . #1#)

(fifth (circular! (list 1 2 3)))
;; => 2
~~~

The [list-length](http://www.lispworks.com/documentation/HyperSpec/Body/f_list_l.htm#list-length)
function recognises circular lists, returning `nil`.

The reader can also create circular lists, using
[Sharpsign Equal-Sign](http://www.lispworks.com/documentation/HyperSpec/Body/02_dho.htm)
notation. An object (like a list) can be prefixed with `#n=` where `n`
is an unsigned decimal integer (one or more digits). The
label `#n#` can be used to refer to the object later in the
expression:

~~~lisp
'#42=(1 2 3 . #42#)
;; => #1=(1 2 3 . #1#)
~~~
Note that the label given to the reader (`n=42`) is discarded after
reading, and the printer defines a new label (`n=1`).

Further reading

* [Let over Lambda](https://letoverlambda.com/index.cl/guest/chap4.html#sec_5) section on cyclic expressions


### car/cdr or first/rest (and second... to tenth)

~~~lisp
(car (cons 1 2)) ;; => 1
(cdr (cons 1 2)) ;; => 2
(first (cons 1 2)) ;; => 1
(first '(1 2 3)) ;; => 1
(rest '(1 2 3)) ;; => (2 3)
~~~

We can assign *any* new value with `setf`.

### last, butlast, nbutlast (&optional n)

return the last cons cell in a list (or the nth last cons cells).

~~~lisp
(last '(1 2 3))
;; => (3)
(car (last '(1 2 3)) ) ;; or (first (last …))
;; => 3
(butlast '(1 2 3))
;; => (1 2)
~~~

In [Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html#Conses), `lastcar` is equivalent of `(first (last …))`:

~~~lisp
(alexandria:lastcar '(1 2 3))
;; => 3
~~~


### reverse, nreverse

`reverse` and `nreverse` return a new sequence.

`nreverse` is destructive. The N stands for **non-consing**, meaning
it doesn't need to allocate any new cons cells. It *might* (but in
practice, does) reuse and modify the original sequence:

~~~lisp
(defparameter mylist '(1 2 3))
;; => (1 2 3)
(reverse mylist)
;; => (3 2 1)
mylist
;; => (1 2 3)
(nreverse mylist)
;; => (3 2 1)
mylist
;; => (1) in SBCL but implementation dependent.
~~~


### append

`append` takes any number of list arguments and returns a new list
containing the elements of all its arguments:

~~~lisp
(append (list 1 2) (list 3 4))
;; => (1 2 3 4)
~~~

The new list shares some cons cells with the `(3 4)`:

http://gigamonkeys.com/book/figures/after-append.png

__Note__: [cl21](cl21.htm)'s `append` is generic (for strings, lists, vectors and
its abstract-sequence).

`nconc` is the recycling equivalent.

### push (item, place)

`push` prepends *item* to the list that is stored in *place*, stores
the resulting list in *place*, and returns the list.

~~~lisp
(defparameter mylist '(1 2 3))
(push 0 mylist)
;; => (0 1 2 3)
~~~

~~~lisp
(defparameter x ’(a (b c) d))
;; => (A (B C) D)
(push 5 (cadr x))
;; => (5 B C)
x
;; => (A (5 B C) D)
~~~

`push` is equivalent to `(setf place (cons item place ))` except that
the subforms of *place* are evaluated only once, and *item* is evaluated
before *place*.

There is no built-in function to **add to the end of a list**. It is a
more costly operation (have to traverse the whole list). So if you
need to do this: either consider using another data structure, either
just `reverse` your list when needed.

### pop

a destructive operation.

### nthcdr (index, list)

Use this if `first`, `second` and the rest up to `tenth` are not
enough.

### car/cdr and composites (cadr, caadr…) - accessing lists inside lists

They make sense when applied to lists containing other lists.

~~~lisp
(caar (list 1 2 3))                  ==> error
(caar (list (list 1 2) 3))           ==> 1
(cadr (list (list 1 2) (list 3 4)))  ==> (3 4)
(caadr (list (list 1 2) (list 3 4))) ==> 3
~~~

### destructuring-bind (parameter*, list)

It binds the parameter values to the list elements. We can destructure
trees, plists and even provide defaults.

Simple matching:

~~~lisp
(destructuring-bind (x y z) (list 1 2 3)
  (list :x x :y y :z z))
;; => (:X 1 :Y 2 :Z 3)
~~~

Matching inside sublists:

~~~lisp
(destructuring-bind (x (y1 y2) z) (list 1 (list 2 20) 3)
  (list :x x :y1 y1 :y2 y2 :z z))
;; => (:X 1 :Y1 2 :Y2 20 :Z 3)
~~~

The parameter list can use the usual `&optional`, `&rest` and `&key`
parameters.

~~~lisp
(destructuring-bind (x (y1 &optional y2) z) (list 1 (list 2) 3)
  (list :x x :y1 y1 :y2 y2 :z z))
;; => (:X 1 :Y1 2 :Y2 NIL :Z 3)
~~~

~~~lisp
(destructuring-bind (&key x y z) (list :z 1 :y 2 :x 3)
  (list :x x :y y :z z))
;; => (:X 3 :Y 2 :Z 1)
~~~

The `&whole` parameter is bound to the whole list. It must be the
first one and others can follow.

~~~lisp
(destructuring-bind (&whole whole-list &key x y z) (list :z 1 :y 2 :x 3)
  (list :x x :y y :z z :whole whole-list))
;; => (:X 3 :Y 2 :Z 1 :WHOLE-LIST (:Z 1 :Y 2 :X 3))
~~~

Destructuring a plist, giving defaults:

(example from Common Lisp Recipes, by E. Weitz, Apress, 2016)

~~~lisp
(destructuring-bind (&key a (b :not-found) c
                     &allow-other-keys)
    ’(:c 23 :d "D" :a #\A :foo :whatever)
  (list a b c))
;; => (#\A :NOT-FOUND 23)
~~~

If this gives you the will to do pattern matching, see
[pattern matching](pattern_matching.html).


### Predicates: null, listp

`null` is equivalent to `not`, but considered better style.

`listp` tests whether an object is a cons cell or nil.

and sequences' predicates.


### ldiff, tailp, list*, make-list, fill, revappend, nreconc, consp, atom

~~~lisp
(make-list 3 :initial-element "ta")
;; => ("ta" "ta" "ta")
~~~

~~~lisp
(make-list 3)
;; => (NIL NIL NIL)
(fill * "hello")
;; => ("hello" "hello" "hello")
~~~

### member (elt, list)

Returns the tail of `list` beginning with the first element satisfying `eql`ity.

Accepts `:test`, `:test-not`, `:key` (functions or symbols).

~~~lisp
(member 2 '(1 2 3))
;; (2 3)
~~~

## Sequences

**lists** and **vectors** (and thus **strings**) are sequences.

_Note_: see also the [strings](strings.html) page.

Many of the sequence functions take keyword arguments. All keyword
arguments are optional and, if specified, may appear in any order.

Pay attention to the `:test` argument. It defaults to `eql` (for
strings, use `:equal`).

The `:key` argument should be passed either nil, or a function of one
argument. This key function is used as a filter through which the
elements of the sequence are seen. For instance, this:

~~~lisp
(find x y :key 'car)
~~~

is similar to `(assoc* x y)`: It searches for an element of the list
whose car equals x, rather than for an element which equals x
itself. If `:key` is omitted or nil, the filter is effectively the
identity function.

Example with an alist (see definition below):

~~~lisp
(defparameter my-alist (list (cons 'foo "foo")
                             (cons 'bar "bar")))
;; => ((FOO . "foo") (BAR . "bar"))
(find 'bar my-alist)
;; => NIL
(find 'bar my-alist :key 'car)
;; => (BAR . "bar")
~~~

For more, use a `lambda` that takes one parameter.

~~~lisp
(find 'bar my-alist :key (lambda (it) (car it)))
~~~

_Note_: and [cl21](cl21.html#shorter-lambda) has short lambdas:

~~~lisp
(find 'bar my-alist :key ^(car %))
(find 'bar my-alist :key (lm (it) (car it)))
~~~


### Predicates: every, some,…

`every, notevery (test, sequence)`: return nil or t, respectively, as
soon as one test on any set of the corresponding elements of sequences
returns nil.

~~~lisp
(defparameter foo '(1 2 3))
(every #'evenp foo)
;; => NIL
(some #'evenp foo)
;; => T
~~~

with a list of strings:

~~~lisp
(defparameter str '("foo" "bar" "team"))
(every #'stringp str)
;; => T
(some #'(lambda (it) (= 3 (length it))) str)
;; => T
(some ^(= 3 (length %)) str) ;; in CL21
;; => T
~~~

`some`, `notany` *(test, sequence)*: return either the value of the test, or nil.

`mismatch` *(sequence-a, sequence-b)*: Return position in sequence-a where
sequence-a and sequence-b begin to mismatch. Return NIL if they match
entirely. Other parameters: `:from-end bool`, `:start1`, `:start2` and
their `:end[1,2]`.

### Functions

See also sequence functions defined in
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html#Sequences):
`starts-with`, `ends-with`, `ends-with-subseq`, `length=`, `emptyp`,…

#### length (sequence)

#### elt (sequence, index) - find by index

beware, here the sequence comes first.

#### count (foo sequence)

Return the number of elements in sequence that match *foo*.

Additional paramaters: `:from-end`, `:start`, `:end`.

See also `count-if`, `count-not` *(test-function sequence)*.

#### subseq (sequence start, [end])

It is "setf"able, but only works if the new sequence has the same
length of the one to replace.

#### sort, stable-sort (sequence, test [, key function])

These sort functions are destructive, so one may prefer to copy the sequence before sorting:

    (sort (copy-seq seq) :test #'string<)

#### find, position (foo, sequence) - get index

also `find-if`, `find-if-not`, `position-if`, `position-if-not` *(test
sequence)*. See `:key` and `:test` parameters.

~~~lisp
(find 20 '(10 20 30))
;; 20
(position 20 '(10 20 30))
;; 1
~~~

#### search (sequence-a, sequence-b)

Search sequence-b for a subsequence matching sequence-a. Return
position in sequence-b, or NIL. Has the `from-end`, `end1/2` and others
parameters.

#### substitute, nsubstitute[if,if-not]

Return a sequence of the same kind as `sequence` with the same elements,
except that all elements equal to `old` are replaced with `new`.

~~~lisp
(substitute #\o #\x "hellx") ;; => "hello"
(substitute :a :x '(:a :x :x)) ;; => (:A :A :A)
(substitute "a" "x" '("a" "x" "x") :test #'string=) ;; => ("a" "a" "a")
~~~

#### sort, stable-sort, merge

(see above)

#### replace (sequence-a, sequence-b)

Replace elements of sequence-a with elements of
sequence-b.

#### remove, delete (foo sequence)

Make a copy of sequence without elements matching foo. Has
`:start/end`, `:key` and `:count` parameters.

`delete` is the recycling version of `remove`.

~~~lisp
(remove "foo" '("foo" "bar" "foo") :test 'equal)
;; => ("bar")
~~~

see also `remove-if[-not]` below.

### mapping (map, mapcar, remove-if[-not],...)

If you're used to map and filter in other languages, you probably want
`mapcar`. But it only works on lists, so to iterate on vectors (and
produce either a vector or a list, use `(map 'list function vector)`.

mapcar also accepts multiple lists with `&rest more-seqs`.  The
mapping stops as soon as the shortest sequence runs out.

_Note: cl21's `map` is a generic `mapcar` for lists and vectors._

`map` takes the output-type as first argument (`'list`, `'vector` or
`'string`):

~~~lisp
(defparameter foo '(1 2 3))
(map 'list (lambda (it) (* 10 it)) foo)
~~~

`reduce` *(function, sequence)*. Special parameter: `:initial-value`.

~~~lisp
(reduce '- '(1 2 3 4))
;; => -8
(reduce '- '(1 2 3 4) :initial-value 100)
;; => 90
~~~

**Filter** is here called `remove-if-not`.

### Flatten a list (Alexandria)

With
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html),
we have the `flatten` function.


### Creating lists with variables

That's one use of the `backquote`:

~~~lisp
(defparameter *var* "bar")
;; First try:
'("foo" *var* "baz") ;; no backquote
;; => ("foo" *VAR* "baz") ;; nope
~~~

Second try, with backquote interpolation:

~~~lisp
`("foo" ,*var* "baz")     ;; backquote, comma
;; => ("foo" "bar" "baz") ;; good
~~~

The backquote first warns we'll do interpolation, the comma introduces
the value of the variable.

If our variable is a list:

~~~lisp
(defparameter *var* '("bar" "baz"))
;; First try:
`("foo" ,*var*)
;; => ("foo" ("bar" "baz")) ;; nested list
`("foo" ,@*var*)            ;; backquote, comma-@ to
;; => ("foo" "bar" "baz")
~~~

E. Weitz warns that "objects generated this way will very likely share
structure (see Recipe 2-7)".


### Comparing lists

We can use sets functions.

## Set

### `intersection` of lists

What elements are both in list-a and list-b ?

~~~lisp
(defparameter list-a '(0 1 2 3))
(defparameter list-b '(0 2 4))
(intersection list-a list-b)
;; => (2 0)
~~~

### Remove the elements of list-b from list-a

`set-difference`

~~~lisp
(set-difference list-a list-b)
;; => (3 1)
(set-difference list-b list-a)
;; => (4)
~~~

### Join two lists

`union`

~~~lisp
(union list-a list-b)
;; => (3 1 0 2 4) ;; order can be different in your lisp
~~~

### Remove elements that are in both lists

`set-exclusive-or`

~~~lisp
(set-exclusive-or list-a list-b)
;; => (4 3 1)
~~~

and their recycling "n" counterpart (`nintersection`,…).

See also functions in
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html#Conses):
`setp`, `set-equal`,…

## Fset - immutable data structure

You may want to have a look at the
[FSet](https://common-lisp.net/project/fset/Site/FSet-Tutorial.html )
library (in Quicklisp).


## Arrays and vectors

**Arrays** have constant-time access characteristics.

They can be fixed or adjustable. A *simple array* is neither displaced
(using `:displaced-to`, to point to another array) nor adjustable
(`:adjust-array`), nor does it have a fill pointer (`fill-pointer`,
that moves when we add or remove elements).

A **vector** is an array with rank 1 (of one dimension). It is also a
*sequence* (see above).

A *simple vector* is a simple array that is also not specialized (it
doesn't use `:element-type` to set the types of the elements).


### Create an array, one or many dimensions

`make-array` *(sizes-list :adjustable bool)*

`adjust-array` *(array, sizes-list, :element-type, :initial-element)*

### Access: aref (array i [j …])

`aref` *(array i j k …)* or `row-major-aref` *(array i)* equivalent to
`(aref i i i …)`.

The result is `setf`able.

~~~lisp
(defparameter myarray (make-array '(2 2 2) :initial-element 1))
myarray
;; => #3A(((1 1) (1 1)) ((1 1) (1 1)))
(aref myarray 0 0 0)
;; => 1
(setf (aref myarray 0 0 0) 9)
;; => 9
(row-major-aref myarray 0)
;; => 9
~~~


### Sizes

`array-total-size` *(array)*: how many elements will fit in the array ?

`array-dimensions` *(array)*: list containing the length of the array's dimensions.

`array-dimension` *(array i)*: length of the *i*th dimension.

`array-rank` number of dimensions of the array.

~~~lisp
(defparameter myarray (make-array '(2 2 2)))
;; => MYARRAY
myarray
;; => #3A(((0 0) (0 0)) ((0 0) (0 0)))
(array-rank myarray)
;; => 3
(array-dimensions myarray)
;; => (2 2 2)
(array-dimension myarray 0)
;; => 2
(array-total-size myarray)
;; => 8
~~~


### Vectors

Create with `vector` or the reader macro `#()`. It returns a _simple
vector._

~~~lisp
(vector 1 2 3)
;; => #(1 2 3)
#(1 2 3)
;; => #(1 2 3)
~~~


`vector-push` *(foo vector)*: replace the vector element pointed to by
the fill pointer by foo. Can be destructive.

`vector-push-extend` *(foo vector [extension-num])*t

`vector-pop` *(vector)*: return the element of vector its fill pointer
points to.

`fill-pointer` *(vector)*. `setf`able.

and see also the *sequence* functions.

### Transforming a vector to a list.

If you're mapping over it, see the `map` function whose first parameter
is the result type.

Or use `(coerce vector 'list)`.

## Hash Table

Hash Tables are a powerful data structure, associating keys with
values in a very efficient way. Hash Tables are often preferred over
association lists whenever performance is an issue, but they introduce
a little overhead that makes assoc lists better if there are only a
few key-value pairs to maintain.

Alists can be used sometimes differently though:

- they can be ordered
- we can push cons cells that have the same key, remove the one in
  front and we have a stack
- they have a human-readable printed representation
- they can be easily (de)serialized
- because of RASSOC, keys and values in alists are essentially
interchangeable; whereas in hash tables, keys and values play very
different roles (as usual, see CL Recipes for more).


<a name="create"></a>

### Creating a Hash Table

Hash Tables are created using the function
[`make-hash-table`](http://www.lispworks.com/documentation/HyperSpec/Body/f_mk_has.htm). It
has no required argument. Its most used optional keyword argument is
`:test`, specifying the function used to test the equality of keys.

If we are using the [cl21](http://cl21.org/) extension library, we can
create a hash table and add elements in the same time with the new
`#H` reader syntax:

~~~lisp
(defparameter *my-hash* #H(:name "Eitaro Fukamachi"))
~~~
then we access an element with

~~~lisp
(getf *my-hash* :name)
~~~


<a name="get"></a>

### Getting a value from a Hash Table

The function
[`gethash`](http://www.lispworks.com/documentation/HyperSpec/Body/f_gethas.htm)
takes two required arguments: a key and a hash table. It returns two
values: the value corresponding to the key in the hash table (or `nil`
if not found), and a boolean indicating whether the key was found in
the table. That second value is necessary since `nil` is a valid value
in a key-value pair, so getting `nil` as first value from `gethash`
does not necessarily mean that the key was not found in the table.

#### Getting a key that does not exist with a default value

`gethash` has an optional third argument:

~~~lisp
(gethash 'bar *my-hash* "default-bar")
;; => "default-bar"
;;     NIL
~~~

#### Getting all keys or all values of a hash table

The
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html)
library (in Quicklisp) has the functions `hash-table-keys` and
`hash-table-values` for that.

~~~lisp
(ql:quickload :alexandria)
;; […]
(alexandria:hash-table-keys *my-hash*)
;; => (BAR)
~~~

<a name="add"></a>

### Adding an Element to a Hash Table

If you want to add an element to a hash table, you can use `gethash`,
the function to retrieve elements from the hash table, in conjunction
with
[`setf`](http://www.lispworks.com/documentation/HyperSpec/Body/m_setf_.htm).

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (setf (gethash 'one-entry *my-hash*) "one")
"one"
CL-USER> (setf (gethash 'another-entry *my-hash*) 2/4)
1/2
CL-USER> (gethash 'one-entry *my-hash*)
"one"
T
CL-USER> (gethash 'another-entry *my-hash*)
1/2
T
~~~


<a name="test"></a>

### Testing for the Presence of a Key in a Hash Table

The first value returned by `gethash` is the object in the hash table
that's associated with the key you provided as an argument to
`gethash` or `nil` if no value exists for this key. This value can act
as a
[generalized boolean](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_g.htm#generalized_boolean">generalized
boolean) if you want to test for the presence of keys.

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (setf (gethash 'one-entry *my-hash*) "one")
"one"
CL-USER> (if (gethash 'one-entry *my-hash*)
           "Key exists"
           "Key does not exist")
"Key exists"
CL-USER> (if (gethash 'another-entry *my-hash*)
           "Key exists"
           "Key does not exist")
"Key does not exist"
~~~

But note that this does _not_ work if `nil` is amongst the values that
you want to store in the hash.

~~~lisp
CL-USER> (setf (gethash 'another-entry *my-hash*) nil)
NIL
CL-USER> (if (gethash 'another-entry *my-hash*)
           "Key exists"
           "Key does not exist")
"Key does not exist"
~~~

In this case you'll have to check the _second_ return value of `gethash` which will always return `nil` if no value is found and T otherwise.

~~~lisp
CL-USER> (if (nth-value 1 (gethash 'another-entry *my-hash*))
           "Key exists"
           "Key does not exist")
"Key exists"
CL-USER> (if (nth-value 1 (gethash 'no-entry *my-hash*))
           "Key exists"
           "Key does not exist")
"Key does not exist"
~~~


<a name="del"></a>

### Deleting from a Hash Table

Use
[`remhash`](http://www.lispworks.com/documentation/HyperSpec/Body/f_remhas.htm)
to delete a hash entry. Both the key and its associated value will be
removed from the hash table. `remhash` returns T if there was such an
entry, `nil` otherwise.

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (setf (gethash 'first-key *my-hash*) 'one)
ONE
CL-USER> (gethash 'first-key *my-hash*)
ONE
T
CL-USER> (remhash 'first-key *my-hash*)
T
CL-USER> (gethash 'first-key *my-hash*)
NIL
NIL
CL-USER> (gethash 'no-entry *my-hash*)
NIL
NIL
CL-USER> (remhash 'no-entry *my-hash*)
NIL
CL-USER> (gethash 'no-entry *my-hash*)
NIL
NIL
~~~


<a name="traverse"></a>

### Traversing a Hash Table

If you want to perform an action on each entry (i.e., each key-value
pair) in a hash table, you have several options:

You can use
[`maphash`](http://www.lispworks.com/documentation/HyperSpec/Body/f_maphas.htm)
which iterates over all entries in the hash table. Its first argument
must be a function which accepts _two_ arguments, the key and the
value of each entry. Note that due to the nature of hash tables you
_can't_ control the order in which the entries are provided by
`maphash` (or other traversing constructs). `maphash` always returns
`nil`.

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (setf (gethash 'first-key *my-hash*) 'one)
ONE
CL-USER> (setf (gethash 'second-key *my-hash*) 'two)
TWO
CL-USER> (setf (gethash 'third-key *my-hash*) nil)
NIL
CL-USER> (setf (gethash nil *my-hash*) 'nil-value)
NIL-VALUE
CL-USER> (defun print-hash-entry (key value)
    (format t "The value associated with the key ~S is ~S~%" key value))
PRINT-HASH-ENTRY
CL-USER> (maphash #'print-hash-entry *my-hash*)
The value associated with the key FIRST-KEY is ONE
The value associated with the key SECOND-KEY is TWO
The value associated with the key THIRD-KEY is NIL
The value associated with the key NIL is NIL-VALUE
~~~

You can also use
[`with-hash-table-iterator`](http://www.lispworks.com/documentation/HyperSpec/Body/m_w_hash.htm),
a macro which turns (via
[`macrolet`](http://www.lispworks.com/documentation/HyperSpec/Body/s_flet_.htm))
its first argument into an iterator that on each invocation returns
three values per hash table entry - a generalized boolean that's true
if an entry is returned, the key of the entry, and the value of the
entry. If there are no more entries, only one value is returned -
`nil`.

~~~lisp
;;; same hash-table as above
CL-USER> (with-hash-table-iterator (my-iterator *my-hash*)
           (loop
              (multiple-value-bind (entry-p key value)
                  (my-iterator)
                (if entry-p
                    (print-hash-entry key value)
                    (return)))))
The value associated with the key FIRST-KEY is ONE
The value associated with the key SECOND-KEY is TWO
The value associated with the key THIRD-KEY is NIL
The value associated with the key NIL is NIL-VALUE
NIL
~~~

Note the following caveat from the HyperSpec: "It is unspecified what
happens if any of the implicit interior state of an iteration is
returned outside the dynamic extent of the `with-hash-table-iterator`
form such as by returning some closure over the invocation form."


And there's always [`loop`](http://www.lispworks.com/documentation/HyperSpec/Body/06_a.htm):

~~~lisp
;;; same hash-table as above
CL-USER> (loop for key being the hash-keys of *my-hash*
           do (print key))
FIRST-KEY
SECOND-KEY
THIRD-KEY
NIL
NIL
CL-USER> (loop for key being the hash-keys of *my-hash*
           using (hash-value value)
           do (format t "The value associated with the key ~S is ~S~%" key value))
The value associated with the key FIRST-KEY is ONE
The value associated with the key SECOND-KEY is TWO
The value associated with the key THIRD-KEY is NIL
The value associated with the key NIL is NIL-VALUE
NIL
CL-USER> (loop for value being the hash-values of *my-hash*
           do (print value))
ONE
TWO
NIL
NIL-VALUE
NIL
CL-USER> (loop for value being the hash-values of *my-hash*
           using (hash-key key)
           do (format t "~&~A -> ~A" key value))
FIRST-KEY -> ONE
SECOND-KEY -> TWO
THIRD-KEY -> NIL
NIL -> NIL-VALUE
NIL
~~~

Last, we also have [cl21](cl21.htm)'s `(doeach ((key val) *hash*) …)`.

#### Traversign keys or values

To map over keys or values we can again rely on Alexandria with
`maphash-keys` and `maphash-values`.


<a name="count"></a>

### Counting the Entries in a Hash Table

No need to use your fingers - Common Lisp has a built-in function to
do it for you:
[`hash-table-count`](http://www.lispworks.com/documentation/HyperSpec/Body/f_hash_1.htm).

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (hash-table-count *my-hash*)
0
CL-USER> (setf (gethash 'first *my-hash*) 1)
1
CL-USER> (setf (gethash 'second *my-hash*) 2)
2
CL-USER> (setf (gethash 'third *my-hash*) 3)
3
CL-USER> (hash-table-count *my-hash*)
3
CL-USER> (setf (gethash 'second *my-hash*) 'two)
TWO
CL-USER> (hash-table-count *my-hash*)
3
CL-USER> (clrhash *my-hash*)
#<EQL hash table, 0 entries {48205F35}>
CL-USER> (hash-table-count *my-hash*)
0
~~~

### Printing a hash table readably

This snippets prints the keys, values and the test function of a
hash-table, and uses `alexandria:alist-hash-table` to read it back in:

~~~lisp
;; https://github.com/phoe/phoe-toolbox/blob/master/phoe-toolbox.lisp
(defun print-hash-table-readably (hash-table
                                  &optional (stream *standard-output*))
  "Prints a hash table readably using ALEXANDRIA:ALIST-HASH-TABLE."
  (let ((test (hash-table-test hash-table))
        (*print-circle* t)
        (*print-readably* t))
    (format stream "#.(ALEXANDRIA:ALIST-HASH-TABLE '(~%")
    (maphash (lambda (k v) (format stream "   (~S . ~S)~%" k v)) hash-table)
    (format stream "   ) :TEST '~A)" test)
    hash-table))
~~~

Example output:

```
#.(ALEXANDRIA:ALIST-HASH-TABLE
'((ONE . 1))
  :TEST 'EQL)
#<HASH-TABLE :TEST EQL :COUNT 1 {10046D4863}>
```

This output can be read back in to create a hash-table:

~~~lisp
(read-from-string
 (with-output-to-string (s)
   (print-hash-table-readably
    (alexandria:alist-hash-table
     '((a . 1) (b . 2) (c . 3))) s)))
;; #<HASH-TABLE :TEST EQL :COUNT 3 {1009592E23}>
;; 83
~~~



<a name="size"></a>

### Performance Issues: The Size of your Hash Table

The `make-hash-table` function has a couple of optional parameters
which control the initial size of your hash table and how it'll grow
if it needs to grow. This can be an important performance issue if
you're working with large hash tables. Here's an (admittedly not very
scientific) example with [CMUCL](http://www.cons.org/cmucl) pre-18d on
Linux:

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table))
*MY-HASH*
CL-USER> (hash-table-size *my-hash*)
65
CL-USER> (hash-table-rehash-size *my-hash*)
1.5
CL-USER> (time (dotimes (n 100000) (setf (gethash n *my-hash*) n)))
Compiling LAMBDA NIL:
Compiling Top-Level Form:

Evaluation took:
  0.27 seconds of real time
  0.25 seconds of user run time
  0.02 seconds of system run time
  0 page faults and
  8754768 bytes consed.
NIL
CL-USER> (time (dotimes (n 100000) (setf (gethash n *my-hash*) n)))
Compiling LAMBDA NIL:
Compiling Top-Level Form:

Evaluation took:
  0.05 seconds of real time
  0.05 seconds of user run time
  0.0 seconds of system run time
  0 page faults and
  0 bytes consed.
NIL
~~~

The values for
[`hash-table-size`](http://www.lispworks.com/documentation/HyperSpec/Body/f_hash_4.htm)
and
[`hash-table-rehash-size`](http://www.lispworks.com/documentation/HyperSpec/Body/f_hash_2.htm)
are implementation-dependent. In our case, CMUCL chooses and initial
size of 65, and it will increase the size of the hash by 50 percent
whenever it needs to grow. Let's see how often we have to re-size the
hash until we reach the final size...

~~~lisp
CL-USER> (log (/ 100000 65) 1.5)
18.099062
CL-USER> (let ((size 65)) (dotimes (n 20) (print (list n size)) (setq size (* 1.5 size))))
(0 65)
(1 97.5)
(2 146.25)
(3 219.375)
(4 329.0625)
(5 493.59375)
(6 740.3906)
(7 1110.5859)
(8 1665.8789)
(9 2498.8184)
(10 3748.2275)
(11 5622.3413)
(12 8433.512)
(13 12650.268)
(14 18975.402)
(15 28463.104)
(16 42694.656)
(17 64041.984)
(18 96062.98)
(19 144094.47)
NIL
~~~

The hash has to be re-sized 19 times until it's big enough to hold
100,000 entries. That explains why we saw a lot of consing and why it
took rather long to fill the hash table. It also explains why the
second run was much faster - the hash table already had the correct
size.

Here's a faster way to do it:
If we know in advance how big our hash will be, we can start with the right size:

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table :size 100000))
*MY-HASH*
CL-USER> (hash-table-size *my-hash*)
100000
CL-USER> (time (dotimes (n 100000) (setf (gethash n *my-hash*) n)))
Compiling LAMBDA NIL:
Compiling Top-Level Form:

Evaluation took:
  0.04 seconds of real time
  0.04 seconds of user run time
  0.0 seconds of system run time
  0 page faults and
  0 bytes consed.
NIL
~~~

That's obviously much faster. And there was no consing involved
because we didn't have to re-size at all. If we don't know the final
size in advance but can guess the growth behaviour of our hash table
we can also provide this value to `make-hash-table`. We can provide an
integer to specify absolute growth or a float to specify relative
growth.

~~~lisp
CL-USER> (defparameter *my-hash* (make-hash-table :rehash-size 100000))
*MY-HASH*
CL-USER> (hash-table-size *my-hash*)
65
CL-USER> (hash-table-rehash-size *my-hash*)
100000
CL-USER> (time (dotimes (n 100000) (setf (gethash n *my-hash*) n)))
Compiling LAMBDA NIL:
Compiling Top-Level Form:

Evaluation took:
  0.07 seconds of real time
  0.05 seconds of user run time
  0.01 seconds of system run time
  0 page faults and
  2001360 bytes consed.
NIL
~~~

Also rather fast (we only needed one re-size) but much more consing
because almost the whole hash table (minus 65 initial elements) had to
be built during the loop.

Note that you can also specify the `rehash-threshold` while creating a
new hash table. One final remark: Your implementation is allowed to
_completely ignore_ the values provided for `rehash-size` and
`rehash-threshold`...

## Alist

An association list is a list of cons cells.

This simple example:

~~~lisp
(defparameter *my-alist* (list (cons 'foo "foo")
                             (cons 'bar "bar")))
;; => ((FOO . "foo") (BAR . "bar"))
~~~

looks like this:

```
[o|o]---[o|/]
 |       |
 |      [o|o]---"bar"
 |       |
 |      BAR
 |
[o|o]---"foo"
 |
FOO
```

We can construct an alist like its representation:


~~~lisp
(setf *my-alist* '((:foo . "foo")
                 (:bar . "bar")))
~~~


The constructor `pairlis` associates a list of keys and a list of values:

~~~lisp
(pairlis '(:foo :bar)
         '("foo" "bar"))
;; => ((:BAR . "bar") (:FOO . "foo"))
~~~

To get a key, we have `assoc` (use `:test 'equal` when your keys are
strings, as usual). It returns the whole cons cell, so you may want to
use `cdr` or `second` to get the value or even better `assoc-value list key` from `Alexandria`.


~~~lisp
(alexandria:assoc-value *my-alist* :foo)
;; it actually returns 2 values
;; "foo"
;; (:FOO . "FOO")
~~~


There is `assoc-if`, and `rassoc` to get a cons cell by its value.

To add a key, we `push` another cons cell:

~~~lisp
(push (cons 'team "team") *my-alist*)
;; => ((TEAM . "team") (FOO . "foo") (BAR . "bar"))
~~~

We can use `pop` and other functions that operate on lists, like `remove`:

~~~lisp
(remove :team *my-alist*)
;; => ((:TEAM . "team") (FOO . "foo") (BAR . "bar")) ;; didn't remove anything
(remove :team *my-alist* :key 'car)
;; => ((FOO . "foo") (BAR . "bar")) ;; returns a copy
~~~

Remove only one element with `:count`:

~~~lisp
(push (cons 'bar "bar2") *my-alist*)
;; => ((BAR . "bar2") (TEAM . "team") (FOO . "foo") (BAR . "bar")) ;; twice the 'bar key
(remove 'bar *my-alist* :key 'car :count 1)
;; => ((TEAM . "team") (FOO . "foo") (BAR . "bar"))
;; because otherwise:
(remove 'bar *my-alist* :key 'car)
;; => ((TEAM . "team") (FOO . "foo")) ;; no more 'bar
~~~

In the
[Alexandria](https://common-lisp.net/project/alexandria/draft/alexandria.html#Conses)
library, see more functions like `remove-from-plist`, `alist-plist`,…


## Plist

A property list is simply a list that alternates a key, a value, and
so on, where its keys are symbols (we can not set its `:test`). More
precisely, it first has a cons cell whose `car` is the key, whose
`cdr` points to the following cons cell whose `car` is the
value.

For example this plist:

~~~lisp
(defparameter my-plist (list 'foo "foo" 'bar "bar"))
~~~

looks like this:

```
[o|o]---[o|o]---[o|o]---[o|/]
 |       |       |       |
FOO     "foo"   BAR     "bar"

```

We access an element with `getf (list elt)` (it returns the value)
(the list comes as first element),

we remove an element with `remf`.

~~~lisp
(defparameter my-plist (list 'foo "foo" 'bar "bar"))
;; => (FOO "foo" BAR "bar")
(setf (getf my-plist 'foo) "foo!!!")
;; => "foo!!!"
~~~

## Structures

Structures offer a way to store data in named slots. They support
single inheritance.

Classes provided by the Common Lisp Object System (CLOS) are more flexible however structures may offer better performance (see for example the SBCL manual).

### Creation

Use `defstruct`:

~~~lisp
(defstruct person
   id name age)
~~~

At creation slots are optional and default to `nil`.

To set a default value:

~~~lisp
(defstruct person
   id
   (name "john doe")
   age)
~~~

Also specify the type after the default value:

~~~lisp
(defstruct person
  id
  (name "john doe" :type string)
  age)
~~~

We create an instance with the generated constructor `make-` +
`<structure-name>`, so `make-person`:

~~~lisp
(defparameter *me* (make-person))
*me*
#S(PERSON :ID NIL :NAME "john doe" :AGE NIL)
~~~

note that printed representations can be read back by the reader.

With a bad name type:

~~~lisp
(defparameter *bad-name* (make-person :name 123))
~~~

```
Invalid initialization argument:
  :NAME
in call for class #<STRUCTURE-CLASS PERSON>.
   [Condition of type SB-PCL::INITARG-ERROR]
```

We can set the structure's constructor so as to create the structure
without using keyword arguments, which can be more convenient
sometimes. We give it a name and the order of the arguments:

~~~lisp
(defstruct (person (:constructor create-person (id name age)))
     id
     name
     age)
~~~

Our new constructor is `create-person`:

~~~lisp
(create-person 1 "me" 7)
#S(PERSON :ID 1 :NAME "me" :AGE 7)
~~~

However, the default `make-person` does *not* work any more:

~~~lisp
(make-person :name "me")
;; debugger:
obsolete structure error for a structure of type PERSON
[Condition of type SB-PCL::OBSOLETE-STRUCTURE]
~~~



### Slot access

We access the slots with accessors created by `<name-of-the-struct>-` + `slot-name`:

~~~lisp
(person-name *me*)
;; "john doe"
~~~

we then also have `person-age` and `person-id`.

### Setting

Slots are `setf`-able:

~~~lisp
(setf (person-name *me*) "Cookbook author")
(person-name *me*)
;; "Cookbook author"
~~~

### Predicate

A predicate function is generated:

~~~lisp
(person-p *me*)
T
~~~

### Single inheritance

Use single inheritance with the `:include <struct>` argument:

~~~lisp
(defstruct (female (:include person))
     (gender "female" :type string))
(make-female :name "Lilie")
;; #S(FEMALE :ID NIL :NAME "Lilie" :AGE NIL :GENDER "female")
~~~

Note that the CLOS object system is more powerful.

### Limitations

After a change, instances are not updated.

If we try to add a slot (`email` below), we have the choice to lose
all instances, or to continue using the new definition of
`person`. But the effects of redefining a structure are undefined by
the standard, so it is best to re-compile and re-run the changed
code.

~~~lisp
(defstruct person
       id
       (name "john doe" :type string)
       age
       email)

attempt to redefine the STRUCTURE-OBJECT class PERSON
incompatibly with the current definition
   [Condition of type SIMPLE-ERROR]

Restarts:
 0: [CONTINUE] Use the new definition of PERSON, invalidating already-loaded code and instances.
 1: [RECKLESSLY-CONTINUE] Use the new definition of PERSON as if it were compatible, allowing old accessors to use new instances and allowing new accessors to use old instances.
 2: [CLOBBER-IT] (deprecated synonym for RECKLESSLY-CONTINUE)
 3: [RETRY] Retry SLIME REPL evaluation request.
 4: [*ABORT] Return to SLIME's top level.
 5: [ABORT] abort thread (#<THREAD "repl-thread" RUNNING {1002A0FFA3}>)
~~~

If we choose restart `0`, to use the new definition, we lose access to `*me*`:

~~~lisp
*me*
obsolete structure error for a structure of type PERSON
   [Condition of type SB-PCL::OBSOLETE-STRUCTURE]
~~~

There is also very little introspection.
Portable Common Lisp does not define ways of finding out defined super/sub-structures nor what slots a structure has.

The Common Lisp Object System (which came after into the language)
doesn't have such limitations. See the [CLOS section](clos.html).

* [structures on the hyperspec](http://www.lispworks.com/documentation/HyperSpec/Body/08_.htm)
* David B. Lamkins, ["Successful Lisp, How to Understand and Use Common Lisp"](http://www.communitypicks.com/r/lisp/s/17592186045679-successful-lisp-how-to-understand-and-use-common).

## Tree

`tree-equal`, `copy-tree`. They descend recursively into the car and
the cdr of the cons cells they visit.

### Sycamore - purely functional weight-balanced binary trees

[https://github.com/ndantam/sycamore](https://github.com/ndantam/sycamore)

Features:

* Fast, purely functional weight-balanced binary trees.
  * Leaf nodes are simple-vectors, greatly reducing tree height.
* Interfaces for tree Sets and Maps (dictionaries).
* [Ropes](http://en.wikipedia.org/wiki/Rope_(data_structure))
* Purely functional [pairing heaps](http://en.wikipedia.org/wiki/Pairing_heap)
* Purely functional amortized queue.


## Appendix A - generic and nested access of alists, plists, hash-tables and CLOS slots

The solutions presented below might help you getting started, but keep
in mind that they'll have a performance impact and that error messages
will be less explicit.

* [CL21](cl21.html) has a generic `getf` (as well as others generic functions),
* [rutils](https://github.com/vseloved/rutils) as a generic `generic-elt` or `?`,
* the [access](https://github.com/AccelerationNet/access) library (battle tested, used by the Djula templating system) has a generic `(access my-var :elt)` ([blog post](https://lisp-journey.gitlab.io/blog/generice-consistent-access-of-data-structures-dotted-path/)). It also has `accesses` (plural) to access and set nested values.

## Appendix B - accessing nested data structures

Sometimes we work with nested data structures, and we might want an
easier way to access a nested element than intricated "getf" and
"assoc" and all. Also, we might want to just be returned a `nil` when
an intermediary key doesn't exist.

The `access` library given above provides this, with `(accesses var key1 key2…)`.

---
title: Strings
---

The most important thing to know about strings in Common Lisp is probably that
they are arrays and thus also sequences. This implies that all concepts that are
applicable to arrays and sequences also apply to strings. If you can't find a
particular string function, make sure you've also searched for the more general
[array or sequence functions](http://www.gigamonkeys.com/book/collections.html). We'll only cover a fraction of what can be done
with and to strings here.

ASDF3, which is included with almost all Common Lisp implementations,
includes
[Utilities for Implementation- and OS- Portability (UIOP)](https://gitlab.common-lisp.net/asdf/asdf/blob/master/uiop/README.md),
which defines functions to work on strings (`strcat`,
`string-prefix-p`, `string-enclosed-p`, `first-char`, `last-char`,
`split-string`, `stripln`).

Some external libraries available on Quicklisp bring some more
functionality or some shorter ways to do.

- [str](https://github.com/vindarel/cl-str) defines `trim`, `words`,
  `unwords`, `lines`, `unlines`, `concat`, `split`, `shorten`, `repeat`,
  `replace-all`, `starts-with?`, `ends-with?`, `blankp`, `emptyp`, …
- [cl-change-case](https://github.com/rudolfochrist/cl-change-case)
  has functions to convert strings between camelCase, param-case,
  snake_case and more. They are also included into `str`.
- [mk-string-metrics](https://github.com/cbaggers/mk-string-metrics)
  has functions to calculate various string metrics efficiently
  (Damerau-Levenshtein, Hamming, Jaro, Jaro-Winkler, Levenshtein, etc),
- and `cl-ppcre` can come in handy, for example
  `ppcre:replace-regexp-all`. See the [regexp](regexp.html) section.


Last but not least, when you'll need to tackle the `format` construct,
don't miss the following resources:

* the official [CLHS documentation](http://www.lispworks.com/documentation/HyperSpec/Body/22_c.htm)
* a [quick reference](http://clqr.boundp.org/)
* a [CLHS summary on HexstreamSoft](https://www.hexstreamsoft.com/articles/common-lisp-format-reference/clhs-summary/#subsections-summary-table)
* plus a Slime tip: type `C-c C-d ~` plus a letter of a format directive to open up its documentation. Again more useful with `ivy-mode` or `helm-mode`.

# Accessing Substrings

As a string is a sequence, you can access substrings with the SUBSEQ
function. The index into the string is, as always, zero-based. The third,
optional, argument is the index of the first character which is not a part of
the substring, it is not the length of the substring.

~~~lisp
* (defparameter *my-string* (string "Groucho Marx"))
*MY-STRING*
* (subseq *my-string* 8)
"Marx"
* (subseq *my-string* 0 7)
"Groucho"
* (subseq *my-string* 1 5)
"rouc"
~~~

You can also manipulate the substring if you use SUBSEQ together with SETF.

~~~lisp
* (defparameter *my-string* (string "Harpo Marx"))
*MY-STRING*
* (subseq *my-string* 0 5)
"Harpo"
* (setf (subseq *my-string* 0 5) "Chico")
"Chico"
* *my-string*
"Chico Marx"
~~~

But note that the string isn't "stretchable". To cite from the HyperSpec: "If
the subsequence and the new sequence are not of equal length, the shorter length
determines the number of elements that are replaced." For example:

~~~lisp
* (defparameter *my-string* (string "Karl Marx"))
*MY-STRING*
* (subseq *my-string* 0 4)
"Karl"
* (setf (subseq *my-string* 0 4) "Harpo")
"Harpo"
* *my-string*
"Harp Marx"
* (subseq *my-string* 4)
" Marx"
* (setf (subseq *my-string* 4) "o Marx")
"o Marx"
* *my-string*
"Harpo Mar"
~~~

# Accessing Individual Characters

You can use the function CHAR to access individual characters of a string. CHAR
can also be used in conjunction with SETF.

~~~lisp
* (defparameter *my-string* (string "Groucho Marx"))
*MY-STRING*
* (char *my-string* 11)
#\x
* (char *my-string* 7)
#\Space
* (char *my-string* 6)
#\o
* (setf (char *my-string* 6) #\y)
#\y
* *my-string*
"Grouchy Marx"
~~~

Note that there's also SCHAR. If efficiency is important, SCHAR can be a bit
faster where appropriate.

Because strings are arrays and thus sequences, you can also use the more generic
functions AREF and ELT (which are more general while CHAR might be implemented
more efficiently).

~~~lisp
* (defparameter *my-string* (string "Groucho Marx"))
*MY-STRING*
* (aref *my-string* 3)
#\u
* (elt *my-string* 8)
#\M
~~~

Each character in a string has an integer code. The range of recognized codes
and Lisp's ability to print them is directed related to your implementation's
character set support, e.g. ISO-8859-1, or Unicode. Here are some examples in
SBCL of UTF-8 which encodes characters as 1 to 4 8 bit bytes. The first example
shows a character outside the first 128 chars, or what is considered the normal
Latin character set. The second example shows a multibyte encoding (beyond the
value 255). Notice the Lisp reader can round-trip characters by name.

~~~lisp
* (stream-external-format *standard-output*)

:UTF-8
* (code-char 200)

#\LATIN_CAPITAL_LETTER_E_WITH_GRAVE
* (char-code #\LATIN_CAPITAL_LETTER_E_WITH_GRAVE)

200
* (code-char 1488)
#\HEBREW_LETTER_ALEF

* (char-code #\HEBREW_LETTER_ALEF)
1488
~~~

Check out the UTF-8 Wikipedia article for the range of supported characters and
their encodings.

# Manipulating Parts of a String

There's a slew of (sequence) functions that can be used to manipulate a string
and we'll only provide some examples here. See the sequences dictionary in the
HyperSpec for more.

~~~lisp
* (remove #\o "Harpo Marx")
"Harp Marx"
* (remove #\a "Harpo Marx")
"Hrpo Mrx"
* (remove #\a "Harpo Marx" :start 2)
"Harpo Mrx"
* (remove-if #'upper-case-p "Harpo Marx")
"arpo arx"
* (substitute #\u #\o "Groucho Marx")
"Gruuchu Marx"
* (substitute-if #\_ #'upper-case-p "Groucho Marx")
"_roucho _arx"
* (defparameter *my-string* (string "Zeppo Marx"))
*MY-STRING*
* (replace *my-string* "Harpo" :end1 5)
"Harpo Marx"
* *my-string*
"Harpo Marx"
~~~

Another function that can be frequently used (but not part of the ANSI standard)
is replace-all. This function provides an easy functionality for search/replace
operations on a string, by returning a new string in which all the occurrences of
the 'part' in string is replaced with 'replacement'".

~~~lisp
* (replace-all "Groucho Marx Groucho" "Groucho" "ReplacementForGroucho")
"ReplacementForGroucho Marx ReplacementForGroucho"
~~~

One of the implementations of replace-all is as follows:

~~~lisp
(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurrences of the part
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))
~~~

However, bear in mind that the above code is not optimized for long strings; if
you intend to perform such an operation on very long strings, files, etc. please
consider using cl-ppcre regular expressions and string processing library which
is heavily optimized.

# Concatenating Strings

The name says it all: CONCATENATE is your friend. Note that this is a generic
sequence function and you have to provide the result type as the first argument.

~~~lisp
* (concatenate 'string "Karl" " " "Marx")
"Karl Marx"
* (concatenate 'list "Karl" " " "Marx")
(#\K #\a #\r #\l #\Space #\M #\a #\r #\x)
~~~

With UIOP, use `strcat`:

~~~lisp
* (uiop:strcat "karl" " " marx")
~~~

or with the library `str`, use `concat`:

~~~lisp
* (str:concat "foo" "bar")
~~~

If you have to construct a string out of many parts, all of these calls to
CONCATENATE seem wasteful, though. There are at least three other good ways to
construct a string piecemeal, depending on what exactly your data is. If you
build your string one character at a time, make it an adjustable VECTOR (a
one-dimensional ARRAY) of type character with a fill-pointer of zero, then use
VECTOR-PUSH-EXTEND on it. That way, you can also give hints to the system if you
can estimate how long the string will be. (See the optional third argument to
VECTOR-PUSH-EXTEND.)

~~~lisp
* (defparameter *my-string* (make-array 0
                                        :element-type 'character
                                        :fill-pointer 0
                                        :adjustable t))
*MY-STRING*
* *my-string*
""
* (dolist (char '(#\Z #\a #\p #\p #\a))
    (vector-push-extend char *my-string*))
NIL
* *my-string*
"Zappa"
~~~

If the string will be constructed out of (the printed representations of)
arbitrary objects, (symbols, numbers, characters, strings, ...), you can use
FORMAT with an output stream argument of NIL. This directs FORMAT to return the
indicated output as a string.

~~~lisp
* (format nil "This is a string with a list ~A in it"
          '(1 2 3))
"This is a string with a list (1 2 3) in it"
~~~

We can use the looping constructs of the FORMAT mini language to emulate
CONCATENATE.

~~~lisp
* (format nil "The Marx brothers are:~{ ~A~}."
          '("Groucho" "Harpo" "Chico" "Zeppo" "Karl"))
"The Marx brothers are: Groucho Harpo Chico Zeppo Karl."
~~~

FORMAT can do a lot more processing but it has a relatively arcane syntax. After
this last example, you can find the details in the CLHS section about formatted
output.

~~~lisp
* (format nil "The Marx brothers are:~{ ~A~^,~}."
          '("Groucho" "Harpo" "Chico" "Zeppo" "Karl"))
"The Marx brothers are: Groucho, Harpo, Chico, Zeppo, Karl."
~~~

Another way to create a string out of the printed representation of various
object is using WITH-OUTPUT-TO-STRING. The value of this handy macro is a string
containing everything that was output to the string stream within the body to
the macro. This means you also have the full power of FORMAT at your disposal,
should you need it.

~~~lisp
* (with-output-to-string (stream)
    (dolist (char '(#\Z #\a #\p #\p #\a #\, #\Space))
      (princ char stream))
    (format stream "~S - ~S" 1940 1993))
"Zappa, 1940 - 1993"
~~~

# Processing a String One Character at a Time

Use the MAP function to process a string one character at a time.

~~~lisp
* (defparameter *my-string* (string "Groucho Marx"))
*MY-STRING*
* (map 'string #'(lambda (c) (print c)) *my-string*)
#\G
#\r
#\o
#\u
#\c
#\h
#\o
#\Space
#\M
#\a
#\r
#\x
"Groucho Marx"
~~~

Or do it with LOOP.

~~~lisp
* (loop for char across "Zeppo"
        collect char)
(#\Z #\e #\p #\p #\o)
~~~

# Reversing a String by Word or Character

Reversing a string by character is easy using the built-in REVERSE function (or
its destructive counterpart NREVERSE).

~~~lisp
*(defparameter *my-string* (string "DSL"))
*MY-STRING*
* (reverse *my-string*)
"LSD"
~~~

There's no one-liner in CL to reverse a string by word (like you would do it in
Perl with split and join). You either have to use functions from an external
library like SPLIT-SEQUENCE or you have to roll your own solution.

Here's an attempt with the `str` library:

~~~lisp
* (defparameter *singing* "singing in the rain")
*SINGING*
* (str:words *SINGING*)
("singing" "in" "the" "rain")
* (reverse *)
("rain" "the" "in" "singing")
* (str:unwords *)
"rain the in singing"
~~~

And here's another one with no external dependencies:

~~~lisp
* (defun split-by-one-space (string)
    "Returns a list of substrings of string
    divided by ONE space each.
    Note: Two consecutive spaces will be seen as
    if there were an empty string between them."
    (loop for i = 0 then (1+ j)
          as j = (position #\Space string :start i)
          collect (subseq string i j)
          while j))
SPLIT-BY-ONE-SPACE
* (split-by-one-space "Singing in the rain")
("Singing" "in" "the" "rain")
* (split-by-one-space "Singing in the  rain")
("Singing" "in" "the" "" "rain")
* (split-by-one-space "Cool")
("Cool")
* (split-by-one-space " Cool ")
("" "Cool" "")
* (defun join-string-list (string-list)
    "Concatenates a list of strings
and puts spaces between the elements."
    (format nil "~{~A~^ ~}" string-list))
JOIN-STRING-LIST
* (join-string-list '("We" "want" "better" "examples"))
"We want better examples"
* (join-string-list '("Really"))
"Really"
* (join-string-list '())
""
* (join-string-list
   (nreverse
    (split-by-one-space
     "Reverse this sentence by word")))
"word by sentence this Reverse"
~~~

# Controlling Case

Common Lisp has a couple of functions to control the case of a string.

~~~lisp
* (string-upcase "cool")
"COOL"
* (string-upcase "Cool")
"COOL"
* (string-downcase "COOL")
"cool"
* (string-downcase "Cool")
"cool"
* (string-capitalize "cool")
"Cool"
* (string-capitalize "cool example")
"Cool Example"
~~~

These functions take the `:start` and `:end` keyword arguments so you can optionally
only manipulate a part of the string. They also have destructive counterparts
whose names starts with "N".

~~~lisp
* (string-capitalize "cool example" :start 5)
"cool Example"
* (string-capitalize "cool example" :end 5)
"Cool example"
* (defparameter *my-string* (string "BIG"))
*MY-STRING*
* (defparameter *my-downcase-string* (nstring-downcase *my-string*))
*MY-DOWNCASE-STRING*
* *my-downcase-string*
"big"
* *my-string*
"big"
~~~

Note this potential caveat: according to the HyperSpec,

> for STRING-UPCASE, STRING-DOWNCASE, and STRING-CAPITALIZE, string is not modified. However, if no characters in string require conversion, the result may be either string or a copy of it, at the implementation's discretion.

This implies that the last result in
the following example is implementation-dependent - it may either be "BIG" or
"BUG". If you want to be sure, use COPY-SEQ.

~~~lisp
* (defparameter *my-string* (string "BIG"))
*MY-STRING*
* (defparameter *my-upcase-string* (string-upcase *my-string*))
*MY-UPCASE-STRING*
* (setf (char *my-string* 1) #\U)
#\U
* *my-string*
"BUG"
* *my-upcase-string*
"BIG"
~~~

## With the format function

The format function has directives to change the case of words:

### To lower case: ~( ~)

~~~lisp
(format t "~(~a~)" "HELLO WORLD")
;; => hello world
~~~


### Capitalize every word: ~:( ~)

~~~lisp
(format t "~:(~a~)" "HELLO WORLD")
Hello World
NIL
~~~

### Capitalize the first word: ~@( ~)

~~~lisp
(format t "~@(~a~)" "hello world")
Hello world
NIL
~~~

### To upper case: ~@:( ~)

Where we re-use the colon and the @:

~~~lisp
(format t "~@:(~a~)" "hello world")
HELLO WORLD
NIL
~~~


# Trimming Blanks from the Ends of a String

Not only can you trim blanks, but you can get rid of arbitrary characters. The
functions STRING-TRIM, STRING-LEFT-TRIM and STRING-RIGHT-TRIM return a substring
of their second argument where all characters that are in the first argument are
removed off the beginning and/or the end. The first argument can be any sequence
of characters.

~~~lisp
* (string-trim " " " trim me ")
"trim me"
* (string-trim " et" " trim me ")
"rim m"
* (string-left-trim " et" " trim me ")
"rim me "
* (string-right-trim " et" " trim me ")
" trim m"
* (string-right-trim '(#\Space #\e #\t) " trim me ")
" trim m"
* (string-right-trim '(#\Space #\e #\t #\m) " trim me ")
~~~

Note: The caveat mentioned in the section about Controlling Case also applies
here.

# Converting between Symbols and Strings

The function INTERN will "convert" a string to a symbol. Actually, it will check
whether the symbol denoted by the string (its first argument) is already
accessible in the package (its second, optional, argument which defaults to the
current package) and enter it, if necessary, into this package. It is beyond the
scope of this chapter to explain all the concepts involved and to address the
second return value of this function. See the CLHS chapter about packages for
details.

Note that the case of the string is relevant.

~~~lisp
* (in-package "COMMON-LISP-USER")
#<The COMMON-LISP-USER package, 35/44 internal, 0/9 external>
* (intern "MY-SYMBOL")
MY-SYMBOL
NIL
* (intern "MY-SYMBOL")
MY-SYMBOL
:INTERNAL
* (export 'MY-SYMBOL)
T
* (intern "MY-SYMBOL")
MY-SYMBOL
:EXTERNAL
* (intern "My-Symbol")
|My-Symbol|
NIL
* (intern "MY-SYMBOL" "KEYWORD")
:MY-SYMBOL
NIL
* (intern "MY-SYMBOL" "KEYWORD")
:MY-SYMBOL
:EXTERNAL
~~~

To do the opposite, convert from a symbol to a string, use SYMBOL-NAME or
STRING.

~~~lisp
* (symbol-name 'MY-SYMBOL)
"MY-SYMBOL"
* (symbol-name 'my-symbol)
"MY-SYMBOL"
* (symbol-name '|my-symbol|)
"my-symbol"
* (string 'howdy)
"HOWDY"
~~~

# Converting between Characters and Strings

You can use COERCE to convert a string of length 1 to a character. You can also
use COERCE to convert any sequence of characters into a string. You can not use
COERCE to convert a character to a string, though - you'll have to use STRING
instead.

~~~lisp
* (coerce "a" 'character)
#\a
* (coerce (subseq "cool" 2 3) 'character)
#\o
* (coerce "cool" 'list)
(#\c #\o #\o #\l)
* (coerce '(#\h #\e #\y) 'string)
"hey"
* (coerce (nth 2 '(#\h #\e #\y)) 'character)
#\y
* (defparameter *my-array* (make-array 5 :initial-element #\x))
*MY-ARRAY*
* *my-array*
#(#\x #\x #\x #\x #\x)
* (coerce *my-array* 'string)
"xxxxx"
* (string 'howdy)
"HOWDY"
* (string #\y)
"y"
* (coerce #\y 'string)
#\y can't be converted to type STRING.
   [Condition of type SIMPLE-TYPE-ERROR]
~~~

# Finding an Element of a String

Use FIND, POSITION, and their -IF counterparts to find characters in a string.

~~~lisp
* (find #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equal)
#\t
* (find #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equalp)
#\T
* (find #\z "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equalp)
NIL
* (find-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks.")
#\1
* (find-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks." :from-end t)
#\0
* (position #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equal)
17
* (position #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equalp)
0
* (position-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks.")
37
* (position-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks." :from-end t)
43
~~~

Or use COUNT and friends to count characters in a string.

~~~lisp
* (count #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equal)
2
* (count #\t "The Hyperspec contains approximately 110,000 hyperlinks." :test #'equalp)
3
* (count-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks.")
6
* (count-if #'digit-char-p "The Hyperspec contains approximately 110,000 hyperlinks." :start 38)
5
~~~

# Finding a Substring of a String

The function SEARCH can find substrings of a string.

~~~lisp
* (search "we" "If we can't be free we can at least be cheap")
3
* (search "we" "If we can't be free we can at least be cheap" :from-end t)
20
* (search "we" "If we can't be free we can at least be cheap" :start2 4)
20
* (search "we" "If we can't be free we can at least be cheap" :end2 5 :from-end t)
3
* (search "FREE" "If we can't be free we can at least be cheap")
NIL
* (search "FREE" "If we can't be free we can at least be cheap" :test #'char-equal)
15
~~~

# Converting a String to a Number

## To an integer: parse-integer

CL provides the `parse-integer` function to convert a string representation of an integer
to the corresponding numeric value. The second return value is the index into
the string where the parsing stopped.

~~~lisp
* (parse-integer "42")
42
2
* (parse-integer "42" :start 1)
2
2
* (parse-integer "42" :end 1)
4
1
* (parse-integer "42" :radix 8)
34
2
* (parse-integer " 42 ")
42
3
* (parse-integer " 42 is forty-two" :junk-allowed t)
42
3
* (parse-integer " 42 is forty-two")

Error in function PARSE-INTEGER:
   There's junk in this string: " 42 is forty-two".
~~~

`parse-integer` doesn't understand radix specifiers like `#X`, nor is there a
built-in function to parse other numeric types. You could use `read-from-string`
in this case.


## To any number: read-from-string

Be aware that the full reader is in effect if you're using this
function. This can lead to vulnerability issues.

~~~lisp
* (read-from-string "#X23")
35
4
* (read-from-string "4.5")
4.5
3
* (read-from-string "6/8")
3/4
3
* (read-from-string "#C(6/8 1)")
#C(3/4 1)
9
* (read-from-string "1.2e2")
120.00001
5
* (read-from-string "symbol")
SYMBOL
6
* (defparameter *foo* 42)
*FOO*
* (read-from-string "#.(setq *foo* \"gotcha\")")
"gotcha"
23
* *foo*
"gotcha"
~~~

## To a float: the parse-float library

There is no built-in function similar to `parse-integer` to parse
other number types. The external library
[parse-float](https://github.com/soemraws/parse-float) does exactly
that. It doesn't use `read-from-string` so it is safe to use.

~~~lisp
(ql:quickload "parse-float")
(parse-float:parse-float "1.2e2")
;; 120.00001
;; 5
~~~

LispWorks also has a [parse-float](http://www.lispworks.com/documentation/lw51/LWRM/html/lwref-228.htm) function.

See also [parse-number](https://github.com/sharplispers/parse-number).


# Converting a Number to a String

The general function WRITE-TO-STRING or one of its simpler variants
PRIN1-TO-STRING or PRINC-TO-STRING may be used to convert a number to a
string. With WRITE-TO-STRING, the :base keyword argument may be used to change
the output base for a single call. To change the output base globally, set
*print-base* which defaults to 10. Remember in Lisp, rational numbers are
represented as quotients of two integers even when converted to strings.

~~~lisp
* (write-to-string 250)
"250"
* (write-to-string 250.02)
"250.02"
* (write-to-string 250 :base 5)
"2000"
* (write-to-string (/ 1 3))
"1/3"
*
~~~

# Comparing Strings

The general functions EQUAL and EQUALP can be used to test whether two strings
are equal. The strings are compared element-by-element, either in a
case-sensitive manner (EQUAL) or not (EQUALP). There's also a bunch of
string-specific comparison functions. You'll want to use these if you're
deploying implementation-defined attributes of characters. Check your vendor's
documentation in this case.

Here are a few examples. Note that all functions that test for inequality return the position of the first mismatch as a generalized boolean. You can also use the generic sequence function MISMATCH if you need more versatility.

~~~lisp
* (string= "Marx" "Marx")
T
* (string= "Marx" "marx")
NIL
* (string-equal "Marx" "marx")
T
* (string< "Groucho" "Zeppo")
0
* (string< "groucho" "Zeppo")
NIL
* (string-lessp "groucho" "Zeppo")
0
* (mismatch "Harpo Marx" "Zeppo Marx" :from-end t :test #'char=)
3
~~~

# String formatting

The `format` function has a lot of directives to print strings,
numbers, lists, going recursively, even calling Lisp functions,
etc. We'll focus here on a few things to print and format strings.

The need of our examples arise when we want to print many strings and
justify them. Let's work with this list of movies:

~~~lisp
(defparameter movies '(
    (1 "Matrix" 5)
    (10 "Matrix Trilogy swe sub" 3.3)
    ))
~~~

We want an aligned and justified result like this:

```
 1 Matrix                  5
10 Matrix Trilogy swe sub  3.3
```

We'll use `mapcar` to iterate over our movies and experiment with the
format constructs.

~~~lisp
(mapcar (lambda (it)
          (format t "~a ~a ~a~%" (first it) (second it) (third it)))
        movies)
~~~

which prints:

```
1 fooo baaar 5
10 last 3.3
```


## Structure of format

Format directives start with `~`. A final character like `A` or `a`
(they are case insensitive) defines the directive. In between, it can
accept coma-separated options and parameters.

Print a tilde with `~~`, or 10 with `~10~`.

Other directives include:

- `R`: Roman (e.g., prints in English): `(format t "~R" 20)` => "twenty".
- `$`: monetary: `(format t "~$" 21982)` => 21982.00
- `D`, `B`, `O`, `X`: Decimal, Binary, Octal, Hexadecimal.
- `F`: fixed-format Floating point.

## Basic primitive: ~A or ~a (Aesthetics)

`(format t "~a" movies)` is the most basic primitive.

~~~lisp
(format nil "~a" movies)
;; => "((1 Matrix 5) (10 Matrix Trilogy swe sub 3.3))"
~~~

## Newlines: ~% and ~&

`~%` is the newline character. `~10%` prints 10 newlines.

`~&` does not print a newline if the output stream is already at one.

## Tabs

with `~T`. Also `~10T` works.

Also `i` for indentation.


## Justifying text / add padding on the right

Use a number as parameter, like `~2a`:

~~~lisp
(format nil "~20a" "yo")
;; "yo                  "
~~~

~~~lisp
(mapcar (lambda (it)
           (format t "~2a ~a ~a~%" (first it) (second it) (third it)))
         movies)
~~~

```
1  Matrix 5
10 Matrix Trilogy swe sub 3.3
```

So, expanding:

~~~lisp
(mapcar (lambda (it)
          (format t "~2a ~25a ~2a~%" (first it) (second it) (third it)))
        movies)
~~~

```
1  Matrix                    5
10 Matrix Trilogy swe sub    3.3
```

text is justified on the right (this would be with option `:`).

### Justifying on the left: @

Use a `@` as in `~2@A`:

~~~lisp
(format nil "~20@a" "yo")
;; "                  yo"
~~~

~~~lisp
(mapcar (lambda (it)
           (format nil "~2@a ~25@a ~2a~%" (first it) (second it) (third it)))
        movies)
~~~

```
 1                    Matrix 5
10    Matrix Trilogy swe sub 3.3
```

## Justifying decimals

In `~,2F`, 2 is the number of decimals and F the floats directive:
`(format t "~,2F" 20.1)` => "20.10".

With `~2,2f`:

~~~lisp
(mapcar (lambda (it)
          (format t "~2@a ~25a ~2,2f~%" (first it) (second it) (third it)))
        movies)
~~~

```
 1 Matrix                    5.00
10 Matrix Trilogy swe sub    3.30
```

And we're happy with this result.

## Formatting a format string

Sometimes you want to justify a string, but the length is a variable
itself. You can't hardcode its value as in `(format nil "~30a"
"foo")`. Enters the `v` directive. We can use it in place of the
comma-separated prefix parameters:

~~~lisp
(let ((padding 30))
    (format nil "~va" padding "foo"))
;; "foo                           "
~~~

---
title: Numbers
---

Common Lisp has a rich set of numerical types, including integer,
rational, floating point, and complex.

Some sources:

* [`Numbers`][numbers] in Common Lisp the Language, 2nd Edition
* [`Numbers, Characters and Strings`][numbers-characters-strings]
  in Practical Common Lisp

## Introduction

### Integer types

Common Lisp provides a true integer type, called `bignum`, limited only by the total
memory available (not the machine word size). For example this would
overflow a 64 bit integer by some way:

~~~lisp
* (expt 2 200)
1606938044258990275541962092341162602522202993782792835301376
~~~

For efficiency, integers can be limited to a fixed number of bits,
called a `fixnum` type. The range of integers which can be represented
is given by:

~~~lisp
* most-positive-fixnum
4611686018427387903
* most-negative-fixnum
-4611686018427387904
~~~

Functions which operate on or evaluate to integers include:

* [`isqrt`][isqrt], which returns the greatest integer less than or equal to the
  exact positive square root of natural.

~~~lisp
* (isqrt 10)
3
* (isqrt 4)
2
~~~

* [`gcd`][gcd] to find the Greatest Common Denominator
* [`lcm`][lcm] for the Least Common Multiple.

Like other low-level programming languages, Common Lisp provides literal
representation for hexadecimals and other radixes up to 36. For example:

~~~lisp
* #xFF
255
* #2r1010
10
* #4r33
15
* #8r11
9
* #16rFF
255
* #36rz
35
~~~

### Rational types

Rational numbers of type [`ratio`][ratio] consist of two `bignum`s, the
numerator and denominator. Both can therefore be arbitrarily large:

~~~lisp
* (/ (1+ (expt 2 100)) (expt 2 100))
1267650600228229401496703205377/1267650600228229401496703205376
~~~

It is a subtype of the [`rational`][rational] class, along with
[`integer`][integer].

### Floating point types

See [Common Lisp the Language, 2nd Edition, section 2.1.3][book-cl-2.1.3].

Floating point types attempt to represent the continuous real numbers
using a finite number of bits. This means that many real numbers
cannot be represented, but are approximated. This can lead to some nasty
surprises, particularly when converting between base-10 and the base-2
internal representation. If you are working with floating point
numbers then reading [What Every Computer Scientist Should Know About
Floating-Point Arithmetic][article-floating-point-arithmetic] is highly
recommended.

The Common Lisp standard allows for several floating point types. In
order of increasing precision these are: `short-float`,
`single-float`, `double-float`, and `long-float`. Their precisions are
implementation dependent, and it is possible for an implementation to
have only one floating point precision for all types.

The constants [`short-float-epsilon`, `single-float-epsilon`,
`double-float-epsilon` and `long-float-epsilon`][float-constants] give
a measure of the precision of the floating point types, and are
implementation dependent.

#### Floating point literals

When reading floating point numbers, the default type is set by the special
variable [`*read-default-float-format*`][read-default-float-format].  By
default this is `SINGLE-FLOAT`, so if you want to ensure that a number is read
as double precision then put a `d0` suffix at the end

~~~lisp
* (type-of 1.24)
SINGLE-FLOAT

* (type-of 1.24d0)
DOUBLE-FLOAT
~~~

Other suffixes are `s` (short), `f` (single float), `d` (double
float), `l` (long float) and `e` (default; usually single float).

The default type can be changed, but note that this may break packages
which assume `single-float` type.

~~~lisp
* (setq *read-default-float-format* 'double-float)
* (type-of 1.24)
DOUBLE-FLOAT
~~~

Note that unlike in some languages, appending a single decimal point
to the end of a number does not make it a float:
~~~lisp
* (type-of 10.)
(INTEGER 0 4611686018427387903)

* (type-of 10.0)
SINGLE-FLOAT
~~~

#### Floating point errors

If the result of a floating point calculation is too large then a floating
point overflow occurs. By default in [SBCL][SBCL] (and other implementations)
this results in an error condition:

~~~lisp
* (exp 1000)
; Evaluation aborted on #<FLOATING-POINT-OVERFLOW {10041720B3}>.
~~~

The error can be caught and handled, or this behaviour can be
changed, to return `+infinity`. In SBCL this is:

~~~lisp
* (sb-int:set-floating-point-modes :traps '(:INVALID :DIVIDE-BY-ZERO))

* (exp 1000)
#.SB-EXT:SINGLE-FLOAT-POSITIVE-INFINITY

* (/ 1 (exp 1000))
0.0
~~~

The calculation now silently continues, without an error condition.

A similar functionality to disable floating overflow errors
exists in [CCL][CCL]:
~~~lisp
* (set-fpu-mode :overflow nil)
~~~

In SBCL the floating point modes can be inspected:

~~~lisp
* (sb-int:get-floating-point-modes)
(:TRAPS (:OVERFLOW :INVALID :DIVIDE-BY-ZERO) :ROUNDING-MODE :NEAREST
 :CURRENT-EXCEPTIONS NIL :ACCRUED-EXCEPTIONS NIL :FAST-MODE NIL)
~~~

#### Arbitrary precision

For arbitrary high precision calculations there is the
[computable-reals][computable-reals] library on QuickLisp:

~~~lisp
* (ql:quickload :computable-reals)
* (use-package :computable-reals)

* (sqrt-r 2)
+1.41421356237309504880...

* (sin-r (/r +pi-r+ 2))
+1.00000000000000000000...
~~~

The precision to print is set by `*PRINT-PREC*`, by default 20
~~~lisp
* (setq *PRINT-PREC* 50)
* (sqrt-r 2)
+1.41421356237309504880168872420969807856967187537695...
~~~

### Complex types

There are 5 types of complex number: The real and imaginary parts must
be of the same type, and can be rational, or one of the floating point
types (short, single, double or long).

Complex values can be created using the `#C` reader macro or the function
[`complex`][complex]. The reader macro does not allow the use of expressions
as real and imaginary parts:

~~~lisp
* #C(1 1)
#C(1 1)

* #C((+ 1 2) 5)
; Evaluation aborted on #<TYPE-ERROR expected-type: REAL datum: (+ 1 2)>.

* (complex (+ 1 2) 5)
#C(3 5)
~~~

If constructed with mixed types then the higher precision type will be used for both parts.

~~~lisp
* (type-of #C(1 1))
(COMPLEX (INTEGER 1 1))

* (type-of #C(1.0 1))
(COMPLEX (SINGLE-FLOAT 1.0 1.0))

* (type-of #C(1.0 1d0))
(COMPLEX (DOUBLE-FLOAT 1.0d0 1.0d0))
~~~

The real and imaginary parts of a complex number can be extracted using
[`realpart` and `imagpart`][realpart-and-imaginary]:

~~~lisp
* (realpart #C(7 9))
7
* (imagpart #C(4.2 9.5))
9.5
~~~

#### Complex arithmetic

Common Lisp's mathematical functions generally handle complex numbers,
and return complex numbers when this is the true result. For example:

~~~lisp
* (sqrt -1)
#C(0.0 1.0)

* (exp #C(0.0 0.5))
#C(0.87758255 0.47942555)

* (sin #C(1.0 1.0))
#C(1.2984576 0.63496387)
~~~

## Reading numbers from strings

The [`parse-integer`][parse-integer] function reads an integer from a string.

The [parse-float][parse-float] library provides a parser which cannot evaluate
arbitrary expressions, so should be safer to use on untrusted input:

~~~lisp
* (ql:quickload :parse-float)
* (use-package :parse-float)

* (parse-float "23.4e2" :type 'double-float)
2340.0d0
6
~~~

See the [strings section][strings] on converting between strings and numbers.

## Converting numbers

Most numerical functions automatically convert types as needed.
The `coerce` function converts objects from one type to another,
including numeric types.

See [Common Lisp the Language, 2nd Edition, section 12.6][book-cl-12.6].

### Convert float to rational

The [`rational` and `rationalize` functions][rational-and-rationalize] convert
a real numeric argument into a rational. `rational` assumes that floating
point arguments are exact; `rationalize` exploits the fact that floating point
numbers are only exact to their precision, so can often find a simpler
rational number.

### Convert rational to integer

If the result of a calculation is a rational number where the numerator
is a multiple of the denominator, then it is automatically converted
to an integer:

~~~lisp
* (type-of (* 1/2 4))
(INTEGER 0 4611686018427387903)
~~~

## Rounding floating-point and rational numbers

The [`ceiling`, `floor`, `round` and `truncate`][ceiling-functions] functions
convert floating point or rational numbers to integers.  The difference
between the result and the input is returned as the second value, so that the
input is the sum of the two outputs.

~~~lisp
* (ceiling 1.42)
2
-0.58000004

* (floor 1.42)
1
0.41999996

* (round 1.42)
1
0.41999996

* (truncate 1.42)
1
0.41999996
~~~

There is a difference between `floor` and `truncate` for negative
numbers:

~~~lisp
* (truncate -1.42)
-1
-0.41999996

* (floor -1.42)
-2
0.58000004

* (ceiling -1.42)
-1
-0.41999996
~~~

Similar functions `fceiling`, `ffloor`, `fround` and `ftruncate`
return the result as floating point, of the same type as their
argument:

~~~lisp
* (ftruncate 1.3)
1.0
0.29999995

* (type-of (ftruncate 1.3))
SINGLE-FLOAT

* (type-of (ftruncate 1.3d0))
DOUBLE-FLOAT
~~~

## Comparing numbers

See [Common Lisp the Language, 2nd Edition, Section 12.3][book-cl-12.3].

The `=` predicate returns `T` if all arguments are numerically equal.
Note that comparison of floating point numbers includes some margin
for error, due to the fact that they cannot represent all real
numbers and accumulate errors.

The constant [`single-float-epsilon`][single-float-epsilon] is the smallest
number which will cause an `=` comparison to fail, if it is added to 1.0:

~~~lisp
* (= (+ 1s0 5e-8) 1s0)
T
* (= (+ 1s0 6e-8) 1s0)
NIL
~~~

Note that this does not mean that a `single-float` is always precise
to within `6e-8`:

~~~lisp
* (= (+ 10s0 4e-7) 10s0)
T
* (= (+ 10s0 5e-7) 10s0)
NIL
~~~

Instead this means that `single-float` is precise to approximately
seven digits. If a sequence of calculations are performed, then error
can accumulate and a larger error margin may be needed. In this case
the absolute difference can be compared:

~~~lisp
* (< (abs (- (+ 10s0 5e-7)
             10s0))
     1s-6)
T
~~~

When comparing numbers with `=` mixed types are allowed. To test both
numerical value and type use `eql`:

~~~lisp
* (= 3 3.0)
T

* (eql 3 3.0)
NIL
~~~

## Operating on a series of numbers

Many Common Lisp functions operate on sequences, which can be either lists
or vectors (1D arrays). See the section on
[mapping][mapping].

Operations on multidimensional arrays are discussed in
[this section][arrays].

Libraries are available for defining and operating on lazy sequences,
including "infinite" sequences of numbers. For example

* [Clazy][clazy] which is on QuickLisp.
* [folio2][folio2] on QuickLisp. Includes an interface to the
* [Series][series] package for efficient sequences.
* [lazy-seq][lazy-seq].

## Working with Roman numerals

The `format` function can convert numbers to roman numerals with the
`~@r` directive:

~~~lisp
* (format nil "~@r" 42)
"XLII"
~~~

There is a [gist by tormaroe][gist-tormaroe] for reading roman numerals.

## Generating random numbers

The [`random`][random] function generates either integer or floating point
random numbers, depending on the type of its argument.

~~~lisp
* (random 10)
7

* (type-of (random 10))
(INTEGER 0 4611686018427387903)
* (type-of (random 10.0))
SINGLE-FLOAT
* (type-of (random 10d0))
DOUBLE-FLOAT
~~~

In SBCL a [Mersenne Twister][mersenne-twister] pseudo-random number generator
 is used. See section [7.13 of the SBCL manual][sbcl-7.13] for details.

The random seed is stored in [`*random-state*`][random-state] whose internal
representation is implementation dependent. The function
[`make-random-state`][make-random-state] can be used to make new random
states, or copy existing states.

To use the same set of random numbers multiple times,
`(make-random-state nil)` makes a copy of the current `*random-state*`:

~~~lisp
* (dotimes (i 3)
    (let ((*random-state* (make-random-state nil)))
      (format t "~a~%"
              (loop for i from 0 below 10 collecting (random 10)))))

(8 3 9 2 1 8 0 0 4 1)
(8 3 9 2 1 8 0 0 4 1)
(8 3 9 2 1 8 0 0 4 1)
~~~

This generates 10 random numbers in a loop, but each time the sequence
is the same because the `*random-state*` special variable is dynamically
bound to a copy of its state before the `let` form.

Other resources:

* The [random-state][random-state] package is available on QuickLisp, and
  provides a number of portable random number generators.

## Bit-wise Operation

Common Lisp also provides many functions to perform bit-wise arithmetic
operations. Some commonly used ones are listed below, together with their
C/C++ equivalence.

{:class="table table-bordered table-stripped"}
| Common  Lisp     | C/C++       | Description                                      |
|------------------|-------------|--------------------------------------------------|
| `(logand a b c)` | `a & b & c` | Bit-wise AND of multiple operands                |
| `(logior a b c)` | `a | b | c` | Bit-wise OR of multiple arguments                |
| `(lognot a)`     | `~a`        | Bit-wise NOT of single operand                   |
| `(logxor a b c)` | `a ^ b ^ c` | Bit-wise exclusive or (XOR) or multiple operands |
| `(ash a 3)`      | `a << 3`    | Bit-wise left shift                              |
| `(ash a -3)`     | `a >> 3`    | Bit-wise right shift                             |

Negative numbers are treated as two's-complements. If you have forgotten this,
please refer to the [Wiki page][twos-complements].

For example:

~~~lisp
* (logior 1 2 4 8)
15
;; Explanation:
;;   0001
;;   0010
;;   0100
;; | 1000
;; -------
;;   1111

* (logand 2 -3 4)
0

;; Explanation:
;;   0010 (2)
;;   1101 (two's complement of -3)
;; & 0100 (4)
;; -------
;;   0000

* (logxor 1 3 7 15)
10

;; Explanation:
;;   0001
;;   0011
;;   0111
;; ^ 1111
;; -------
;;   1010

* (lognot -1)
0
;; Explanation:
;;   11 -> 00

* (lognot -3)
2
;;   101 -> 010

* (ash 3 2)
12
;; Explanation:
;;   11 -> 1100

* (ash -5 -2)
-2
;; Explanation
;;   11011 -> 110
~~~

Please see the [CLHS page][logand-functions] for a more detailed explanation
or other bit-wise functions.


[numbers]: https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node16.html#SECTION00610000000000000000
[numbers-characters-strings]: http://www.gigamonkeys.com/book/numbers-characters-and-strings.html
[isqrt]: http://clhs.lisp.se/Body/f_sqrt_.htm
[gcd]: http://clhs.lisp.se/Body/f_gcd.htm
[lcm]: http://clhs.lisp.se/Body/f_lcm.htm#lcm
[ratio]: http://clhs.lisp.se/Body/t_ratio.htm
[rational]: http://clhs.lisp.se/Body/t_ration.htm#rational
[integer]: http://clhs.lisp.se/Body/t_intege.htm#integer
[book-cl-2.1.3]: https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node19.html
[article-floating-point-arithmetic]: https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
[float-constants]: http://clhs.lisp.se/Body/v_short_.htm
[read-default-float-format]: http://clhs.lisp.se/Body/v_rd_def.htm
[SBCL]: http://www.sbcl.org/
[CCL]: https://ccl.clozure.com/
[computable-reals]: http://quickdocs.org/computable-reals/
[complex]: http://clhs.lisp.se/Body/f_comp_2.htm#complex
[realpart-and-imaginary]: http://clhs.lisp.se/Body/f_realpa.htm
[parse-integer]: http://clhs.lisp.se/Body/f_parse_.htm
[parse-float]: https://github.com/soemraws/parse-float/blob/master/parse-float.lisp
[strings]: https://lispcookbook.github.io/cl-cookbook/strings.html#converting-a-string-to-a-number
[book-cl-12.6]: https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node130.html
[rational-and-rationalize]: http://clhs.lisp.se/Body/f_ration.htm
[ceiling-functions]: http://www.lispworks.com/documentation/HyperSpec/Body/f_floorc.htm
[book-cl-12.3]: https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node124.html
[single-float-epsilon]: http://clhs.lisp.se/Body/v_short_.htm
[mapping]: https://lispcookbook.github.io/cl-cookbook/data-structures.html#mapping-map-mapcar-remove-if-not
[arrays]: https://lispcookbook.github.io/cl-cookbook/arrays.html
[clazy]: https://common-lisp.net/project/clazy/
[series]: https://github.com/tokenrove/series/wiki/Documentation
[lazy-seq]: https://github.com/fredokun/lisp-lazy-seq
[folio2]: https://github.com/mikelevins/folio2
[gist-tormaroe]: https://gist.github.com/tormaroe/90ddd9dc7cc191040be4
[random]: http://clhs.lisp.se/Body/f_random.htm#random
[mersenne-twister]: https://en.wikipedia.org/wiki/Mersenne_Twister
[sbcl-7.13]: http://www.sbcl.org/manual/#Random-Number-Generation
[random-state]: http://clhs.lisp.se/Body/v_rnd_st.htm#STrandom-stateST
[make-random-state]: http://clhs.lisp.se/Body/f_mk_rnd.htm
[random-state]: http://quickdocs.org/random-state/
[logand-functions]: http://www.lispworks.com/documentation/HyperSpec/Body/f_logand.htm
[twos-complements]: https://en.wikipedia.org/wiki/Twos_complement

---
title: Loop, iteration, mapping
---

<!-- needs some text before the first heading -->

# Introduction: loop, iterate, for, mapcar

**[loop](http://www.lispworks.com/documentation/lw51/CLHS/Body/m_loop.htm)**
is the built-in macro for iteration.

Its simplest form is `(loop (print "hello"))`: this will print forever.

A simple iteration over a list is:

~~~lisp
(loop for x in '(1 2 3)
  do (print x))
~~~

If you want to return a list, use `collect`:

~~~lisp
(loop for x in '(1 2 3)
  collect (* x 10))
;; (10 20 30)
~~~

The Loop macro is different than most Lisp expressions in having a complex
internal domain specific language that doesn't use s-expressions.
So you need to read Loop expressions with half of your brain in Lisp mode, and
the other half in Loop mode. You love it or you hate it.

Think of Loop expressions as having four parts: expressions that set up
variables that will be iterated, expressions that conditionally terminate the
iteration, expressions that do something on each iteration, and expressions that
do something right before the Loop exits.  In addition, Loop expressions can
return a value.  It is very rare to use all of these parts in a given Loop
expression, but you can combine them in many ways.

**[iterate](https://common-lisp.net/project/iterate/doc/index.html)** is a
popular iteration macro that aims at being simpler, "lispier" and more
predictable than `loop`, besides being extensible. However it isn't built-in,
so you have to import it:

    (ql:quickload :iterate)
    (use-package :iterate)

Iterate looks like this:

~~~lisp
(iter (for i from 1 to 5)
    (collect (* i i)))
~~~

(if you use loop and iterate in the same package, you might run into name conflicts)

Iterate also comes with `display-iterate-clauses` that can be quite handy:
~~~lisp
(display-iterate-clauses '(for))
;; FOR PREVIOUS &OPTIONAL INITIALLY BACK     Previous value of a variable
;; FOR FIRST THEN            Set var on first, and then on subsequent iterations
;; ...
~~~

Much of the examples on this page that are valid for loop are also valid for iterate,
with minor modifications.

**[for](https://github.com/Shinmera/for/)** is an extensible iteration
macro that is often shorter than loop, that "unlike loop is extensible
and sensible, and unlike iterate does not require code-walking and is
easier to extend".

It has the other advantage of having one construct that works for all
data structures (lists, vectors, hash-tables…): in doubt, just use
`for… over`:

~~~lisp
(for:for ((x over <your data structure>))
   (print …))
~~~

You also have to quickload it:

    (ql:quickload :for)


We'll also give examples with **`mapcar`** and `map`, and eventually
with their friends `mapcon`, `mapcan`, `maplist`, `mapc` and `mapl`
which E. Weitz categorizes very well in his "Common Lisp Recipes",
chap. 7. The one you are certainly accustomed to from other languages is
`mapcar`: it takes a function, one or more lists as arguments,
applies the function on each *element* of the lists one by one and
returns a list of result.

~~~lisp
(mapcar (lambda (it) (+ it 10)) '(1 2 3))
(11 12 13)
~~~

`map` is generic, it accepts list and vectors as arguments, and
expects the type for its result as first argument:

~~~lisp
(map 'vector (lambda (it) (+ it 10)) '(1 2 3))
;; #(11 12 13)
(map 'list (lambda (it) (+ it 10)) #(1 2 3))
;; (11 12 13)
(map 'string (lambda (it) (code-char it)) '#(97 98 99))
;; "abc"
~~~

The other constructs have their advantages in some situations ;) They
either process the *tails* of lists, or *concatenate* the return
values, or don't return anything. We'll see some of them.

If you like `mapcar`, use it a lot, and would like a quicker and
shorter way to write lambdas, then you might like one of those
[lambda shorthand libraries](https://github.com/CodyReichert/awesome-cl#lambda-shorthands).

Here is an example with [cl-punch](https://github.com/windymelt/cl-punch/):

~~~lisp
(mapcar ^(* _ 10) '(1 2 3))
;; (10 20 30)
~~~

and voilà :) We won't use this more in this recipe, but feel free to do.

Last but not least, you might like
**[series](https://github.com/tokenrove/series/wiki/Documentation)**,
a library that describes itself as combining aspects of sequences,
streams, and loops. Series expressions look like operations on
sequences (= functional programming), but can achieve the same high level of efficiency as a
loop. Series first appeared in "Common Lisp the Language", in the
appendix A (it nearly became part of the language). Series looks like
this:

~~~lisp
(collect
  (mapping ((x (scan-range :from 1 :upto 5)))
    (* x x)))
;; (1 4 9 16 25)
~~~

# Recipes

## Looping forever, return

~~~lisp
(loop
    (print "hello"))
~~~

`return` can return a result:

~~~lisp
(loop for i in '(1 2 3)
     when (> i 1)
     return i)
2
~~~


## Looping a fixed number of times

### dotimes

~~~lisp
(dotimes (n 10)
  (print n))
~~~

Here `dotimes` returns `nil`. The return value is evaluated at the end of the loop.

You can use `return` inside of it:

~~~lisp
(dotimes (i 10)
   (if (> i 3)
       (return)
       (print i)))
~~~


### loop… repeat

~~~lisp
(loop repeat 10
  do (format t "Hello!~%"))
~~~

This prints 10 times "hello" and returns `nil`.

~~~lisp
(loop repeat 10 collect (random 10))
;; (5 1 3 5 4 0 7 4 9 1)
~~~

with `collect`, this returns a list.

### Series

~~~lisp
(iterate ((n (scan-range :below 10)))
  (print n))
~~~

## Iterate's for loop

For lists and vectors:

~~~lisp
(iter (for item in '(1 2 3))
  (print item))
(iter (for i in-vector #(1 2 3))
  (print i))
~~~

Looping over a hash-table is also straightforward:
~~~lisp
(let ((h (let ((h (make-hash-table)))
           (setf (gethash 'a h) 1)
           (setf (gethash 'b h) 2)
           h)))
  (iter (for (k v) in-hashtable h)
    (print k)))
;; b
;; a
~~~

In fact, take a look [here](https://common-lisp.net/project/iterate/doc/Sequence-Iteration.html),
or `(display-iterate-clauses '(for))` to know about iterating over

- symbols in-package
- forms - or lines, or whatever-you-wish - in-file, or in-stream
- elements in-sequence - sequences can be vectors or lists

## Looping over a list

### dolist

~~~lisp
(dolist (item '(1 2 3))
  (print item))
~~~

`dolist` returns `nil`.

### loop

with `in`, no surprises:

~~~lisp
(loop for x in '(a b c)
      do (print x))
;; A
;; B
;; C
;; NIL
~~~

~~~lisp
(loop for x in '(a b c)
      collect x)
;; (A B C)
~~~

With `on`, we loop over the cdr of the list:

~~~lisp
(loop for i on '(1 2 3) do (print i))
;; (1 2 3)
;; (2 3)
;; (3)
~~~


### mapcar

~~~lisp
(mapcar (lambda (x)
             (print (* x 10)))
         '(1 2 3))
10
20
30
(10 20 30)
~~~

`mapcar` returns the results of the lambda function as a list.

### Series
~~~lisp
(iterate ((item (scan '(1 2 3))))
  (print item))
~~~

`scan-sublists` is the equivalent of `loop for ... on`:

~~~lisp
(iterate ((i (scan-sublists '(1 2 3))))
  (print i))
~~~

## Looping over a vector

### loop: `across`

~~~lisp
(loop for i across #(1 2 3) do (print i))
~~~

### Series

~~~lisp
(iterate ((i (scan #(1 2 3))))
  (print i))
~~~

## Looping over a hash-table

We create a hash-table:

~~~lisp
(setf h (make-hash-table))
(setf (gethash 'a h) 1)
(setf (gethash 'b h) 2)
~~~

### loop

Looping over keys:

~~~lisp
(loop for k being the hash-key of h do (print k))
;; b
;; a
~~~

same with `hash-value`.

Looping over key-values pairs:

~~~lisp
(loop for k
    being the hash-key
    using (hash-value v) of h
    do (format t "~a ~a~%" k v))
b 2
a 1
~~~

### for

the same with `for`:

~~~lisp
(for:for ((it over h))
    (print it))
(A 1)
(B 2)
NIL
~~~


### maphash

The lambda function of `maphash` takes two arguments: the key and the
value:

~~~lisp
(maphash (lambda (key val)
             (format t "key: ~a val:~a~&" key val))
          h)
;; key: A val:1
;; key: B val:2
;; NIL
~~~

See also [with-hash-table-iterator](http://www.lispworks.com/documentation/HyperSpec/Body/m_w_hash.htm).

### Series
~~~lisp
(iterate (((k v) (scan-hash h)))
  (format t "~&~a ~a~%" k v))
~~~

## Looping over two lists in parallel

### loop

~~~lisp
(loop for x in '(a b c)
      for y in '(1 2 3)
      collect (list x y))
;; ((A 1) (B 2) (C 3))
~~~

### mapcar
~~~lisp
(mapcar (lambda (x y)
           (list x y))
        '(a b c)
        '(1 2 3))
;; ((A 1) (B 2) (C 3))
~~~

or simply:

~~~lisp
(mapcar #'list
        '(a b c)
        '(1 2 3))
;; ((A 1) (B 2) (C 3))
~~~

Return a flat list:

~~~lisp
(mapcan (lambda (x y)
                   (list x y))
                 '(a b c)
                 '(1 2 3))
;; (A 1 B 2 C 3)
~~~

### Series
~~~lisp
(collect
  (#Mlist (scan '(a b c))
          (scan '(1 2 3))))
~~~

A more efficient way, when the lists are known to be of equal length:

~~~lisp
(collect
  (mapping (((x y) (scan-multiple 'list
                                  '(a b c)
                                  '(1 2 3))))
    (list x y)))
~~~
Return a flat list:
~~~lisp
(collect-append ; or collect-nconc
 (mapping (((x y) (scan-multiple 'list
                                 '(a b c)
                                 '(1 2 3))))
   (list x y)))
~~~


## Nested loops
### loop
~~~lisp
(loop for x from 1 to 3
      collect (loop for y from 1 to x
		    collect y))
;; ((1) (1 2) (1 2 3))
~~~

### iterate
~~~lisp
(iter outer
   (for i below 2)
   (iter (for j below 3)
      (in outer (collect (list i j)))))
;; ((0 0) (0 1) (0 2) (1 0) (1 1) (1 2))
~~~

### Series
~~~lisp
(collect
  (mapping ((x (scan-range :from 1 :upto 3)))
    (collect (scan-range :from 1 :upto x))))
~~~


## Computing an intermediate value

with `=`:

~~~lisp
(loop for x from 1 to 3
      for y = (* x 10)
      collect y)
;; (10 20 30)
~~~

## Loop with a counter
### loop
Iterate through a list, and have a counter iterate in parallel. The length of
the list determines when the iteration ends. Two sets of actions are defined,
one of which is executed conditionally.

~~~lisp
* (loop for x in '(a b c d e)
      for y from 1

      when (> y 1)
      do (format t ", ")

      do (format t "~A" x)
      )

A, B, C, D, E
NIL
~~~

We could also write the preceding loop using the IF construct.

~~~lisp
* (loop for x in '(a b c d e)
      for y from 1

      if (> y 1)
      do (format t ", ~A" x)
      else do (format t "~A" x)
      )

A, B, C, D, E
NIL
~~~

### Series

By iterating on multiple series in parallel, and using an infinite
range, we can make a counter.

~~~lisp
(iterate ((x (scan '(a b c d e)))
          (y (scan-range :from 1)))
  (when (> y 1) (format t ", "))
  (format t "~A" x))
~~~

## Ascending, descending order, limits
### loop

`from… to…`:

~~~lisp
(loop for i from 0 to 10
      do (print i))
;; 0 1 2 3 4 5 6 7 8 9 10
~~~

`from… below…`: this stops at 9:

~~~lisp
(loop for i from 0 below 10
      do (print i))
~~~

Similarly, use `from 10 downto 0` (10…0) and `from 10 above 0` (10…1).

### Series

`:from ... :upto`, including the upper limit:
~~~lisp
(iterate ((i (scan-range :from 0 :upto 10)))
  (print i))
~~~

`:from ... :below`, excluding the upper limit:
~~~lisp
(iterate ((i (scan-range :from 0 :below 10)))
  (print i))
~~~


## Steps
### loop

with `by`:

~~~lisp
(loop for i from 1 to 10 by 2
      do (print i))
~~~

if you use `by (1+ (random 3))`, the random is evaluated only once, as
if it was in a closure:

~~~lisp
(let ((step (random 3)))
   (loop for i from 1 to 10 by (+ 1 step)
      do (print i))
~~~

### Series
with `:by`
~~~lisp
(iterate ((i (scan-range :from 1 :upto 10 :by 2)))
  (print i))
~~~


## Loop and conditionals
### loop

with `if`, `else` and `finally`:

~~~lisp
;; https://riptutorial.com/common-lisp/example/11095/conditionally-executing-loop-clauses
(loop repeat 10
      for x = (random 100)
      if (evenp x)
        collect x into evens
      else
        collect x into odds
      finally (return (values evens odds)))
~~~

```
(42 82 24 92 92)
(55 89 59 13 49)
```

Combining multiple clauses in an if body requires special syntax (`and
do`, `and count`):

~~~lisp
 (loop repeat 10
       for x = (random 100)
       if (evenp x)
          collect x into evens
          and do (format t "~a is even!~%" x)
       else
          collect x into odds
          and count t into n-odds
       finally (return (values evens odds n-odds)))
~~~
```
46 is even!
8 is even!
76 is even!
58 is even!
0 is even!
(46 8 76 58 0)
(7 45 43 15 69)
5
```

### iterate

Translating (or even writing!) the above example using iterate is straight-forward:

~~~lisp
(iter (repeat 10)
   (for x = (random 100))
   (if (evenp x)
       (progn
         (collect x into evens)
         (format t "~a is even!~%" x))
       (progn
         (collect x into odds)
         (count t into n-odds)))
   (finally (return (values evens odds n-odds))))
~~~

### Series

The preceding loop would be done a bit differently in Series. `split`
sorts one series into multiple according to provided boolean series.

~~~lisp
(let* ((number (#M(lambda (n) (random 100))
                  (scan-range :below 10)))
       (parity (#Mevenp number)))
  (iterate ((n number) (p parity))
    (when p (format t "~a is even!~%" n)))
  (multiple-value-bind (evens odds) (split number parity)
    (values (collect evens)
            (collect odds)
            (collect-length odds))))
~~~

Note that although `iterate` and the three `collect` expressions are
written sequentially, only one iteration is performed, the same as the
example with loop.


## Terminate the loop with a test (until, while)
### loop

~~~lisp
(loop for x in '(1 2 3 4 5)
	until (> x 3)
	collect x)
;; (1 2 3)
~~~

the same, with `while`:

~~~lisp
(loop for x in '(1 2 3 4 5)
	while (< x 4)
	collect x)
~~~

### Series

We truncate the series with `until-if`, then collect from its result.

~~~lisp
(collect
  (until-if (lambda (i) (> i 3))
            (scan '(1 2 3 4 5))))
~~~

## Loop, print and return a result
### loop

`do` and `collect` can be combined in one expression

~~~lisp
(loop for x in '(1 2 3 4 5)
	while (< x 4)
        do (format t "x is ~a~&" x)
	collect x)
x is 1
x is 2
x is 3
(1 2 3)
~~~

### Series
By mapping we can perform a side effect and also collect items
~~~lisp
(collect
  (mapping ((x (until-if (complement (lambda (x) (< x 4)))
                         (scan '(1 2 3 4 5)))))
    (format t "x is ~a~&" x)
    x))
~~~


## Named loops and early exit
### loop

The special `loop named` foo syntax allows you to create a loop that
you can exit early from. The exit is performed using `return-from`,
and can be used from within nested loops.


~~~lisp
;; useless example
(loop named loop-1
    for x from 0 to 10 by 2
    do (loop for y from 0 to 100 by (1+ (random 3))
            when (< x y)
            do (return-from loop-1 (values x y))))
0
2
~~~

### Loop shorthands for when/return

Several actions provide shorthands for combinations of when/return:

~~~lisp
* (loop for x in '(foo 2)
      thereis (numberp x))
T
~~~

~~~lisp
* (loop for x in '(foo 2)
      never (numberp x))
NIL
~~~

~~~lisp
* (loop for x in '(foo 2)
      always (numberp x))
NIL
~~~

### Series

A block is manually created and returned from.

~~~lisp
(block loop-1
  (iterate ((x (scan-range :from 0 :upto 10 :by 2)))
    (iterate ((y (scan-range :from 0 :upto 100 :by (1+ (random 3)))))
      (when (< x y)
        (return-from loop-1 (values x y))))))
~~~

## Count
### loop
~~~lisp
(loop for i from 1 to 3 count (oddp i))
;; 2
~~~

### Series
~~~lisp
(collect-length (choose-if #'oddp (scan-range :from 1 :upto 3)))
~~~

## Summation
### loop

~~~lisp
(loop for i from 1 to 3 sum (* i i))
;; 14
~~~

### Series

~~~lisp
(collect-sum (#M(lambda (i) (* i i))
                (scan-range :from 1 :upto 3)))
~~~

## max, min
### loop

~~~lisp
(loop for i from 1 to 3 maximize (mod i 3))
;; 2
~~~

and `minimize`.

### Series
~~~lisp
(collect-max (#M(lambda (i) (mod i 3))
                (scan-range :from 1 :upto 3)))
~~~
and `collect-min`.

## Destructuring, aka pattern matching against the list or dotted pairs
### loop

~~~lisp
(loop for (a b) in '((x 1) (y 2) (z 3))
      collect (list b a) )
;; ((1 X) (2 Y) (3 Z))
~~~

~~~lisp
(loop for (x . y) in '((1 . a) (2 . b) (3 . c)) collect y)
;; (A B C)
~~~

Use `nil` to ignore a term:

~~~lisp
(loop for (a nil) in '((x 1) (y 2) (z 3))
      collect a )
;; (X Y Z)
~~~

### Series
In general, with `destructuring-bind`:
~~~lisp
(collect
  (mapping ((l (scan '((x 1) (y 2) (z 3)))))
    (destructuring-bind (a b) l
      (list b a))))
~~~

But for alists, `scan-alist` is provided:

~~~lisp
(collect
  (mapping (((a b) (scan-alist '((1 . a) (2 . b) (3 . c)))))
    b))
~~~

# Custom series scanners

If we often scan the same type of object, we can write our own scanner
 for it: the iteration itself can be factored out. Taking the example
 above, of scanning a list of two-element lists, we'll write a scanner
 that returns a series of the first elements, and a series of the
 second.

~~~lisp
(defun scan-listlist (listlist)
  (declare (optimizable-series-function 2))
  (map-fn '(values t t)
          (lambda (l)
            (destructuring-bind (a b) l
              (values a b)))
          (scan listlist)))

(collect
  (mapping (((a b) (scan-listlist '((x 1) (y 2) (z 3)))))
    (list b a)))
~~~

# Shorter series expressions

Consider this series expression:

~~~lisp

(collect-sum (mapping ((i (scan-range :length 5)))
                    (* i 2)))
~~~

It's a bit longer than it needs to be—the `mapping` form's only
purpose is to bind the variable `i`, and `i` is used in only one
place. Series has a "hidden feature" which allows us to simplify this
expression to the following:

~~~lisp
(collect-sum (* 2 (scan-range :length 5)))
~~~

This is called implicit mapping, and can be enabled in the call to
`series::install`:

~~~lisp
(series::install :implicit-map t)
~~~

When using implicit mapping, the `#M` reader macro demonstrated above
becomes redundant.

# Loop gotchas

- the keyword `it`, often used in functional constructs, can be
  recognized as a loop keyword. Don't use it inside a loop.


# Appendix: list of loop keywords

**Name Clause**

```
named
```

**Variable Clauses**

```
initially finally for as with
```

**Main Clauses**

```
do collect collecting append
appending nconc nconcing into count
counting sum summing maximize return
maximizing minimize minimizing doing
thereis always never if when
unless repeat while until
```

These don’t introduce clauses:

```
= and it else end from upfrom
above below to upto downto downfrom
in on then across being each the hash-key
hash-keys of using hash-value hash-values
symbol symbols present-symbol
present-symbols external-symbol
external-symbols fixnum float t nil of-type
```

But note that it’s the parsing that determines what is a keyword. For example in:

~~~lisp
(loop for key in hash-values)
~~~

Only `for` and `in` are keywords.


©Dan Robertson on [Stack Overflow](https://stackoverflow.com/questions/52236803/list-of-loop-keywords).

# Credit and references

## Loop

* [Tutorial for the Common Lisp Loop Macro](http://www.ai.sri.com/~pkarp/loop.html) by Peter D. Karp
* [http://www.unixuser.org/~euske/doc/cl/loop.html](http://www.unixuser.org/~euske/doc/cl/loop.html)
* [riptutorial.com](https://riptutorial.com/common-lisp/)
*

## Iterate

* [The Iterate Manual](https://common-lisp.net/project/iterate/doc/index.html) -
* [iterate](https://digikar99.github.io/cl-iterate-docs/) - highlights at a glance and examples
* [Loop v Iterate - SabraOnTheHill](https://sites.google.com/site/sabraonthehill/loop-v-iter)

## Series

* [SERIES for Common Lisp - Richard C. Waters](http://series.sourceforge.net/)

## Others

* See also: [more functional constructs](https://lisp-journey.gitlab.io/blog/snippets-functional-style-more/) (do-repeat, take,…)

---
title: Multidimensional arrays
---

Common Lisp has native support for multidimensional arrays, with some
special treatment for 1-D arrays, called `vectors`. Arrays can be
*generalised* and contain any type (`element-type t`), or they
can be *specialised* to contain specific types such as `single-float`
or `integer`. A good place to start is
[Practical Common Lisp Chapter 11, Collections](http://www.gigamonkeys.com/book/collections.html) by
Peter Seibel.

A quick reference to some common operations on arrays is given in the
section on [Arrays and vectors](data-structures.html).

Some libraries available on [Quicklisp](https://www.quicklisp.org/beta/) for manipulating arrays:

- [array-operations](https://github.com/tpapp/array-operations) defines
  functions `generate`, `permute`, `displace`, `flatten`, `split`,
  `combine`, `reshape`. It also defines `each`, for element-wise
  operations. This library is not maintained by the original author,
  but there is an [actively maintained fork](https://github.com/bendudson/array-operations).
- [cmu-infix](https://github.com/rigetticomputing/cmu-infix) includes
  array indexing syntax for multidimensional arrays.
- [lla](https://github.com/tpapp/lla) is a library for linear algebra, calling BLAS and LAPACK
  libraries. It differs from most CL linear algebra packages in using
  intuitive function names, and can operate on native arrays as well as
  CLOS objects.

This page covers what can be done with the built-in multidimensional
arrays, but there are limitations. In particular:

* Interoperability with foreign language arrays, for example when
  calling libraries such as BLAS, LAPACK or GSL.
* Extending arithmetic and other mathematical operators to handle
  arrays, for example so that `(+ a b)` works
  when `a` and/or `b` are arrays.

Both of these problems can be solved by using CLOS to define an
extended array class, with native arrays as a special case.
Some libraries available through
[quicklisp](https://www.quicklisp.org/beta/) which take this approach
are:

* [matlisp](https://github.com/bharath1097/matlisp/), some of which is
  described in sections below.
* [MGL-MAT](https://github.com/melisgl/mgl-mat), which has a manual
  and provides bindings to BLAS and CUDA. This is used in a machine
  learning library [MGL](https://github.com/melisgl/mgl).
* [cl-ana](https://github.com/ghollisjr/cl-ana/wiki), a data analysis
  package with a manual, which includes operations on arrays.
* [Antik](https://www.common-lisp.net/project/antik/), used in
  [GSLL](https://common-lisp.net/project/gsll/), a binding to the GNU
  Scientific Library.

A relatively new but actively developed package is
[MAGICL](https://github.com/rigetticomputing/magicl), which provides
wrappers around BLAS and LAPACK libraries. At the time of writing this
package is not on Quicklisp, and only works under SBCL and CCL. It
seems to be particularly focused on complex arrays, but not
exclusively.
To install, clone the repository in your quicklisp `local-projects`
directory e.g. under Linux/Unix:

~~~bash
$ cd ~/quicklisp/local-projects
$ git clone https://github.com/rigetticomputing/magicl.git
~~~

Instructions for installing dependencies (BLAS, LAPACK and Expokit)
are given on the [github web pages](https://github.com/rigetticomputing/magicl).
Low-level routines wrap foreign functions, so have the Fortran names
e.g `magicl.lapack-cffi::%zgetrf`. Higher-level interfaces to some of
these functions also exist, see the
[source code](https://github.com/rigetti/magicl/blob/master/src/high-level/high-level.lisp).

Taking this further, domain specific languages have been built on Common
Lisp, which can be used for numerical calculations with arrays.
At the time of writing the most widely used and supported of these are:

* [Maxima](http://maxima.sourceforge.net/documentation.html)
* [Axiom](https://github.com/daly/axiom)


[CLASP](https://github.com/drmeister/clasp) is a project
which aims to ease interoperability of Common Lisp with other
languages (particularly C++), by using [LLVM](http://llvm.org/).
One of the main applications of this project is to numerical/scientific
computing.

# Creating

The function [CLHS: make-array](http://clhs.lisp.se/Body/f_mk_ar.htm)
can create arrays filled with a single value

~~~lisp
* (defparameter *my-array* (make-array '(3 2) :initial-element 1.0))
*MY-ARRAY*
* *my-array*
#2A((1.0 1.0) (1.0 1.0) (1.0 1.0))
~~~

More complicated array values can be generated by first making an
array, and then iterating over the elements to fill in the values (see
section below on element access).

The [array-operations](https://github.com/tpapp/array-operations)
library provides `generate`, a convenient
function for creating arrays which wraps this iteration.

~~~lisp
* (ql:quickload :array-operations)
To load "array-operations":
  Load 1 ASDF system:
    array-operations
; Loading "array-operations"

(:ARRAY-OPERATIONS)

* (aops:generate #'identity 7 :position)
#(0 1 2 3 4 5 6)
~~~

Note that the nickname for `array-operations` is `aops`. The
`generate` function can also iterate over the array subscripts by
passing the key `:subscripts`. See the
[github repository](https://github.com/tpapp/array-operations) for
more examples.

## Random numbers

To create an 3x3 array containing random numbers drawn from a uniform
distribution, `generate` can be used to call the CL
[random](http://clhs.lisp.se/Body/f_random.htm) function:

~~~lisp
* (aops:generate (lambda () (random 1.0)) '(3 3))
#2A((0.99292254 0.929777 0.93538976)
    (0.31522608 0.45167792 0.9411855)
    (0.96221936 0.9143338 0.21972346))
~~~

An array of Gaussian (normal) random numbers with mean of zero and
standard deviation of one, using the
[alexandria](https://common-lisp.net/project/alexandria/) package:

~~~lisp
* (ql:quickload :alexandria)
To load "alexandria":
  Load 1 ASDF system:
    alexandria
; Loading "alexandria"

(:ALEXANDRIA)

* (aops:generate #'alexandria:gaussian-random 4)
#(0.5522547885338768d0 -1.2564808468164517d0 0.9488161476129733d0
  -0.10372852118266523d0)
~~~

Note that this is not particularly efficient: It requires a function
call for each element, and although `gaussian-random` returns
two random numbers, only one of them is used.

For more efficient implementations, and a wider range of probability
distributions, there are packages available on Quicklisp. See
[CLiki](https://www.cliki.net/statistics) for a list.

# Accessing elements

To access the individual elements of an array there are the [aref](http://clhs.lisp.se/Body/f_aref.htm)
and [row-major-aref](http://clhs.lisp.se/Body/f_row_ma.htm#row-major-aref) functions.

The [aref](http://clhs.lisp.se/Body/f_aref.htm) function takes the same number of index arguments as the
array has dimensions. Indexing is from 0 and row-major as in C, but
not Fortran.
~~~lisp
* (defparameter *a* #(1 2 3 4))
*A*
* (aref *a* 0)
1
* (aref *a* 3)
4
* (defparameter *b* #2A((1 2 3) (4 5 6)))
*B*
* (aref *b* 1 0)
4
* (aref *b* 0 2)
3
~~~

The range of these indices can be found using
[array-dimensions](http://clhs.lisp.se/Body/f_ar_d_1.htm):

~~~
* (array-dimensions *a*)
(4)
* (array-dimensions *b*)
(2 3)
~~~

or the rank of the array can be found, and then the size of each
dimension queried:
~~~lisp
* (array-rank *a*)
1
* (array-dimension *a* 0)
4
* (array-rank *b*)
2
* (array-dimension *b* 0)
2
* (array-dimension *b* 1)
3
~~~

To loop over an array nested loops can be used, such as
~~~lisp
* (defparameter a #2A((1 2 3) (4 5 6)))
A
* (destructuring-bind (n m) (array-dimensions a)
    (loop for i from 0 below n do
      (loop for j from 0 below m do
        (format t "a[~a ~a] = ~a~%" i j (aref a i j)))))

a[0 0] = 1
a[0 1] = 2
a[0 2] = 3
a[1 0] = 4
a[1 1] = 5
a[1 2] = 6
NIL
~~~

A utility macro which does this for multiple dimensions is `nested-loop`:

~~~lisp
(defmacro nested-loop (syms dimensions &body body)
  "Iterates over a multidimensional range of indices.

   SYMS must be a list of symbols, with the first symbol
   corresponding to the outermost loop.

   DIMENSIONS will be evaluated, and must be a list of
   dimension sizes, of the same length as SYMS.

   Example:
    (nested-loop (i j) '(10 20) (format t '~a ~a~%' i j))

  "
  (unless syms (return-from nested-loop `(progn ,@body))) ; No symbols

  ;; Generate gensyms for dimension sizes
  (let* ((rank (length syms))
         (syms-rev (reverse syms)) ; Reverse, since starting with innermost
         (dims-rev (loop for i from 0 below rank collecting (gensym))) ; innermost dimension first
         (result `(progn ,@body))) ; Start with innermost expression
    ;; Wrap previous result inside a loop for each dimension
    (loop for sym in syms-rev for dim in dims-rev do
         (unless (symbolp sym) (error "~S is not a symbol. First argument to nested-loop must be a list of symbols" sym))
         (setf result
               `(loop for ,sym from 0 below ,dim do
                     ,result)))
    ;; Add checking of rank and dimension types, and get dimensions into gensym list
    (let ((dims (gensym)))
      `(let ((,dims ,dimensions))
         (unless (= (length ,dims) ,rank) (error "Incorrect number of dimensions: Expected ~a but got ~a" ,rank (length ,dims)))
         (dolist (dim ,dims)
           (unless (integerp dim) (error "Dimensions must be integers: ~S" dim)))
         (destructuring-bind ,(reverse dims-rev) ,dims ; Dimensions reversed so that innermost is last
           ,result)))))

~~~

so that the contents of a 2D array can be printed using
~~~lisp
* (defparameter a #2A((1 2 3) (4 5 6)))
A
* (nested-loop (i j) (array-dimensions a)
      (format t "a[~a ~a] = ~a~%" i j (aref a i j)))

a[0 0] = 1
a[0 1] = 2
a[0 2] = 3
a[1 0] = 4
a[1 1] = 5
a[1 2] = 6
NIL
~~~

[Note: This macro is available in [this fork](https://github.com/bendudson/array-operations) of array-operations, but
not Quicklisp]

## Row major indexing

In some cases, particularly element-wise operations, the number of
dimensions does not matter. To write code which is independent of the
number of dimensions, array element access can be done using a
single flattened index via
[row-major-aref](http://clhs.lisp.se/Body/f_row_ma.htm#row-major-aref).
The array size is given by [array-total-size](http://clhs.lisp.se/Body/f_ar_tot.htm), with the flattened
index starting at 0.

~~~lisp
* (defparameter a #2A((1 2 3) (4 5 6)))
A
* (array-total-size a)
6
* (loop for i from 0 below (array-total-size a) do
     (setf (row-major-aref a i) (+ 2.0 (row-major-aref a i))))
NIL
* a
#2A((3.0 4.0 5.0) (6.0 7.0 8.0))
~~~

## Infix syntax

The [cmu-infix](https://github.com/rigetticomputing/cmu-infix) library
provides some different syntax which can make mathematical expressions
easier to read:

~~~lisp
* (ql:quickload :cmu-infix)
To load "cmu-infix":
  Load 1 ASDF system:
    cmu-infix
; Loading "cmu-infix"

(:CMU-INFIX)

* (named-readtables:in-readtable cmu-infix:syntax)
(("COMMON-LISP-USER" . #<NAMED-READTABLE CMU-INFIX:SYNTAX {10030158B3}>)
 ...)

* (defparameter arr (make-array '(3 2) :initial-element 1.0))
ARR

* #i(arr[0 1] = 2.0)
2.0

* arr
#2A((1.0 2.0) (1.0 1.0) (1.0 1.0))
~~~

A matrix-matrix multiply operation can be implemented as:
~~~lisp
(let ((A #2A((1 2) (3 4)))
      (B #2A((5 6) (7 8)))
      (result (make-array '(2 2) :initial-element 0.0)))

     (loop for i from 0 to 1 do
           (loop for j from 0 to 1 do
                 (loop for k from 0 to 1 do
                       #i(result[i j] += A[i k] * B[k j]))))
      result)
~~~
See the section below on linear algebra, for alternative
matrix-multiply implementations.

# Element-wise operations

To multiply two arrays of numbers of the same size, pass a function
to `each` in the [array-operations](https://github.com/tpapp/array-operations) library:

~~~lisp
* (aops:each #'* #(1 2 3) #(2 3 4))
#(2 6 12)
~~~

For improved efficiency there is the `aops:each*` function, which
takes a type as first argument to specialise the result array.

To add a constant to all elements of an array:

~~~lisp
* (defparameter *a* #(1 2 3 4))
*A*
* (aops:each (lambda (it) (+ 42 it)) *a*)
#(43 44 45 46)
* *a*
#(1 2 3 4)
~~~

Note that `each` is not destructive, but makes a new array.
All arguments to `each` must be arrays of the same size,
so `(aops:each #'+ 42 *a*)` is not valid.

## Vectorising expressions

An alternative approach to the `each` function above, is to use a
macro to iterate over all elements of an array:

~~~lisp
(defmacro vectorize (variables &body body)
  ;; Check that variables is a list of only symbols
  (dolist (var variables)
    (if (not (symbolp var))
        (error "~S is not a symbol" var)))

    ;; Get the size of the first variable, and create a new array
    ;; of the same type for the result
    `(let ((size (array-total-size ,(first variables)))  ; Total array size (same for all variables)
           (result (make-array (array-dimensions ,(first variables)) ; Returned array
                               :element-type (array-element-type ,(first variables)))))
       ;; Check that all variables have the same sizeo
       ,@(mapcar (lambda (var) `(if (not (equal (array-dimensions ,(first variables))
                                                (array-dimensions ,var)))
                                    (error "~S and ~S have different dimensions" ',(first variables) ',var)))
              (rest variables))

       (dotimes (indx size)
         ;; Locally redefine variables to be scalars at a given index
         (let ,(mapcar (lambda (var) (list var `(row-major-aref ,var indx))) variables)
           ;; User-supplied function body now evaluated for each index in turn
           (setf (row-major-aref result indx) (progn ,@body))))
       result))
~~~

[Note: Expanded versions of this macro are available in [this
fork](https://github.com/bendudson/array-operations) of array-operations, but
not Quicklisp]

This can be used as:
~~~lisp
* (defparameter *a* #(1 2 3 4))
*A*
* (vectorize (*a*) (* 2 *a*))
#(2 4 6 8)
~~~

Inside the body of the expression (second form in `vectorize`
expression) the symbol `*a*` is bound to a single element.
This means that the built-in mathematical functions can be used:

~~~lisp
* (defparameter a #(1 2 3 4))
A
* (defparameter b #(2 3 4 5))
B
* (vectorize (a b) (* a (sin b)))
#(0.9092974 0.28224 -2.2704074 -3.8356972)
~~~

and combined with `cmu-infix`
~~~lisp
* (vectorize (a b) #i(a * sin(b)) )
#(0.9092974 0.28224 -2.2704074 -3.8356972)
~~~

## Calling BLAS

Several packages provide wrappers around BLAS, for fast matrix manipulation.

The [lla](https://github.com/tpapp/lla) package in quicklisp includes
calls to some functions:

### Scale an array

scaling by a constant factor:
~~~lisp
* (defparameter a #(1 2 3))
* (lla:scal! 2.0 a)
* a
#(2.0d0 4.0d0 6.0d0)
~~~

### AXPY

This calculates `a * x + y` where `a` is a constant, `x` and `y` are arrays.
The `lla:axpy!` function is destructive, modifying the last argument (`y`).
~~~lisp
* (defparameter x #(1 2 3))
A
* (defparameter y #(2 3 4))
B
* (lla:axpy! 0.5 x y)
#(2.5d0 4.0d0 5.5d0)
* x
#(1.0d0 2.0d0 3.0d0)
* y
#(2.5d0 4.0d0 5.5d0)
~~~
If the `y` array is complex, then this operation calls the complex
number versions of these operators:

~~~lisp
* (defparameter x #(1 2 3))
* (defparameter y (make-array 3 :element-type '(complex double-float)
                                :initial-element #C(1d0 1d0)))
* y
#(#C(1.0d0 1.0d0) #C(1.0d0 1.0d0) #C(1.0d0 1.0d0))

* (lla:axpy! #C(0.5 0.5) a b)
#(#C(1.5d0 1.5d0) #C(2.0d0 2.0d0) #C(2.5d0 2.5d0))
~~~

### Dot product

The dot product of two vectors:
~~~lisp
* (defparameter x #(1 2 3))
* (defparameter y #(2 3 4))
* (lla:dot x y)
20.0d0
~~~

## Reductions

The [reduce](http://clhs.lisp.se/Body/f_reduce.htm) function operates
on sequences, including vectors (1D arrays), but not on
multidimensional arrays.
To get around this, multidimensional arrays can be displaced to create
a 1D vector.
Displaced arrays share storage with the original array, so this is a
fast operation which does not require copying data:
~~~lisp
* (defparameter a #2A((1 2) (3 4)))
A
* (reduce #'max (make-array (array-total-size a) :displaced-to a))
4
~~~

The `array-operations` package contains `flatten`, which returns a
displaced array i.e doesn't copy data:
~~~lisp
* (reduce #'max (aops:flatten a))
~~~

An SBCL extension,
[array-storage-vector](http://www.sbcl.org/manual/#index-array_002dstorage_002dvector)
provides an efficient but not portable way to achieve the same thing:
~~~lisp
* (reduce #'max (array-storage-vector a))
4
~~~

More complex reductions are sometimes needed, for example finding the
maximum absolute difference between two arrays. Using the above
methods we could do:

~~~lisp
* (defparameter a #2A((1 2) (3 4)))
A
* (defparameter b #2A((1 3) (5 4)))
B
* (reduce #'max (aops:flatten
                  (aops:each
                    (lambda (a b) (abs (- a b))) a b)))
2
~~~

This involves allocating an array to hold the intermediate result,
which for large arrays could be inefficient. Similarly to `vectorize`
defined above, a macro which does not allocate can be defined as:

~~~lisp
(defmacro vectorize-reduce (fn variables &body body)
  "Performs a reduction using FN over all elements in a vectorized expression
   on array VARIABLES.

   VARIABLES must be a list of symbols bound to arrays.
   Each array must have the same dimensions. These are
   checked at compile and run-time respectively.
  "
  ;; Check that variables is a list of only symbols
  (dolist (var variables)
    (if (not (symbolp var))
        (error "~S is not a symbol" var)))

  (let ((size (gensym)) ; Total array size (same for all variables)
        (result (gensym)) ; Returned value
        (indx (gensym)))  ; Index inside loop from 0 to size

    ;; Get the size of the first variable
    `(let ((,size (array-total-size ,(first variables))))
       ;; Check that all variables have the same size
       ,@(mapcar (lambda (var) `(if (not (equal (array-dimensions ,(first variables))
                                                (array-dimensions ,var)))
                                    (error "~S and ~S have different dimensions" ',(first variables) ',var)))
              (rest variables))

       ;; Apply FN with the first two elements (or fewer if size < 2)
       (let ((,result (apply ,fn (loop for ,indx below (min ,size 2) collecting
                                      (let ,(map 'list (lambda (var) (list var `(row-major-aref ,var ,indx))) variables)
                                        (progn ,@body))))))

         ;; Loop over the remaining indices
         (loop for ,indx from 2 below ,size do
            ;; Locally redefine variables to be scalars at a given index
              (let ,(mapcar (lambda (var) (list var `(row-major-aref ,var ,indx))) variables)
                ;; User-supplied function body now evaluated for each index in turn
                (setf ,result (funcall ,fn ,result (progn ,@body)))))
         ,result))))

~~~
[Note: This macro is available in [this fork](https://github.com/bendudson/array-operations)
of array-operations, but not Quicklisp]

Using this macro, the maximum value in an array A (of any shape) is:
~~~lisp
* (vectorize-reduce #'max (a) a)
~~~

The maximum absolute difference between two arrays A and B, of any
shape as long as they have the same shape, is:
~~~lisp
* (vectorize-reduce #'max (a b) (abs (- a b)))
~~~

# Linear algebra

Several packages provide bindings to BLAS and LAPACK libraries,
including:

- [lla](https://github.com/tpapp/lla)
- [MAGICL](https://github.com/rigetticomputing/magicl)

A longer list of available packages is on [CLiki's linear algebra page](https://www.cliki.net/linear%20algebra).

In the examples below the lla package is loaded:
~~~lisp
* (ql:quickload :lla)

To load "lla":
  Load 1 ASDF system:
    lla
; Loading "lla"
.
(:LLA)
~~~

## Matrix multiplication

The [lla](https://github.com/tpapp/lla) function `mm` performs
vector-vector, matrix-vector and matrix-matrix
multiplication.

### Vector dot product

Note that one vector is treated as a row vector, and the other as
column:
~~~lisp
* (lla:mm #(1 2 3) #(2 3 4))
20
~~~

### Matrix-vector product
~~~lisp
* (lla:mm #2A((1 1 1) (2 2 2) (3 3 3))  #(2 3 4))
#(9.0d0 18.0d0 27.0d0)
~~~
which has performed the sum over `j` of `A[i j] * x[j]`

### Matrix-matrix multiply
~~~lisp
* (lla:mm #2A((1 2 3) (1 2 3) (1 2 3))  #2A((2 3 4) (2 3 4) (2 3 4)))
#2A((12.0d0 18.0d0 24.0d0) (12.0d0 18.0d0 24.0d0) (12.0d0 18.0d0 24.0d0))
~~~
which summed over `j` in `A[i j] * B[j k]`

Note that the type of the returned arrays are simple arrays,
specialised to element type `double-float`
~~~lisp
* (type-of (lla:mm #2A((1 0 0) (0 1 0) (0 0 1)) #(1 2 3)))
(SIMPLE-ARRAY DOUBLE-FLOAT (3))
~~~

### Outer product

The [array-operations](https://github.com/tpapp/array-operations)
package contains a generalised [outer product](https://en.wikipedia.org/wiki/Outer_product)
function:
~~~lisp
* (ql:quickload :array-operations)
To load "array-operations":
  Load 1 ASDF system:
    array-operations
; Loading "array-operations"

(:ARRAY-OPERATIONS)
* (aops:outer #'* #(1 2 3) #(2 3 4))
#2A((2 3 4) (4 6 8) (6 9 12))
~~~
which has created a new 2D array `A[i j] = B[i] * C[j]`. This `outer`
function can take an arbitrary number of inputs, and inputs with
multiple dimensions.

## Matrix inverse

The direct inverse of a dense matrix can be calculated with `invert`
~~~lisp
* (lla:invert #2A((1 0 0) (0 1 0) (0 0 1)))
#2A((1.0d0 0.0d0 -0.0d0) (0.0d0 1.0d0 -0.0d0) (0.0d0 0.0d0 1.0d0))
~~~
e.g
~~~lisp
* (defparameter a #2A((1 2 3) (0 2 1) (1 3 2)))
A
* (defparameter b (lla:invert a))
B
* (lla:mm a b)
#2A((1.0d0 2.220446049250313d-16 0.0d0)
    (0.0d0 1.0d0 0.0d0)
    (0.0d0 1.1102230246251565d-16 0.9999999999999998d0))
~~~

Calculating the direct inverse is generally not advisable,
particularly for large matrices. Instead the
[LU decomposition](https://en.wikipedia.org/wiki/LU_decomposition) can be
calculated and used for multiple inversions.

~~~lisp
* (defparameter a #2A((1 2 3) (0 2 1) (1 3 2)))
A
* (defparameter b (lla:mm a #(1 2 3)))
B
* (lla:solve (lla:lu a) b)
#(1.0d0 2.0d0 3.0d0)
~~~

## Singular value decomposition

The `svd` function calculates the [singular value decomposition](https://en.wikipedia.org/wiki/Singular-value_decomposition)
of a given matrix, returning an object with slots for the three returned
matrices:

~~~lisp
* (defparameter a #2A((1 2 3) (0 2 1) (1 3 2)))
A
* (defparameter a-svd (lla:svd a))
A-SVD
* a-svd
#S(LLA:SVD
   :U #2A((-0.6494608633564334d0 0.7205486773948702d0 0.24292013188045855d0)
          (-0.3744175632000917d0 -0.5810891192666799d0 0.7225973455785591d0)
          (-0.6618248071322363d0 -0.3783451320875919d0 -0.6471807210432038d0))
   :D #S(CL-NUM-UTILS.MATRIX:DIAGONAL-MATRIX
         :ELEMENTS #(5.593122609997059d0 1.2364443401235103d0
                     0.43380279311714376d0))
   :VT #2A((-0.2344460799312531d0 -0.7211054639318696d0 -0.6519524104506949d0)
           (0.2767642134809678d0 -0.6924017945853318d0 0.6663192365460215d0)
           (-0.9318994611765425d0 -0.02422116311440764d0 0.3619070730398283d0)))
~~~

The diagonal matrix (singular values) and vectors can be accessed with
functions:
~~~lisp
(lla:svd-u a-svd)
#2A((-0.6494608633564334d0 0.7205486773948702d0 0.24292013188045855d0)
    (-0.3744175632000917d0 -0.5810891192666799d0 0.7225973455785591d0)
    (-0.6618248071322363d0 -0.3783451320875919d0 -0.6471807210432038d0))

* (lla:svd-d a-svd)
#S(CL-NUM-UTILS.MATRIX:DIAGONAL-MATRIX
   :ELEMENTS #(5.593122609997059d0 1.2364443401235103d0 0.43380279311714376d0))

* (lla:svd-vt a-svd)
#2A((-0.2344460799312531d0 -0.7211054639318696d0 -0.6519524104506949d0)
    (0.2767642134809678d0 -0.6924017945853318d0 0.6663192365460215d0)
    (-0.9318994611765425d0 -0.02422116311440764d0 0.3619070730398283d0))
~~~


# Matlisp

The [Matlisp](https://github.com/bharath1097/matlisp/) scientific
computation library provides high performance operations on arrays,
including wrappers around BLAS and LAPACK functions.
It can be loaded using quicklisp:

~~~lisp
* (ql:quickload :matlisp)
~~~

The nickname for `matlisp` is `m`. To avoid typing `matlisp:` or
`m:` in front of each symbol, you can define your own package which
uses matlisp
(See the [PCL section on packages](http://www.gigamonkeys.com/book/programming-in-the-large-packages-and-symbols.html)):
~~~lisp
* (defpackage :my-new-code
     (:use :common-lisp :matlisp))
#<PACKAGE "MY-NEW-CODE">

* (in-package :my-new-code)
~~~
and to use the `#i` infix reader (note the same name as for
`cmu-infix`), run:
~~~lisp
* (named-readtables:in-readtable :infix-dispatch-table)
~~~

## Creating tensors

~~~lisp
* (matlisp:zeros '(2 2))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 2)
  0.000    0.000
  0.000    0.000
>
~~~

Note that by default matrix storage types are `double-float`.
To create a complex array using `zeros`, `ones` and `eye`, specify the type:

~~~lisp
* (matlisp:zeros '(2 2) '((complex double-float)))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: (COMPLEX DOUBLE-FLOAT)>| #(2 2)
  0.000    0.000
  0.000    0.000
>
~~~

As well as `zeros` and `ones` there is `eye` which creates an identity
matrix:
~~~lisp
* (matlisp:eye '(3 3) '((complex double-float)))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: (COMPLEX DOUBLE-FLOAT)>| #(3 3)
  1.000    0.000    0.000
  0.000    1.000    0.000
  0.000    0.000    1.000
>
~~~

### Ranges

To generate 1D arrays there are the `range` and `linspace` functions:

~~~lisp
* (matlisp:range 1 10)
#<|<SIMPLE-DENSE-TENSOR: (INTEGER 0 4611686018427387903)>| #(9)
 1   2   3   4   5   6   7   8   9
>
~~~

The `range` function rounds down it's final argument to an integer:
~~~lisp
* (matlisp:range 1 -3.5)
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: SINGLE-FLOAT>| #(5)
 1.000   0.000   -1.000  -2.000  -3.000
>
* (matlisp:range 1 3.3)
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: SINGLE-FLOAT>| #(3)
 1.000   2.000   3.000
>
~~~

Linspace is a bit more general, and the values returned include the
end point.

~~~lisp
* (matlisp:linspace 1 10)
#<|<SIMPLE-DENSE-TENSOR: (INTEGER 0 4611686018427387903)>| #(10)
 1   2   3   4   5   6   7   8   9   10
>
~~~

~~~lisp
* (matlisp:linspace 0 (* 2 pi) 5)
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(5)
 0.000   1.571   3.142   4.712   6.283
>
~~~

Currently `linspace` requires real inputs, and doesn't work with complex numbers.

### Random numbers

~~~lisp
* (matlisp:random-uniform '(2 2))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 2)
  0.7287       0.9480
  2.6703E-2    0.1834
>
~~~

~~~lisp
(matlisp:random-normal '(2 2))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 2)
  0.3536    -1.291
 -0.3877    -1.371
>
~~~
There are functions for other distributions, including
`random-exponential`, `random-beta`, `random-gamma` and
`random-pareto`.

### Reader macros

The `#d` and `#e` reader macros provide a way to create `double-float`
and `single-float` tensors:

~~~lisp
* #d[1,2,3]
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(3)
 1.000   2.000   3.000
>

* #d[[1,2,3],[4,5,6]]
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 3)
  1.000    2.000    3.000
  4.000    5.000    6.000
>
~~~
Note that the comma separators are needed.

### Tensors from arrays

Common lisp arrays can be converted to Matlisp tensors by copying:

~~~lisp
* (copy #2A((1 2 3)
            (4 5 6))
        '#.(tensor 'double-float))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 3)
  1.000    2.000    3.000
  4.000    5.000    6.000
>
~~~

Instances of the `tensor` class can also be created, specifying the
dimensions. The internal storage of `tensor` objects is a 1D array
(`simple-vector`) in a slot `store`.

For example, to create a `double-float` type tensor:
~~~lisp
(make-instance (tensor 'double-float)
    :dimensions  (coerce '(2) '(simple-array index-type (*)))
    :store (make-array 2 :element-type 'double-float))
~~~

### Arrays from tensors

The array store can be accessed using slots:
~~~lisp
* (defparameter vec (m:range 0 5))
* vec
#<|<SIMPLE-DENSE-TENSOR: (INTEGER 0 4611686018427387903)>| #(5)
 0   1   2   3   4
>
* (slot-value vec 'm:store)
#(0 1 2 3 4)
~~~

Multidimensional tensors are also stored in 1D arrays, and are stored
in column-major order rather than the row-major ordering used for
common lisp arrays. A displaced array will therefore be
transposed.

The contents of a tensor can be copied into an array
~~~lisp
* (let ((tens (m:ones '(2 3))))
    (m:copy tens 'array))
#2A((1.0d0 1.0d0 1.0d0) (1.0d0 1.0d0 1.0d0))
~~~

or a list:
~~~lisp
* (m:copy (m:ones '(2 3)) 'cons)
((1.0d0 1.0d0 1.0d0) (1.0d0 1.0d0 1.0d0))
~~~

## Element access

The `ref` function is the equivalent of `aref` for standard CL
arrays, and is also setf-able:
~~~lisp
* (defparameter a (matlisp:ones '(2 3)))

* (setf (ref a 1 1) 2.0)
2.0d0
* a
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 3)
  1.000    1.000    1.000
  1.000    2.000    1.000
>
~~~

## Element-wise operations

The `matlisp-user` package, loaded when `matlisp` is loaded, contains
functions for operating element-wise on tensors.
~~~lisp
* (matlisp-user:* 2 (ones '(2 3)))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 3)
  2.000    2.000    2.000
  2.000    2.000    2.000
>
~~~

This includes arithmetic operators '+', '-', '*', '/' and 'expt', but
also `sqrt`,`sin`,`cos`,`tan`, hyperbolic functions, and their inverses.
The `#i` reader macro recognises many of these, and uses the
`matlisp-user` functions:

~~~lisp
* (let ((a (ones '(2 2)))
        (b (random-normal '(2 2))))
     #i( 2 * a + b ))
#<|<BLAS-MIXIN SIMPLE-DENSE-TENSOR: DOUBLE-FLOAT>| #(2 2)
  0.9684    3.250
  1.593     1.508
>

* (let ((a (ones '(2 2)))
        (b (random-normal '(2 2))))
     (macroexpand-1 '#i( 2 * a + b )))
(MATLISP-USER:+ (MATLISP-USER:* 2 A) B)
~~~

---
title: Dates and Times
---

Common Lisp provides two different ways of looking at time: universal time,
meaning time in the "real world", and run time, meaning time as seen by your
computer's CPU. We will deal with both of them separately.

<a name="univ"></a>

## Universal Time

Universal time is represented as the number of seconds that have elapsed since
00:00 of January 1, 1900 in the GMT time zone. The function
[`GET-UNIVERSAL-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_get_un.htm)
returns the current universal time:

~~~lisp
CL-USER> (get-universal-time)
3220993326
~~~

Of course this value is not very readable, so you can use the function
[`DECODE-UNIVERSAL-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_dec_un.htm)
to turn it into a "calendar time" representation:

~~~lisp
CL-USER> (decode-universal-time 3220993326)
6
22
19
25
1
2002
4
NIL
5
~~~

This call returns nine values: seconds, minutes, hours, day, month, year, day of
the week, daylight savings time flag and time zone. Note that the day of the
week is represented as an integer in the range 0..6 with 0 being Monday and 6
being Sunday. Also, the time zone is represented as the number of hours you need
to add to the current time in order to get GMT time. So in this example the
decoded time would be 19:22:06 of Friday, January 25, 2002, in the EST time
zone, with no daylight savings in effect. This, of course, relies on the
computer's own clock, so make sure that it is set correctly (including the time
zone you are in and the DST flag). As a shortcut, you can use
[`GET-DECODED-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_get_un.htm)
to get the calendar time representation of the current time directly:

~~~lisp
CL-USER> (get-decoded-time)
~~~

is equivalent to

~~~lisp
CL-USER> (decode-universal-time (get-universal-time))
~~~

Here is an example of how to use these functions in a program:

~~~lisp
CL-USER> (defconstant *day-names*
           '("Monday" "Tuesday" "Wednesday"
	         "Thursday" "Friday" "Saturday"
	         "Sunday"))
*DAY-NAMES*

CL-USER> (multiple-value-bind
           (second minute hour day month year day-of-week dst-p tz)
    	   (get-decoded-time)
           (format t "It is now ~2,'0d:~2,'0d:~2,'0d of ~a, ~d/~2,'0d/~d (GMT~@d)"
	    	 hour
	    	 minute
	    	 second
	    	 (nth day-of-week *day-names*)
	    	 month
	    	 day
	    	 year
	    	 (- tz)))
It is now 17:07:17 of Saturday, 1/26/2002 (GMT-5)
~~~

Of course the call to `GET-DECODED-TIME` above could be replaced by
`(DECODE-UNIVERSAL-TIME n)`, where n is any integer number, to print an
arbitrary date. You can also go the other way around: the function
[`ENCODE-UNIVERSAL-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_encode.htm)
lets you encode a calendar time into the corresponding universal time. This
function takes six mandatory arguments (seconds, minutes, hours, day, month and
year) and one optional argument (the time zone) and it returns a universal time:

~~~lisp
CL-USER> (encode-universal-time 6 22 19 25 1 2002)
3220993326
~~~

Note that the result is automatically adjusted for daylight savings time if the time zone is not supplied. If it is supplied, than Lisp assumes that the specified time zone already accounts for daylight savings time, and no adjustment is performed.

Since universal times are simply numbers, they are easier and safer to manipulate than calendar times. Dates and times should always be stored as universal times if possible, and only converted to string representations for output purposes. For example, it is straightforward to know which of two dates came before the other, by simply comparing the two corresponding universal times with <. Another typical problem is how to compute the "temporal distance" between two given dates. Let's see how to do this with an example: specifically, we will calculate the temporal distance between the first landing on the moon (4:17pm EDT, July 20 1969) and the last takeoff of the space shuttle Challenger (11:38 a.m. EST, January 28, 1986).

~~~lisp
CL-USER> (setq *moon* (encode-universal-time 0 17 16 20 7 1969 4))
2194805820

CL-USER> (setq *takeoff* (encode-universal-time 0 38 11 28 1 1986 5))
2716303080

CL-USER> (- *takeoff* *moon*)
521497260
~~~

That's a bit over 52 million seconds, corresponding to 6035 days, 20 hours and 21 minutes (you can verify this by dividing that number by 60, 60 and 24 in succession). Going beyond days is a bit harder because months and years don't have a fixed length, but the above is approximately 16 and a half years.

You can in theory use differences between universal times to measure how long the execution of a part of your program took, but the universal times are represented in seconds, and this resolution will usually be too low to be useful. We will see a better method of doing this in the section about internal time.

To sum up, we have seen how to turn a universal time into a calendar time and vice-versa, how to perform calculations on universal times, and how to format calendar times in a human-readable way. The last piece that is missing is how to parse a string represented as a human-readable string (e.g. "03/11/1997") and turn it into a calendar time. Unfortunately this turns out to be very difficult in the general case, due to the multitude of different ways of writing dates and times that we use. In some cases it might not even be possible without context information: the above example can be correctly parsed both as March 11th or as November 3rd according to where you are living. In conclusion, either force your users to write dates in a fixed format, or be prepared to write a very intelligent parsing function!<a name="intern"></a>

### Internal Time

Internal time is the time as measured by your Lisp environment, using your computer's clock. It differs from universal time in three important respects. First, internal time is not measured starting from a specified point in time: it could be measured from the instant you started your Lisp, from the instant you booted your machine, or from any other arbitrary time point in the past. As we will see shortly, the absolute value of an internal time is almost always meaningless; only differences between internal times are useful. The second difference is that internal time is not measured in seconds, but in a (usually smaller) unit whose value can be deduced from [`INTERNAL-TIME-UNITS-PER-SECOND`](http://www.lispworks.com/documentation/HyperSpec/Body/v_intern.htm):

~~~lisp
CL-USER> internal-time-units-per-second
1000
~~~

This means that in the Lisp environment used in this example, internal time is measured in milliseconds. Finally, what is being measured by the "internal time" clock? There are actually two different internal time clocks in your Lisp: one of them measures the passage of "real" time (the same time that universal time measures, but in different units), and the other one measures the passage of CPU time, that is, the time your CPU spends doing actual computation for the current Lisp process. On most modern computers these two times will be different, since your CPU will never be entirely dedicated to your program (even on single-user machines, the CPU has to devote part of its time to processing interrupts, performing I/O, etc). The two functions used to retrieve internal times are called [`GET-INTERNAL-REAL-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_get_in.htm) and [`GET-INTERNAL-RUN-TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/f_get__1.htm) respectively. Using them, we can solve the above problem about measuring a function's run time:

~~~lisp
CL-USER> (let ((real1 (get-internal-real-time))
	           (run1 (get-internal-run-time)))
	       (... your call here ...)
	       (let ((run2 (get-internal-run-time))
		         (real2 (get-internal-real-time)))
	         (format t "Computation took:~%")
	         (format t "  ~f seconds of real time~%"
		       (/ (- real2 real1) internal-time-units-per-second))
	         (format t "  ~f seconds of run time~%"
		       (/ (- run2 run1) internal-time-units-per-second))))
~~~

A good way to see the difference between real time and run time is to test the above code using a call such as `(SLEEP 3)`. The [`SLEEP`](http://www.lispworks.com/documentation/HyperSpec/Body/f_sleep.htm) function suspends the execution of your code for the specified number of seconds. You should therefore see a real time very close to the argument of `SLEEP` and a run time very close to zero. Let's turn the above code into a macro in order to make it more general:

~~~lisp
CL-USER> (defmacro timing (&body forms)
	       (let ((real1 (gensym))
		         (real2 (gensym))
		         (run1 (gensym))
		         (run2 (gensym))
		         (result (gensym)))
	         `(let* ((,real1 (get-internal-real-time))
		             (,run1 (get-internal-run-time))
		             (,result (progn ,@forms))
		             (,run2 (get-internal-run-time))
		             (,real2 (get-internal-real-time)))
		       (format *debug-io* ";;; Computation took:~%")
	       	   (format *debug-io* ";;;  ~f seconds of real time~%"
			     (/ (- ,real2 ,real1) internal-time-units-per-second))
		       (format t ";;;  ~f seconds of run time~%"
			     (/ (- ,run2 ,run1) internal-time-units-per-second))
		,result)))
TIMING

CL-USER> (timing (sleep 1))
;;; Computation took: 0.994 seconds of real time 0.0 seconds of run
;;; time
NIL
~~~

The built-in macro [`TIME`](http://www.lispworks.com/documentation/HyperSpec/Body/m_time.htm) does roughly the same as the above macro (it executes a form and prints timing information at the end), but it also usually provides information about memory usage, time spent in garbage collection, page faults, etc. The format of the output is implementation-dependent, but in general it's pretty useful and informative. This is an example under Allegro Common Lisp 6.0: we generate a list of 100 real numbers and we measure the time it takes to sort them in ascending order.

~~~lisp
CL-USER> (let ((numbers (loop for i from 1 to 100 collect (random 1.0))))
           (time (sort numbers #'<)))
; cpu time (non-gc) 0 msec user, 10 msec system
; cpu time (gc)     0 msec user, 0 msec system
; cpu time (total)  0 msec user, 10 msec system
; real time  9 msec
; space allocation:
;  3,586 cons cells, 11,704 other bytes, 0 static bytes
~~~

<a name="weekday"></a>

### Computing the day of the week

In the section about [Universal Time](#univ) we've learned enough to write a small function that computes the day of the week. Unfortunately, by definition, this function won't work for dates before January 1, 1900.

~~~lisp
CL-USER> (defun day-of-week (day month year)
           "Returns the day of the week as an integer. Monday is 0."
	       (nth-value
	         6
	         (decode-universal-time
	           (encode-universal-time 0 0 0 day month year 0)
	     0)))
DAY-OF-WEEK
CL-USER> (day-of-week 23 12 1965)
3
CL-USER> (day-of-week 1 1 1900)
0
CL-USER> (day-of-week 31 12 1899)

Type-error in KERNEL::OBJECT-NOT-TYPE-ERROR-HANDLER:
   1899 is not of type (OR (MOD 100) (INTEGER 1900))
~~~

If this is a problem for you, here's a small function by Gerald Doussot (adapted from the comp.lang.c FAQ) that will help you:

~~~lisp
(defun day-of-week (day month year)
  "Returns the day of the week as an integer. Sunday is 0. Works for years after 1752."
  (let ((offset '(0 3 2 5 0 3 5 1 4 6 2 4)))
    (when (< month 3)
      (decf year 1))
    (mod
     (truncate (+ year
                  (/ year 4)
                  (/ (- year)
                     100)
                  (/ year 400)
                  (nth (1- month) offset)
                  day
                  -1))
     7)))
~~~

## The `local-time` library

The `local-time` library (available on Quicklisp) is a very handy extension to
the somewhat limited functionalities as defined by the standard.

In particular, it can

- print timestamp in various standard or custom formats (e.g. RFC1123 or RFC3339)
- parse timestrings,
- perform time arithmetic,
- convert Unix times, timestamps, and universal times to and fro.

For example, here is a function that returns Unix times as a human readable string:

~~~lisp
(defun unix-time-to-human-string (unix-time)
  (local-time:format-timestring
   nil
   (local-time:unix-to-timestamp unix-time)
   :format local-time:+asctime-format+))
~~~

See the [manual](https://common-lisp.net/project/local-time/manual.html) for
the full details.

### Get today's date

Use `now` or `today`:

~~~lisp
(local-time:now)
@2019-11-13T20:02:13.529541+01:00

(local-time:today)
@2019-11-13T01:00:00.000000+01:00
~~~

"today" is the midnight of the current day in the UTC zone.


### Formatting time strings

~~~lisp
(local-time:now)
@2019-11-13T18:07:57.425654+01:00
~~~

~~~lisp
(format nil "~a" (local-time:now))
"2019-11-13T18:08:23.312664+01:00"
~~~

`format-timestring` takes a stream argument like `format`:

~~~lisp
(local-time:format-timestring nil (local-time:now))
"2019-11-13T18:09:06.313650+01:00"
~~~

Here `nil` returns a new string. `t` would print to `*standard-output*`.

It also accepts a `:format` argument. Its default value is
`+iso-8601-format+`, with the output shown above. The
`+rfc3339-format+` format defaults to it.

With `+rfc-1123-format+`:

~~~lisp
(local-time:format-timestring nil (local-time:now) :format local-time:+rfc-1123-format+)
"Wed, 13 Nov 2019 18:11:38 +0100"
~~~

With `+asctime-format+`:

~~~lisp
(local-time:format-timestring nil (local-time:now) :format local-time:+asctime-format+)
"Wed Nov 13 18:13:15 2019"
~~~

With `+iso-week-date-format+`:

~~~lisp
(local-time:format-timestring nil (local-time:now) :format local-time:+iso-week-date-format+)
"2019-W46-3"
~~~


### Defining format strings

We can pass a custom `:format` argument to `format-timestring`.

The syntax consists of a list made of symbols with special meanings
(`:year`, `:day`…), strings and characters:

~~~lisp
(local-time:format-timestring nil (local-time:now) :format '(:year "-" :month "-" :day))
"2019-11-13"
~~~

The list of symbols is available in the documentation: https://common-lisp.net/project/local-time/manual.html#Parsing-and-Formatting

There are `:year :month :day :weekday :hour :min :sec :msec`, long and
short notations ("Monday", "Mo."), gmt offset, timezone markers and
more.

The `+rfc-1123-format+` itself is defined like this:

~~~lisp
(defparameter +rfc-1123-format+
  ;; Sun, 06 Nov 1994 08:49:37 GMT
  '(:short-weekday ", " (:day 2) #\space :short-month #\space (:year 4) #\space
    (:hour 2) #\: (:min 2) #\: (:sec 2) #\space :gmt-offset-hhmm)
  "See the RFC 1123 for the details about the possible values of the timezone field.")
~~~

We see the form `(:day 2)`: the 2 is for padding, to ensure that the
day is printed with two digits (not only `1`, but `01`). There could be
an optional third argument, the character with which to fill the
padding (by default, `#\0`).

### Parsing time strings

Use `parse-timestring` to parse timestrings, in the form
`2019-11-13T18:09:06.313650+01:00`. It works in a variety of formats
by default, and we can change parameters to adapt it to our needs.

To parse more formats such as "Thu Jul 23 19:42:23 2013" (asctime),
we'll use the [cl-date-time-parser](https://github.com/tkych/cl-date-time-parser) library.

The `parse-timestring` docstring is:

>  Parses a timestring and returns the corresponding timestamp. Parsing begins at start and stops at the end position. If there are invalid characters within timestring and fail-on-error is T, then an invalid-timestring error is signaled, otherwise NIL is returned.
>
> If there is no timezone specified in timestring then offset is used as the default timezone offset (in seconds).

Examples:

~~~lisp
(local-time:parse-timestring "2019-11-13T18:09:06.313650+01:00")
;; @2019-11-13T18:09:06.313650+01:00
~~~

~~~lisp
(local-time:parse-timestring "2019-11-13")
;; @2019-11-13T01:00:00.000000+01:00
~~~

This custom format fails by default: "2019/11/13", but we can set the
`:date-separator` to "/":

~~~lisp
(local-time:parse-timestring "2019/11/13" :date-separator #\/)
;; @2019-11-13T19:42:32.394092+01:00
~~~

There is also a `:time-separator` (defaulting to `#\:`) and
`:date-time-separator` (`#\T`).

Other options include:

- the start and end positions
- `fail-on-error` (defaults to `t`)
- `(allow-missing-elements t)`
- `(allow-missing-date-part allow-missing-elements)`
- `(allow-missing-time-part allow-missing-elements)`
- `(allow-missing-timezone-part allow-missing-elements)`
- `(offset 0)`

Now a format like ""Wed Nov 13 18:13:15 2019" will fail. We'll use the
`cl-date-time-parser` library:

~~~lisp
(cl-date-time-parser:parse-date-time "Wed Nov 13 18:13:15 2019")
;; 3782657595
;; 0
~~~

It returns the universal time which, in turn, we can ingest with the
local-time library:

~~~lisp
(local-time:universal-to-timestamp *)
;; @2019-11-13T19:13:15.000000+01:00
~~~

---
title: Pattern Matching
---

The ANSI Common Lisp standard does not include facilities for pattern
matching, but libraries existed for this task and
[Trivia](https://github.com/guicho271828/trivia) became a community
standard.

For an introduction to the concepts of pattern matching, see [Trivia's wiki](https://github.com/guicho271828/trivia/wiki/What-is-pattern-matching%3F-Benefits%3F).

Trivia matches against *a lot* of lisp objects and is extensible.

The library is in Quicklisp:

~~~lisp
(ql:quickload "trivia")
~~~

For the following examples, let's `use` the library:

~~~lisp
(use-package :trivia)
~~~


## Common destructuring patterns

### `cons`

~~~lisp
(match '(1 2 3)
  ((cons x y)
  ; ^^ pattern
   (print x)
   (print y)))
;; |-> 1
;; |-> (2 3)
~~~

### `list`, `list*`

~~~lisp
(match '(something #(0 1 2))
  ((list a (vector 0 _ b))
   (values a b)))
SOMETHING
2
~~~

`list*` pattern:

~~~lisp
(match '(1 2 . 3)
  ((list* _ _ x)
   x))
3
~~~

Note that using `list` would match nothing.

### `vector`, `vector*`

`vector` checks if the object is a vector, if the lengths are the
same, and if the contents matches against each subpatterns.

`vector*` is similar, but called a soft-match variant that allows if
the length is larger-than-equal to the length of subpatterns.

~~~lisp
(match #(1 2 3)
  ((vector _ x _)
   x))
;; -> 2
~~~

~~~lisp
(match #(1 2 3 4)
  ((vector _ x _)
   x))
;; -> NIL : does not match
~~~

~~~lisp
(match #(1 2 3 4)
  ((vector* _ x _)
   x))
;; -> 2 : soft match.
~~~

~~~
<vector-pattern> : vector      | simple-vector
                   bit-vector  | simple-bit-vector
                   string      | simple-string
                   base-string | simple-base-string | sequence
(<vector-pattern> &rest subpatterns)
~~~

### Class and structure pattern

There are three styles that are equivalent:

~~~lisp
(defstruct foo bar baz)
(defvar *x* (make-foo :bar 0 :baz 1)

(match *x*
  ;; make-instance style
  ((foo :bar a :baz b)
   (values a b))
  ;; with-slots style
  ((foo (bar a) (baz b))
   (values a b))
  ;; slot name style
  ((foo bar baz)
   (values bar baz)))
~~~

### `type`, `satisfies`

The `type` pattern matches if the object is of type. `satisfies` matches
if the predicate returns true for the object. A lambda form is
acceptable.

### `assoc`, `property`, `alist`, `plist`

All these patterns first check if the pattern is a list. If that is
satisfied, then they obtain the contents, and the value is matched
against the subpattern.


### Array, simple-array, row-major-array patterns

See https://github.com/guicho271828/trivia/wiki/Type-Based-Destructuring-Patterns#array-simple-array-row-major-array-pattern !

## Logic based patterns

### `and`, `or`

~~~lisp
(match x
  ((or (list 1 a)
       (cons a 3))
   a))
~~~

matches against both `(1 2)` and `(4 . 3)` and returns 2 and 4, respectively.

### `not`

It does not match when subpattern matches. The variables used in the
subpattern are not visible in the body.

### `guards`

The syntax is `guard` + `subpattern` + `a test form`, and the body.

~~~lisp
(match (list 2 5)
  ((guard (list x y)     ; subpattern
          (= 10 (* x y)) ; test-form
          (- x y) (satisfies evenp)) ; generator1, subpattern1
   t))
~~~

If the subpattern is true, the test form is evaluated, and if it is
true it is matched against subpattern1.

The above returns `nil`, since `(- x y) == 3` does not satisfies `evenp`.


## Nesting patterns

Patterns can be nested:

~~~lisp
(match '(:a (3 4) 5)
  ((list :a (list _ c) _)
   c))
~~~

returns `4`.

## See more

See [special patterns](https://github.com/guicho271828/trivia/wiki/Special-Patterns): `place`, `bind` and `access`.

---
title: Regular Expressions
---

The [ANSI Common Lisp
standard](http://www.lispworks.com/documentation/HyperSpec/index.html)
does not include facilities for regular expressions, but a couple of
libraries exist for this task, for instance:
[cl-ppcre](https://github.com/edicl/cl-ppcre).

See also the respective [Cliki:
regexp](http://www.cliki.net/Regular%20Expression) page for more
links.

Note that some CL implementations include regexp facilities, notably
[CLISP](http://clisp.sourceforge.net/impnotes.html#regexp) and
[ALLEGRO
CL](https://franz.com/support/documentation/current/doc/regexp.htm). If
in doubt, check your manual or ask your vendor.

The description provided below is far from complete, so don't forget
to check the reference manual that comes along with the CL-PPCRE
library.

# PPCRE

## Using PPCRE

[CL-PPCRE](https://github.com/edicl/cl-ppcre) (abbreviation for
Portable Perl-compatible regular expressions) is a portable regular
expression library for Common Lisp with a broad set of features and
good performance. It has been ported to a number of Common Lisp
implementations and can be easily installed (or added as a dependency)
via Quicklisp:

~~~lisp
(ql:quickload "cl-ppcre")
~~~

Basic operations with the CL-PPCRE library functions are described
below.


## Looking for matching patterns

The `scan` function tries to match the given pattern and on success
returns four multiple-values values - the start of the match, the end
of the match, and two arrays denoting the beginnings and ends of
register matches. On failure returns `NIL`.

A regular expression pattern can be compiled with the `create-scanner`
function call. A "scanner" will be created that can be used by other
functions.

For example:

~~~lisp
(let ((ptrn (ppcre:create-scanner "(a)*b")))
  (ppcre:scan ptrn "xaaabd"))
~~~

will yield the same results as:

~~~lisp
(ppcre:scan "(a)*b" "xaaabd")
~~~

but will require less time for repeated `scan` calls as parsing the
expression and compiling it is done only once.


## Extracting information

CL-PPCRE provides a several ways to extract matching fragments, among
them: the `scan-to-strings` and `register-groups-bind` functions.

The `scan-to-strings` function is similar to `scan` but returns
substrings of target-string instead of positions. This function
returns two values on success: the whole match as a string plus an
array of substrings (or NILs) corresponding to the matched registers.

The `register-groups-bind` function tries to match the given pattern
against the target string and binds matching fragments with the given
variables.

~~~lisp
(ppcre:register-groups-bind (first second third fourth)
      ("((a)|(b)|(c))+" "abababc" :sharedp t)
    (list first second third fourth))
;; => ("c" "a" "b" "c")
~~~

CL-PPCRE also provides a shortcut for calling a function before
assigning the matching fragment to the variable:

~~~lisp
(ppcre:register-groups-bind (fname lname (#'parse-integer date month year))
      ("(\\w+)\\s+(\\w+)\\s+(\\d{1,2})\\.(\\d{1,2})\\.(\\d{4})" "Frank Zappa 21.12.1940")
    (list fname lname (encode-universal-time 0 0 0 date month year 0)))
;; => ("Frank" "Zappa" 1292889600)
~~~

## Syntactic sugar

It might be more convenient to use CL-PPCRE with the
[CL-INTERPOL](https://github.com/edicl/cl-interpol)
library. CL-INTERPOL is a library for Common Lisp which modifies the
reader in a way that introduces interpolation within strings similar
to Perl, Scala, or Unix Shell scripts.

In addition to loading the CL-INTERPOL library, initialization call
must be made to properly configure the Lisp reader. This is
accomplished by either calling the `enable-interpol-syntax` function
from the REPL or placing that call in the source file before using any
of its features:

~~~lisp
(interpol:enable-interpol-syntax)
~~~

A lot more syntax sugar is introduced by the [cl21](cl21.html) project
but in a somewhat more intrusive way.

---
title: Input/Output
---

<a name="redir"></a>

# Redirecting the Standard Output of your Program

You do it like this:

~~~lisp
(let ((*standard-output* <some form generating a stream>))
  ...)
~~~

Because
[`*STANDARD-OUTPUT*`](http://www.lispworks.com/documentation/HyperSpec/Body/v_debug_.htm)
is a dynamic variable, all references to it during execution of the body of the
`LET` form refer to the stream that you bound it to. After exiting the `LET`
form, the old value of `*STANDARD-OUTPUT*` is restored, no matter if the exit
was by normal execution, a `RETURN-FROM` leaving the whole function, an
exception, or what-have-you. (This is, incidentally, why global variables lose
much of their brokenness in Common Lisp compared to other languages: since they
can be bound for the execution of a specific form without the risk of losing
their former value after the form has finished, their use is quite safe; they
act much like additional parameters that are passed to every function.)

If the output of the program should go to a file, you can do the following:

~~~lisp
(with-open-file (*standard-output* "somefile.dat" :direction :output
                                   :if-exists :supersede)
  ...)
~~~

[`WITH-OPEN-FILE`](http://www.lispworks.com/documentation/HyperSpec/Body/m_w_open.htm)
opens the file - creating it if necessary - binds `*STANDARD-OUTPUT*`, executes
its body, closes the file, and restores `*STANDARD-OUTPUT*` to its former
value. It doesn't get more comfortable than this!<a name="faith"></a>

# Faithful Output with Character Streams

By _faithful output_ I mean that characters with codes between 0 and 255 will be
written out as is. It means, that I can `(PRINC (CODE-CHAR 0..255) s)` to a
stream and expect 8-bit bytes to be written out, which is not obvious in the
times of Unicode and 16 or 32 bit character representations. It does _not_
require that the characters ä, ß, or þ must have their
[`CHAR-CODE`](http://www.lispworks.com/documentation/HyperSpec/Body/f_char_c.htm)
in the range 0..255 - the implementation is free to use any code. But it does
require that no `#\Newline` to CRLF translation takes place, among others.

Common Lisp has a long tradition of distinguishing character from byte (binary)
I/O,
e.g. [`READ-BYTE`](http://www.lispworks.com/documentation/HyperSpec/Body/f_rd_by.htm)
and
[`READ-CHAR`](http://www.lispworks.com/documentation/HyperSpec/Body/f_rd_cha.htm)
are in the standard. Some implementations let both functions be called
interchangeably. Others allow either one or the other. (The
[simple stream proposal](https://www.cliki.net/simple-stream) defines the
notion of a _bivalent stream_ where both are possible.)

Varying element-types are useful as some protocols rely on the ability to send
8-Bit output on a channel. E.g. with HTTP, the header is normally ASCII and
ought to use CRLF as line terminators, whereas the body can have the MIME type
application/octet-stream, where CRLF translation would destroy the data. (This
is how the Netscape browser on MS-Windows destroys data sent by incorrectly
configured Webservers which declare unknown files as having MIME type
text/plain - the default in most Apache configurations).

 What follows is a list of implementation dependent choices and behaviours and some code to experiment.

## CLISP

On CLISP, faithful output is possible using

~~~lisp
:external-format
(ext:make-encoding :charset 'charset:iso-8859-1 :line-terminator :unix)
~~~

You can also use `(SETF (STREAM-ELEMENT-TYPE F) '(UNSIGNED-BYTE 8))`, where the
ability to `SETF` is a CLISP-specific extension. Using `:EXTERNAL-FORMAT :UNIX`
will cause portability problems, since the default character set on MS-Windows
is `CHARSET:CP1252`. `CHARSET:CP1252` doesn't allow output of e.g. `(CODE-CHAR
#x81)`:

~~~lisp
;*** - Character #\u0080 cannot be represented in the character set CHARSET:CP1252
~~~

Characters with code > 127 cannot be represented in ASCII:

~~~lisp
;*** - Character #\u0080 cannot be represented in the character set CHARSET:ASCII
~~~

## CMUCL

`:EXTERNAL-FORMAT :DEFAULT` (untested) - no unicode, so probably no problems.

## AllegroCL

`#+(AND ALLEGRO UNIX) :DEFAULT` (untested) - seems enough on UNIX, but would not
work on the MS-Windows port of AllegroCL.

## LispWorks

`:EXTERNAL-FORMAT '(:LATIN-1 :EOL-STYLE :LF)` (confirmed by Marc Battyani)

## Example

Here's some sample code to play with:

~~~lisp
(defvar *unicode-test-file* "faithtest-out.txt")

(defun generate-256 (&key (filename *unicode-test-file*)
			  #+CLISP (charset 'charset:iso-8859-1)
                          external-format)
  (let ((e (or external-format
	       #+CLISP (ext:make-encoding :charset charset :line-terminator :unix))))
    (describe e)
    (with-open-file (f filename :direction :output
		     :external-format e)
      (write-sequence
        (loop with s = (make-string 256)
	      for i from 0 to 255
	      do (setf (char s i) (code-char i))
	      finally (return s))
       f)
      (file-position f))))

;(generate-256 :external-format :default)
;#+CLISP (generate-256 :external-format :unix)
;#+CLISP (generate-256 :external-format 'charset:ascii)
;(generate-256)

(defun check-256 (&optional (filename *unicode-test-file*))
  (with-open-file (f filename :direction :input
		     :element-type '(unsigned-byte 8))
    (loop for i from 0
	  for c = (read-byte f nil nil)
	  while c
	  unless (= c i)
	  do (format t "~&Position ~D found ~D(#x~X)." i c c)
	  when (and (= i 33) (= c 32))
	  do (let ((c (read-byte f)))
	       (format t "~&Resync back 1 byte ~D(#x~X) - cause CRLF?." c c) ))
    (file-length f)))

#| CLISP
(check-256 *unicode-test-file*)
(progn (generate-256 :external-format :unix) (check-256))
; uses UTF-8 -> 385 bytes

(progn (generate-256 :charset 'charset:iso-8859-1) (check-256))

(progn (generate-256 :external-format :default) (check-256))
; uses UTF-8 + CRLF(on MS-Windows) -> 387 bytes

(progn (generate-256 :external-format
  (ext:make-encoding :charset 'charset:iso-8859-1 :line-terminator :mac)) (check-256))
(progn (generate-256 :external-format
  (ext:make-encoding :charset 'charset:iso-8859-1 :line-terminator :dos)) (check-256))
|#
~~~

<a name="bulk"></a>

# Fast Bulk I/O

If you need to copy a lot of data and the source and destination are both
streams (of the same
[element type](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_e.htm#element_type)),
it's very fast to use
[`READ-SEQUENCE`](http://www.lispworks.com/documentation/HyperSpec/Body/f_rd_seq.htm)
and
[`WRITE-SEQUENCE`](http://www.lispworks.com/documentation/HyperSpec/Body/f_wr_seq.htm):

~~~lisp
(let ((buf (make-array 4096 :element-type (stream-element-type input-stream))))
  (loop for pos = (read-sequence buf input-stream)
        while (plusp pos)
        do (write-sequence buf output-stream :end pos)))
~~~

---
title: Files and Directories
---

Note: In this chapter, we use mainly
[namestrings](http://www.lispworks.com/documentation/HyperSpec/Body/19_aa.htm)
to
[specify filenames](http://www.lispworks.com/documentation/HyperSpec/Body/19_.htm). The
issue of
[pathnames](http://www.lispworks.com/documentation/HyperSpec/Body/19_ab.htm)
needs to be covered separately.

Many functions will come from UIOP, so we suggest you have a look directly at it:

* [UIOP/filesystem](https://common-lisp.net/project/asdf/uiop.html#UIOP_002fFILESYSTEM)
* [UIOP/pathname](https://common-lisp.net/project/asdf/uiop.html#UIOP_002fPATHNAME)

Of course, do not miss:

* [Files and File I/O in Practical Common Lisp](http://gigamonkeys.com/book/files-and-file-io.html)


### Testing whether a file exists

Use the function
[`probe-file`](http://www.lispworks.com/documentation/HyperSpec/Body/f_probe_.htm)
which will return a
[generalized boolean](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_g.htm#generalized_boolean) -
either `nil` if the file doesn't exists, or its
[truename](http://www.lispworks.com/documentation/HyperSpec/Body/20_ac.htm)
(which might be different from the argument you supplied).

~~~lisp
$ ln -s /etc/passwd foo

* (probe-file "/etc/passwd")
#p"/etc/passwd"

* (probe-file "foo")
#p"/etc/passwd"

* (probe-file "bar")
NIL
~~~

### Creating directories

The function
[ensure-directories-exist](http://www.lispworks.com/documentation/HyperSpec/Body/f_ensu_1.htm)
creates the directories if they do not exist:

~~~lisp
(ensure-directories-exist "foo/bar/baz/")
~~~

This may create `foo`, `bar` and `baz`. Don't forget the trailing slash.


### Opening a file

Common Lisp has
[`open`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm) and
[`close`](http://www.lispworks.com/documentation/HyperSpec/Body/f_close.htm)
functions which resemble the functions of the same denominator from other
programming languages you're probably familiar with. However, it is almost
always recommendable to use the macro
[`with-open-file`](http://www.lispworks.com/documentation/HyperSpec/Body/m_w_open.htm)
instead. Not only will this macro open the file for you and close it when you're
done, it'll also take care of it if your code leaves the body abnormally (such
as by a use of
[throw](http://www.lispworks.com/documentation/HyperSpec/Body/s_throw.htm)). A
typical use of `with-open-file` looks like this:

~~~lisp
(with-open-file (str <_file-spec_>
    :direction <_direction_>
    :if-exists <_if-exists_>
    :if-does-not-exist <_if-does-not-exist_>)
  <_your code here_>)
~~~

*   `str` is a variable which'll be bound to the stream which is created by
    opening the file.
*   `<_file-spec_>` will be a truename or a pathname.
*   `<_direction_>` is usually `:input` (meaning you want to read from the file),
    `:output` (meaning you want to write to the file) or `:io` (which is for
    reading _and_ writing at the same time) - the default is `:input`.
*   `<_if-exists_>` specifies what to do if you want to open a file for writing
    and a file with that name already exists - this option is ignored if you
    just want to read from the file. The default is `:error` which means that an
    error is signalled. Other useful options are `:supersede` (meaning that the
    new file will replace the old one), `:append` (content is added to the file),
    `nil` (the stream variable will be bound to `nil`),
    and `:rename` (i.e. the old file is renamed).
*   `<_if-does-not-exist_>` specifies what to do if the file you want to open does
    not exist. It is one of `:error` for signalling an error, `:create` for
    creating an empty file, or `nil` for binding the stream variable to
    `nil`. The default is, to be brief, to do the right thing depending on the
    other options you provided. See the CLHS for details.

Note that there are a lot more options to `with-open-file`. See
[the CLHS entry for `open`](http://www.lispworks.com/documentation/HyperSpec/Body/f_open.htm)
for all the details. You'll find some examples on how to use `with-open-file`
below. Also note that you usually don't need to provide any keyword arguments if
you just want to open an existing file for reading.


### Reading files

#### Reading a file into a string or a list of lines

It's quite common to need to access the contents of a file in string
form, or to get a list of lines.

uiop is included in ASDF (there is no extra library to install or
system to load) and has the following functions:


~~~lisp
(uiop:read-file-string "file.txt")
~~~

and

~~~lisp
(uiop:read-file-lines "file.txt")
~~~

*Otherwise*, this can be achieved by using `read-line` or `read-char` functions,
that probably won't be the best solution. The file might not be divided into
multiple lines or reading one character at a time might bring significant
performance problems. To solve this problems, you can read files using buckets
of specific sizes.

~~~lisp
(with-output-to-string (out)
  (with-open-file (in "/path/to/big/file")
    (loop with buffer = (make-array 8192 :element-type 'character)
          for n-characters = (read-sequence buffer in)
          while (< 0 n-characters)
          do (write-sequence buffer out :start 0 :end n-characters)))))
~~~

Furthermore, you're free to change the format of the read/written data, instead
of using elements of type character every time. For instance, you can set
`:element-type` type argument of `with-output-to-string`, `with-open-file` and
`make-array` functions to `'(unsigned-byte 8)` to read data in octets.

#### Reading with an utf-8 encoding

To avoid an `ASCII stream decoding error` you might want to specify an UTF-8 encoding:

~~~lisp
(with-open-file (in "/path/to/big/file"
                     :external-format :utf-8)
                 ...
~~~

#### Set SBCL's default encoding format to utf-8

Sometimes you don't control the internals of a library, so you'd
better set the default encoding to utf-8.  Add this line to your
`~/.sbclrc`:

    (setf sb-impl::*default-external-format* :utf-8)

and optionally

    (setf sb-alien::*default-c-string-external-format* :utf-8)

#### Reading a file one line at a time

[`read-line`](http://www.lispworks.com/documentation/HyperSpec/Body/f_rd_lin.htm)
will read one line from a stream (which defaults to
[_standard input_](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_s.htm#standard_input))
the end of which is determined by either a newline character or the end of the
file. It will return this line as a string _without_ the trailing newline
character. (Note that `read-line` has a second return value which is true if there
was no trailing newline, i.e. if the line was terminated by the end of the
file.) `read-line` will by default signal an error if the end of the file is
reached. You can inhibit this by supplying NIL as the second argument. If you do
this, `read-line` will return `nil` if it reaches the end of the file.

~~~lisp
(with-open-file (stream "/etc/passwd")
  (do ((line (read-line stream nil)
       (read-line stream nil)))
       ((null line))
       (print line)))
~~~

You can also supply a third argument which will be used instead of `nil` to signal
the end of the file:

~~~lisp
(with-open-file (stream "/etc/passwd")
  (loop for line = (read-line stream nil 'foo)
   until (eq line 'foo)
   do (print line)))
~~~

#### Reading a file one character at a time

[`read-char`](http://www.lispworks.com/documentation/HyperSpec/Body/f_rd_cha.htm)
is similar to `read-line`, but it only reads one character as opposed to one
line. Of course, newline characters aren't treated differently from other
characters by this function.

~~~lisp
(with-open-file (stream "/etc/passwd")
  (do ((char (read-char stream nil)
       (read-char stream nil)))
       ((null char))
       (print char)))
~~~

#### Looking one character ahead

You can 'look at' the next character of a stream without actually removing it
from there - this is what the function
[`peek-char`](http://www.lispworks.com/documentation/HyperSpec/Body/f_peek_c.htm)
is for. It can be used for three different purposes depending on its first
(optional) argument (the second one being the stream it reads from): If the
first argument is `nil`, `peek-char` will just return the next character that's
waiting on the stream:

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (print (read-char stream))
           (print (peek-char nil stream))
           (print (read-char stream))
           (values))

#\I
#\'
#\'
~~~

If the first argument is `T`, `peek-char` will skip
[whitespace](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_w.htm#whitespace)
characters, i.e. it will return the next non-whitespace character that's waiting
on the stream. The whitespace characters will vanish from the stream as if they
had been read by `read-char`:

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (print (read-char stream))
           (print (read-char stream))
           (print (read-char stream))
           (print (peek-char t stream))
           (print (read-char stream))
           (print (read-char stream))
           (values))

#\I
#\'
#\m
#\n
#\n
#\o
~~~

If the first argument to `peek-char` is a character, the function will skip all
characters until that particular character is found:

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (print (read-char stream))
           (print (peek-char #\a stream))
           (print (read-char stream))
           (print (read-char stream))
           (values))

#\I
#\a
#\a
#\m
~~~

Note that `peek-char` has further optional arguments to control its behaviour on
end-of-file similar to those for `read-line` and `read-char` (and it will signal an
error by default):

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (print (read-char stream))
           (print (peek-char #\d stream))
           (print (read-char stream))
           (print (peek-char nil stream nil 'the-end))
           (values))

#\I
#\d
#\d
THE-END
~~~

You can also put one character back onto the stream with the function
[`unread-char`](http://www.lispworks.com/documentation/HyperSpec/Body/f_unrd_c.htm). You
can use it as if, _after_ you have read a character, you decide that you'd
better used `peek-char` instead of `read-char`:

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (let ((c (read-char stream)))
             (print c)
             (unread-char c stream)
             (print (read-char stream))
             (values)))

#\I
#\I
~~~

Note that the front of a stream doesn't behave like a stack: You can only put
back exactly _one_ character onto the stream. Also, you _must_ put back the same
character that has been read previously, and you can't unread a character if
none has been read before.

#### Random access to a File

Use the function
[`file-position`](http://www.lispworks.com/documentation/HyperSpec/Body/f_file_p.htm)
for random access to a file. If this function is used with one argument (a
stream), it will return the current position within the stream. If it's used
with two arguments (see below), it will actually change the
[file position](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_f.htm#file_position)
in the stream.

~~~lisp
CL-USER> (with-input-from-string (stream "I'm not amused")
           (print (file-position stream))
           (print (read-char stream))
           (print (file-position stream))
           (file-position stream 4)
           (print (file-position stream))
           (print (read-char stream))
           (print (file-position stream))
           (values))

0
#\I
1
4
#\n
5
~~~

### Writing content to a file

With `with-open-file`, specify `:direction :output` and use `write-sequence` inside:

~~~lisp
(with-open-file (f <pathname> :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create)
    (write-sequence s f)))
~~~

If the file exists, you can also `:append` content to it.

If it doesn't exist, you can `:error` out. See the standard for more details.


The library [str](https://github.com/vindarel/cl-str) has a shortcut:

~~~lisp
(str:to-file "file.txt" content) ;; with optional options
~~~

### Getting the file extension

The file extension is a pathname type in Lisp parlance:

~~~lisp
(pathname-type "~/foo.org")  ;; => "org"
~~~


### Getting file attributes (size, access time,...)

[Osicat](https://www.common-lisp.net/project/osicat/) (in Quicklisp)
is a lightweight operating system interface for Common Lisp on
POSIX-like systems, including Windows. With Osicat we can get and set
**environment variables**, manipulate **files and directories**,
**pathnames** and a bit more.

Once it is installed, Osicat also defines the `osicat-posix` system,
which permits us to get file attributes.

~~~lisp
(ql:quickload :osicat)

(let ((stat (osicat-posix:stat #P"./files.md")))
  (osicat-posix:stat-size stat))  ;; => 10629
~~~

We can get the other attributes with the following methods:

~~~
osicat-posix:stat-dev
osicat-posix:stat-gid
osicat-posix:stat-ino
osicat-posix:stat-uid
osicat-posix:stat-mode
osicat-posix:stat-rdev
osicat-posix:stat-size
osicat-posix:stat-atime
osicat-posix:stat-ctime
osicat-posix:stat-mtime
osicat-posix:stat-nlink
osicat-posix:stat-blocks
osicat-posix:stat-blksize
~~~

### Listing files and directories

Some functions below return pathnames, so you might need the following:

~~~lisp
(namestring #p"/foo/bar/baz.txt")           ==> "/foo/bar/baz.txt"
(directory-namestring #p"/foo/bar/baz.txt") ==> "/foo/bar/"
(file-namestring #p"/foo/bar/baz.txt")      ==> "baz.txt"
~~~


#### Listing files in a directory

~~~lisp
(uiop:directory-files "./")
~~~

Returns a list of pathnames:

```
(#P"/home/vince/projects/cl-cookbook/.emacs"
 #P"/home/vince/projects/cl-cookbook/.gitignore"
 #P"/home/vince/projects/cl-cookbook/AppendixA.jpg"
 #P"/home/vince/projects/cl-cookbook/AppendixB.jpg"
 #P"/home/vince/projects/cl-cookbook/AppendixC.jpg"
 #P"/home/vince/projects/cl-cookbook/CHANGELOG"
 #P"/home/vince/projects/cl-cookbook/CONTRIBUTING.md"
 […]
```

#### Listing sub-directories

~~~lisp
(uiop:subdirectories "./")
~~~

```
(#P"/home/vince/projects/cl-cookbook/.git/"
 #P"/home/vince/projects/cl-cookbook/.sass-cache/"
 #P"/home/vince/projects/cl-cookbook/_includes/"
 #P"/home/vince/projects/cl-cookbook/_layouts/"
 #P"/home/vince/projects/cl-cookbook/_site/"
 #P"/home/vince/projects/cl-cookbook/assets/")
```

#### Traversing (walking) directories

See `uiop/filesystem:collect-sub*directories`. It takes as arguments:

- a `directory`
- a `recursep` function
- a `collectp` function
- a `collector` function

Given a directory, when `collectp` returns true with the directory,
call the `collector` function on the directory, and recurse
each of its subdirectories on which `recursep` returns true.

This function will thus let you traverse a filesystem hierarchy,
superseding the functionality of `cl-fad:walk-directory`.

The behavior in presence of symlinks is not portable. Use IOlib to handle such situations.

Example:

~~~lisp
(defparameter *dirs* nil "All recursive directories.")

(uiop:collect-sub*directories "~/cl-cookbook"
    (constantly t)
    (constantly t)
    (lambda (it) (push it *dirs*)))
~~~

With `cl-fad:walk-directory`, we can also collect files, not only subdirectories:

~~~lisp
(cl-fad:walk-directory "./"
  (lambda (name)
     (format t "~A~%" name))
~~~



#### Finding files matching a pattern

Below we simply list files of a directory and check that their name
contains a given string.

~~~lisp
(remove-if-not (lambda (it)
                   (search "App" (namestring it)))
               (uiop:directory-files "./"))
~~~

```
(#P"/home/vince/projects/cl-cookbook/AppendixA.jpg"
 #P"/home/vince/projects/cl-cookbook/AppendixB.jpg"
 #P"/home/vince/projects/cl-cookbook/AppendixC.jpg")
```

We used `namestring` to convert a `pathname` to a string, thus a
sequence that `search` can deal with.


#### Finding files with a wildcard

We can not transpose unix wildcards to portable Common Lisp.

In pathname strings we can use `*` and `**` as wildcards. This works
in absolute and relative pathnames.

~~~lisp
(directory #P"*.jpg")
~~~

~~~lisp
(directory #P"**/*.png")
~~~


#### Change the default pathname

The concept of `.` denoting the current directory does not exist in
portable Common Lisp. This may exist in specific filesystems and
specific implementations.

Also `~` to denote the home directory does not exist. They may be
recognized by some implementations as non-portable extensions.


`*default-pathname-defaults*`provides a default for some pathname
operations.

~~~lisp
(let ((*default-pathname-defaults* (pathname "/bin/")))
          (directory "*sh"))
(#P"/bin/zsh" #P"/bin/tcsh" #P"/bin/sh" #P"/bin/ksh" #P"/bin/csh" #P"/bin/bash")
~~~

See also `(user-homedir-pathname)`.

---
title: Error and exception handling
---

Common Lisp has mechanisms for error and condition handling as found
in other languages, and can do more.

What is a condition ?

> Just like in languages that support exception handling (Java, C++,
> Python, etc.), a condition represents, for the most part, an
> “exceptional” situation. However, even more so that those languages,
> *a condition in Common Lisp can represent a general situation where
> some branching in program logic needs to take place*, not
> necessarily due to some error condition. Due to the highly
> interactive nature of Lisp development (the Lisp image in
> conjunction with the REPL), this makes perfect sense in a language
> like Lisp rather than say, a language like Java or even Python,
> which has a very primitive REPL. In most cases, however, we may not
> need (or even allow) the interactivity that this system offers
> us. Thankfully, the same system works just as well even in
> non-interactive mode.
>
> [z0ltan](https://z0ltan.wordpress.com/2016/08/06/conditions-and-restarts-in-common-lisp/)


Let's dive into it step by step. More resources are given afterwards.


## Ignore all errors (and return nil)

Sometimes you know that a function can fail and you just want to
ignore it: use `ignore-errors`:

~~~lisp
(ignore-errors
  (/ 3 0))
; in: IGNORE-ERRORS (/ 3 0)
;     (/ 3 0)
;
; caught STYLE-WARNING:
;   Lisp error during constant folding:
;   arithmetic error DIVISION-BY-ZERO signalled
;   Operation was (/ 3 0).
;
; compilation unit finished
;   caught 1 STYLE-WARNING condition
NIL
#<DIVISION-BY-ZERO {1008FF5F13}>
~~~

We get a welcome `division-by-zero` warning but the code runs well and
it returns two things: `nil` and the condition that was signaled. We
could not choose what to return.

Remember that we can `inspect` the condition with a right click in Slime.


## Catching any condition - handler-case

<!-- we will say "handling" for handler-bind -->

`ignore-error` is built from `handler-case`. We can write the previous
example by catching the general `error` but now we can return whatever
we want:

~~~lisp
(handler-case (/ 3 0)
  (error (c)
    (format t "We caught a condition.~&")
    (values 0 c)))
; in: HANDLER-CASE (/ 3 0)
;     (/ 3 0)
;
; caught STYLE-WARNING:
;   Lisp error during constant folding:
;   Condition DIVISION-BY-ZERO was signalled.
;
; compilation unit finished
;   caught 1 STYLE-WARNING condition
We caught a condition.
0
#<DIVISION-BY-ZERO {1004846AE3}>
~~~

We also returned two values, 0 and the signaled condition.

The general form of `handler-case` is

~~~lisp
(handler-case (code that errors out)
   (condition-type (the-condition) ;; <-- optional argument
      (code))
   (another-condition (the-condition)
       ...))
~~~

We can also catch all conditions by matching `t`, like in a `cond`:

~~~lisp
(handler-case
    (progn
      (format t "This won't work…~%")
      (/ 3 0))
  (t (c)
    (format t "Got an exception: ~a~%" c)
    (values 0 c)))
;; …
;; This won't work…
;; Got an exception: arithmetic error DIVISION-BY-ZERO signalled
;; Operation was (/ 3 0).
;; 0
;; #<DIVISION-BY-ZERO {100608F0F3}>
~~~


## Catching a specific condition

We can specify what condition to handle:

~~~lisp
(handler-case (/ 3 0)
  (division-by-zero (c)
    (format t "Caught division by zero: ~a~%" c)))
;; …
;; Caught division by zero: arithmetic error DIVISION-BY-ZERO signalled
;; Operation was (/ 3 0).
;; NIL
~~~

This workflow is similar to a try/catch as found in other languages, but we can do more.


### Ignoring the condition argument

If you don't access the condition object in your handlers, but you still keep it has an argument for good practice, you'll see this compiler warning often:

```
; caught STYLE-WARNING:
;   The variable C is defined but never used.
```

To remove it, use a `declare` call as in:

~~~lisp
(handler-case (/ 3 0)
  (division-by-zero (c)
   (declare (ignore c))
   (format t "Caught division by zero~%"))) ;; we don't print "c" here and don't get the warning.
~~~

## handler-case VS handler-bind

`handler-case` is similar to the `try/catch` forms that we find in
other languages.

`handler-bind` (see the next examples), is what to use
when we need absolute control over what happens when a signal is
raised. It allows us to use the debugger and restarts, either
interactively or programmatically.

If some library doesn't catch all conditions and lets some bubble out
to us, we can see the restarts (established by `restart-case`)
anywhere deep in the stack, including restarts established by other
libraries whose this library called.  And *we can see the stack
trace*, with every frame that was called and, in some lisps, even see
local variables and such. Once we `handler-case`, we "forget" about
this, everything is unwound. `handler-bind` does *not* rewind the
stack.


## Handling conditions - handler-bind

Here we use `handler-bind`.

Its general form is:

~~~lisp
(handler-bind ((a-condition #'function-to-handle-it)
               (another-one #'another-function))
    (code that can...)
    (...error out))
~~~

So, our simple example:

~~~lisp
(handler-bind
             ((division-by-zero #'(lambda (c) (format t "hello condition~&"))))
           (/ 3 0))
~~~

This prints some warnings, then it prints our "hello" *and still
enters the debugger*. If we don't want to enter the debugger, we have
to define a restart and invoke it.


A real example with the
[`unix-opts`](https://github.com/mrkkrp/unix-opts) library, that
parses command line arguments. It defined some conditions:
`unknown-option`, `missing-arg` and `arg-parser-failed`, and it is up
to use to write what to do in these cases.

~~~lisp
(handler-bind ((opts:unknown-option #'unknown-option)
               (opts:missing-arg #'missing-arg)
               (opts:arg-parser-failed #'arg-parser-failed))
  (opts:get-opts))
~~~

Our `unknown-option` function is simple and looks like this:

~~~lisp
(defun unknown-option (condition)
  (format t "~s option is unknown.~%" (opts:option condition))
  (opts:describe)
  (exit)) ;; <-- we return to the command line, no debugger.
~~~

it takes the condition as parameter, so we can read information from
it if needed. Here we get the name of the erroneous option with the
defined reader `(opts:option condition)` (see below).


## Defining and making conditions

We define conditions with `define-condition` and we make (initialize) them with `make-condition`.

<!-- todo: differences ? -->

<!-- make-condition ? -->

~~~lisp
(define-condition my-division-by-zero (error) ())
~~~

Conditions are not classes (though we can specialize on them with `defmethod`) and we cannot use `defclass` to define them and we must instead use `define-condition`.

~~~lisp
(define-condition my-division-by-zero (error)
  ((dividend :initarg :dividend
            :reader dividend)) ;; <= so we'll get the dividend with (dividend condition), as soon as on the next line.
  ;; the :report is the message into the debugger:
  (:report (lambda (condition stream) (format stream "You were going to divide ~a by zero.~&" (dividend condition)))))
~~~

The general form looks like a regular class definition:

~~~lisp
(define-condition my-condition (condition-it-inherits-from)
  ;; list of arguments, can be "()".
  ((message :initarg :message
            :reader my-condition-message)
   (second ...))
  ;; class arguments
  (:report (lambda (condition stream) (...))) ;; what is printed in the REPL.
  (:documentation "a string")) ;; good practice ;)
~~~

Now when we throw this condition we must pass it an error message (it
is a required argument), and read it with `error-message` (the
`:reader`).

What's in `:report` will be printed in the REPL.

Let's try our condition. We define a simple function that checks our
divisor, and signals our condition if it is equal to zero:

~~~lisp
(defun my-division (x y)
    (if (= y 0)
        (error 'MY-DIVISION-BY-ZERO :dividend x))
    (/ x y))
~~~

When we use it, we enter the debugger:

~~~lisp
(my-division 3 0)
;;
;; into the debugger:
;;
You were going to divide 3 by zero.
   [Condition of type MY-DIVISION-BY-ZERO]

Restarts:
 0: [RETRY] Retry SLIME REPL evaluation request.
 1: [*ABORT] Return to SLIME's top level.
 2: [ABORT] abort thread (#<THREAD "repl-thread" RUNNING {1002957FA3}>)

Backtrace:
  0: (MY-DIVISION 3 0)
~~~

We can inspect the backtrace, go to the source (`v` in Slime), etc.

---

Here is how `unix-opts` defines its `unknown-option` condition:

~~~lisp
(define-condition troublesome-option (simple-error)
  ((option
    :initarg :option
    :reader option))
  (:report (lambda (c s) (format s "troublesome option: ~s" (option c))))
  (:documentation "Generalization over conditions that have to do with some
particular option."))

(define-condition unknown-option (troublesome-option)
  ()
  (:report (lambda (c s) (format s "unknown option: ~s" (option c))))
  (:documentation "This condition is thrown when parser encounters
unknown (not previously defined with `define-opts`) option."))
~~~


## Signaling (throwing) conditions

We can use `error`, like we did above, in two ways:

- `(error "some text")`: signals a condition of type `simple-error`
- `(error 'my-error :message "We did this and this and it didn't work.")`

Throwing these conditions will enter the interactive debugger,
where the user may select a restart. Use `signal` if you do not want to enter the debugger if your program failed to handle the condition.

Simple example from `unix-opts`: it adds information into the `option` slot:

~~~lisp
(error 'unknown-option
        :option opt)
~~~


## Restarts, interactive choices in the debugger

### Defining restarts

Restarts are the choices we get in the debugger, which always has the
`RETRY` and `ABORT` ones. We can add choices to the top of the list:

~~~lisp
(defun division-restarter ()
  (restart-case (/ 3 0)
    (return-zero () 0)
    (divide-by-one () (/ 3 1))))
~~~

By calling this stupid function we get two new choices at the top of the debugger:

![](simple-restarts.png)

Note: read in lisper.in's blogpost on csv parsing (see Resources) how
this system was used effectively in production.

But that's not all, by handling restarts we can start over the
operation as if the error didn't occur (as seen in the stack).

### Calling restarts programmatically

With `invoke-restart`.

~~~lisp
(defun division-and-bind ()
           (handler-bind
               ((error (lambda (c)
                         (format t "Got error: ~a~%" c) ;; error-message
                         (format t "and will divide by 1~&")
                         (invoke-restart 'divide-by-one))))
             (division-restarter)))
;; (DIVISION-AND-BIND)
;; Got error: arithmetic error DIVISION-BY-ZERO signalled
;; and will divide by 1
;; Operation was (/ 3 0).
;; 3
~~~

Note that we called the form that contains our restarts
(`division-restarter`) and not the function that throws the error.

### Using other restarts

`find-restart 'name-of-restart` will return the most recent bound
restart with the given name, or `nil`. We can invoke it with
`invoke-restart`.

### Prompting the user to enter a new value

Let's add a restart in our `division-restarter` to offer the user to
enter a new dividend, and run the division again.

~~~lisp
(defun division-restarter ()
  (restart-case (/ 3 0)
    (return-nil () nil)
    (divide-by-one () (/ 3 1))
    (choose-another-dividend (new-dividend)
      :report "Please choose another dividend"
      :interactive (lambda ()
                     (format t "Enter a new dividend: ")
                     (list (read))) ;; <-- must return a list.
      (format t "New division: 3/~a = ~a~&" new-dividend (/ 3 new-dividend)))))
~~~

We get prompted in the debugger:

```
arithmetic error DIVISION-BY-ZERO signalled
Operation was (/ 3 0).
  [Condition of type DIVISION-BY-ZERO]

Restarts:
 0: [RETURN-NIL] RETURN-NIL
 1: [DIVIDE-BY-ONE] DIVIDE-BY-ONE
 2: [CHOOSE-ANOTHER-DIVIDEND] Please choose another dividend <-- new
 3: [RETRY] Retry SLIME REPL evaluation request.
 4: [*ABORT] Return to SLIME's top level.
 5: [ABORT] abort thread (#<THREAD "repl-thread" RUNNING {1002A47FA3}>)
```

The new `choose-another-dividend` restart takes an argument for the
new dividend, that will be fed by the `:interactive` lambda, which
`read`s for user input and must return a list.

We use it like this:

~~~lisp
(division-restarter)
;;
;; Entered debugger, chose the 2nd restart.
;;
Enter a new dividend: 10  <-- got prompted to enter a new value.
New division: 3/10 = 3/10
NIL
~~~

In a real situation we might want to call our "restarter" recursively,
to get into the debugger again if we enter a bad value.

### Hide and show restarts

Restarts can be hidden. In `restart-case`, in addition to `:report`
and `:interactive`, they also accept a `:test` key:

~~~lisp
(restart-case
   (return-zero ()
     :test (lambda ()
             (some-test))
    ...
~~~

## Run some code, condition or not ("finally")

The "finally" part of others `try/catch/finally` forms is done with `unwind-protect`.

It is the construct used in "with-" macros, like `with-open-file`,
which always closes the file after it.


You're now more than ready to write some code and to dive into other resources !


## Resources

* [Practical Common Lisp: "Beyond Exception Handling: Conditions and Restarts"](http://gigamonkeys.com/book/beyond-exception-handling-conditions-and-restarts.html) - the go-to tutorial, more explanations and primitives.
* Common Lisp Recipes, chap. 12, by E. Weitz
* [language reference](https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node317.html)
* [lisper.in](https://lisper.in/restarts#signaling-validation-errors) - example with parsing a csv file and using restarts with success, [in a flight travel company](https://www.reddit.com/r/lisp/comments/7k85sf/a_tutorial_on_conditions_and_restarts/drceozm/).
* [Condition Handling in the Lisp family of languages](http://www.nhplace.com/kent/Papers/Condition-Handling-2001.html)
* [z0ltan.wordpress.com](https://z0ltan.wordpress.com/2016/08/06/conditions-and-restarts-in-common-lisp/)
* [https://github.com/svetlyak40wt/python-cl-conditions](https://github.com/svetlyak40wt/python-cl-conditions) - implementation of the CL conditions system in Python.

## See also

* [Algebraic effects - You can touch this !](http://jacek.zlydach.pl/blog/2019-07-24-algebraic-effects-you-can-touch-this.html) - how to use conditions and restarts to implement progress reporting and aborting of a long-running calculation, possibly in an interactive or GUI context.
* [A tutorial on conditions and restarts](https://github.com/stylewarning/lisp-random/blob/master/talks/4may19/root.lisp),  based around computing the roots of a real function. It was presented by the author at a Bay Area Julia meetup on may 2019 ([talk slides here](https://github.com/stylewarning/talks/blob/master/4may19-julia-meetup/Bay%20Area%20Julia%20Users%20Meetup%20-%204%20May%202019.pdf)).

---
title: Packages
---

See: [The Complete Idiot's Guide to Common Lisp Packages][guide]

# List all Symbols in a Package

Common Lisp provides some macros to iterate through the symbols of a
package. The two most interesting are:
[`DO-SYMBOLS` and `DO-EXTERNAL-SYMBOLS`][do-sym]. `DO-SYMBOLS` iterates over the
symbols accessible in the package and `DO-EXTERNAL-SYMBOLS` only iterates over
the external symbols (you can see them as the real package API).

To print all exported symbols of a package named "PACKAGE", you can write:

~~~lisp
(do-external-symbols (s (find-package "PACKAGE"))
  (print s))
~~~

You can also collect all these symbols in a list by writing:

~~~lisp
(let (symbols)
  (do-external-symbols (s (find-package "PACKAGE"))
    (push s symbols))
  symbols)
~~~

Or you can do it with [`LOOP`][loop].

~~~lisp
(loop for s being the external-symbols of (find-package "PACKAGE")
  collect s)
~~~

# Package nickname

## Nickname Provided by Packages

When defining a package, it is trivial to give it a nickname for better user
experience. The following example is a snippet of `PROVE` package:

~~~lisp
(defpackage prove
  (:nicknames :cl-test-more :test-more)
  (:export :run
           :is
           :ok)
~~~

Afterwards, a user may use nickname instead of the package name to refer to this
package. For example:

~~~lisp
(prove:run)
(cl-test-more:is)
(test-more:ok)
~~~

Please note that although Common Lisp allows defining multiple nicknames for
one package, too many nicknames may bring maintenance complexity to the
users. Thus the nicknames shall be meaningful and straightforward. For
example:

~~~lisp
(defpackage #:iterate
  (:nicknames #:iter))

(defpackage :cl-ppcre
  (:nicknames :ppcre)
~~~

## Give a Package a Local Nickname

Sometimes it is handy to give a local name to an imported package to save some
typing, especially when the imported package does not provide nice nicknames.

This can be achieved by using [`RENAME-PACKAGE`][rename-package]. For example:

~~~lisp
(asdf:load-system :cl-ppcre)

(defpackage :mypackage
  (:use :cl))
(in-package :mypackage)

;; Add nickname :RE to package :CL-PPCRE.
(rename-package :cl-ppcre :cl-ppcre '(re))

;; You can use :RE instead of :CL-PPCRE now.
(re:scan "a" "abc")
~~~

The function head of `RENAME-PACKAGE` is as follows:

~~~lisp
(rename-package package-designator name &optional (nicknames nil))
~~~

As you may guess, this function can be used to rename a package by giving it a
new *name* instead of a new *nickname*.

However, it is highly recommended **not** to do so. Because otherwise the
package will be modified and the original name (`:CL-PPCRE` in the example
above) will **not** be available any more after renaming. For example:

~~~lisp
;; DO NOT DO THIS!
(rename-package :cl-ppcre :re)

;; ERROR!
(cl-ppcre:scan "a" "abc")
~~~

This might cause problems if you want to load another package that relies on
the modified package.

## Package locks

The package `common-lisp` and SBCL internal implementation packages are locked
by default, including `sb-ext`.

In addition, any user-defined package can be declared to be locked so that it
cannot be modified by the user. Attempts to change its symbol table or
redefine functions which its symbols name result in an error.

More detailed information can be obtained from documents of
[SBCL][sbcl-package-lock] and [CLisp][clisp-package-lock].

For example, if you try the following code:

~~~lisp
(asdf:load-system :alexandria)
(rename-package :alexandria :alex)
~~~

You will get the following error (on SBCL):

~~~
Lock on package ALEXANDRIA violated when renaming as ALEX while
in package COMMON-LISP-USER.
   [Condition of type PACKAGE-LOCKED-ERROR]
See also:
  SBCL Manual, Package Locks [:node]

Restarts:
 0: [CONTINUE] Ignore the package lock.
 1: [IGNORE-ALL] Ignore all package locks in the context of this operation.
 2: [UNLOCK-PACKAGE] Unlock the package.
 3: [RETRY] Retry SLIME REPL evaluation request.
 4: [*ABORT] Return to SLIME's top level.
 5: [ABORT] abort thread (#<THREAD "repl-thread" RUNNING {10047A8433}>)

...
~~~

If a modification is required anyway, a package named
[cl-package-lock][cl-package-lock] can be used to ignore package locks. For
example:

~~~lisp
(cl-package-locks:without-package-locks
  (rename-package :alexandria :alex))
~~~

[guide]: http://www.flownet.com/gat/packages.pdf
[do-sym]: http://www.lispworks.com/documentation/HyperSpec/Body/m_do_sym.htm
[loop]: http://www.lispworks.com/documentation/HyperSpec/Body/06_a.htm
[rename-package]: http://www.lispworks.com/documentation/HyperSpec/Body/f_rn_pkg.htm
[sbcl-package-lock]: http://www.sbcl.org/manual/#Package-Locks
[clisp-package-lock]: https://clisp.sourceforge.io/impnotes/pack-lock.html
[cl-package-lock]: https://www.cliki.net/CL-PACKAGE-LOCKS

---
title: Macros
---


<a name="LtohTOCentry-1"></a>

## How Macros Work

The word _macro_ is used generally in computer science to mean a syntactic extension to a programming language. (Note: The name comes from the word "macro-instruction," which was a useful feature of many second-generation assembly languages. A macro-instruction looked like a single instruction, but expanded into a sequence of actual instructions. The basic idea has since been used many times, notably in the C preprocessor. The name "macro" is perhaps not ideal, since it connotes nothing relevant to what it names, but we're stuck with it.) Although many languages have a macro facility, none of them are as powerful as Lisp's. The basic mechanism of Lisp macros is simple, but has subtle complexities, so learning your way around it takes a bit of practice.

A macro is an ordinary piece of Lisp code that operates on _another piece of putative Lisp code,_ translating it into (a version closer to) executable Lisp. That may sound a bit complicated, so let's give a simple example. Suppose you want a version of [`setq`](http://www.lispworks.com/documentation/HyperSpec/Body/s_setq.htm) that sets two variables to the same value. So if you write

~~~lisp
(setq2 x y (+ z 3))
~~~

when `z`=8 both `x` and `y` are set to 11. (I can't think of any use for this, but it's just an example.)

It should be obvious that we can't define `setq2` as a function. If `x`=50 and `y`=_-5_, this function would receive the values 50, _-5_, and 11; it would have no knowledge of what variables were supposed to be set. What we really want to say is, When you (the Lisp system) see <code>(setq2 <i>v<sub>1</sub></i> <i>v<sub>2</sub></i> <i>e</i>)</code>, treat it as equivalent to <code>(progn (setq <i>v<sub>1</sub></i> <i>e</i>) (setq <i>v<sub>2</sub></i> <i>e</i>))</code>. Actually, this isn't quite right, but it will do for now. A macro allows us to do precisely this, by specifying a program for transforming the input pattern <code>(setq2 <i>v<sub>1</sub></i> <i>v<sub>2</sub></i> <i>e</i>)</code> into the output pattern `(progn ...)`.

Here's how we could define the `setq2` macro:

~~~lisp
(defmacro setq2 (v1 v2 e)
  (list 'progn (list 'setq v1 e) (list 'setq v2 e)))
~~~

This is very close to the following function definition:

~~~lisp
(defun setq2F (v1 v2 e)
  (list 'progn (list 'setq v1 e) (list 'setq v2 e)))
~~~

If we evaluated `(setq2F 'x 'y '(+ z 3))`, we would get `(progn (setq x (+ z 3)) (setq y (+ z 3)))`. This is a perfectly ordinary Lisp computation, whose sole point of interest is that its output is a piece of executable Lisp code. What `defmacro` does is create this function implicitly and arrange that whenever an expression of the form `(setq2 x y (+ z 3))` is seen, `setq2F` is called with the pieces of the form as arguments, namely `x`, `y`, and `(+ z 3)`. The resulting piece of code then replaces the call to `setq2`, and execution resumes as if the new piece of code had occurred in the first place. The macro form is said to _expand_ into the new piece of code.

This is all there is to it, except, of course, for the myriad subtle consequences. The main consequence is that _run time for the `setq2` macro is compile time for its context._ That is, suppose the Lisp system is compiling a function, and midway through it finds the expression `(setq2 x y (+ z 3))`. The job of the compiler is, of course, to translate source code into something executable, such as machine language or perhaps byte code. Hence it doesn't execute the source code, but operates on it in various mysterious ways. However, once the compiler sees the `setq2` expression, it must suddenly switch to executing the body of the `setq2` macro. As I said, this is an ordinary piece of Lisp code, which can in principle do anything any other piece of Lisp code can do. That means that when the compiler is running, the entire Lisp (run-time) system must be present.

Novices often make the following sort of mistake. Suppose that the `setq2` macro needs to do some complex transformation on its `e` argument before plugging it into the result. Suppose this transformation can be written as a Lisp procedure `comtran`. The novice will often write:

~~~lisp
(defmacro setq2 (v1 v2 e)
  (let ((e1 (comtran e)))
    (list 'progn (list 'setq v1 e1) (list 'setq v2 e1))))

(defmacro comtran (exp) ...) ;; _Wrong!_
~~~

The mistake is to suppose that once a macro is called, the Lisp system enters a "macro world," so naturally everything in that world must be defined using `defmacro`. This is the wrong picture. The right picture is that `defmacro` enables a step into the _ordinary Lisp world_, but in which the principal object of manipulation is Lisp code. Once that step is taken, one uses ordinary Lisp function definitions:

~~~lisp
(defmacro setq2 (v1 v2 e)
  (let ((e1 (comtran e)))
    (list 'progn (list 'setq v1 e1) (list 'setq v2 e1))))

(defun comtran (exp) ...) ;; _Right!_
~~~

One possible explanation for this mistake may be that in other languages, such as C, invoking a preprocessor macro _does_ get you into a different world; you can't run an arbitrary C program. It might be worth pausing to think about what it might mean to be able to.

Another subtle consequence is that we must spell out how the arguments to the macro get distributed to the hypothetical behind-the-scenes function (called `setq2F` in my example). In most cases, it is easy to do so: In defining a macro, we use all the usual `lambda`-list syntax, such as `&optional`, `&rest`, `&key`, but what gets bound to the formal parameters are pieces of the macro form, not their values (which are mostly unknown, this being compile time for the macro form). So if we defined a macro thus:

~~~lisp
(defmacro foo (x &optional y &key (cxt 'null)) ...)
~~~

then

_If we call it thus ..._     |_The parameters' values are ..._
-----------------------------|-----------------------------------
`(foo a)`                    |`x=a, y=nil, cxt=null`
`(foo (+ a 1) (- y 1))`      |`x=(+ a 1), y=(- y 1), cxt=null`
`(foo a b :cxt (zap zip))`   |`x=a, y=b, cxt=(zap zip)`


Note that the values of the variables are the actual expressions `(+ a 1)` and `(zap zip)`. There is no requirement that these expressions' values be known, or even that they have values. The macro can do anything it likes with them. For instance, here's an even more useless variant of `setq`: <code>(setq-reversible <i>e<sub>1</sub></i> <i>e<sub>2</sub></i> <i>d</i>)</code> behaves like <code>(setq <i>e<sub>1</sub></i> <i>e<sub>2</sub></i>)</code> if <i>d=</i><code>:normal</code>, and behaves like <code>(setq <i>e<sub>2</sub></i> <i>e<sub>1</sub></i>)</code> if _d=_`:backward`. It could be defined thus:

~~~lisp
(defmacro setq-reversible (e1 e2 d)
  (case d
    (:normal (list 'setq e1 e2))
    (:backward (list 'setq e2 e1))
    (t (error ...))))
~~~


<a name="LtohTOCentry-2"></a>

## Backquote

Before taking another step, we need to introduce a piece of Lisp notation that is indispensable to defining macros, even though technically it is quite independent of macros. This is the _backquote facility_. As we saw above, the main job of a macro, when all is said and done, is to define a piece of Lisp code, and that means evaluating expressions such as `(list 'prog (list 'setq ...) ...)`. As these expressions grow in complexity, it becomes hard to read them and write them. What we find ourselves wanting is a notation that provides the skeleton of an expression, with some of the pieces filled in with new expressions. That's what backquote provides. Instead of the the `list` expression given above, one writes

~~~lisp
`(progn (setq ,v1 ,e) (setg ,v2 ,e))
~~~

The backquote (`) character signals that in the expression that follows, every subexpression _not_ preceded by a comma is to be quoted, and every subexpression preceded by a comma is to be evaluated.

That's mostly all there is to backquote. There are just two extra items to point out. First, if you write "`,@e`" instead of "`,e`" then the value of _e_ is _spliced_ into the result. So if `v=(oh boy)`, then `(zap ,@v ,v)` evaluates to `(zap oh boy (oh boy))`. The second occurrence of `v` is replaced by its value. The first is replaced by the elements of its value. If `v` had had value `()`, it would have disappeared entirely: the value of `(zap ,@v ,v)` would have been `(zap ())`, which is the same as `(zap nil)`.

Second, one might wonder what happens if a backquote expression occurs inside another backquote. The answer is that the backquote becomes essentially unreadable and unwriteable; using nested backquote is usually a tedious debugging exercise. The reason, in my not-so-humble opinion, is that backquote is defined wrong. A comma pairs up with the innermost backquote when the default should be that it pairs up with the outermost. But this is not the place for a rant or tutorial; consult your favorite Lisp reference for the exact behavior of nested backquote plus some examples.

One problem with backquote is that once you learn it you tend to use for every list-building occasion. For instance, you might write

~~~lisp
(mapcan (lambda (x)
          (cond ((symbolp x) `((,x)))
                ((> x 10) `(,x ,x))
                (t '())))
        some-list)
~~~

which yields `((a) 15 15)` when `some-list` = `(a 6 15)`. The problem is that [`mapcan`](http://www.lispworks.com/documentation/HyperSpec/Body/f_mapc_.htm) destructively alters the results returned by the [`lambda`](http://www.lispworks.com/documentation/HyperSpec/Body/s_lambda.htm)-expression. Can we be sure that the lists returned by that expression are "[fresh](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_f.htm#fresh)," that is, they are different (in the [`eq`](http://www.lispworks.com/documentation/HyperSpec/Body/f_eq.htm) sense) from the structures returned on other calls of that `lambda` expression? In the present case, close analysis will show that they must be fresh, but in general backquote is not obligated to return a fresh list every time (whether it does or not is implementation-dependent). If the example above got changed to

~~~lisp
(mapcan (lambda (x)
          (cond ((symbolp x) `((,x)))
                ((> x 10) `(,x ,x))
                ((>= x 0) `(low))
                (t '())))
        some-list)
~~~

then backquote may well treat `` `(low)`` as if it were  `'(low)`; the list will be allocated at load time, and every time the `lambda` is evaluated, that same chunk of storage will be returned. So if we evaluate the expression with `some-list` = `(a 6 15)`, we will get `((a) low 15 15)`, but as a side effect the constant `(low)` will get clobbered to become `(low 15 15)`. If we then evaluate the expression with, say, `some-list` = `(8 oops)`, the result will be `(low 15 15 (oops))`, and now the "constant" that started off as `'(low)` will be `(low 15 15 (oops))`. (Note: The bug exemplified here takes other forms, and has often bit newbies - as well as experienced programmers - in the ass. The general form is that a constant list is produced as the value of something that is later destructively altered. The first line of defense against this bug is never to destructively alter any list. For newbies, this is also the last line of defense. For those of us who imagine we're more sophisticated, the next line of defense is to think very carefully any time you use [`nconc`](http://www.lispworks.com/documentation/HyperSpec/Body/f_nconc.htm) or `mapcan`.)

To fix the bug, you can write `(map 'list ...)` instead of `mapcan`. However, if you are determined to use `mapcan`, write the expression this way:

~~~lisp
(mapcan (lambda (x)
          (cond ((symbolp x) (list `(,x)))
                ((> x 10) (list x x))
                ((>= x 0) (list 'low))
                (t '())))
        some-list)
~~~

My personal preference is to use backquote _only_ to build S-expressions, that is, hierarchical expressions that consist of symbols, numbers, and strings, and that are not conceptualized as changing in length. For instance, I would never write

~~~lisp
(setq sk `(,x ,@sk))
~~~

If `sk` is being used as a stack, that is, it's going to be [`pop`](http://www.lispworks.com/documentation/HyperSpec/Body/m_pop.htm)ped in the normal course of things, I would write tt (push x sk). If not, I would write `(setq sk (cons x sk))`.<a name="LtohTOCentry-3"></a>


## Getting Macros Right

I said in [the first section](#LtohTOCentry-1) that my definition of `setq2` wasn't quite right, and now it's time to fix it.

Suppose we write `(setq2 x y (+ x 3))`, when `x`_=8_. Then according to the definition given above, this form will expand into `(progn (setq x (+ x 3)) (setq y (+ x 3)))`, so that `x` will have value 11 and `y` will have value 14\. Chances are that isn't what the macro is expected to do (although you never know). Another problematic case is `(setq2 x y (pop l))`, which causes `l` to be popped twice; again, probably not right.

The solution is to evaluate `e` just once, save it in a temporary variable, and then set `v1` and `v2` to it. To make temporary variables, we use the `gensym` function, which returns a fresh variable guaranteed to appear nowhere else. Here is what the macro should look like:

~~~lisp
(defmacro setq2 (v1 v2 e)
  (let ((tempvar (gensym)))
    `(let ((,tempvar ,e))
       (progn (setq ,v1 ,tempvar)
              (setq ,v2 ,tempvar)))))
~~~

Now `(setq2 x y (+ x 3))` expands to

~~~lisp
(let ((#:g2003 (+ x 3)))
  (progn (setq x #:g2003) (setq y #:g2003)))
~~~

Here `gensym` has returned the symbol `#:g2003`, which prints in this funny way because it won't be recognized by the reader. (Nor is there any need for the reader to recognize it, since it exists only long enough for the code that contains it to be compiled.)

Exercise: Verify that this new version works correctly for the case `(setq2 x y (pop l1))`.

Exercise: Try writing the new version of the macro without using backquote. If you can't do it, you have done the exercise correctly, and learned what backquote is for!

The moral of this section is to think carefully about which expressions in a macro get evaluated and when. Be on the lookout for situations where the same expression gets plugged into the output twice (as `e` was in my original macro design). For complex macros, watch out for cases where the order that expressions are evaluated differs from the order in which they are written. This is sure to trip up some user of the macro - even if you are the only user.<a name="LtohTOCentry-4"></a>

### What Macros are For

Macros are for making syntactic extensions to Lisp. One often hears it said that macros are a bad idea, that users can't be trusted with them, and so forth. Balderdash. It is just as reasonable to extend a language syntactically as to extend it by defining your own procedures. It may be true that the casual reader of your code can't understand the code without seeing the macro definitions, but then the casual reader can't understand it without seeing function definitions either. Having [`defmethod`](http://www.lispworks.com/documentation/HyperSpec/Body/m_defmet.htm)s strewn around several files contributes far more to unclarity than macros ever have, but that's a different diatribe.

Before surveying what sorts of syntactic extensions I have found useful, let me point out what sorts of syntactic extensions are generally _not_ useful, or best accomplished using means other than macros. Some novices think macros are useful for open-coding functions. So, instead of defining

~~~lisp
(defun sqone (x)
  (let ((y (+ x 1))) (* y y)))
~~~

they might define

~~~lisp
(defmacro sqone (x)
  `(let ((y (+ ,x 1))) (* y y)))
~~~

So that `(sqone (* z 13))` might expand into

~~~lisp
(let ((y (+ (* z 13) 1)))
  (* y y))
~~~

This is correct, but a waste of effort. For one thing, the amount of time saved is almost certainly negligible. If it's really important that `sqone` be expanded inline, one can put `(declaim (inline sqone))` before `sqone` is defined (although the compiler is not obligated to honor this declaration). For another, once `sqone` is defined as a macro, it becomes impossible to write `(mapcar #'sqone ll)`, or to do anything else with it except call it.

But macros have a thousand and one legitimate uses. Why write `(lambda (x) ...)` when you can write `(\\ (x) ...)`? Just define `\\` as a macro: <code>(defmacro \\ (&rest l) `(lambda ,@l))</code>.

Many people find `mapcar` and `mapcan` a bit too obscure, especially when used with large `lambda` expressions. Rather than write something like

~~~lisp
(mapcar (lambda (x)
          (let ((y (hairy-fun1 x)) (z (hairy-fun2 x)))
            (dolist (y1 y)
              (dolist (z1 z)
                _... and further meaningless_
                _space-filling nonsense..._
                ))))
        l)
~~~

we might prefer to write

~~~lisp
(for (x :in l)
     (let ((y (hairy-fun1 x)) (z (hairy-fun2 x)))
       (dolist (y1 y)
         (dolist (z1 z)
           _... and further meaningless_
           _space-filling nonsense..._
           ))))
~~~

This macro might be defined thus:

~~~lisp
(defmacro for (listspec exp)
  (cond ((and (= (length listspec) 3)
              (symbolp (car listspec))
              (eq (cadr listspec) ':in))
         `(mapcar (lambda (,(car listspec))
                    ,exp)
                  ,(caddr listspec)))
        (t (error "Ill-formed: %s" `(for ,listspec ,exp)))))
~~~

(This is a simplified version of a macro by Chris Riesbeck.)

It's worth stopping for a second to discuss the role the keyword `:in` plays in this macro. It serves as a sort of "local syntax marker," in that it has no meaning as far as Lisp is concerned, but does serve as a syntactic guidepost for the macro itself. I will refer to these markers as _guide symbols_. (Here its job may seem trivial, but if we generalized the `for` macro to allow multiple list arguments and an implicit `progn` in the body the `:in`s would be crucial in telling us where the arguments stopped and the body began.)

It is not strictly necessary for the guide symbols of a macro to be in the [keyword package](http://www.lispworks.com/documentation/HyperSpec/Body/11_abc.htm), but it is a good idea, for two reasons. First, they highlight to the reader that something idiosyncratic is going on. A form like `(for ((x in (foobar a b 'oof))) (something-hairy x (list x)))` looks a bit wrong already, because of the double parentheses before the `x`. But using "`:in`" makes it more obvious.

Second, notice that I wrote `(eq (cadr listspec) ':in)` in the macro definition to check for the presence of the guide symbol. If I had used `in` instead, I would have had to think about which package _my_ `in` lives in and which package the macro user's `in` lives in. One way to avoid trouble would be to write

~~~lisp
(and (symbolp (cadr listspec))
     (eq (intern (symbol-name (cadr listspec))
                 :keyword)
         ':in))
~~~

Another would be to write

~~~lisp
(and (symbolp (cadr listspec))
     (string= (symbol-name (cadr listspec)) (symbol-name 'in)))
~~~

which neither of which is particularly clear or aesthetic. The keyword package is there to provide a home for symbols whose home is not per se relevant to anything; you might as well use it. (Note: In ANSI Lisp, I could have written `"IN"` instead of `(symbol-name 'in)`, but there are Lisp implementations that do not convert symbols' names to uppercase. Since I think the whole uppercase conversion idea is an embarrassing relic, I try to write code that is portable to those implementations.)

Let's look at another example, both to illustrate a nice macro, and to provide an auxiliary function for some of the discussion below. One often wants to create new symbols in Lisp, and `gensym` is not always adequate for building them. Here is a description of an alternative facility called `build-symbol`:

> <code>(build-symbol [(:package <em>p</em>)] <em>-pieces-</em>)</code> builds a symbol by concatenating the given _pieces_ and interns it as specified by _p_. For each element of _pieces_, if it is a ...
>
> *   ... string: The string is added to the new symbol's name.
> *   ... symbol: The name of the symbol is added to the new symbol's name.
> *   ... expression of the form <code>(:< <em>e</em>)</code>: _e_ should evaluate to a string, symbol, or number; the characters of the value of _e_ (as printed by `princ`) are concatenated into the new symbol's name.
> *   ... expression of the form <code>(:++ <em>p</em>)</code>: _p_ should be a place expression (i.e., appropriate as the first argument to `setf`), whose value is an integer; the value is incremented by 1, and the new value is concatenated into the new symbol's name.
>
> If the `:package` specification is omitted, it defaults to the value of `*package*`. If _p_ is `nil`, the symbol is interned nowhere. Otherwise, it should evaluate to a package designator (usually, a keyword whose name is the same of a package).

For example, `(build-symbol (:< x) "-" (:++ *x-num*))`, when `x` = `foo` and `*x-num*` = 8, sets `*x-num*` to 9 and evaluates to `FOO-9`. If evaluated again, the result will be `FOO-10`, and so forth.

Obviously, `build-symbol` can't be implemented as a function; it has to be a macro. Here is an implementation:

~~~lisp
(defmacro build-symbol (&rest l)
  (let ((p (find-if (lambda (x) (and (consp x) (eq (car x) ':package)))
                    l)))
    (cond (p
           (setq l (remove p l))))
    (let ((pkg (cond ((eq (cadr p) 'nil)
                      nil)
                     (t `(find-package ',(cadr p))))))
      (cond (p
             (cond (pkg
                    `(values (intern ,(symstuff l) ,pkg)))
                   (t
                    `(make-symbol ,(symstuff l)))))
            (t
             `(values (intern ,(symstuff l))))))))

(defun symstuff (l)
  `(concatenate 'string
                ,@(for (x :in l)
                       (cond ((stringp x)
                              `',x)
                             ((atom x)
                              `',(format nil "~a" x))
                             ((eq (car x) ':<)
                              `(format nil "~a" ,(cadr x)))
                             ((eq (car x) ':++)
                              `(format nil "~a" (incf ,(cadr x))))
                             (t
                              `(format nil "~a" ,x))))))
~~~

(Another approach would be have `symstuff` return a single call of the form <code>(format nil <em>format-string -forms-</em>)</code>, where the _forms_ are derived from the _pieces_, and the _format-string_ consists of interleaved ~a's and strings.)

Sometimes a macro is needed only temporarily, as a sort of syntactic scaffolding. Suppose you need to define 12 functions, but they fall into 3 stereotyped groups of 4:

~~~lisp
(defun make-a-zip (y z)
  (vector 2 'zip y z))
(defun test-whether-zip (x)
  (and (vectorp x) (eq (aref x 1) 'zip)))
(defun zip-copy (x) ...)
(defun zip-deactivate (x) ...)

(defun make-a-zap (u v w)
  (vector 3 'zap u v w))
(defun test-whether-zap (x) ...)
(defun zap-copy (x) ...)
(defun zap-deactivate (x) ...)

(defun make-a-zep ()
  (vector 0 'zep))
(defun test-whether-zep (x) ...)
(defun zep-copy (x) ...)
(defun zep-deactivate (x) ...)
~~~

Where the omitted pieces are the same in all similarly named functions. (That is, the "..." in `zep-deactivate` is the same code as the "..." in `zip-deactivate`, and so forth.) Here, for the sake of concreteness, if not plausibility, `zip`, `zap`, and `zep` are behaving like odd little data structures. The functions could be rather large, and it would get tedious keeping them all in sync as they are debugged. An alternative would be to use a macro:

~~~lisp
(defmacro odd-define (name buildargs)
  `(progn (defun ,(build-symbol make-a- (:< name))
                                ,buildargs
            (vector ,(length buildargs) ',name ,@buildargs))
          (defun ,(build-symbol test-whether- (:< name)) (x)
            (and (vectorp x) (eq (aref x 1) ',name))
          (defun ,(build-symbol (:< name) -copy) (x)
            ...)
          (defun ,(build-symbol (:< name) -deactivate) (x)
            ...))))

(odd-define zip (y z))
(odd-define zap (u v w))
(odd-define zep ())
~~~

If all the uses of this macro are collected in this one place, it might be clearer to make it a local macro using `macrolet`:

~~~lisp
(macrolet ((odd-define (name buildargs)
             `(progn (defun ,(build-symbol make-a- (:< name))
                                           ,buildargs
                       (vector ,(length buildargs)
                               ',name
                                ,@buildargs))
                     (defun ,(build-symbol test-whether- (:< name))
                            (x)
                       (and (vectorp x) (eq (aref x 1) ',name))
                     (defun ,(build-symbol (:< name) -copy) (x)
                       ...)
                     (defun ,(build-symbol (:< name) -deactivate) (x)
                       ...)))))
(odd-define zip (y z))
(odd-define zap (u v w))
(odd-define zep ()))
~~~

Finally, macros are essential for defining "command languages." A _command_ is a function with a short name for use by users in interacting with Lisp's read-eval-print loop. A short name is useful and possible because we want it to be easy to type and we don't care much whether the name clashes some other command; if two command names clash, we can change one of them.

As an example, let's define a little command language for debugging macros. (You may actually find this useful.) There are just two commands, `ex` and `fi`. They keep track of a "current form," the thing to be macro-expanded or the result of such an expansion:

1.  <code>(ex [<em>form</em>])</code>: Apply `macro-expand1` to _form_ (if supplied) or the current form, and make the result the current form. Then pretty-print the current form.
2.  <code>(fi <em>s</em> [<em>k</em>])</code>: Find the _k_'th subexpression of the current form whose `car` is _s_. (_k_ defaults to 0.) Make that subexpression the current form and pretty-print it.

Suppose you're trying to debug a macro `hair-squared` that expands into something complex containing a subform that is itself a macro form beginning with the symbol `odd-define`. You suspect there is a bug in the subform. You might issue the following commands:

~~~lisp
> (ex (hair-squared ...))
(PROGN (DEFUN ...)
         (ODD-DEFINE ZIP (U V W))
         ...)
> (fi odd-define)
(ODD-DEFINE ZIP (U V W))
> (ex)
(PROGN (DEFUN MAKE-A-ZIP (U V W) ...)
   ...)
~~~

Once again, it is clear that `ex` and `fi` cannot be functions, although they could easily be made into functions if we were willing to type a quote before their arguments. But using "quote" often seems inappropriate in commands. For one thing, having to type it is a nuisance in a context where we are trying to save keystrokes, especially if the argument in question is always quoted. For another, in many cases it just seems inappropriate. If we had a command that took a symbol as one of its arguments and set it to a value, it would just be strange to write <code>(<em>command</em> 'x ...)</code> instead of <code>(<em>command</em> x ...)</code>, because we want to think of the command as a variant of `setq`.

Here is how `ex` and `fi` might be defined:

~~~lisp
(defvar *current-form*)

(defmacro ex (&optional (form nil form-supplied))
  `(progn
     (pprint (setq *current-form*
                   (macroexpand-1
                    ,(cond (form-supplied
                            `',form)
                           (t '*current-form*)))))
     (values)))

(defmacro fi (s &optional (k 0))
  `(progn
     (pprint (setq *current-form*
                   (find-nth-occurrence ',s *current-form* ,k)))
     (values)))
~~~

The `ex` macro expands to a form containing a call to `macroexpand-1`, a built-in function that does one step of macro expansion to a form whose `car` is the name of a macro. (If given some other form, it returns the form unchanged.) `pprint` is a built-in function that pretty-prints its argument. Because we are using `ex` and `fi` at a read-eval-print loop, any value returned by their expansions will be printed. Here the expansion is executed for side effect, so we arrange to return no values at all by having the expansion return `(values)`.

In some Lisp implementations, read-eval-print loops routinely print results using `pprint`. In those implementations we could simplify `ex` and `fi` by having them print nothing, but just return the value of `*current-form*`, which the read-eval-print loop will then print prettily. Use your judgment.

I leave the definition of `find-nth-occurrence` as an exercise. You might also want to define a command that just sets and prints the current form: <code>(cf <em>e</em>)</code>.

One caution: In general, command languages will consist of a mixture of macros and functions, with convenience for their definer (and usually sole user) being the main consideration. If a command seems to "want" to evaluate some of its arguments sometimes, you have to decide whether to define two (or more) versions of it, or just one, a function whose arguments must be quoted to prevent their being evaluated. For the `cf` command mentioned in the previous paragraph, some users might prefer `cf` to be a function, some a macro.


## Screencast

The following video, from the series
["Little bits of Lisp"](https://www.youtube.com/user/CBaggers/playlists)
by [cbaggers](https://github.com/cbaggers/), is a two hours long talk
on macros, showing simple to advanced concepts such as compiler
macros:
[https://www.youtube.com/watch?v=ygKXeLKhiTI](https://www.youtube.com/watch?v=ygKXeLKhiTI)
It also shows how to manipulate macros (and their expansion) in Emacs.

[![video](http://img.youtube.com/vi/ygKXeLKhiTI/0.jpg)](https://www.youtube.com/watch?v=ygKXeLKhiTI)

---
title: Fundamentals of CLOS
---


CLOS is the "Common Lisp Object System", arguably one of the most
powerful object systems available in any language.

Some of its features include:

* it is **dynamic**, making it a joy to work with in a Lisp REPL. For
  example, changing a class definition will update the existing
  objects, given certain rules which we have control upon.
* it supports **multiple dispatch** and **multiple inheritance**,
* it is different from most object systems in that class and method
  definitions are not tied together,
* it has excellent **introspection** capabilities,
* it is provided by a **meta-object protocol**, which provides a
  standard interface to the CLOS, and can be used to create new object
  systems.

The functionality belonging to this name was added to the Common Lisp
language between the publication of Steele's first edition of "Common
Lisp, the Language" in 1984 and the formalization of the language as
an ANSI standard ten years later.

This page aims to give a good understanding of how to use CLOS, but
only a brief introduction to the MOP.

To learn the subjects in depth, you will need two books:

- [Object-Oriented Programming in Common Lisp: a Programmer's Guide to CLOS](http://www.communitypicks.com/r/lisp/s/17592186046723-object-oriented-programming-in-common-lisp-a-programmer), by Sonya Keene,
- [the Art of the Metaobject Protocol](http://www.communitypicks.com/r/lisp/s/17592186045709-the-art-of-the-metaobject-protocol), by Gregor Kiczales, Jim des Rivières et al.

But see also

- the introduction in [Practical Common Lisp](http://www.gigamonkeys.com/book/object-reorientation-generic-functions.html) (online), by Peter Seibel.
-  [Common Lisp, the Language](https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node260.html#SECTION003200000000000000000)
- and for reference, the complete [CLOS-MOP specifications](https://clos-mop.hexstreamsoft.com/).


#  Classes and instances

## Diving in

Let's dive in with an example showing class definition, creation of
objects, slot access, methods specialized for a given class, and
inheritance.

~~~lisp
(defclass person ()
  ((name
    :initarg :name
    :accessor name)
   (lisper
    :initform nil
    :accessor lisper)))

;; => #<STANDARD-CLASS PERSON>

(defvar p1 (make-instance 'person :name "me" ))
;;                                 ^^^^ initarg
;; => #<PERSON {1006234593}>

(name p1)
;;^^^ accessor
;; => "me"

(lisper p1)
;; => nil
;;    ^^ initform (slot unbound by default)

(setf (lisper p1) t)


(defclass child (person)
  ())

(defclass child (person)
  ((can-walk-p
     :accessor can-walk-p
     :initform t)))
;; #<STANDARD-CLASS CHILD>

(can-walk-p (make-instance 'child))
;; T
~~~

## Defining classes (defclass)

The macro used for defining new data types in CLOS is `defclass`.

We used it like this:

~~~lisp
(defclass person ()
  ((name
    :initarg :name
    :accessor name)
   (lisper
    :initform nil
    :accessor lisper)))
~~~

This gives us a CLOS type (or class) called `person` and two slots,
named `name` and `lisper`.

~~~lisp
(class-of p1)
#<STANDARD-CLASS PERSON>

(type-of p1)
PERSON
~~~

The general form of `defclass` is:

```
(defclass <class-name> (list of super classes)
  ((slot-1
     :slot-option slot-argument)
   (slot-2, etc))
  (:optional-class-option
   :another-optional-class-option))
```

So, our `person` class doesn't explicitly inherit from another class
(it gets the empty parentheses `()`). However it still inherits by default from
the class `t` and from `standard-object`. See below under
"inheritance".

We could write a minimal class definition without slots options like this:

~~~lisp
(defclass point ()
  (x y z))
~~~

or even without slots specifiers: `(defclass point () ())`.

## Creating objects (make-instance)

We create instances of a class with `make-instance`:

~~~lisp
(defvar p1 (make-instance 'person :name "me" ))
~~~

It is generally good practice to define a constructor:

~~~lisp
(defun make-person (name &key lisper)
  (make-instance 'person :name name :lisper lisper))
~~~

This has the direct advantage that you can control the required
arguments. You should now export the constructor from your package and
not the class itself.

## Slots

### A function that always works (slot-value)

The function to access any slot anytime is `(slot-value <object> <slot-name>)`.

Given our `point` class above, which didn't define any slot accessors:


```lisp
(defvar pt (make-instance 'point))

(inspect pt)
The object is a STANDARD-OBJECT of type POINT.
0. X: "unbound"
1. Y: "unbound"
2. Z: "unbound"
```

We got an object of type `POINT`, but **slots are unbound by
default**: trying to access them will raise an `UNBOUND-SLOT`
condition:

~~~lisp
(slot-value pt 'x) ;; => condition: the slot is unbound
~~~


`slot-value` is `setf`-able:

~~~lisp
(setf (slot-value pt 'x) 1)
(slot-value pt 'x) ;; => 1
~~~

### Initial and default values (initarg, initform)

- `:initarg :foo` is the keyword we can pass to `make-instance` to
  give a value to this slot:

~~~lisp
(make-instance 'person :name "me")
~~~

(again: slots are unbound by default)

- `:initform <val>` is the *default value* in case we didn't specify
 an initarg.  This form is evaluated each time it's needed, in the
 lexical environment of the `defclass`.

Sometimes we see the following trick to clearly require a slot:

~~~lisp
(defclass foo ()
    ((a
      :initarg :a
      :initform (error "you didn't supply an initial value for slot a"))))
;; #<STANDARD-CLASS FOO>

(make-instance 'foo) ;; => enters the debugger.
~~~


### Getters and setters (accessor, reader, writer)

- `:accessor foo`: an accessor is both a **getter** and a
  **setter**. Its argument is a name that will become a **generic
  function**.

~~~lisp
(name p1) ;; => "me"

(type-of #'name)
STANDARD-GENERIC-FUNCTION
~~~

- `:reader` and `:writer` do what you expect. Only the `:writer` is `setf`-able.

If you don't specify any of these, you can still use `slot-value`.

You can give a slot more than one `:accessor`, `:reader` or `:initarg`.


We introduce two macros to make the access to slots shorter in some situations:

1- `with-slots` allows to abbreviate several calls to slot-value. The
first argument is a list of slot names. The second argument evaluates
to a CLOS instance. This is followed by optional declarations and an
implicit `progn`. Lexically during the evaluation of the body, an
access to any of these names as a variable is equivalent to accessing
the corresponding slot of the instance with `slot-value`.


~~~lisp
(with-slots (name lisper)
    c1
  (format t "got ~a, ~a~&" name lisper))
~~~

or

~~~lisp
(with-slots ((n name)
             (l lisper))
    c1
  (format t "got ~a, ~a~&" n l))
~~~

2- `with-accessors` is equivalent, but instead of a list of slots it
takes a list of accessor functions. Any reference to the variable
inside the macro is equivalent to a call to the accessor function.

~~~lisp
(with-accessors ((name        name)
                  ^^variable  ^^accessor
                 (lisper lisper))
            p1
          (format t "name: ~a, lisper: ~a" name lisper))
~~~

### Class VS instance slots

`:allocation` specifies whether this slot is *local* or *shared*.

* a slot is *local* by default, that means it can be different for each instance of the class. In that case `:allocation` equals `:instance`.

* a *shared* slot will always be equal for all instances of the
    class. We set it with `:allocation :class`.

In the following example, note how changing the value of the class
slot `species` of `p2` affects all instances of the
class (whether or not those instances exist yet).

~~~lisp
(defclass person ()
  ((name :initarg :name :accessor name)
   (species
      :initform 'homo-sapiens
      :accessor species
      :allocation :class)))

;; Note that the slot "lisper" was removed in existing instances.
(inspect p1)
;; The object is a STANDARD-OBJECT of type PERSON.
;; 0. NAME: "me"
;; 1. SPECIES: HOMO-SAPIENS
;; > q

(defvar p2 (make-instance 'person))

(species p1)
(species p2)
;; HOMO-SAPIENS

(setf (species p2) 'homo-numericus)
;; HOMO-NUMERICUS

(species p1)
;; HOMO-NUMERICUS

(species (make-instance 'person))
;; HOMO-NUMERICUS

(let ((temp (make-instance 'person)))
    (setf (species temp) 'homo-lisper))
;; HOMO-LISPER
(species (make-instance 'person))
;; HOMO-LISPER
~~~

### Slot documentation

Each slot accepts one `:documentation` option.

### Slot type

The `:type` slot option may not do the job you expect it does. If you
are new to the CLOS, we suggest you skip this section and use your own
constructors to manually check slot types.

Indeed, whether slot types are being checked or not is undefined. See the [Hyperspec](http://www.lispworks.com/documentation/HyperSpec/Body/m_defcla.htm#defclass).

Few implementations will do it. Clozure CL does it, SBCL does it when
safety is high (`(declaim (optimize safety))`).

To do it otherwise, see [this Stack-Overflow answer](https://stackoverflow.com/questions/51723992/how-to-force-slots-type-to-be-checked-during-make-instance), and see also [quid-pro-quo](https://github.com/sellout/quid-pro-quo), a contract programming library.


## find-class, class-name, class-of

~~~lisp
(find-class 'point)
;; #<STANDARD-CLASS POINT 275B78DC>

(class-name (find-class 'point))
;; POINT

(class-of my-point)
;; #<STANDARD-CLASS POINT 275B78DC>

(typep my-point (class-of my-point))
;; T
~~~

CLOS classes are also instances of a CLOS class, and we can find out
what that class is, as in the example below:

~~~lisp
(class-of (class-of my-point))
;; #<STANDARD-CLASS STANDARD-CLASS 20306534>
~~~

<u>Note</u>: this is your first introduction to the MOP. You don't need that to get started !

The object `my-point` is an instance of the class named `point`, and the
class named `point` is itself an instance of the class named
`standard-class`. We say that the class named `standard-class` is
the *metaclass* (i.e. the class of the class) of
`my-point`. We can make good uses of metaclasses, as we'll see later.



## Subclasses and inheritance

As illustrated above, `child` is a subclass of `person`.

All objects inherit from the class `standard-object` and `t`.

Every child instance is also an instance of `person`.

~~~lisp
(type-of c1)
;; CHILD

(subtypep (type-of c1) 'person)
;; T

(ql:quickload "closer-mop")
;; ...

(closer-mop:subclassp (class-of c1) 'person)
;; T
~~~

The [closer-mop](https://github.com/pcostanza/closer-mop) library is *the*
portable way to do CLOS/MOP operations.


A subclass inherits all of its parents slots, and it can override any
of their slot options. Common Lisp makes this process dynamic, great
for REPL session, and we can even control parts of it (like, do
something when a given slot is removed/updated/added, etc).

The **class precedence list** of a `child` is thus:

    child <- person <-- standard-object <- t

Which we can get with:

~~~lisp
(closer-mop:class-precedence-list (class-of c1))
;; (#<standard-class child>
;;  #<standard-class person>
;;  #<standard-class standard-object>
;;  #<sb-pcl::slot-class sb-pcl::slot-object>
;;  #<sb-pcl:system-class t>)
~~~

However, the **direct superclass** of a `child` is only:

~~~lisp
(closer-mop:class-direct-superclasses (class-of c1))
;; (#<standard-class person>)
~~~

We can further inspect our classes with
`class-direct-[subclasses, slots, default-initargs]` and many more functions.

How slots are combined follows some rules:

- `:accessor` and `:reader` are combined by the **union** of accessors
   and readers from all the inherited slots.

- `:initarg`: the **union** of initialization arguments from all the
  inherited slots.

- `:initform`: we get **the most specific** default initial value
  form, i.e. the first `:initform` for that slot in the precedence
  list.

- `:allocation` is not inherited. It is controlled solely by the class
  being defined and defaults to `:instance`.


Last but not least, be warned that inheritance is fairly easy to
misuse, and multiple inheritance is multiply so, so please take a
little care. Ask yourself whether `foo` really wants to inherit from
`bar`, or whether instances of `foo` want a slot containing a `bar`. A
good general guide is that if `foo` and `bar` are "same sort of thing"
then it's correct to mix them together by inheritance, but if they're
really separate concepts then you should use slots to keep them apart.


## Multiple inheritance

CLOS supports multiple inheritance.


~~~lisp
(defclass baby (child person)
  ())
~~~

The first class on the list of parent classes is the most specific
one, `child`'s slots will take precedence over the `person`'s. Note
that both `child` and `person` have to be defined prior to defining
`baby` in this example.


## Redefining and changing a class

This section briefly covers two topics:

- redefinition of an existing class, which you might already have done
  by following our code snippets, and what we do naturally during
  development, and
- changing an instance of one class into an instance of another,
  a powerful feature of CLOS that you'll probably won't use very often.

We'll gloss over the details. Suffice it to say that everything's
configurable by implementing methods exposed by the MOP.

To redefine a class, simply evaluate a new `defclass` form. This then
takes the place of the old definition, the existing class object is
updated, and **all instances of the class** (and, recursively, its
subclasses) **are lazily updated to reflect the new definition**. You don't
have to recompile anything other than the new `defclass`, nor to
invalidate any of your objects. Think about it for a second: this is awesome !

For example, with our `person` class:

~~~lisp
(defclass person ()
  ((name
    :initarg :name
    :accessor name)
   (lisper
    :initform nil
    :accessor lisper)))

(setf p1 (make-instance 'person :name "me" ))
~~~

Changing, adding, removing slots,...

~~~lisp
(lisper p1)
;; NIL

(defclass person ()
  ((name
    :initarg :name
    :accessor name)
   (lisper
    :initform t        ;; <-- from nil to t
    :accessor lisper)))

(lisper p1)
;; NIL (of course!)

(lisper (make-instance 'person :name "You"))
;; T

(defclass person ()
  ((name
    :initarg :name
    :accessor name)
   (lisper
    :initform nil
    :accessor lisper)
   (age
    :initarg :arg
    :initform 18
    :accessor age)))

(age p1)
;; => slot unbound error. This is different from "slot missing":

(slot-value p1 'bwarf)
;; => "the slot bwarf is missing from the object #<person…>"

(setf (age p1) 30)
(age p1) ;; => 30

(defclass person ()
  ((name
    :initarg :name
    :accessor name)))

(slot-value p1 'lisper) ;; => slot lisper is missing.
(lisper p1) ;; => there is no applicable method for the generic function lisper when called with arguments #(lisper).
~~~


To change the class of an instance, use `change-class`:

~~~lisp
(change-class p1 'child)
;; we can also set slots of the new class:
(change-class p1 'child :can-walk-p nil)

(class-of p1)
;; #<STANDARD-CLASS CHILD>

(can-walk-p p1)
;; T
~~~

In the above example, I became a `child`, and I inherited the `can-walk-p` slot, which is true by default.


## Pretty printing

Every time we printed an object so far we got an output like

    #<PERSON {1006234593}>

which doesn't say much.

What if we want to show more information ? Something like

    #<PERSON me lisper: t>

Pretty printing is done by specializing the generic `print-object` method for this class:

~~~lisp
(defmethod print-object ((obj person) stream)
      (print-unreadable-object (obj stream :type t)
        (with-accessors ((name name)
                         (lisper lisper))
            obj
          (format stream "~a, lisper: ~a" name lisper))))
~~~

It gives:

~~~lisp
p1
;; #<PERSON me, lisper: T>
~~~

`print-unreadable-object` prints the `#<...>`, that says to the reader
that this object can not be read back in. Its `:type t` argument asks
to print the object-type prefix, that is, `PERSON`. Without it, we get
`#<me, lisper: T>`.

We used the `with-accessors` macro, but of course for simple cases this is enough:

~~~lisp
(defmethod print-object ((obj person) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "~a, lisper: ~a" (name obj) (lisper obj))))
~~~

Caution: trying to access a slot that is not bound by default will
lead to an error. Use `slot-boundp`.


For reference, the following reproduces the default behaviour:

~~~lisp
(defmethod print-object ((obj person) stream)
  (print-unreadable-object (obj stream :type t :identity t)))
~~~

Here, `:identity` to `t` prints the `{1006234593}` address.

## Classes of traditional lisp types

Where we approach that we don't need CLOS objects to use CLOS.

Generously, the functions introduced in the last section also work on
lisp objects which are <u>not</u> CLOS instances:

~~~lisp
(find-class 'symbol)
;; #<BUILT-IN-CLASS SYMBOL>
(class-name *)
;; SYMBOL
(eq ** (class-of 'symbol))
;; T
(class-of ***)
;; #<STANDARD-CLASS BUILT-IN-CLASS>
~~~

We see here that symbols are instances of the system class
`symbol`. This is one of 75 cases in which the language requires a
class to exist with the same name as the corresponding lisp
type. Many of these cases are concerned with CLOS itself (for
example, the correspondence between the type `standard-class` and
the CLOS class of that name) or with the condition system (which
might or might not be built using CLOS classes in any given
implementation). However, 33 correspondences remain relating to
"traditional" lisp types:


|`array`|`hash-table`|`readtable`|
|`bit-vector`|`integer`|`real`|
|`broadcast-stream`|`list`|`sequence`|
|`character`|`logical-pathname`|`stream`|
|`complex`|`null`|`string`|
|`concatenated-stream`|`number`|`string-stream`|
|`cons`|`package`|`symbol`|
|`echo-stream`|`pathname`|`synonym-stream`|
|`file-stream`|`random-state`|`t`|
|`float`|`ratio`|`two-way-stream`|
|`function`|`rational`|`vector`|


Note that not all "traditional" lisp types are included in this
list. (Consider: `atom`, `fixnum`, `short-float`, and any type not
denoted by a symbol.)


The presence of `t` is interesting. Just as every lisp
object is of type `t`, every lisp object is also a member
of the class named `t`. This is a simple example of
membership of more then one class at a time, and it brings into
question the issue of *inheritance*, which we will consider
in some detail later.

~~~lisp
(find-class t)
;; #<BUILT-IN-CLASS T 20305AEC>
~~~

In addition to classes corresponding to lisp types, there is also a
    CLOS class for every structure type you define:

~~~lisp
(defstruct foo)
FOO

(class-of (make-foo))
;; #<STRUCTURE-CLASS FOO 21DE8714>
~~~

The metaclass of a `structure-object` is the class
    `structure-class`. It is implementation-dependent whether
    the metaclass of a "traditional" lisp object is
    `standard-class`, `structure-class`, or
    `built-in-class`. Restrictions:

|`built-in-class`| May not use `make-instance`, may not use `slot-value`, may not use `defclass` to modify, may not create subclasses.|
|`structure-class`| May not use `make-instance`, might work with `slot-value` (implementation-dependent). Use `defstruct` to subclass application structure types. Consequences of modifying an existing `structure-class` are undefined: full recompilation may be necessary.|
|`standard-class`|None of these restrictions.|


## Introspection

we already saw some introspection functions.

Your best option is to discover the
[closer-mop](https://github.com/pcostanza/closer-mop) library and to
keep the [CLOS & MOP specifications](https://clos-mop.hexstreamsoft.com/) at
hand.

More functions:

```
closer-mop:class-default-initargs
closer-mop:class-direct-default-initargs
closer-mop:class-direct-slots
closer-mop:class-direct-subclasses
closer-mop:class-direct-superclasses
closer-mop:class-precedence-list
closer-mop:class-slots
closer-mop:classp
closer-mop:extract-lambda-list
closer-mop:extract-specializer-names
closer-mop:generic-function-argument-precedence-order
closer-mop:generic-function-declarations
closer-mop:generic-function-lambda-list
closer-mop:generic-function-method-class
closer-mop:generic-function-method-combination
closer-mop:generic-function-methods
closer-mop:generic-function-name
closer-mop:method-combination
closer-mop:method-function
closer-mop:method-generic-function
closer-mop:method-lambda-list
closer-mop:method-specializers
closer-mop:slot-definition
closer-mop:slot-definition-allocation
closer-mop:slot-definition-initargs
closer-mop:slot-definition-initform
closer-mop:slot-definition-initfunction
closer-mop:slot-definition-location
closer-mop:slot-definition-name
closer-mop:slot-definition-readers
closer-mop:slot-definition-type
closer-mop:slot-definition-writers
closer-mop:specializer-direct-generic-functions
closer-mop:specializer-direct-methods
closer-mop:standard-accessor-method
```


## See also

### defclass/std: write shorter classes

The library [defclass/std](https://github.com/EuAndreh/defclass-std)
provides a macro to write shorter `defclass` forms.

By default, it adds an accessor, an initarg and an initform to `nil` to your slots definition:

This:

~~~lisp
(defclass/std example ()
  ((slot1 slot2 slot3)))
~~~

expands to:

~~~lisp
(defclass example ()
  ((slot1
    :accessor slot1
    :initarg :slot1
    :initform nil)
   (slot2
     :accessor slot2
     :initarg :slot2
     :initform nil)
   (slot3
     :accessor slot3
     :initarg :slot3
     :initform nil)))
~~~

It does much more and it is very flexible, however it is seldom used
by the Common Lisp community: use at your own risks©.


# Methods

## Diving in
Recalling our `person` and `child` classes from the beginning:

~~~lisp
(defclass person ()
  ((name
    :initarg :name
    :accessor name)))
;; => #<STANDARD-CLASS PERSON>

(defclass child (person)
  ())
;; #<STANDARD-CLASS CHILD>

(setf p1 (make-instance 'person :name "me"))
(setf c1 (make-instance 'child :name "Alice"))
~~~

Below we create methods, we specialize them, we use method combination
(before, after, around), and qualifiers.

~~~lisp
(defmethod greet (obj)
  (format t "Are you a person ? You are a ~a.~&" (type-of obj)))
;; style-warning: Implicitly creating new generic function common-lisp-user::greet.
;; #<STANDARD-METHOD GREET (t) {1008EE4603}>

(greet :anything)
;; Are you a person ? You are a KEYWORD.
;; NIL
(greet p1)
;; Are you a person ? You are a PERSON.

(defgeneric greet (obj)
  (:documentation "say hello"))
;; STYLE-WARNING: redefining COMMON-LISP-USER::GREET in DEFGENERIC
;; #<STANDARD-GENERIC-FUNCTION GREET (2)>

(defmethod greet ((obj person))
  (format t "Hello ~a !~&" (name obj)))
;; #<STANDARD-METHOD GREET (PERSON) {1007C26743}>

(greet p1) ;; => "Hello me !"
(greet c1) ;; => "Hello Alice !"

(defmethod greet ((obj child))
  (format t "ur so cute~&"))
;; #<STANDARD-METHOD GREET (CHILD) {1008F3C1C3}>

(greet p1) ;; => "Hello me !"
(greet c1) ;; => "ur so cute"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Method combination: before, after, around.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod greet :before ((obj person))
  (format t "-- before person~&"))
#<STANDARD-METHOD GREET :BEFORE (PERSON) {100C94A013}>

(greet p1)
;; -- before person
;; Hello me

(defmethod greet :before ((obj child))
  (format t "-- before child~&"))
;; #<STANDARD-METHOD GREET :BEFORE (CHILD) {100AD32A43}>
(greet c1)
;; -- before child
;; -- before person
;; ur so cute

(defmethod greet :after ((obj person))
  (format t "-- after person~&"))
;; #<STANDARD-METHOD GREET :AFTER (PERSON) {100CA2E1A3}>
(greet p1)
;; -- before person
;; Hello me
;; -- after person

(defmethod greet :after ((obj child))
  (format t "-- after child~&"))
;; #<STANDARD-METHOD GREET :AFTER (CHILD) {10075B71F3}>
(greet c1)
;; -- before child
;; -- before person
;; ur so cute
;; -- after person
;; -- after child

(defmethod greet :around ((obj child))
  (format t "Hello my dear~&"))
;; #<STANDARD-METHOD GREET :AROUND (CHILD) {10076658E3}>
(greet c1) ;; Hello my dear


;; call-next-method

(defmethod greet :around ((obj child))
  (format t "Hello my dear~&")
  (when (next-method-p)
    (call-next-method)))
;; #<standard-method greet :around (child) {100AF76863}>

(greet c1)
;; Hello my dear
;; -- before child
;; -- before person
;; ur so cute
;; -- after person
;; -- after child

;;;;;;;;;;;;;;;;;
;; Adding in &key
;;;;;;;;;;;;;;;;;

;; In order to add "&key" to our generic method, we need to remove its definition first.
(fmakunbound 'greet)  ;; with Slime: C-c C-u (slime-undefine-function)
(defmethod greet ((obj person) &key talkative)
  (format t "Hello ~a~&" (name obj))
  (when talkative
    (format t "blah")))

(defgeneric greet (obj &key &allow-other-keys)
  (:documentation "say hi"))

(defmethod greet (obj &key &allow-other-keys)
  (format t "Are you a person ? You are a ~a.~&" (type-of obj)))

(defmethod greet ((obj person) &key talkative &allow-other-keys)
  (format t "Hello ~a !~&" (name obj))
  (when talkative
    (format t "blah")))

(greet p1 :talkative t) ;; ok
(greet p1 :foo t) ;; still ok


;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric greet (obj)
  (:documentation "say hello")
  (:method (obj)
    (format t "Are you a person ? You are a ~a~&." (type-of obj)))
  (:method ((obj person))
    (format t "Hello ~a !~&" (name obj)))
  (:method ((obj child))
    (format t "ur so cute~&")))

;;;;;;;;;;;;;;;;
;;; Specializers
;;;;;;;;;;;;;;;;

(defgeneric feed (obj meal-type)
  (:method (obj meal-type)
    (declare (ignorable meal-type))
    (format t "eating~&")))

(defmethod feed (obj (meal-type (eql :dessert)))
    (declare (ignorable meal-type))
    (format t "mmh, dessert !~&"))

(feed c1 :dessert)
;; mmh, dessert !

(defmethod feed ((obj child) (meal-type (eql :soup)))
    (declare (ignorable meal-type))
    (format t "bwark~&"))

(feed p1 :soup)
;; eating
(feed c1 :soup)
;; bwark
~~~


## Generic functions (defgeneric, defmethod)

A `generic function` is a lisp function which is associated
with a set of methods and dispatches them when it's invoked. All
the methods with the same function name belong to the same generic
function.

The `defmethod` form is similar to a `defun`. It associates a body of
code with a function name, but that body may only be executed if the
types of the arguments match the pattern declared by the lambda list.

They can have optional, keyword and `&rest` arguments.

The `defgeneric` form defines the generic function. If we write a
`defmethod` without a corresponding `defgeneric`, a generic function
is automatically created (see examples).

It is generally a good idea to write the `defgeneric`s. We can add a
default implementation and even some documentation.

~~~lisp
(defgeneric greet (obj)
  (:documentation "says hi")
  (:method (obj)
    (format t "Hi")))
~~~

The required parameters in the method's lambda list may take one of
the following three forms:

1- a simple variable:

~~~lisp
(defmethod greet (foo)
  ...)
~~~

This method can take any argument, it is always applicable.

The variable `foo` is bound to the corresponding argument value, as
usual.

2- a variable and a **specializer**, as in:

~~~lisp
(defmethod greet ((foo person))
  ...)
~~~

In this case, the variable `foo` is bound to the corresponding
argument only if that argument is of specializer class `person` *or a subclass*,
like `child` (indeed, a "child" is also a "person").

If any argument fails to match its
specializer then the method is not *applicable* and it cannot be
executed with those arguments.We'll get an error message like
"there is no applicable method for the generic function xxx when
called with arguments yyy".

**Only required parameters can be specialized**. We can't specialize on optional `&key` arguments.


3- a variable and an **eql specializer**

~~~lisp
(defmethod feed ((obj child) (meal-type (eql :soup)))
    (declare (ignorable meal-type))
    (format t "bwark~&"))

(feed c1 :soup)
;; "bwark"
~~~

In place of a simple symbol (`:soup`), the eql specializer can be any
lisp form. It is evaluated at the same time of the defmethod.

You can define any number of methods with the same function name but
with different specializers, as long as the form of the lambda list is
*congruent* with the shape of the generic function. The system chooses
the most *specific* applicable method and executes its body. The most
specific method is the one whose specializers are nearest to the head
of the `class-precedence-list` of the argument (classes on the left of
the lambda list are more specific). A method with specializers is more
specific to one without any.


**Notes:**

-   It is an error to define a method with the same function name as
    an ordinary function. If you really want to do that, use the
    shadowing mechanism.

-   To add or remove `keys` or `rest` arguments to an existing generic
    method's lambda list, you will need to delete its declaration with
    `fmakunbound` (or `C-c C-u` (slime-undefine-function) with the
    cursor on the function in Slime) and start again. Otherwise,
    you'll see:

```
attempt to add the method
  #<STANDARD-METHOD NIL (#<STANDARD-CLASS CHILD>) {1009504233}>
to the generic function
  #<STANDARD-GENERIC-FUNCTION GREET (2)>;
but the method and generic function differ in whether they accept
&REST or &KEY arguments.
```

-   Methods can be redefined (exactly as for ordinary functions).

-   The order in which methods are defined is irrelevant, although
    any classes on which they specialize must already exist.

-   An unspecialized argument is more or less equivalent to being
    specialized on the class `t`. The only difference is that
    all specialized arguments are implicitly taken to be "referred to" (in
    the sense of `declare ignore`.)

-   Each `defmethod` form generates (and returns) a CLOS
    instance, of class `standard-method`.

- An `eql` specializer won't work as is with strings. Indeed, strings
need `equal` or `equalp` to be compared. But, we can assign our string
to a variable and use the variable both in the `eql` specializer and
for the function call.

- All the methods with the same function name belong to the same generic function.

- All slot accessors and readers defined by `defclass` are methods. They can override or be overridden by other methods on the same generic function.


See more about [defmethod on the CLHS](http://www.lispworks.com/documentation/lw70/CLHS/Body/m_defmet.htm).

## Multimethods

Multimethods explicitly specialize more than one of the generic
function's required parameters.

They don't belong to a particular class. Meaning, we don't have to
decide on the class that would be best to host this method, as we might
have to in other languages.

~~~lisp
(defgeneric hug (a b)
   (:documentation "Hug between two persons."))
;; #<STANDARD-GENERIC-FUNCTION HUG (0)>

(defmethod hug ((a person) (b person))
  :person-person-hug)

(defmethod hug ((a person) (b child))
  :person-child-hug)
~~~

Read more on [Practical Common Lisp](http://www.gigamonkeys.com/book/object-reorientation-generic-functions.html#multimethods).

## Controlling setters (setf-ing methods)

In Lisp, we can define `setf` counterparts of functions or methods. We
might want this to have more control on how to update an object.

~~~lisp
(defmethod (setf name) (new-val (obj person))
  (if (equalp new-val "james bond")
    (format t "Dude that's not possible.~&")
    (setf (slot-value obj 'name) new-val)))

(setf (name p1) "james bond") ;; -> no rename
~~~

If you know Python, this behaviour is provided by the `@property` decorator.


## Dispatch mechanism and next methods


When a generic function is invoked, the application cannot directly invoke a method. The dispatch mechanism proceeds as follows:

1.  compute the list of applicable methods
2.  if no method is applicable then signal an error
3.  sort the applicable methods in order of specificity
4.  invoke the most specific method.

Our `greet` generic function has three applicable methods:

~~~lisp
(closer-mop:generic-function-methods #'greet)
(#<STANDARD-METHOD GREET (CHILD) {10098406A3}>
 #<STANDARD-METHOD GREET (PERSON) {1009008EC3}>
 #<STANDARD-METHOD GREET (T) {1008E6EBB3}>)
~~~

During the execution of a method, the remaining applicable methods
are still accessible, via the *local function*
`call-next-method`. This function has lexical scope within
the body of a method but indefinite extent. It invokes the next most
specific method, and returns whatever value that method returned. It
can be called with either:

*   no arguments, in which case the *next method* will
    receive exactly the same arguments as this method did, or

*   explicit arguments, in which case it is required that the
    sorted set of methods applicable to the new arguments must be the same
    as that computed when the generic function was first called.

For example:

~~~lisp
(defmethod greet ((obj child))
  (format t "ur so cute~&")
  (when (next-method-p)
    (call-next-method)))
;; STYLE-WARNING: REDEFINING GREET (#<STANDARD-CLASS CHILD>) in DEFMETHOD
;; #<STANDARD-METHOD GREET (child) {1003D3DB43}>

(greet c1)
;; ur so cute
;; Hello Alice !
~~~

Calling `call-next-method` when there is no next method
signals an error. You can find out whether a next method exists by
calling the local function `next-method-p` (which also has
has lexical scope and indefinite extent).

Note finally that the body of every method establishes a block with the same name as the method’s generic function. If you `return-from` that name you are exiting the current method, not the call to the enclosing generic function.


## Method qualifiers (before, after, around)

In our "Diving in" examples, we saw some use of the `:before`, `:after` and `:around` *qualifiers*:

- `(defmethod foo :before (obj) (...))`
- `(defmethod foo :after (obj) (...))`
- `(defmethod foo :around (obj) (...))`

By default, in the *standard method combination* framework provided by
CLOS, we can only use one of those three qualifiers, and the flow of control is as follows:

- a **before-method** is called, well, before the applicable primary
  method. If they are many before-methods, **all** are called. The
  most specific before-method is called first (child before person).
- the most specific applicable **primary method** (a method without
  qualifiers) is called (only one).
- all applicable **after-methods** are called. The most specific one is
  called *last* (after-method of person, then after-method of child).

**The generic function returns the value of the primary method**. Any
values of the before or after methods are ignored. They are used for
their side effects.

And then we have **around-methods**. They are wrappers around the core
mechanism we just described. They can be useful to catch return values
or to set up an environment around the primary method (set up a catch,
a lock, timing an execution,…).

If the dispatch mechanism finds an around-method, it calls it and
returns its result. If the around-method has a `call-next-method`, it
calls the next most applicable around-method. It is only when we reach
the primary method that we start calling the before and after-methods.

Thus, the full dispatch mechanism for generic functions is as follows:

1.  compute the applicable methods, and partition them into
    separate lists according to their qualifier;
2.  if there is no applicable primary method then signal an
    error;
3.  sort each of the lists into order of specificity;
4.  execute the most specific `:around` method and
    return whatever that returns;
5.  if an `:around` method invokes
    `call-next-method`, execute the next most specific
    `:around` method;
6.  if there were no `:around` methods in the first
    place, or if an `:around` method invokes
    `call-next-method` but there are no further
    `:around` methods to call, then proceed as follows:

    a.  run all the `:before` methods, in order,
            ignoring any return values and not permitting calls to
            `call-next-method` or
            `next-method-p`;

    b.  execute the most specific primary method and return
            whatever that returns;

    c.  if a primary method invokes `call-next-method`,
            execute the next most specific primary method;

    d.  if a primary method invokes `call-next-method`
            but there are no further primary methods to call then signal an
            error;

    e.  after the primary method(s) have completed, run all the
            `:after` methods, in **<u>reverse</u>**
            order, ignoring any return values and not permitting calls to
            `call-next-method` or
            `next-method-p`.

Think of it as an onion, with all the `:around`
    methods in the outermost layer, `:before` and
    `:after` methods in the middle layer, and primary methods
    on the inside.


## Other method combinations

The default method combination type we just saw is named `standard`,
but other method combination types are available, and no need to say
that you can define your own.

The built-in types are:

    progn + list nconc and max or append min

You notice that these types are named after a lisp operator. Indeed,
what they do is they define a framework that combines the applicable
primary methods inside a call to the lisp operator of that name. For
example, using the `progn` combination type is equivalent to calling **all**
the primary methods one after the other:

~~~lisp
(progn
  (method-1 args)
  (method-2 args)
  (method-3 args))
~~~

Here, unlike the standard mechanism, all the primary methods
applicable for a given object are called, the most specific
first.

To change the combination type, we set the `:method-combination`
option of `defgeneric` and we use it as the methods' qualifier:

~~~lisp
(defgeneric foo (obj)
  (:method-combination progn))

(defmethod foo progn ((obj obj))
   (...))
~~~

An example with **progn**:

~~~lisp
(defgeneric dishes (obj)
   (:method-combination progn)
   (:method progn (obj)
     (format t "- clean and dry.~&"))
   (:method progn ((obj person))
     (format t "- bring a person's dishes~&"))
   (:method progn ((obj child))
     (format t "- bring the baby dishes~&")))
;; #<STANDARD-GENERIC-FUNCTION DISHES (3)>

(dishes c1)
;; - bring the baby dishes
;; - bring a person's dishes
;; - clean and dry.

(greet c1)
;; ur so cute  --> only the most applicable method was called.
~~~

Similarly, using the `list` type is equivalent to returning the list
of the values of the methods.

~~~lisp
(list
  (method-1 args)
  (method-2 args)
  (method-3 args))
~~~

~~~lisp
(defgeneric tidy (obj)
  (:method-combination list)
  (:method list (obj)
    :foo)
  (:method list ((obj person))
    :books)
  (:method list ((obj child))
    :toys))
;; #<STANDARD-GENERIC-FUNCTION TIDY (3)>

(tidy c1)
;; (:toys :books :foo)
~~~

**Around methods** are accepted:

~~~lisp
(defmethod tidy :around (obj)
   (let ((res (call-next-method)))
     (format t "I'm going to clean up ~a~&" res)
     (when (> (length res)
              1)
       (format t "that's too much !~&"))))

(tidy c1)
;; I'm going to clean up (toys book foo)
;; that's too much !
~~~

Note that these operators don't support `before`, `after` and `around`
methods (indeed, there is no room for them anymore). They do support
around methods, where `call-next-method` is allowed, but they don't
support calling `call-next-method` in the primary methods (it would
indeed be redundant since all primary methods are called, or clunky to
*not* call one).

CLOS allows us to define a new operator as a method combination type, be
it a lisp function, macro or special form. We'll let you refer to the
books if you feel the need.


## Debugging: tracing method combination

It is possible to [trace](http://www.xach.com/clhs?q=trace) the method
combination, but this is implementation dependent.

In SBCL, we can use `(trace foo :methods t)`. See [this post by an SBCL core developer](http://christophe.rhodes.io/notes/blog/posts/2018/sbcl_method_tracing/).

For example, given a generic:

~~~lisp
(defgeneric foo (x)
  (:method (x) 3))
(defmethod foo :around ((x fixnum))
  (1+ (call-next-method)))
(defmethod foo ((x integer))
  (* 2 (call-next-method)))
(defmethod foo ((x float))
  (* 3 (call-next-method)))
(defmethod foo :before ((x single-float))
  'single)
(defmethod foo :after ((x double-float))
 'double)
~~~

Let's trace it:

~~~lisp
(trace foo :methods t)

(foo 2.0d0)
  0: (FOO 2.0d0)
    1: ((SB-PCL::COMBINED-METHOD FOO) 2.0d0)
      2: ((METHOD FOO (FLOAT)) 2.0d0)
        3: ((METHOD FOO (T)) 2.0d0)
        3: (METHOD FOO (T)) returned 3
      2: (METHOD FOO (FLOAT)) returned 9
      2: ((METHOD FOO :AFTER (DOUBLE-FLOAT)) 2.0d0)
      2: (METHOD FOO :AFTER (DOUBLE-FLOAT)) returned DOUBLE
    1: (SB-PCL::COMBINED-METHOD FOO) returned 9
  0: FOO returned 9
9
~~~


# MOP

We gather here some examples that make use of the framework provided
by the meta-object protocol, the configurable object system that rules
Lisp's object system. We touch advanced concepts so, new reader, don't
worry: you don't need to understand this section to start using the
Common Lisp Object System.

We won't explain much about the MOP here, but hopefully sufficiently
to make you see its possibilities or to help you understand how some
CL libraries are built. We invite you to read the books referenced in
the introduction.


## Metaclasses

Metaclasses are needed to control the behaviour of other classes.

*As announced, we won't talk much. See also Wikipedia for [metaclasses](https://en.wikipedia.org/wiki/Metaclass) or [CLOS](https://en.wikipedia.org/wiki/Common_Lisp_Object_System)*.

The standard metaclass is `standard-class`:

~~~lisp
(class-of p1) ;; #<STANDARD-CLASS PERSON>
~~~

But we'll change it to one of our own, so that we'll be able to
**count the creation of instances**. This same mechanism could be used
to auto increment the primary key of a database system (this is
how the Postmodern or Mito libraries do), to log the creation of objects,
etc.

Our metaclass inherits from `standard-class`:

~~~lisp
(defclass counted-class (standard-class)
  ((counter :initform 0)))
#<STANDARD-CLASS COUNTED-CLASS>

(unintern 'person)
;; this is necessary to change the metaclass of person.
;; or (setf (find-class 'person) nil)
;; https://stackoverflow.com/questions/38811931/how-to-change-classs-metaclass#38812140

(defclass person ()
  ((name
    :initarg :name
    :accessor name))
  (:metaclass counted-class)) ;; <- metaclass
;; #<COUNTED-CLASS PERSON>
;;   ^^^ not standard-class anymore.
~~~

The `:metaclass` class option can appear only once.

Actually you should have gotten a message asking to implement
`validate-superclass`. So, still with the `closer-mop` library:

~~~lisp
(defmethod closer-mop:validate-superclass ((class counted-class)
                                           (superclass standard-class))
  t)
~~~

Now we can control the creation of new `person` instances:

~~~lisp
(defmethod make-instance :after ((class counted-class) &key)
  (incf (slot-value class 'counter)))
;; #<STANDARD-METHOD MAKE-INSTANCE :AFTER (COUNTED-CLASS) {1007718473}>
~~~

See that an `:after` qualifier is the safest choice, we let the
standard method run as usual and return a new instance.

The `&key` is necessary, remember that `make-instance` is given initargs.

Now testing:

~~~lisp
(defvar p3 (make-instance 'person :name "adam"))
#<PERSON {1007A8F5B3}>

(slot-value p3 'counter)
;; => error. No, our new slot isn't on the person class.
(slot-value (find-class 'person) 'counter)
;; 1

(make-instance 'person :name "eve")
;; #<PERSON {1007AD5773}>
(slot-value (find-class 'person) 'counter)
;; 2
~~~

It's working.


## Controlling the initialization of instances (initialize-instance)

To further customize the creation of instances by specializing
`initialize-instance`, which is called by `make-instance`, just after
it has created a new instance but didn't initialize it yet with the
default initargs and initforms.

It is recommended (Keene) to create an after method, since creating a
primary method would prevent slots' initialization.

~~~lisp
(defmethod initialize-instance :after ((obj person) &key)
  (do something with obj))
~~~

Another rational. The CLOS implementation of
    `make-instance` is in two stages: allocate the new object,
    and then pass it along with all the `make-instance` keyword
    arguments, to the generic function
    `initialize-instance`. Implementors and application writers
    define `:after` methods on
    `initialize-instance`, to initialize the slots of the
    instance. The system-supplied primary method does this with regard to
    (a) `:initform` and `:initarg` values supplied
    with the class was defined and (b) the keywords passed through from
    `make-instance`. Other methods can extend this behaviour as
    they see fit. For example, they might accept an additional keyword
    which invokes a database access to fill certain slots. The lambda list
    for `initialize-instance` is:

~~~
initialize-instance instance &rest initargs &key &allow-other-keys
~~~

See more in the books !

---
title: Type System
---

Common Lisp has a complete and flexible type system and corresponding
tools to inspect, check and manipulate types. It allows creating
custom types, adding type declarations to variables and functions and
thus to get compile-time warnings and errors.


## Values Have Types, Not Variables

Being different from some languages such as C/C++, variables in Lisp are just
*placeholders* for objects[^1]. When you [`setf`][setf] a variable, an object
is "placed" in it. You can place another value to the same variable later, as
you wish.

This implies a fact that in Common Lisp **objects have types**, while
variables do not. This might be surprising at first if you come from a C/C++
background.

For example:

~~~lisp
(defvar *var* 1234)
*VAR*

(type-of *var*)
(INTEGER 0 4611686018427387903)
~~~

The function [`type-of`][type-of] returns the type of the given object. The
returned result is a [type-specifier][type-specifiers]. In this case the first
element is the type and the remaining part is extra information (lower and
upper bound) of that type.  You can safely ignore it for now. Also remember
that integers in Lisp have no limit!

Now let's try to [`setf`][setf] the variable:

~~~lisp
* (setf *var* "hello")
"hello"

* (type-of *var*)
(SIMPLE-ARRAY CHARACTER (5))
~~~

You see, `type-of` returns a different result: [`simple-array`][simple-array]
of length 5 with contents of type [`character`][character]. This is because
`*var*` is evaluated to string `"hello"` and the function `type-of` actually
returns the type of object `"hello"` instead of variable `*var*`.

## Type Hierarchy

The inheritance relationship of Lisp types consists a type graph and the root
of all types is `T`. For example:

~~~lisp
* (describe 'integer)
COMMON-LISP:INTEGER
  [symbol]

INTEGER names the built-in-class #<BUILT-IN-CLASS COMMON-LISP:INTEGER>:
  Class precedence-list: INTEGER, RATIONAL, REAL, NUMBER, T
  Direct superclasses: RATIONAL
  Direct subclasses: FIXNUM, BIGNUM
  No direct slots.

INTEGER names a primitive type-specifier:
  Lambda-list: (&OPTIONAL (SB-KERNEL::LOW '*) (SB-KERNEL::HIGH '*))
~~~

The function [`describe`][describe] shows that the symbol [`integer`][integer]
is a primitive type-specifier that has optional information lower bound and
upper bound. Meanwhile, it is a built-in class. But why?

Most common Lisp types are implemented as CLOS classes. Some types are simply
"wrappers" of other types. Each CLOS class maps to a corresponding type. In
Lisp types are referred to indirectly by the use of [`type
specifiers`][type-specifiers].

There are some differences between the function [`type-of`][type-of] and
[`class-of`][class-of]. The function `type-of` returns the type of a given
object in type specifier format while `class-of` returns the implementation
details.

~~~lisp
* (type-of 1234)
(INTEGER 0 4611686018427387903)

* (class-of 1234)
#<BUILT-IN-CLASS COMMON-LISP:FIXNUM>
~~~

## Checking Types

The function [`typep`][typep] can be used to check if the first argument is of
the given type specified by the second argument.

~~~lisp
* (typep 1234 'integer)
T
~~~

The function [`subtypep`][subtypep] can be used to inspect if a type inherits
from the another one. It returns 2 values:
- `T, T` means first argument is sub-type of the second one.
- `NIL, T` means first argument is *not* sub-type of the second one.
- `NIL, NIL` means "not determined".

For example:

~~~lisp
* (subtypep 'integer 'number)
T
T

* (subtypep 'string 'number)
NIL
T
~~~

Sometimes you may want to perform different actions according to the type of
an argument. The macro [`typecase`][typecase] is your friend:

~~~lisp
* (defun plus1 (arg)
    (typecase arg
      (integer (+ arg 1))
      (string (concatenate 'string arg "1"))
      (t 'error)))
PLUS1

* (plus1 100)
101 (7 bits, #x65, #o145, #b1100101)

* (plus1 "hello")
"hello1"

* (plus1 'hello)
ERROR
~~~

## Type Specifier

A type specifier is a form specifying a type. As mentioned above, returning
value of the function `type-of` and the second argument of `typep` are both
type specifiers.

As shown above, `(type-of 1234)` returns `(INTEGER 0
4611686018427387903)`. This kind of type specifiers are called compound type
specifier. It is a list whose head is a symbol indicating the type. The rest
part of it is complementary information.

~~~lisp
* (typep '#(1 2 3) '(vector number 3))
T
~~~

Here the complementary information of the type `vector` is its elements type
and size respectively.

The rest part of a compound type specifier can be a `*`, which means
"anything". For example, the type specifier `(vector number *)` denotes a
vector consisting of any number of numbers.

~~~lisp
* (typep '#(1 2 3) '(vector number *))
T
~~~

The trailing parts can be omitted, the omitted elements are treated as
`*`s:

~~~lisp
* (typep '#(1 2 3) '(vector number))
T

* (typep '#(1 2 3) '(vector))
T
~~~

As you may have guessed, the type specifier above can be shortened as
following:

~~~lisp
* (typep '#(1 2 3) 'vector)
T
~~~

You may refer to the [CLHS page][type-specifiers] for more information.

## Defining New Types

You can use the macro [`deftype`][deftype] to define a new type-specifier.

Its argument list can be understood as a direct mapping to elements of rest
part of a compound type specifier. They are be defined as optional to allow
symbol type specifier.

Its body should be a macro checking whether given argument is of this type
(see [`defmacro`][defmacro]).

Now let us define a new data type. The data type should be a array with at
most 10 elements. Also each element should be a number smaller than 10. See
following code for an example:

~~~lisp
* (defun small-number-array-p (thing)
    (and (arrayp thing)
      (<= (length thing) 10)
      (every #'numberp thing)
      (every (lambda (x) (< x 10)) thing)))

* (deftype small-number-array (&optional type)
    `(and (array ,type 1)
          (satisfies small-number-array-p)))

* (typep '#(1 2 3 4) '(small-number-array number))
T

* (typep '#(1 2 3 4) 'small-number-array)
T

* (typep '#(1 2 3 4 100) 'small-number-array)
NIL

* (small-number-array-p '#(1 2 3 4 5 6 7 8 9 0 1))
NIL
~~~

## Type Checking

Common Lisp supports run-time type checking via the macro
[`check-type`][check-type]. It accepts a [`place`][place] and a type specifier
as arguments and signals an [`type-error`][type-error] if the contents of
place are not of the given type.

~~~lisp
* (defun plus1 (arg)
    (check-type arg number)
    (1+ arg))
PLUS1

* (plus1 1)
2 (2 bits, #x2, #o2, #b10)

* (plus1 "hello")
; Debugger entered on #<SIMPLE-TYPE-ERROR expected-type: NUMBER datum: "Hello">

The value of ARG is "Hello", which is not of type NUMBER.
   [Condition of type SIMPLE-TYPE-ERROR]
...
~~~


## Compile-time type checking

You may provide type information for variables, function arguments
etc via [`proclaim`][proclaim], [`declaim`][declaim] and [`declare`][declare].
However, similar to the `:type` slot
introduced in [CLOS section][clos], the effects of type declarations are
undefined in Lisp standard and are implementation specific. So there is no
guarantee that the Lisp compiler will perform compile-time type checking.

However, it is possible, and SBCL is an implementation that does
thorough type checking.

Let's recall first that Lisp already warns about simple type
warnings. The following function wrongly wants to concatenate a string
and a number. When we compile it, we get a type warning.

~~~lisp
(defconstant +foo+ 3)
(defun bar ()
  (concatenate 'string "+" +foo+))
; caught WARNING:
;   Constant 3 conflicts with its asserted type SEQUENCE.
;   See also:
;     The SBCL Manual, Node "Handling of Types"
~~~

The example is simple, but it already shows a capacity some other
languages don't have, and it is actually useful during development ;)
Now, we'll do better.


### Declaring the type of variables

Use the macro [`declaim`][declaim].

Let's declare that our global variable `*name*` is a string (you can
type the following in any order in the REPL):

~~~lisp
(declaim (type (string) *name*))
(defparameter *name* "book")
~~~

Now if we try to set it with a bad type, we get a `simple-type-error`:

~~~lisp
(setf *name* :me)
Value of :ME in (THE STRING :ME) is :ME, not a STRING.
   [Condition of type SIMPLE-TYPE-ERROR]
~~~

We can do the same with our custom types. Let's quickly declare the type `list-of-strings`:

~~~lisp
(defun list-of-strings-p (list)
  "Return t if LIST is non nil and contains only strings."
  (and (consp list)
       (every #'stringp list)))

(deftype list-of-strings ()
  `(satisfies list-of-strings-p))
~~~

Now let's declare that our `*all-names*` variables is a list of strings:

~~~lisp
(declaim (type (list-of-strings) *all-names*))
;; and with a wrong value:
(defparameter *all-names* "")
;; we get an error:
Cannot set SYMBOL-VALUE of *ALL-NAMES* to "", not of type
(SATISFIES LIST-OF-STRINGS-P).
   [Condition of type SIMPLE-TYPE-ERROR]
~~~

We can compose types:

~~~lisp
(declaim (type (or null list-of-strings) *all-names*))
~~~

### Declaring the input and output types of functions

We use again the `declaim` macro, with `ftype (function …)` instead of just `type`:

~~~lisp
(declaim (ftype (function (fixnum) fixnum) add))
;;                         ^^input ^^output [optional]
(defun add (n)
	(+ n  1))
~~~

With this we get nice type warnings at compile time.

If we change the function to erroneously return a string instead of a
fixnum, we get a warning:

~~~lisp
(defun add (n)
	(format nil "~a" (+ n  1)))
; caught WARNING:
;   Derived type of ((GET-OUTPUT-STREAM-STRING STREAM)) is
;     (VALUES SIMPLE-STRING &OPTIONAL),
;   conflicting with the declared function return type
;     (VALUES FIXNUM &REST T).
~~~

If we use `add` inside another function, to a place that expects a
string, we get a warning:

~~~lisp
(defun bad-concat (n)
    (concatenate 'string (add n)))
; caught WARNING:
;   Derived type of (ADD N) is
;     (VALUES FIXNUM &REST T),
;   conflicting with its asserted type
;     SEQUENCE.
~~~

If we use `add` inside another function, and that function declares
its argument types which appear to be incompatible with those of
`add`, we get a warning:

~~~lisp
(declaim (ftype (function (string)) bad-arg))
(defun bad-arg (n)
    (add n))
; caught WARNING:
;   Derived type of N is
;     (VALUES STRING &OPTIONAL),
;   conflicting with its asserted type
;     FIXNUM.
~~~

This all happens indeed *at compile time*, either in the REPL,
either with a simple `C-c C-c` in Slime, or when we `load` a file.


## See also

- the article [Static type checking in SBCL](https://medium.com/@MartinCracauer/static-type-checking-in-the-programmable-programming-language-lisp-79bb79eb068a), by Martin Cracauer
- the article [Typed List, a Primer](https://alhassy.github.io/TypedLisp/) - let's explore Lisp's fine-grained type hierarchy! with a shallow comparison to Haskell.
- the [Coalton](https://github.com/stylewarning/coalton) library
  (pre-alpha): adding Hindley-Milner type checking to Common Lisp
  which allows for gradual adoption, in the same way Typed Racket or
  Hack allows for. It is as an embedded DSL in Lisp that resembles
  Standard ML or OCaml, but lets you seamlessly interoperate with
  non-statically-typed Lisp code (and vice versa).

---

[^1]: The term *object* here has nothing to do with Object-Oriented or so. It
    means "any Lisp datum".

[defvar]: http://www.lispworks.com/documentation/lw51/CLHS/Body/m_defpar.htm
[setf]: http://www.lispworks.com/documentation/lw50/CLHS/Body/m_setf_.htm
[type-of]: http://www.lispworks.com/documentation/HyperSpec/Body/f_tp_of.htm
[type-specifiers]: http://www.lispworks.com/documentation/lw51/CLHS/Body/04_bc.htm
[number]: http://www.lispworks.com/documentation/lw61/CLHS/Body/t_number.htm
[typep]: http://www.lispworks.com/documentation/lw51/CLHS/Body/f_typep.htm
[subtypep]: http://www.lispworks.com/documentation/lw71/CLHS/Body/f_subtpp.htm
[string]: http://www.lispworks.com/documentation/lw71/LW/html/lw-426.htm
[simple-array]: http://www.lispworks.com/documentation/lw70/CLHS/Body/t_smp_ar.htm
[integer]: http://www.lispworks.com/documentation/lw71/CLHS/Body/t_intege.htm
[describe]: http://www.lispworks.com/documentation/lw51/CLHS/Body/f_descri.htm
[clos]: clos.html
[character]: http://www.lispworks.com/documentation/lcl50/ics/ics-14.html
[number]: http://www.lispworks.com/documentation/lw61/CLHS/Body/t_number.htm
[complex]: http://www.lispworks.com/documentation/lw70/CLHS/Body/t_comple.htm
[real]: http://www.lispworks.com/documentation/lw70/CLHS/Body/t_real.htm
[rational]: http://www.lispworks.com/documentation/HyperSpec/Body/t_ration.htm
[class-of]: http://www.lispworks.com/documentation/HyperSpec/Body/f_clas_1.htm
[typecase]: http://www.lispworks.com/documentation/lw60/CLHS/Body/m_tpcase.htm
[deftype]: http://www.lispworks.com/documentation/lw51/CLHS/Body/m_deftp.htm
[defmacro]: http://www.lispworks.com/documentation/lw70/CLHS/Body/m_defmac.htm
[check-type]: http://www.lispworks.com/documentation/HyperSpec/Body/m_check_.htm#check-type
[type-error]: http://www.lispworks.com/documentation/HyperSpec/Body/e_tp_err.htm#type-error
[place]: http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_p.htm#place
[proclaim]: http://www.lispworks.com/documentation/HyperSpec/Body/f_procla.htm
[declaim]: http://www.lispworks.com/documentation/HyperSpec/Body/m_declai.htm
[declare]: http://www.lispworks.com/documentation/HyperSpec/Body/s_declar.htm
[safety]: http://www.lispworks.com/documentation/HyperSpec/Body/d_optimi.htm#speed

---
title: TCP/UDP programming with sockets
---

This is a short guide to TCP/IP and UDP/IP client/server programming in Common
Lisp using [usockets](https://github.com/usocket/usocket).


# TCP/IP

As usual, we will use quicklisp to load usocket.

    (ql:quickload "usocket")

Now we need to create a server. There are 2 primary functions that we need
to call. `usocket:socket-listen` and `usocket:socket-accept`.

`usocket:socket-listen` binds to a port and listens on it. It returns a socket
object. We need to wait with this object until we get a connection that we
accept. That's where `usocket:socket-accept` comes in. It's a blocking call
that returns only when a connection is made. This returns a new socket object
that is specific to that connection. We can then use that connection to
communicate with our client.

So, what were the problems I faced due to my mistakes?
Mistake 1 - My initial understanding was that `socket-accept` would return
a stream object. NO.... It returns a socket object. In hindsight, its correct
and my own mistake cost me time. So, if you want to write to the socket, you
need to actually get the corresponding stream from this new socket. The socket
object has a stream slot and we need to explicitly use that. And how does one
know that? `(describe connection)` is your friend!

Mistake 2 - You need to close both the new socket and the server socket.
Again this is pretty obvious but since my initial code was only closing
the connection, I kept running into a socket in use problem. Of course
one more option is to reuse the socket when we listen.

Once you get past these mistakes, it's pretty easy to do the rest. Close
the connections and the server socket and boom you are done!


~~~lisp
(defun create-server (port)
  (let* ((socket (usocket:socket-listen "127.0.0.1" port))
	 (connection (usocket:socket-accept socket :element-type 'character)))
    (unwind-protect
        (progn
	      (format (usocket:socket-stream connection) "Hello World~%")
	      (force-output (usocket:socket-stream connection)))
      (progn
	    (format t "Closing sockets~%")
	    (usocket:socket-close connection)
        (usocket:socket-close socket)))))
~~~

Now for the client. This part is easy. Just connect to the server port
and you should be able to read from the server. The only silly mistake I
made here was to use read and not read-line. So, I ended up seeing only a
"Hello" from the server. I went for a walk and came back to find the issue
and fix it.


~~~lisp
(defun create-client (port)
  (usocket:with-client-socket (socket stream "127.0.0.1" port :element-type 'character)
    (unwind-protect
         (progn
           (usocket:wait-for-input socket)
           (format t "Input is: ~a~%" (read-line stream)))
      (usocket:socket-close socket))))
~~~

So, how do you run this? You need two REPLs, one for the server
and one for the client. Load this file in both REPLs. Create the
server in the first REPL.

    (create-server 12321)

Now you are ready to run the client on the second REPL

    (create-client 12321)

Voila! You should see "Hello World" on the second REPL.


# UDP/IP

As a protocol, UDP is connection-less, and therefore there is no
concept of binding and accepting a connection. Instead we only do a
`socket-connect` but pass a specific set of parameters to make sure that
we create an UDP socket that's waiting for data on a particular port.

So, what were the problems I faced due to my mistakes?
Mistake 1 - Unlike TCP, you don't pass host and port to `socket-connect`.
If you do that, then you are indicating that you want to send a packet.
Instead, you pass `nil` but you set `:local-host` and `:local-port` to the address
and port that you want to receive data on. This part took some time to
figure out, because the documentation didn't cover it. Instead reading
a bit of code from
https://code.google.com/p/blackthorn-engine-3d/source/browse/src/examples/usocket/usocket.lisp helped a lot.

Also, since UDP is connectionless, anyone can send data to it at any
time. So, we need to know which host/port did we get data from so
that we can respond on it. So we bind multiple values to `socket-receive`
and use those values to send back data to our peer "client".

~~~lisp
(defun create-server (port buffer)
  (let* ((socket (usocket:socket-connect nil nil
					:protocol :datagram
					:element-type '(unsigned-byte 8)
					:local-host "127.0.0.1"
					:local-port port)))
    (unwind-protect
	 (multiple-value-bind (buffer size client receive-port)
	     (usocket:socket-receive socket buffer 8)
	   (format t "~A~%" buffer)
	   (usocket:socket-send socket (reverse buffer) size
				:port receive-port
				:host client))
      (usocket:socket-close socket))))
~~~


Now for the sender/receiver. This part is pretty easy. Create a socket,
send data on it and receive data back.

~~~lisp
(defun create-client (port buffer)
  (let ((socket (usocket:socket-connect "127.0.0.1" port
					 :protocol :datagram
					 :element-type '(unsigned-byte 8))))
    (unwind-protect
	 (progn
	   (format t "Sending data~%")
	   (replace buffer #(1 2 3 4 5 6 7 8))
	   (format t "Receiving data~%")
	   (usocket:socket-send socket buffer 8)
	   (usocket:socket-receive socket buffer 8)
	   (format t "~A~%" buffer))
      (usocket:socket-close socket))))
~~~


So, how do you run this? You need again two REPLs, one for the server
and one for the client. Load this file in both REPLs. Create the
server in the first REPL.

    (create-server 12321 (make-array 8 :element-type '(unsigned-byte 8)))

Now you are ready to run the client on the second REPL

    (create-client 12321 (make-array 8 :element-type '(unsigned-byte 8)))

Voila! You should see a vector `#(1 2 3 4 5 6 7 8)` on the first REPL
and `#(8 7 6 5 4 3 2 1)` on the second one.


# Credit

This guide originally comes from https://gist.github.com/shortsightedsid/71cf34282dfae0dd2528

---
title: Interfacing with your OS
---


The ANSI Common Lisp standard doesn't mention this topic. (Keep in mind that it was written at a time where [Lisp Machines](http://kogs-www.informatik.uni-hamburg.de/~moeller/symbolics-info/symbolics.html) were at their peak. On these boxes Lisp _was_ your operating system!) So almost everything that can be said here depends on your OS and your implementation.
There are, however, some widely used libraries, which either come with your Common Lisp implementation, or are easily
available through [Quicklisp](https://www.quicklisp.org/beta/). These include:

* ASDF3, which is included with almost all Common Lisp implementations,
  includes [Utilities for Implementation- and OS- Portability (UIOP)](https://common-lisp.net/project/asdf/uiop.html).
* [osicat](https://common-lisp.net/project/osicat/)
* [unix-opts](http://quickdocs.org/unix-opts/) is a command-line argument parser, similar to Python's `argparse`.


<a name="env"></a>

## Accessing Environment variables

UIOP comes with a function that'll allow you to look at Unix/Linux environment variables on a lot of different CL implementations:

~~~lisp
* (uiop:getenv "HOME")
  "/home/edi"
~~~

Below is an example implementation:

~~~lisp
* (defun my-getenv (name &optional default)
    "Obtains the current value of the POSIX environment variable NAME."
    (declare (type (or string symbol) name))
    (let ((name (string name)))
      (or #+abcl (ext:getenv name)
         #+ccl (ccl:getenv name)
         #+clisp (ext:getenv name)
         #+cmu (unix:unix-getenv name) ; since CMUCL 20b
         #+ecl (si:getenv name)
         #+gcl (si:getenv name)
         #+mkcl (mkcl:getenv name)
         #+sbcl (sb-ext:posix-getenv name)
         default)))
MY-GETENV
* (my-getenv "HOME")
"/home/edi"
* (my-getenv "HOM")
NIL
* (my-getenv "HOM" "huh?")
"huh?"
~~~

You should also note that some of these implementations also provide the ability to _set_ these variables. These include ECL (`si:setenv`) and AllegroCL, LispWorks, and CLISP where you can use the functions from above together with [`setf`](http://www.lispworks.com/documentation/HyperSpec/Body/m_setf_.htm). This feature might be important if you want to start subprocesses from your Lisp environment.

Also note that the
[Osicat](https://www.common-lisp.net/project/osicat/manual/osicat.html#Environment)
library has the method `(environment-variable "name")`, on POSIX-like
systems including Windows. It is also `fset`-able.

<a name="accessing-command-line"></a>

## Accessing the command line arguments

### Basics

Accessing command line arguments is implementation-specific but it
appears most implementations have a way of getting at
them. [Roswell](https://github.com/roswell/roswell/wiki) or external
libraries (see next section) make it portable.

[SBCL](http://www.sbcl.org) stores the arguments list in the special variable `sb-ext:*posix-argv*`

~~~lisp
$ sbcl my-command-line-arg
~~~

....

~~~lisp
* sb-ext:*posix-argv*

("sbcl" "my-command-line-arg")
*
~~~

More on using this to write standalone Lisp scripts can be found in the [SBCL Manual](http://www.sbcl.org/manual/index.html#Command_002dline-arguments)

[LispWorks](http://www.lispworks.com) has `system:*line-arguments-list*`

~~~lisp
* system:*line-arguments-list*
("/Users/cbrown/Projects/lisptty/tty-lispworks" "-init" "/Users/cbrown/Desktop/lisp/lispworks-init.lisp")
~~~

[CMUCL](http://www.cons.org/cmucl/) has interesting extensions for [manipulating the arguments](https://common-lisp.net/project/cmucl/docs/cmu-user/html/UNIX-Interface.html)

Here's a quick function to return the argument strings list across multiple implementations:

~~~lisp
(defun my-command-line ()
  (or
   #+SBCL *posix-argv*
   #+LISPWORKS system:*line-arguments-list*
   #+CMU extensions:*command-line-words*
   nil))
~~~

Now it would be handy to access them in a portable way and to parse
them according to a schema definition.

### Parsing command line arguments

We have a look at the
[Awesome CL list#scripting](https://github.com/CodyReichert/awesome-cl#scripting)
section and we'll show how to use [unix-opts](https://github.com/mrkkrp/unix-opts).

~~~lisp
(ql:quickload "unix-opts")
~~~

We can now refer to it with its `opts` nickname.

We first declare our arguments with `opts:define-opts`, for example

~~~lisp
(opts:define-opts
    (:name :help
           :description "print this help text"
           :short #\h
           :long "help")
    (:name :level
           :description "The level of something (integer)."
           :short #\l
           :long "level"
           :arg-parser #'parse-integer))
~~~

Everything should be self-explanatory. Note that `#'parse-integer` is
a built-in CL function.

Now we can parse and get them with `opts:get-opts`, which returns two
values: the first is the list of valid options and the second the
remaining free arguments. We then must use multiple-value-bind to
catch everything:

~~~lisp
(multiple-value-bind (options free-args)
    ;; There is no error handling yet (specially for options not having their argument).
    (opts:get-opts)
~~~

We can explore this by giving a list of strings (as options) to
`get-opts`:


~~~lisp
(multiple-value-bind (options free-args)
                   (opts:get-opts '("hello" "-h" "-l" "1"))
                 (format t "Options: ~a~&" options)
                 (format t "free args: ~a~&" free-args))
Options: (HELP T LEVEL 1)
free args: (hello)
NIL
~~~

If we put an unknown option, we get into the debugger. We refer you to
unix-opts' documentation and code sample to deal with erroneous
options and other errors.

We can access the arguments stored in `options` with `getf` (it is a
property list), and we can exit (in a portable way) with
`opts:exit`. So, for example:

~~~lisp
(multiple-value-bind (options free-args)
    ;; No error handling.
    (opts:get-opts)

  (if (getf options :help)
      (progn
        (opts:describe
         :prefix "My app. Usage:"
         :args "[keywords]")
        (exit))) ;; <= exit takes an optional return status.
    ...
~~~

And that's it for now, you know the essential. See the documentation
for a complete example, and the Awesome CL list for useful packages to
use in the terminal (ansi colors, printing tables and progress bars,
interfaces to readline,…).


## Running external programs

**uiop** has us covered, and is probably included in your Common Lisp
implementation.

### Synchronously

[`uiop:run-program`](https://common-lisp.net/project/asdf/uiop.html#UIOP_002fRUN_002dPROGRAM) either takes a string as argument, denoting the
name of the executable to run, or a list of strings, for the program and its arguments:

~~~lisp
(uiop:run-program "firefox")
~~~

or

~~~lisp
(uiop:run-program (list "firefox" "http:url"))
~~~

This will process the program output as specified and return the
processing results when the program and its output processing are
complete.

Use `:output t` to print to standard output.

This function has the following optional arguments:

~~~lisp
run-program (command &rest keys &key
                         ignore-error-status
                         (force-shell nil force-shell-suppliedp)
                         input
                         (if-input-does-not-exist :error)
                         output
                         (if-output-exists :supersede)
                         error-output
                         (if-error-output-exists :supersede)
                         (element-type #-clozure *default-stream-element-type* #+clozure 'character)
                         (external-format *utf-8-external-format*)
                       &allow-other-keys)
~~~

It will always call a shell (rather than directly executing the command when possible)
if `force-shell` is specified.  Similarly, it will never call a shell if `force-shell` is
specified to be `nil`.

Signal a continuable `subprocess-error` if the process wasn't successful (exit-code 0),
unless `ignore-error-status` is specified.

If `output` is a pathname, a string designating a pathname, or `nil` (the default)
designating the null device, the file at that path is used as output.
If it's `:interactive`, output is inherited from the current process;
beware that this may be different from your `*standard-output*`,
and under `slime` will be on your `*inferior-lisp*` buffer.
If it's `t`, output goes to your current `*standard-output*` stream.
Otherwise, `output` should be a value that is a suitable first argument to
`slurp-input-stream` (qv.), or a list of such a value and keyword arguments.
In this case, `run-program` will create a temporary stream for the program output;
the program output, in that stream, will be processed by a call to `slurp-input-stream`,
using `output` as the first argument (or the first element of `output`, and the rest as keywords).
The primary value resulting from that call (or `nil` if no call was needed)
will be the first value returned by `run-program.`
E.g., using `:output :string` will have it return the entire output stream as a string.
And using `:output '(:string :stripped t`) will have it return the same string
stripped of any ending newline.

`if-output-exists`, which is only meaningful if `output` is a string or a
pathname, can take the values `:error`, `:append`, and `:supersede` (the
default). The meaning of these values and their effect on the case
where `output` does not exist, is analogous to the `if-exists` parameter
to `open` with `:direction` `:output`.

`error-output` is similar to `output`, except that the resulting value is returned
as the second value of `run-program`. t designates the `*error-output*`.
Also `:output` means redirecting the error output to the output stream,
in which case `nil` is returned.

`if-error-output-exists` is similar to `if-output-exist`, except that it
affects `error-output` rather than `output`.

`input` is similar to `output`, except that `vomit-output-stream` is used,
no value is returned, and T designates the `*standard-input*`.

`if-input-does-not-exist`, which is only meaningful if `input` is a string
or a pathname, can take the values `:create` and `:error` (the
default). The meaning of these values is analogous to the
`if-does-not-exist` parameter to `open` with `:direction :input`.

`element-type` and `external-format` are passed on
to your Lisp implementation, when applicable, for creation of the output stream.

One and only one of the stream slurping or vomiting may or may not happen
in parallel in parallel with the subprocess,
depending on options and implementation,
and with priority being given to output processing.
Other streams are completely produced or consumed
before or after the subprocess is spawned, using temporary files.

`run-program` returns 3 values:

* the result of the `output` slurping if any, or `nil`
* the result of the `error-output` slurping if any, or `nil`
* either 0 if the subprocess exited with success status, or an
  indication of failure via the `exit-code` of the process


### Asynchronously

With [`uiop:launch-program`](https://common-lisp.net/project/asdf/uiop.html#UIOP_002fLAUNCH_002dPROGRAM).

Its signature is the following:

~~~lisp
launch-program (command &rest keys
                         &key
                           input
                           (if-input-does-not-exist :error)
                           output
                           (if-output-exists :supersede)
                           error-output
                           (if-error-output-exists :supersede)
                           (element-type #-clozure *default-stream-element-type*
                                         #+clozure 'character)
                           (external-format *utf-8-external-format*)
                           directory
                           #+allegro separate-streams
                           &allow-other-keys)
~~~

Output (stdout) from the launched program is set using the `output`
keyword:

 - If `output` is a pathname, a string designating a pathname, or
   `nil` (the default) designating the null device, the file at that
   path is used as output.
 - If it's `:interactive`, output is inherited from the current process;
   beware that this may be different from your `*standard-output*`, and
   under Slime will be on your `*inferior-lisp*` buffer.
 - If it's `T`, output goes to your current `*standard-output*` stream.
 - If it's `:stream`, a new stream will be made available that can be accessed via
   `process-info-output` and read from.
 - Otherwise, `output` should be a value that the underlying lisp
   implementation knows how to handle. 

`if-output-exists`, which is only meaningful if `output` is a string or a
pathname, can take the values `:error`, `:append`, and `:supersede` (the
default). The meaning of these values and their effect on the case
where `output` does not exist, is analogous to the `if-exists` parameter
to `open` with `:DIRECTION :output`.

`error-output` is similar to `output`. T designates the `*error-output*`,
`:output` means redirecting the error output to the output stream,
and `:stream` causes a stream to be made available via
`process-info-error-output`.

`launch-program` returns a `process-info` object, which look like the following ([source](https://gitlab.common-lisp.net/asdf/asdf/blob/master/uiop/launch-program.lisp#L205)):


~~~lisp
(defclass process-info ()
    (
     ;; The advantage of dealing with streams instead of PID is the
     ;; availability of functions like `sys:pipe-kill-process`.
     (process :initform nil)
     (input-stream :initform nil)
     (output-stream :initform nil)
     (bidir-stream :initform nil)
     (error-output-stream :initform nil)
     ;; For backward-compatibility, to maintain the property (zerop
     ;; exit-code) <-> success, an exit in response to a signal is
     ;; encoded as 128+signum.
     (exit-code :initform nil)
     ;; If the platform allows it, distinguish exiting with a code
     ;; >128 from exiting in response to a signal by setting this code
     (signal-code :initform nil)))
~~~

See the [docstrings](https://gitlab.common-lisp.net/asdf/asdf/blob/master/uiop/launch-program.lisp#L508).

#### Test if a subprocess is alive

`uiop:process-alive-p` tests if a process is still alive, given a
`process-info` object returned by `launch-program`:

~~~lisp
* (defparameter *shell* (uiop:launch-program "bash" :input :stream :output :stream))

;; inferior shell process now running
* (uiop:process-alive-p *shell*)
T

;; Close input and output streams
* (uiop:close-streams *shell*)
* (uiop:process-alive-p *shell*)
NIL
~~~

#### Get the exit code

We can use `uiop:wait-process`. If the process is finished, it returns
immediately, and returns the exit code. If not, it waits for the
process to terminate.

~~~lisp
(uiop:process-alive-p *process*)
NIL
(uiop:wait-process *process*)
0
~~~

An exit code to 0 means success (use `zerop`).

The exit code is also stored in the `exit-code` slot of our
`process-info` object. We see from the class definition above that it
has no accessor, so we'll use `slot-value`. It has an `initform` to
nil, so we don't have to check if the slot is bound.  We can do:

~~~lisp
(slot-value *my-process* 'uiop/launch-program::exit-code)
0
~~~

The trick is that we *must* run `wait-process` beforehand, otherwise
the result will be `nil`.

Since `wait-process` is blocking, we can do it on a new thread:

~~~lisp
(bt:make-thread
  (lambda ()
    (let ((exit-code (uiop:wait-process
                       (uiop:launch-program (list "of" "commands"))))
      (if (zerop exit-code)
          (print :success)
          (print :failure)))))
  :name "Waiting for <program>")
~~~


Note that `run-program` returns the exit code as the third value.


### Input and output from subprocess

If the `input` keyword is set to `:stream`, then a stream is created
and can be written to in the same way as a file. The stream can be
accessed using `uiop:process-info-input`:

~~~lisp
;; Start the inferior shell, with input and output streams
* (defparameter *shell* (uiop:launch-program "bash" :input :stream :output :stream))
;; Write a line to the shell
* (write-line "find . -name '*.md'" (uiop:process-info-input *shell*))
;; Flush stream
* (force-output (uiop:process-info-input *shell*))
~~~

where [write-line](http://clhs.lisp.se/Body/f_wr_stg.htm) writes the
string to the given stream, adding a newline at the end. The
[force-output](http://clhs.lisp.se/Body/f_finish.htm) call attempts to
flush the stream, but does not wait for completion.

Reading from the output stream is similar, with
`uiop:process-info-output` returning the output stream:

~~~lisp
* (read-line (uiop:process-info-output *shell*))
~~~

In some cases the amount of data to be read is known, or there are
delimiters to determine when to stop reading. If this is not the case,
then calls to [read-line](http://clhs.lisp.se/Body/f_rd_lin.htm) can
hang while waiting for data. To avoid this,
[listen](http://clhs.lisp.se/Body/f_listen.htm) can be used to test if
a character is available:

~~~lisp
* (let ((stream (uiop:process-info-output *shell*)))
     (loop while (listen stream) do
         ;; Characters are immediately available
         (princ (read-line stream))
         (terpri)))
~~~

There is also
[read-char-no-hang](http://clhs.lisp.se/Body/f_rd_c_1.htm) which reads
a single character, or returns `nil` if no character is available.
Note that due to issues like buffering, and the timing of when the
other process is executed, there is no guarantee that all data sent
will be received before `listen` or `read-char-no-hang` return `nil`. 

<a name="fork-cmucl"></a>

## Forking with CMUCL

Here's a function by Martin Cracauer that'll allow you to compile a couple of files in parallel with [CMUCL](http://www.cons.org/cmucl/). It demonstrates how to use the UNIX [`fork`](http://www.freebsd.org/cgi/man.cgi?query=fork&apropos=0&sektion=0&manpath=FreeBSD+4.5-RELEASE&format=html) system call with this CL implementation.

~~~lisp
(defparameter *sigchld* 0)

(defparameter *compile-files-debug* 2)

(defun sigchld-handler (p1 p2 p3)
  (when (> 0 *compile-files-debug*)
    (print (list "returned" p1 p2 p3))
    (force-output))
  (decf *sigchld*))

(defun compile-files (files &key (load nil))
  (setq *sigchld* 0)
  (system:enable-interrupt unix:sigchld #'sigchld-handler)
  (do ((f files (cdr f)))
      ((not f))
    (format t "~&process ~d diving for ~a" (unix:unix-getpid)
            `(compile-file ,(car f)))
    (force-output)
    (let ((pid (unix:unix-fork)))
      (if (/= 0 pid)
          ;; parent
          (incf *sigchld*)
          ;; child
          (progn
            (compile-file (car f) :verbose nil :print nil)
            (unix:unix-exit 0)))))
  (do () ((= 0 *sigchld*))
    (sleep 1)
    (when (> 0 *compile-files-debug*)
      (format t "~&process ~d still waiting for ~d childs"
              (unix:unix-getpid) *sigchld*)))
  (when (> 0 *compile-files-debug*)
    (format t "~&finished"))
  (when load
    (do ((f files (cdr f)))
        ((not f))
      (load (compile-file-pathname (car f))))))
~~~

---
title: Foreign Function Interfaces
---

The ANSI Common Lisp standard doesn't mention this topic. So almost everything that can be said here depends on your OS and your implementation.<a name="clisp-gethost">### Example: Calling 'gethostname' from CLISP

Note: You should read the</a> [relevant chapter](http://clisp.sourceforge.net/impnotes.html#dffi) from the CLISP implementation notes before you proceed.

`int gethostname(char *name, int len)` follows a typical pattern of C "out"-parameter convention - it expects a pointer to a buffer it's going to fill. So you must view this parameter as either `:OUT` or `:IN-OUT`. Additionally, one must tell the function the size of the buffer. Here `len` is just an `:IN` parameter. Sometimes this will be an `:IN-OUT` parameter, returning the number of bytes actually filled in.

So `name` is actually a pointer to an array of up to `len` characters, regardless of what the poor "`char *`" C prototype says, to be used like a C string (0-termination). How many elements are in the array? Luckily, in our case, you can find it out without calculating the `sizeof()` a C structure. It's a hostname that will be returned. The Solaris 2.x manpage says "Host names are limited to MAXHOSTNAMELEN characters, currently 256."

Also, in the present example, you can use allocation `:ALLOCA`, like you'd do in C: stack-allocate a temporary. Why make things worse when using Lisp than when using C?

This yields the following useful signature for your foreign function:

~~~lisp
(ffi:def-c-call-out gethostname
     (:arguments (name (ffi:c-ptr (ffi:c-array-max ffi:char 256))
     :out :alloca)
     (len ffi:int))
     ;; (:return-type BOOLEAN) could have been used here
     ;; (Solaris says it's either 0 or -1).
     (:return-type ffi:int))

     (defun myhostname ()
     (multiple-value-bind (success name)
     ;; :OUT or :IN-OUT parameters are returned via multiple values
     (gethostname 256)
     (if (zerop success)
     (subseq name 0 (position #\null name))
     (error ... ; errno may be set
     ...))))
     (defvar hostname (myhostname))
~~~

Possibly `SUBSEQ` and `POSITION` are superfluous, thanks to `C-ARRAY-MAX` as opposed to `C-ARRAY`:

~~~lisp
(defun myhostname ()
     (multiple-value-bind (success name)
     ;; :out or :in-out parameters are returned via multiple values
     (gethostname 256)
     (if (zerop success) name
     (error ... ; errno may be set
     ...))))
~~~

<a name="alisp-gethost"></a>

### Example: Calling 'gethostname' from Allegro CL

This is how the same example above would be written in Allegro Common Lisp version 6 and above. ACL doesn't explicitly distinguish between `input` and `output` arguments. The way to declare an argument as `output` (i.e., modifiable by C) is to use an array, since arrays are passed by reference and C therefore receives a pointer to a memory location (which is what it expects). In this case things are made even easier by the fact that `gethostname()` expects an array of char, and a `SIMPLE-ARRAY` of `CHARACTER` represents essentially the same thing in Lisp. The foreign function definition is therefore the following:

~~~lisp
(def-foreign-call (c-get-hostname "gethostname")
     ((name (* :char) (simple-array 'character (*)))
     (len :int integer))
     :returning :int)
~~~

Let's read this line by line: this form defines a Lisp function called `C-GET-HOSTNAME` that calls the C function `gethostname()`. It takes two arguments: the first one, called `NAME`, is a pointer to a char (`*char` in C), and a `SIMPLE-ARRAY` of characters in Lisp; the second one is called `LEN`, and is an integer. The function returns an integer value.

And now the Lisp side:

~~~lisp
(defun get-hostname ()
     (let* ((name (make-array 256 :element-type 'character))
     (result (c-get-hostname name 256)))
     (if (zerop result)
     (let ((pos (position #\null name)))
     (subseq name 0 pos))
     (error "gethostname() failed."))))
~~~

This function creates the `NAME` array, calls `C-GET-HOSTNAME` to fill it and then checks the returned value. If the value is zero, then the call was successful, and we return the contents of `NAME` up to the first 0 character (the string terminator in C), otherwise we signal an error. Note that, unlike the previous example, we allocate the string in Lisp, and we rely on the Lisp garbage collector to get rid of it after the function terminates. Here is a usage example:

~~~lisp
* (get-hostname)
     "terminus"
~~~

Working with strings is, in general, easier than the previous example showed. Let's say you want to call `getenv()` from Lisp to access the value of an environment variable. `getenv()` takes a string argument (the variable name) and returns another string (the variable value). To be more precise, the argument is a _pointer_ to a sequence of characters that should have been allocated by the caller, and the return value is a pointer to an already-existing sequence of chars (in the environment). Here is the definition of `C-GETENV`:

~~~lisp
(def-foreign-call (c-getenv "getenv")
     ((var (* :char) string))
     :returning :int
     :strings-convert t)
~~~

The argument in this case is still a pointer to char in C, but we can declare it a `STRING` to Lisp. The return value is a pointer, so we declare it as integer. Finally, the `:STRINGS-CONVERT` keyword argument specifies that ACL should automatically translate the Lisp string passed as the first argument into a C string. Here is how it's used:

~~~lisp
* (c-getenv "SHELL")
     -1073742215
~~~

If you are surprised by the return value, just remember that `C-GETENV` returns a pointer, and we must tell Lisp how to interpret the contents of the memory location pointed to by it. Since in this case we know that it will point to a C string, we can use the `FF:NATIVE-TO-STRING` function to convert it to a Lisp string:

~~~lisp
* (native-to-string (c-getenv "SHELL"))
     "/bin/tcsh"
     9
     9
~~~

(The second and third values are the number of characters and bytes copied, respectively). One caveat: if you ask for the value of a non-existent variable, `C-GETENV` will return 0, and `NATIVE-TO-STRING` will fail. So a safer example would be:

~~~lisp
* (let ((ptr (c-getenv "NOSUCHVAR")))
     (unless (zerop ptr)
     (native-to-string ptr)))
     NIL
~~~

---
title: Threads
---


<a name="intro"></a>

## Introduction

By _threads_, we mean separate execution strands within a single Lisp
process, sharing the same address space. Typically, execution is
automatically switched between these strands by the system (either by
the lisp kernel or by the operating system) so that tasks appear to be
completed in parallel (asynchronously). This page discusses the
creation and management of threads and some aspects of interactions
between them. For information about the interaction between lisp and
other _processes_, see [Interfacing with your OS](os.html).

An instant pitfall for the unwary is that most implementations refer
(in nomenclature) to threads as _processes_ - this is a historical
feature of a language which has been around for much longer than the
term _thread_. Call this maturity a sign of stable implementations, if
you will.

The ANSI Common Lisp standard doesn't mention this topic. We will
present here the portable
[bordeaux-threads](https://github.com/sionescu/bordeaux-threads)
library, [SBCL threads](http://www.sbcl.org/manual/index.html#sb_002dconcurrency) and the [lparallel](https://lparallel.org)
library.

Bordeaux-threads is a de-facto standard portable library, that exposes
rather low-level primitives. Lparallel builds on it and features:

-  a simple model of task submission with receiving queue
-  constructs for expressing fine-grained parallelism
-  **asynchronous condition handling** across thread boundaries
-  **parallel versions of map, reduce, sort, remove**, and many others
-  **promises**, futures, and delayed evaluation constructs
-  computation trees for parallelizing interconnected tasks
-  bounded and unbounded FIFO **queues**
-  **channels**
-  high and low priority tasks
-  task killing by category
-  integrated timeouts

For more libraries on parallelism and concurrency, see the [awesome CL list](https://github.com/CodyReichert/awesome-cl#parallelism-and-concurrency)
and [Quickdocs](http://quickdocs.org/).

<a name="why_bother"></a>

### Why bother?

The first question to resolve is: why bother with threads? Sometimes
your answer will simply be that your application is so straightforward
that you need not concern yourself with threads at all. But in many
other cases it's difficult to imagine how a sophisticated application
can be written without multi-threading. For example:

*   you might be writing a server which needs to be able to respond to
    more than one user / connection at a time (for instance: a web
    server) on the Sockets page);
*   you might want to perform some background activity, without
    halting the main application while this is going on;
*   you might want your application to be notified when a certain time
    has elapsed;
*   you might want to keep the application running and active while
    waiting for some system resource to become available;
*   you might need to interface with some other system which requires
    multithreading (for example, "windows" under Windows which
    generally run in their own threads);
*   you might want to associate different contexts (e.g. different
    dynamic bindings) with different parts of the application;
*   you might even have the simple need to do two things at once.


<a name="emergency"></a>

### What is Concurrency? What is Parallelism?

*Credit: The following was first written on
[z0ltan.wordpress.com](https://z0ltan.wordpress.com/2016/09/02/basic-concurrency-and-parallelism-in-common-lisp-part-3-concurrency-using-bordeaux-and-sbcl-threads/)
by Timmy Jose.*

Concurrency is a way of running different, possibly related, tasks
seemingly simultaneously. What this means is that even on a single
processor machine, you can simulate simultaneity using threads (for
instance) and context-switching them.

In the case of system (native OS) threads, the scheduling and context
switching is ultimately determined by the OS. This is the case with
Java threads and Common Lisp threads.

In the case of “green” threads, that is to say threads that are
completely managed by the program, the scheduling can be completely
controlled by the program itself. Erlang is a great example of this
approach.

So what is the difference between Concurrency and Parallelism?
Parallelism is usually defined in a very strict sense to mean
independent tasks being run in parallel, simultaneously, on different
processors or on different cores. In this narrow sense, you really
cannot have parallelism on a single-core, single-processor machine.

It rather helps to differentiate between these two related concepts on
a more abstract level – concurrency primarily deals with providing the
illusion of simultaneity to clients so that the system doesn’t appear
locked when a long running operation is underway. GUI systems are a
wonderful example of this kind of system. Concurrency is therefore
concerned with providing good user experience and not necessarily
concerned with performance benefits.

Java’s Swing toolkit and JavaScript are both single-threaded, and yet
they can give the appearance of simultaneity because of the context
switching behind the scenes. Of course, concurrency is implemented
using multiple threads/processes in most cases.

Parallelism, on the other hand, is mostly concerned with pure
performance gains. For instance, if we are given a task to find the
squares of all the even numbers in a given range, we could divide the
range into chunks which are then run in parallel on different cores or
different processors, and then the results can be collated together to
form the final result. This is an example of Map-Reduce in action.

So now that we have separated the abstract meaning of Concurrency from
that of Parallelism, we can talk a bit about the actual mechanism used
to implement them. This is where most of the confusion arise for a lot
of people. They tend to tie down abstract concepts with specific means
of implementing them. In essence, both abstract concepts may be
implemented using the same mechanisms! For instance, we may implement
concurrent features and parallel features using the same basic thread
mechanism in Java. It’s only the conceptual intertwining or
independence of tasks at an abstract level that makes the difference
for us.

For instance, if we have a task where part of the work can be done on
a different thread (possibly on a different core/processor), but the
thread which spawns this thread is logically dependent on the results
of the spawned thread (and as such has to “join” on that thread), it
is still Concurrency!

So the bottomline is this – Concurrency and Parallelism are different
concepts, but their implementations may be done using the same
mechanisms — threads, processes, etc.


## Bordeaux threads

The Bordeaux library provides a platform independent way to handle
basic threading on multiple Common Lisp implementations. The
interesting bit is that it itself does not really create any native
threads — it relies entirely on the underlying implementation to do
so.

On there other hand, it does provide some useful extra features in its
own abstractions over the lower-level threads.

Also, you can see from the demo programs that a lot of the Bordeaux
functions seem quite similar to those used in SBCL. I don’t really
think that this is a coincidence.

You can refer to the documentation for more details (check the
“Wrap-up” section).

### Installing Bordeaux Threads

First let’s load up the Bordeaux library using Quicklisp:

~~~lisp
CL-USER> (ql:quickload :bt-semaphore)
To load "bt-semaphore":
  Load 1 ASDF system:
    bt-semaphore
; Loading "bt-semaphore"

(:BT-SEMAPHORE)
~~~


### Checking for thread support in Common Lisp

Regardless of the Common Lisp implementation, there is a standard way
to check for thread support availability:

~~~lisp
CL-USER> (member :thread-support *FEATURES*)
(:THREAD-SUPPORT :SWANK :QUICKLISP :ASDF-PACKAGE-SYSTEM :ASDF3.1 :ASDF3 :ASDF2
 :ASDF :OS-MACOSX :OS-UNIX :NON-BASE-CHARS-EXIST-P :ASDF-UNICODE :64-BIT
 :64-BIT-REGISTERS :ALIEN-CALLBACKS :ANSI-CL :ASH-RIGHT-VOPS :BSD
 :C-STACK-IS-CONTROL-STACK :COMMON-LISP :COMPARE-AND-SWAP-VOPS
 :COMPLEX-FLOAT-VOPS :CYCLE-COUNTER :DARWIN :DARWIN9-OR-BETTER :FLOAT-EQL-VOPS
 :FP-AND-PC-STANDARD-SAVE :GENCGC :IEEE-FLOATING-POINT :INLINE-CONSTANTS
 :INODE64 :INTEGER-EQL-VOP :LINKAGE-TABLE :LITTLE-ENDIAN
 :MACH-EXCEPTION-HANDLER :MACH-O :MEMORY-BARRIER-VOPS :MULTIPLY-HIGH-VOPS
 :OS-PROVIDES-BLKSIZE-T :OS-PROVIDES-DLADDR :OS-PROVIDES-DLOPEN
 :OS-PROVIDES-PUTWC :OS-PROVIDES-SUSECONDS-T :PACKAGE-LOCAL-NICKNAMES
 :PRECISE-ARG-COUNT-ERROR :RAW-INSTANCE-INIT-VOPS :SB-DOC :SB-EVAL :SB-LDB
 :SB-PACKAGE-LOCKS :SB-SIMD-PACK :SB-SOURCE-LOCATIONS :SB-TEST :SB-THREAD
 :SB-UNICODE :SBCL :STACK-ALLOCATABLE-CLOSURES :STACK-ALLOCATABLE-FIXED-OBJECTS
 :STACK-ALLOCATABLE-LISTS :STACK-ALLOCATABLE-VECTORS
 :STACK-GROWS-DOWNWARD-NOT-UPWARD :SYMBOL-INFO-VOPS :UD2-BREAKPOINTS :UNIX
 :UNWIND-TO-FRAME-AND-CALL-VOP :X86-64)
~~~

If there were no thread support, it would show “NIL” as the value of the expression.

Depending on the specific library being used, we may also have
different ways of checking for concurrency support, which may be used
instead of the common check mentioned above.

For instance, in our case, we are interested in using the Bordeaux
library. To check whether there is support for threads using this
library, we can see whether the *supports-threads-p* global variable
is set to NIL (no support) or T (support available):

~~~lisp
CL-USER> bt:*supports-threads-p*
T
~~~

Okay, now that we’ve got that out of the way, let’s test out both the
platform-independent library (Bordeaux) as well as the
platform-specific support (SBCL in this case).

To do this, let us work our way through a number of simple examples:

-    Basics — list current thread, list all threads, get thread name
-    Update a global variable from a thread
-    Print a message onto the top-level using a thread
-    Print a message onto the top-level — fixed
-    Print a message onto the top-level — better
-    Modify a shared resource from multiple threads
-    Modify a shared resource from multiple threads — fixed using locks
-    Modify a shared resource from multiple threads — using atomic operations
-    Joining on a thread, destroying a thread example

### Basics — list current thread, list all threads, get thread name

~~~lisp
    ;;; Print the current thread, all the threads, and the current thread's name
    (defun print-thread-info ()
      (let* ((curr-thread (bt:current-thread))
             (curr-thread-name (bt:thread-name curr-thread))
             (all-threads (bt:all-threads)))
        (format t "Current thread: ~a~%~%" curr-thread)
        (format t "Current thread name: ~a~%~%" curr-thread-name)
        (format t "All threads:~% ~{~a~%~}~%" all-threads))
      nil)
~~~

And the output:

~~~lisp
    CL-USER> (print-thread-info)
    Current thread: #<THREAD "repl-thread" RUNNING {10043B8003}>

    Current thread name: repl-thread

    All threads:
     #<THREAD "repl-thread" RUNNING {10043B8003}>
    #<THREAD "auto-flush-thread" RUNNING {10043B7DA3}>
    #<THREAD "swank-indentation-cache-thread" waiting on: #<WAITQUEUE  {1003A28103}> {1003A201A3}>
    #<THREAD "reader-thread" RUNNING {1003A20063}>
    #<THREAD "control-thread" waiting on: #<WAITQUEUE  {1003A19E53}> {1003A18C83}>
    #<THREAD "Swank Sentinel" waiting on: #<WAITQUEUE  {1003790043}> {1003788023}>
    #<THREAD "main thread" RUNNING {1002991CE3}>

    NIL
~~~

Update a global variable from a thread:

~~~lisp
    (defparameter *counter* 0)

    (defun test-update-global-variable ()
      (bt:make-thread
       (lambda ()
         (sleep 1)
         (incf *counter*)))
      *counter*)
~~~

We create a new thread using `bt:make-thread`, which takes a lambda
abstraction as a parameter. Note that this lambda abstraction cannot
take any parameters.

Another point to note is that unlike some other languages (Java, for
instance), there is no separation from creating the thread object and
starting/running it. In this case, as soon as the thread is created,
it is executed.

The output:

~~~lisp
    CL-USER> (test-update-global-variable)

    0
    CL-USER> *counter*
    1
~~~

As we can see, because the main thread returned immediately, the
initial value of `*counter*` is 0, and then around a second later, it
gets updated to 1 by the anonymous thread.

### Create a thread: print a message onto the top-level

~~~lisp
    ;;; Print a message onto the top-level using a thread
    (defun print-message-top-level-wrong ()
      (bt:make-thread
       (lambda ()
         (format *standard-output* "Hello from thread!"))
       :name "hello")
      nil)
~~~

And the output:

~~~lisp
    CL-USER> (print-message-top-level-wrong)
    NIL
~~~

So what went wrong? The problem is variable binding. Now, the ’t’
parameter to the format function refers to the top-level, which is a
Common Lisp term for the main console stream, also referred to by the
global variable `*standard-output*`. So we could have expected the
output to be shown on the main console screen.

The same code would have run fine if we had not run it in a separate
    thread. What happens is that each thread has its own stack where
    the variables are rebound. In this case, even for
    `*standard-output*`, which being a global variable, we would assume
    should be available to all threads, is rebound inside each thread!
    This is similar to the concept of ThreadLocal storage in Java.
    Print a message onto the top-level — fixed:

So how do we fix the problem of the previous example? By binding the top-level at the time of thread creation of course. Pure lexical scoping to the rescue!

~~~lisp
    ;;; Print a message onto the top-level using a thread — fixed
    (defun print-message-top-level-fixed ()
      (let ((top-level *standard-output*))
        (bt:make-thread
         (lambda ()
           (format top-level "Hello from thread!"))
         :name "hello")))
      nil)
~~~

Which produces:

~~~lisp
    CL-USER> (print-message-top-level-fixed)
    Hello from thread!
    NIL
~~~

Phew! However, there is another way of producing the same result using
a very interesting reader macro as we’ll see next.

### Print a message onto the top-level — read-time eval macro

Let’s take a look at the code first:

~~~lisp
    ;;; Print a message onto the top-level using a thread - reader macro

    (eval-when (:compile-toplevel)
      (defun print-message-top-level-reader-macro ()
        (bt:make-thread
         (lambda ()
           (format #.*standard-output* "Hello from thread!")))
        nil))

    (print-message-top-level-reader-macro)
~~~

And the output:

~~~lisp
    CL-USER> (print-message-top-level-reader-macro)
    Hello from thread!
    NIL
~~~

So it works, but what’s the deal with the eval-when and what is that
strange #. symbol before `*standard-output*`?

eval-when controls when evaluation of Lisp expressions takes place. We
can have three targets — :compile-toplevel, :load-toplevel, and
:execute.

The `#.` symbol is what is called a “Reader macro”. A reader (or read)
macro is called so because it has special meaning to the Common Lisp
Reader, which is the component that is responsible for reading in
Common Lisp expressions and making sense out of them. This specific
reader macro ensures that the binding of `*standard-output*` is done
at read time.

Binding the value at read-time ensures that the original value of
`*standard-output*` is maintained when the thread is run, and the output
is shown on the correct top-level.

Now this is where the eval-when bit comes into play. By wrapping the
whole function definition inside the eval-when, and ensuring that
evaluation takes place during compile time, the correct value of
`*standard-output*` is bound. If we had skipped the eval-when, we would
see the following error:

~~~lisp
      error:
        don't know how to dump #<SWANK/GRAY::SLIME-OUTPUT-STREAM {100439EEA3}> (default MAKE-LOAD-FORM method called).
        ==>
          #<SWANK/GRAY::SLIME-OUTPUT-STREAM {100439EEA3}>

      note: The first argument never returns a value.
      note:
        deleting unreachable code
        ==>
          "Hello from thread!"


    Compilation failed.
~~~

And that makes sense because SBCL cannot make sense of what this
output stream returns since it is a stream and not really a defined
value (which is what the ‘format’ function expects). That is why we
see the “unreachable code” error.

Note that if the same code had been run on the REPL directly, there
would be no problem since the resolution of all the symbols would be
done correctly by the REPL thread.


### Modify a shared resource from multiple threads

Suppose we have the following setup with a minimal bank-account class (no error checks):

~~~lisp
    ;;; Modify a shared resource from multiple threads

    (defclass bank-account ()
      ((id :initarg :id
           :initform (error "id required")
           :accessor :id)
       (name :initarg :name
             :initform (error "name required")
             :accessor :name)
       (balance :initarg :balance
                :initform 0
                :accessor :balance)))

    (defgeneric deposit (account amount)
      (:documentation "Deposit money into the account"))

    (defgeneric withdraw (account amount)
      (:documentation "Withdraw amount from account"))

    (defmethod deposit ((account bank-account) (amount real))
      (incf (:balance account) amount))

    (defmethod withdraw ((account bank-account) (amount real))
      (decf (:balance account) amount))
~~~

And we have a simple client which apparently does not believe in any form of synchronisation:

~~~lisp
    (defparameter *rich*
      (make-instance 'bank-account
                     :id 1
                     :name "Rich"
                     :balance 0))
    ; compiling (DEFPARAMETER *RICH* ...)

    (defun demo-race-condition ()
      (loop repeat 100
         do
           (bt:make-thread
            (lambda ()
              (loop repeat 10000 do (deposit *rich* 100))
              (loop repeat 10000 do (withdraw *rich* 100))))))
~~~

This is all we are doing – create a new bank account instance (balance
0), and then create a 100 threads, each of which simply deposits an
amount of 100 10000 times, and then withdraws the same amount the same
number of times. So the final result should be the same as that of the
opening balance, which is 0, right? Let’s check that and see.

On a sample run, we might get the following results:

~~~lisp
    CL-USER> (:balance *rich*)
    0
    CL-USER> (dotimes (i 5)
               (demo-race-condition))
    NIL
    CL-USER> (:balance *rich*)
    22844600
~~~

Whoa! The reason for this discrepancy is that incf and decf are not
atomic operations — they consist of multiple sub-operations, and the
order in which they are executed is not in our control.

This is what is called a “race condition” — multiple threads
contending for the same shared resource with at least one modifying
thread which, more likely than not, reads the wrong value of the
object while modifying it. How do we fix it? One simple way it to use
locks (mutex in this case, could be semaphores for more complex
situations).

### Modify a shared resource from multiple threads — fixed using locks

Let’s rest the balance for the account back to 0 first:

~~~lisp
    CL-USER> (setf (:balance *rich*) 0)
    0
    CL-USER> (:balance *rich*)
    0
~~~

Now let’s modify the demo-race-condition function to access the shared resource using locks (created using bt:make-lock and used as shown):

~~~lisp
    (defvar *lock* (bt:make-lock))
    ; compiling (DEFVAR *LOCK* …)

    (defun demo-race-condition-locks ()
      (loop repeat 100
         do
           (bt:make-thread
            (lambda ()
              (loop repeat 10000 do (bt:with-lock-held (*lock*)
                                      (deposit *rich* 100)))
              (loop repeat 10000 do (bt:with-lock-held (*lock*)
                                      (withdraw *rich* 100)))))))
    ; compiling (DEFUN DEMO-RACE-CONDITION-LOCKS ...)
~~~

And let’s do a bigger sample run this time around:

~~~lisp
    CL-USER> (dotimes (i 100)
               (demo-race-condition-locks))
    NIL
    CL-USER> (:balance *rich*)
    0
~~~

Excellent! Now this is better. Of course, one has to remember that
using a mutex like this is bound to affect performance. There is a
better way in quite a few circumstances — using atomic operations when
possible. We’ll cover that next.

### Modify a shared resource from multiple threads — using atomic operations

Atomic operations are operations that are guaranteed by the system to
all occur inside a conceptual transaction, i.e., all the
sub-operations of the main operation all take place together without
any interference from outside. The operation succeeds completely or
fails completely. There is no middle ground, and there is no
inconsistent state.

Another advantage is that performance is far superior to using locks
to protect access to the shared state. We will see this difference in
the actual demo run.

The Bordeaux library does not provide any real support for atomics, so
we will have to depend on the specific implementation support for
that. In our case, that is SBCL, and so we will have to defer this
demo to the SBCL section.

### Joining on a thread, destroying a thread

To join on a thread, we use the `bt:join-thread` function, and for
destroying a thread (not a recommended operation), we can use the
`bt:destroy-thread` function.

A simple demo:

~~~lisp
    (defmacro until (condition &body body)
      (let ((block-name (gensym)))
        `(block ,block-name
           (loop
               (if ,condition
                   (return-from ,block-name nil)
                   (progn
                       ,@body))))))

    (defun join-destroy-thread ()
      (let* ((s *standard-output*)
            (joiner-thread (bt:make-thread
                            (lambda ()
                              (loop for i from 1 to 10
                                 do
                                   (format s "~%[Joiner Thread]  Working...")
                                   (sleep (* 0.01 (random 100)))))))
            (destroyer-thread (bt:make-thread
                               (lambda ()
                                 (loop for i from 1 to 1000000
                                    do
                                      (format s "~%[Destroyer Thread] Working...")
                                      (sleep (* 0.01 (random 10000))))))))
        (format t "~%[Main Thread] Waiting on joiner thread...")
        (bt:join-thread joiner-thread)
        (format t "~%[Main Thread] Done waiting on joiner thread")
        (if (bt:thread-alive-p destroyer-thread)
            (progn
              (format t "~%[Main Thread] Destroyer thread alive... killing it")
              (bt:destroy-thread destroyer-thread))
            (format t "~%[Main Thread] Destroyer thread is already dead"))
        (until (bt:thread-alive-p destroyer-thread)
               (format t "[Main Thread] Waiting for destroyer thread to die..."))
        (format t "~%[Main Thread] Destroyer thread dead")
        (format t "~%[Main Thread] Adios!~%")))
~~~

And the output on a run:

~~~lisp
    CL-USER> (join-destroy-thread)

    [Joiner Thread]  Working...
    [Destroyer Thread] Working...
    [Main Thread] Waiting on joiner thread...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Main Thread] Done waiting on joiner thread
    [Main Thread] Destroyer thread alive... killing it
    [Main Thread] Destroyer thread dead
    [Main Thread] Adios!
    NIL
~~~

The until macro simply loops around until the condition becomes
true. The rest of the code is pretty much self-explanatory — the main
thread waits for the joiner-thread to finish, but it immediately
destroys the destroyer-thread.

Again, it is not recommended to use `bt:destroy-thread`. Any conceivable
situation which requires this function can probably be done better
with another approach.

Now let’s move onto some more comprehensive examples which tie
together all the concepts discussed thus far.

### Useful functions

Here is a summary of the functions, macros and global variables which
were used in the demo examples along with some extras. These should
cover most of the basic programming scenarios:

-    `bt:*supports-thread-p*` (to check for basic thread support)
-    `bt:make-thread` (create a new thread)
-    `bt:current-thread` (return the current thread object)
-    `bt:all-threads` (return a list of all running threads)
-    `bt:thread-alive-p` (checks if the thread is still alive)
-    `bt:thread-name` (return the name of the thread)
-    `bt:join-thread` (join on the supplied thread)
-    `bt:interrupt-thread` (interrupt the given thread)
-    `bt:destroy-thread` (attempt to abort the thread)
-    `bt:make-lock` (create a mutex)
-    `bt:with-lock-held` (use the supplied lock to protect critical code)


## SBCL threads

SBCL provides support for native threads via its [sb-thread](http://www.sbcl.org/manual/index.html#Threading)
package. These are very low-level functions, but we can build our own
abstractions on top of these as shown in the demo examples.

You can refer to the documentation for more details (check the
“Wrap-up” section).

You can see from the examples below that there is a strong
correspondence between Bordeaux and SBCL Thread functions. In most
cases, the only difference is the change of package name from bt to
sb-thread.

It is evident that the Bordeaux thread library was more or less based
on the SBCL implementation. As such, explanation will be provided only
in those cases where there is a major difference in syntax or
semantics.

### Basics — list current thread, list all threads, get thread name

The code:


~~~lisp
    ;;; Print the current thread, all the threads, and the current thread's name

    (defun print-thread-info ()
      (let* ((curr-thread sb-thread:*current-thread*)
             (curr-thread-name (sb-thread:thread-name curr-thread))
             (all-threads (sb-thread:list-all-threads)))
        (format t "Current thread: ~a~%~%" curr-thread)
        (format t "Current thread name: ~a~%~%" curr-thread-name)
        (format t "All threads:~% ~{~a~%~}~%" all-threads))
      nil)
~~~

And the output:

~~~lisp
    CL-USER> (print-thread-info)
    Current thread: #<THREAD "repl-thread" RUNNING {10043B8003}>

    Current thread name: repl-thread

    All threads:
     #<THREAD "repl-thread" RUNNING {10043B8003}>
    #<THREAD "auto-flush-thread" RUNNING {10043B7DA3}>
    #<THREAD "swank-indentation-cache-thread" waiting on: #<WAITQUEUE  {1003A28103}> {1003A201A3}>
    #<THREAD "reader-thread" RUNNING {1003A20063}>
    #<THREAD "control-thread" waiting on: #<WAITQUEUE  {1003A19E53}> {1003A18C83}>
    #<THREAD "Swank Sentinel" waiting on: #<WAITQUEUE  {1003790043}> {1003788023}>
    #<THREAD "main thread" RUNNING {1002991CE3}>

    NIL
~~~

### Update a global variable from a thread

The code:

~~~lisp
    ;;; Update a global variable from a thread

    (defparameter *counter* 0)

    (defun test-update-global-variable ()
      (sb-thread:make-thread
       (lambda ()
         (sleep 1)
         (incf *counter*)))
      *counter*)
~~~

And the output:

~~~lisp
    CL-USER> (test-update-global-variable)
    0
~~~

### Print a message onto the top-level using a thread

The code:

~~~lisp
    ;;; Print a message onto the top-level using a thread

    (defun print-message-top-level-wrong ()
      (sb-thread:make-thread
       (lambda ()
         (format *standard-output* "Hello from thread!")))
      nil)
~~~

And the output:

~~~lisp
    CL-USER> (print-message-top-level-wrong)
    NIL
~~~

Print a message onto the top-level — fixed:

The code:

~~~lisp
    ;;; Print a message onto the top-level using a thread - fixed

    (defun print-message-top-level-fixed ()
      (let ((top-level *standard-output*))
        (sb-thread:make-thread
         (lambda ()
           (format top-level "Hello from thread!"))))
      nil)
~~~

And the output:

~~~lisp
    CL-USER> (print-message-top-level-fixed)
    Hello from thread!
    NIL
~~~

### Print a message onto the top-level — better

The code:

~~~lisp
    ;;; Print a message onto the top-level using a thread - reader macro

    (eval-when (:compile-toplevel)
      (defun print-message-top-level-reader-macro ()
        (sb-thread:make-thread
         (lambda ()
           (format #.*standard-output* "Hello from thread!")))
        nil))
~~~

And the output:

~~~lisp
    CL-USER> (print-message-top-level-reader-macro)
    Hello from thread!
    NIL
~~~

###    Modify a shared resource from multiple threads

The code:

~~~lisp
    ;;; Modify a shared resource from multiple threads

    (defclass bank-account ()
      ((id :initarg :id
           :initform (error "id required")
           :accessor :id)
       (name :initarg :name
             :initform (error "name required")
             :accessor :name)
       (balance :initarg :balance
                :initform 0
                :accessor :balance)))

    (defgeneric deposit (account amount)
      (:documentation "Deposit money into the account"))

    (defgeneric withdraw (account amount)
      (:documentation "Withdraw amount from account"))

    (defmethod deposit ((account bank-account) (amount real))
      (incf (:balance account) amount))

    (defmethod withdraw ((account bank-account) (amount real))
      (decf (:balance account) amount))

    (defparameter *rich*
      (make-instance 'bank-account
                     :id 1
                     :name "Rich"
                     :balance 0))

    (defun demo-race-condition ()
      (loop repeat 100
         do
           (sb-thread:make-thread
            (lambda ()
              (loop repeat 10000 do (deposit *rich* 100))
              (loop repeat 10000 do (withdraw *rich* 100))))))
~~~

And the output:

~~~lisp
    CL-USER> (:balance *rich*)
    0
    CL-USER> (demo-race-condition)
    NIL
    CL-USER> (:balance *rich*)
    3987400
~~~

###    Modify a shared resource from multiple threads — fixed using locks

The code:

~~~lisp
    (defvar *lock* (sb-thread:make-mutex))

    (defun demo-race-condition-locks ()
      (loop repeat 100
         do
           (sb-thread:make-thread
            (lambda ()
              (loop repeat 10000 do (sb-thread:with-mutex (*lock*)
                                      (deposit *rich* 100)))
              (loop repeat 10000 do (sb-thread:with-mutex (*lock*)
                                      (withdraw *rich* 100)))))))
~~~

The only difference here is that instead of make-lock as in Bordeaux,
we have make-mutex and that is used along with the macro with-mutex as
shown in the example.

And the output:

~~~lisp
    CL-USER> (:balance *rich*)
    0
    CL-USER> (demo-race-condition-locks)
    NIL
    CL-USER> (:balance *rich*)
    0
~~~

### Modify a shared resource from multiple threads — using atomic operations

First, the code:

~~~lisp
    ;;; Modify a shared resource from multiple threads - atomics

    (defgeneric atomic-deposit (account amount)
      (:documentation "Atomic version of the deposit method"))

    (defgeneric atomic-withdraw (account amount)
      (:documentation "Atomic version of the withdraw method"))

    (defmethod atomic-deposit ((account bank-account) (amount real))
      (sb-ext:atomic-incf (car (cons (:balance account) nil)) amount))

    (defmethod atomic-withdraw ((account bank-account) (amount real))
      (sb-ext:atomic-decf (car (cons (:balance account) nil)) amount))

    (defun demo-race-condition-atomics ()
      (loop repeat 100
         do (sb-thread:make-thread
             (lambda ()
               (loop repeat 10000 do (atomic-deposit *rich* 100))
               (loop repeat 10000 do (atomic-withdraw *rich* 100))))))
~~~

And the output:

~~~lisp
    CL-USER> (dotimes (i 5)
               (format t "~%Opening: ~d" (:balance *rich*))
               (demo-race-condition-atomics)
               (format t "~%Closing: ~d~%" (:balance *rich*)))

    Opening: 0
    Closing: 0

    Opening: 0
    Closing: 0

    Opening: 0
    Closing: 0

    Opening: 0
    Closing: 0

    Opening: 0
    Closing: 0
    NIL
~~~

As you can see, SBCL’s atomic functions are a bit quirky. The two
functions used here: `sb-ext:incf` and `sb-ext:atomic-decf` have the
following signatures:


    Macro: atomic-incf [sb-ext] place &optional diff

and


    Macro: atomic-decf [sb-ext] place &optional diff

The interesting bit is that the “place” parameter must be any of the
following (as per the documentation):

- a defstruct slot with declared type (unsigned-byte 64) or aref of a (simple-array (unsigned-byte 64) (*)) The type `sb-ext:word` can be used for these purposes.
- car or cdr (respectively first or REST) of a cons.
- a variable defined using defglobal with a proclaimed type of fixnum.

This is the reason for the bizarre construct used in the
`atomic-deposit` and `atomic-decf` methods.

One major incentive to use atomic operations as much as possible is
performance. Let’s do a quick run of the demo-race-condition-locks and
demo-race-condition-atomics functions over 1000 times and check the
difference in performance (if any):

With locks:

~~~lisp
    CL-USER> (time
                        (loop repeat 100
                          do (demo-race-condition-locks)))
    Evaluation took:
      57.711 seconds of real time
      431.451639 seconds of total run time (408.014746 user, 23.436893 system)
      747.61% CPU
      126,674,011,941 processor cycles
      3,329,504 bytes consed

    NIL
~~~

With atomics:

~~~lisp
    CL-USER> (time
                        (loop repeat 100
                         do (demo-race-condition-atomics)))
    Evaluation took:
      2.495 seconds of real time
      8.175454 seconds of total run time (6.124259 user, 2.051195 system)
      [ Run times consist of 0.420 seconds GC time, and 7.756 seconds non-GC time. ]
      327.66% CPU
      5,477,039,706 processor cycles
      3,201,582,368 bytes consed

    NIL
~~~

The results? The locks version took around 57s whereas the lockless
atomics version took just 2s! This is a massive difference indeed!

### Joining on a thread, destroying a thread example

The code:

~~~lisp
    ;;; Joining on and destroying a thread

    (defmacro until (condition &body body)
      (let ((block-name (gensym)))
        `(block ,block-name
           (loop
               (if ,condition
                   (return-from ,block-name nil)
                   (progn
                       ,@body))))))

    (defun join-destroy-thread ()
      (let* ((s *standard-output*)
            (joiner-thread (sb-thread:make-thread
                            (lambda ()
                              (loop for i from 1 to 10
                                 do
                                   (format s "~%[Joiner Thread]  Working...")
                                   (sleep (* 0.01 (random 100)))))))
            (destroyer-thread (sb-thread:make-thread
                               (lambda ()
                                 (loop for i from 1 to 1000000
                                    do
                                      (format s "~%[Destroyer Thread] Working...")
                                      (sleep (* 0.01 (random 10000))))))))
        (format t "~%[Main Thread] Waiting on joiner thread...")
        (bt:join-thread joiner-thread)
        (format t "~%[Main Thread] Done waiting on joiner thread")
        (if (sb-thread:thread-alive-p destroyer-thread)
            (progn
              (format t "~%[Main Thread] Destroyer thread alive... killing it")
              (sb-thread:terminate-thread destroyer-thread))
            (format t "~%[Main Thread] Destroyer thread is already dead"))
        (until (sb-thread:thread-alive-p destroyer-thread)
               (format t "[Main Thread] Waiting for destroyer thread to die..."))
        (format t "~%[Main Thread] Destroyer thread dead")
        (format t "~%[Main Thread] Adios!~%")))
~~~

And the output:

~~~lisp
    CL-USER> (join-destroy-thread)

    [Joiner Thread]  Working...
    [Destroyer Thread] Working...
    [Main Thread] Waiting on joiner thread...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Joiner Thread]  Working...
    [Main Thread] Done waiting on joiner thread
    [Main Thread] Destroyer thread alive... killing it
    [Main Thread] Destroyer thread dead
    [Main Thread] Adios!
    NIL
~~~

### Useful functions

Here is a summarised list of the functions, macros and global
variables used in the examples along with some extras:

-    `(member :thread-support *features*)` (check thread support)
-    `sb-thread:make-thread` (create a new thread)
-    `sb-thread:*current-thread*` (holds the current thread object)
-    `sb-thread:list-all-threads` (return a list of all running threads)
-    `sb-thread:thread-alive-p` (checks if the thread is still alive)
-    `sb-thread:thread-name` (return the name of the thread)
-    `sb-thread:join-thread` (join on the supplied thread)
-    `sb-thread:interrupt-thread` (interrupt the given thread)
-    `sb-thread:destroy-thread` (attempt to abort the thread)
-    `sb-thread:make-mutex` (create a mutex)
-    `sb-thread:with-mutex` (use supplied lock to protect critical code)

## Wrap-up

As you can see, concurrency support is rather primitive in Common
Lisp, but that’s primarily due to the glaring absence of this
important feature in the ANSI Common Lisp specification. That does not
detract in the least from the support provided by Common Lisp
implementations, nor wonderful libraries like the Bordeaux library.

You should follow up on your own by reading a lot more on this
topic. I share some of my own references here:

-    [Common Lisp Recipes](http://weitz.de/cl-recipes/)
-    [Bordeaux API Reference](https://trac.common-lisp.net/bordeaux-threads/wiki/ApiDocumentation)
-    [SBCL Manual](http://www.sbcl.org/manual/#Threading%E2%80%9D)
-    [The Common Lisp Hyperspec](https://www.lispworks.com/documentation/HyperSpec/Front/)

Next up, the final post in this mini-series: parallelism in Common
Lisp using the **lparallel** library.

## Parallel programming with lparallel

It is important to note that lparallel also provides extensive support
for asynchronous programming, and is not a purely parallel programming
library. As stated before, parallelism is merely an abstract concept
in which tasks are conceptually independent of one another.

The lparallel library is built on top of the Bordeaux threading
library.

As mentioned previously, parallelism and concurrency can be
(and usually are) implemented using the same means — threads,
processes, etc. The difference between lies in their conceptual
differences.

Note that not all the examples shown in this post are necessarily
parallel. Asynchronous constructs such as Promises and Futures are, in
particular, more suited to concurrent programming than parallel
programming.

The modus operandi of using the lparallel library (for a basic use case) is as follows:

- Create an instance of what the library calls a kernel using
  `lparallel:make-kernel`. The kernel is the component that schedules
  and executes tasks.
-    Design the code in terms of futures, promises and other higher
     level functional concepts. To this end, lparallel provides
     support for **channels**, **promises**, **futures**, and **cognates**.
-    Perform operations using what the library calls cognates, which
     are simply functions which have equivalents in the Common Lisp
     language itself. For instance, the `lparallel:pmap` function is
     the parallel equivalent of the Common Lisp `map` function.
-    Finally, close the kernel created in the first step using
     `lparallel:end-kernel`.

Note that the onus of ensuring that the tasks being carried out are
logically parallelisable as well as taking care of all mutable state
is on the developer.

_Credit: this article first appeared on
[z0ltan.wordpress.com](https://z0ltan.wordpress.com/2016/09/09/basic-concurrency-and-parallelism-in-common-lisp-part-4a-parallelism-using-lparallel-fundamentals/)._

### Installation

Let’s check if lparallel is available for download using Quicklisp:

~~~lisp
CL-USER> (ql:system-apropos "lparallel")
#<SYSTEM lparallel / lparallel-20160825-git / quicklisp 2016-08-25>
#<SYSTEM lparallel-bench / lparallel-20160825-git / quicklisp 2016-08-25>
#<SYSTEM lparallel-test / lparallel-20160825-git / quicklisp 2016-08-25>
; No value
~~~

Looks like it is. Let’s go ahead and install it:

~~~lisp
CL-USER> (ql:quickload :lparallel)
To load "lparallel":
  Load 2 ASDF systems:
    alexandria bordeaux-threads
  Install 1 Quicklisp release:
    lparallel
; Fetching #<URL "http://beta.quicklisp.org/archive/lparallel/2016-08-25/lparallel-20160825-git.tgz">
; 76.71KB
==================================================
78,551 bytes in 0.62 seconds (124.33KB/sec)
; Loading "lparallel"
[package lparallel.util]..........................
[package lparallel.thread-util]...................
[package lparallel.raw-queue].....................
[package lparallel.cons-queue]....................
[package lparallel.vector-queue]..................
[package lparallel.queue].........................
[package lparallel.counter].......................
[package lparallel.spin-queue]....................
[package lparallel.kernel]........................
[package lparallel.kernel-util]...................
[package lparallel.promise].......................
[package lparallel.ptree].........................
[package lparallel.slet]..........................
[package lparallel.defpun]........................
[package lparallel.cognate].......................
[package lparallel]
(:LPARALLEL)
~~~

And that’s all it took! Now let’s see how this library actually works.

### Preamble - get the number of cores with a call to CFFI

First, let’s get hold of the number of threads that we are going to
use for our parallel examples. Ideally, we’d like to have a 1:1 match
between the number of worker threads and the number of available
cores.

We can use the wonderful **cffi** library to this end. I plan to have a
detailed blog post for this extremely useful library soon, but for
now, let’s get on with it:

Install CFFI:

~~~lisp
CL-USER> (ql:quickload :cffi)
To load "cffi":
  Load 4 ASDF systems:
    alexandria babel trivial-features uiop
  Install 1 Quicklisp release:
    cffi
; Fetching #<URL "http://beta.quicklisp.org/archive/cffi/2016-03-18/cffi_0.17.1.tgz">
; 234.48KB
==================================================
240,107 bytes in 5.98 seconds (39.22KB/sec)
; Loading "cffi"
[package cffi-sys]................................
[package cffi]....................................
..................................................
[package cffi-features]
(:CFFI)
~~~

Write C code to get the number of logical cores on the machine:

```
#include <stdio.h>
#include <sys/types.h>
#include <sys/sysctl.h>

int get_core_count();

int main()
{
    printf("%d\n", get_core_count());

    return 0;
}

int32_t get_core_count()
{
    const char* s = "hw.logicalcpu";
    int32_t core_count;
    size_t len = sizeof(core_count);

    sysctlbyname(s, &core_count, &len, NULL, 0);

    return core_count;
}
```

Bundle the C code into a shared library (note, I am using Mac OS X
which comes bundled with Clang. For pure gcc, refer to the relevant
documentation): 1

```
Timmys-MacBook-Pro:Parallelism z0ltan$ clang -dynamiclib get_core_count.c -o libcorecount.dylib
```

Invoke the function from Common Lisp:

~~~lisp
CL-USER> (cffi:use-foreign-library "libcorecount.dylib")
#<CFFI:FOREIGN-LIBRARY LIBCORECOUNT.DYLIB-853 "libcorecount.dylib">
CL-USER> (cffi:foreign-funcall "get_core_count" :int)
8
~~~

We can see that the result is 8 cores on the machine (which is
correct) and can be verified from the command line as well:

```
Timmys-MacBook-Pro:Parallelism z0ltan$ sysctl -n "hw.logicalcpu"
```

### Common Setup

In this example, we will go through the initial setup bit, and also
show some useful information once the setup is done.

Load the library:

~~~lisp
CL-USER> (ql:quickload :lparallel)
To load "lparallel":
  Load 1 ASDF system:
    lparallel
; Loading "lparallel"

(:LPARALLEL)
~~~

Initialise the lparallel kernel:

~~~lisp
CL-USER> (setf lparallel:*kernel* (lparallel:make-kernel 8 :name "custom-kernel"))
#<LPARALLEL.KERNEL:KERNEL :NAME "custom-kernel" :WORKER-COUNT 8 :USE-CALLER NIL :ALIVE T :SPIN-COUNT 2000 {1003141F03}>
~~~

Note that the `*kernel*` global variable can be rebound — this allows
multiple kernels to co-exist during the same run. Now, some useful
information about the kernel:

~~~lisp
CL-USER> (defun show-kernel-info ()
           (let ((name (lparallel:kernel-name))
                 (count (lparallel:kernel-worker-count))
                 (context (lparallel:kernel-context))
                 (bindings (lparallel:kernel-bindings)))
             (format t "Kernel name = ~a~%" name)
             (format t "Worker threads count = ~d~%" count)
             (format t "Kernel context = ~a~%" context)
             (format t "Kernel bindings = ~a~%" bindings)))


WARNING: redefining COMMON-LISP-USER::SHOW-KERNEL-INFO in DEFUN
SHOW-KERNEL-INFO

CL-USER> (show-kernel-info)
Kernel name = custom-kernel
Worker threads count = 8
Kernel context = #<FUNCTION FUNCALL>
Kernel bindings = ((*STANDARD-OUTPUT* . #<SLIME-OUTPUT-STREAM {10044EEEA3}>)
                   (*ERROR-OUTPUT* . #<SLIME-OUTPUT-STREAM {10044EEEA3}>))
NIL
~~~

End the kernel (this is important since `*kernel*` does not get
garbage collected until we explicitly end it):

~~~lisp
CL-USER> (lparallel:end-kernel :wait t)
(#<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {100723FA83}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {100723FE23}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {10072581E3}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {1007258583}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {1007258923}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {1007258CC3}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {1007259063}>
 #<SB-THREAD:THREAD "custom--kernel" FINISHED values: NIL {1007259403}>)
~~~

Let’s move on to some more examples of different aspects of the lparallel library.

For these demos, we will be using the following initial setup from a coding perspective:

~~~lisp
(require ‘lparallel)
(require ‘bt-semaphore)

(defpackage :lparallel-user
  (:use :cl :lparallel :lparallel.queue :bt-semaphore))

(in-package :lparallel-user)

;;; initialise the kernel
(defun init ()
  (setf *kernel* (make-kernel 8 :name "channel-queue-kernel")))

(init)
~~~

So we will be using a kernel with 8 worker threads (one for each CPU core on the machine).

And once we’re done will all the examples, the following code will be
run to close the kernel and free all used system resources:

~~~lisp
;;; shut the kernel down
(defun shutdown ()
  (end-kernel :wait t))

(shutdown)
~~~

### Using channels and queues

First some definitions are in order.

A **task** is a job that is submitted to the kernel. It is simply a
function object along with its arguments.

A **channel** in lparallel is similar to the same concept in Go. A channel
is simply a means of communication with a worker thread. In our case,
it is one particular way of submitting tasks to the kernel.

A channel is created in lparallel using `lparallel:make-channel`. A
task is submitted using `lparallel:submit-task`, and the results
received via `lparallel:receive-result`.

For instance, we can calculate the square of a number as:

~~~lisp
(defun calculate-square (n)
  (let* ((channel (lparallel:make-channel))
         (res nil))
    (lparallel:submit-task channel #'(lambda (x)
                                       (* x x))
                           n)
    (setf res (lparallel:receive-result channel))
    (format t "Square of ~d = ~d~%" n res)))
~~~

And the output:

~~~lisp
LPARALLEL-USER> (calculate-square 100)
Square of 100 = 10000
NIL
~~~

Now let’s try submitting multiple tasks to the same channel. In this
simple example, we are simply creating three tasks that square, triple,
and quadruple the supplied input respectively.

Note that in case of multiple tasks, the output will be in non-deterministic order:

~~~lisp
(defun test-basic-channel-multiple-tasks ()
  (let ((channel (make-channel))
        (res '()))
    (submit-task channel #'(lambda (x)
                             (* x x))
                 10)
    (submit-task channel #'(lambda (y)
                             (* y y y))
                 10)
    (submit-task channel #'(lambda (z)
                             (* z z z z))
                 10)
     (dotimes (i 3 res)
       (push (receive-result channel) res))))
~~~

And the output:

~~~lisp
LPARALLEL-USER> (dotimes (i 3)
                              (print (test-basic-channel-multiple-tasks)))

(100 1000 10000)
(100 1000 10000)
(10000 1000 100)
NIL
~~~

lparallel also provides support for creating a blocking queue in order
to enable message passing between worker threads. A queue is created
using `lparallel.queue:make-queue`.

Some useful functions for using queues are:

-    `lparallel.queue:make-queue`: create a FIFO blocking queue
-    `lparallel.queue:push-queue`: insert an element into the queue
-    `lparallel.queue:pop-queue`: pop an item from the queue
-    `lparallel.queue:peek-queue`: inspect value without popping it
-    `lparallel.queue:queue-count`: the number of entries in the queue
-    `lparallel.queue:queue-full-p`: check if the queue is full
-    `lparallel.queue:queue-empty-p:chec`k if the queue is empty
-    `lparallel.queue:with-locked-queue`: lock the queue during access

A basic demo showing basic queue properties:

~~~lisp
    (defun test-queue-properties ()
      (let ((queue (make-queue :fixed-capacity 5)))
        (loop
           when (queue-full-p queue)
           do (return)
           do (push-queue (random 100) queue))
         (print (queue-full-p queue))
        (loop
           when (queue-empty-p queue)
           do (return)
           do (print (pop-queue queue)))
        (print (queue-empty-p queue)))
      nil)
~~~

Which produces:

~~~lisp
    LPARALLEL-USER> (test-queue-properties)

    T
    17
    51
    55
    42
    82
    T
    NIL
~~~

Note: `lparallel.queue:make-queue` is a generic interface which is
actually backed by different types of queues. For instance, in the
previous example, the actual type of the queue is
`lparallel.vector-queue` since we specified it to be of fixed size using
the `:fixed-capacity` keyword argument.

The documentation doesn’t actually specify what keyword arguments we
can pass to `lparallel.queue:make-queue`, so let’s and find that out in
a different way:

~~~lisp
    LPARALLEL-USER> (describe 'lparallel.queue:make-queue)
    LPARALLEL.QUEUE:MAKE-QUEUE
      [symbol]

    MAKE-QUEUE names a compiled function:
      Lambda-list: (&REST ARGS)
      Derived type: FUNCTION
      Documentation:
        Create a queue.

        The queue contents may be initialized with the keyword argument
        `initial-contents'.

        By default there is no limit on the queue capacity. Passing a
        `fixed-capacity' keyword argument limits the capacity to the value
        passed. `push-queue' will block for a full fixed-capacity queue.
      Source file: /Users/z0ltan/quicklisp/dists/quicklisp/software/lparallel-20160825-git/src/queue.lisp

    MAKE-QUEUE has a compiler-macro:
      Source file: /Users/z0ltan/quicklisp/dists/quicklisp/software/lparallel-20160825-git/src/queue.lisp
    ; No value
~~~

So, as we can see, it supports the following keyword arguments:
*:fixed-capacity*, and *initial-contents*.

Now, if we do specify `:fixed-capacity`, then the actual type of the
queue will be `lparallel.vector-queue`, and if we skip that keyword
argument, the queue will be of type `lparallel.cons-queue` (which is a
queue of unlimited size), as can be seen from the output of the
following snippet:

~~~lisp
    (defun check-queue-types ()
      (let ((queue-one (make-queue :fixed-capacity 5))
            (queue-two (make-queue)))
        (format t "queue-one is of type: ~a~%" (type-of queue-one))
        (format t "queue-two is of type: ~a~%" (type-of queue-two))))


    LPARALLEL-USER> (check-queue-types)
    queue-one is of type: VECTOR-QUEUE
    queue-two is of type: CONS-QUEUE
    NIL
~~~

Of course, you can always create instances of the specific queue types
yourself, but it is always better, when you can, to stick to the
generic interface and letting the library create the proper type of
queue for you.

Now, let’s just see the queue in action!

~~~lisp
    (defun test-basic-queue ()
      (let ((queue (make-queue))
            (channel (make-channel))
            (res '()))
        (submit-task channel #'(lambda ()
                         (loop for entry = (pop-queue queue)
                            when (queue-empty-p queue)
                            do (return)
                            do (push (* entry entry) res))))
        (dotimes (i 100)
          (push-queue i queue))
        (receive-result channel)
        (format t "~{~d ~}~%" res)))
~~~

Here we submit a single task that repeatedly scans the queue till it’s
empty, pops the available values, and pushes them into the res list.

And the output:

~~~lisp
    LPARALLEL-USER> (test-basic-queue)
    9604 9409 9216 9025 8836 8649 8464 8281 8100 7921 7744 7569 7396 7225 7056 6889 6724 6561 6400 6241 6084 5929 5776 5625 5476 5329 5184 5041 4900 4761 4624 4489 4356 4225 4096 3969 3844 3721 3600 3481 3364 3249 3136 3025 2916 2809 2704 2601 2500 2401 2304 2209 2116 2025 1936 1849 1764 1681 1600 1521 1444 1369 1296 1225 1156 1089 1024 961 900 841 784 729 676 625 576 529 484 441 400 361 324 289 256 225 196 169 144 121 100 81 64 49 36 25 16 9 4 1 0
    NIL
~~~

###    Killing tasks

A small note mentioning the `lparallel:kill-task` function would be
apropos at this juncture. This function is useful in those cases when
tasks are unresponsive. The lparallel documentation clearly states
that this must only be used as a last resort.

All tasks which are created are by default assigned a category of
:default. The dynamic property, `*task-category*` holds this value, and
can be dynamically bound to different values (as we shall see).

~~~lisp
;;; kill default tasks
(defun test-kill-all-tasks ()
  (let ((channel (make-channel))
        (stream *query-io*))
    (dotimes (i 10)
      (submit-task channel #'(lambda (x)
                               (sleep (random 10))
                               (format stream "~d~%" (* x x))) (random 10)))
    (sleep (random 2))
    (kill-tasks :default)))
~~~

Sample run:

~~~lisp
LPARALLEL-USER> (test-kill-all-tasks)
16
1
8
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
~~~

Since we had created 10 tasks, all the 8 kernel worker threads were
presumably busy with a task each. When we killed tasks of category
:default, all these threads were killed as well and had to be
regenerated (which is an expensive operation). This is part of the
reason why `lparallel:kill-tasks` must be avoided.

Now, in the example above, all running tasks were killed since all of
them belonged to the :default category. Suppose we wish to kill only
specific tasks, we can do that by binding `*task-category*` when we
create those tasks, and then specifying the category when we invoke
`lparallel:kill-tasks`.

For example, suppose we have two categories of tasks – tasks which
square their arguments, and tasks which cube theirs. Let’s assign them
categories ’squaring-tasks and ’cubing-tasks respectively. Let’s then
kill tasks of a randomly chosen category ’squaring-tasks or
’cubing-tasks.

Here is the code:

~~~lisp
;;; kill tasks of a randomly chosen category
(defun test-kill-random-tasks ()
  (let ((channel (make-channel))
        (stream *query-io*))
    (let ((*task-category* 'squaring-tasks))
      (dotimes (i 5)
        (submit-task channel #'(lambda (x)
                                 (sleep (random 5))
                                 (format stream "~%[Squaring] ~d = ~d" x (* x x))) i)))
    (let ((*task-category* 'cubing-tasks))
      (dotimes (i 5)
        (submit-task channel #'(lambda (x)
                                 (sleep (random 5))
                                 (format stream "~%[Cubing] ~d = ~d" x (* x x x))) i)))
    (sleep 1)
    (if (evenp (random 10))
        (progn
          (print "Killing squaring tasks")
          (kill-tasks 'squaring-tasks))
        (progn
          (print "Killing cubing tasks")
          (kill-tasks 'cubing-tasks)))))
~~~

And here is a sample run:

~~~lisp
LPARALLEL-USER> (test-kill-random-tasks)

[Cubing] 2 = 8
[Squaring] 4 = 16
[Cubing] 4
 = [Cubing] 643 = 27
"Killing squaring tasks"
4
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.

[Cubing] 1 = 1
[Cubing] 0 = 0

LPARALLEL-USER> (test-kill-random-tasks)

[Squaring] 1 = 1
[Squaring] 3 = 9
"Killing cubing tasks"
5
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.

[Squaring] 2 = 4
WARNING: lparallel: Replacing lost or dead worker.
WARNING: lparallel: Replacing lost or dead worker.

[Squaring] 0 = 0
[Squaring] 4 = 16
~~~

### Using promises and futures

Promises and Futures provide support for Asynchronous Programming.

In lparallel-speak, a `lparallel:promise` is a placeholder for a
result which is fulfilled by providing it with a value. The promise
object itself is created using `lparallel:promise`, and the promise is
given a value using the `lparallel:fulfill` macro.

To check whether the promise has been fulfilled yet or not, we can use
the `lparallel:fulfilledp` predicate function.  Finally, the
`lparallel:force` function is used to extract the value out of the
promise. Note that this function blocks until the operation is
complete.

Let’s solidify these concepts with a very simple example first:

~~~lisp
(defun test-promise ()
  (let ((p (promise)))
    (loop
       do (if (evenp (read))
              (progn
                (fulfill p 'even-received!)
                (return))))
    (force p)))
~~~

Which generates the output:

~~~lisp
LPARALLEL-USER> (test-promise)
5
1
3
10
EVEN-RECEIVED!
~~~

Explanation: This simple example simply keeps looping forever until an
even number has been entered. The promise is fulfilled inside the loop
using `lparallel:fulfill`, and the value is then returned from the
function by forcing it with `lparallel:force`.

Now, let’s take a bigger example. Assuming that we don’t want to have
to wait for the promise to be fulfilled, and instead have the current
do some useful work, we can delegate the promise fulfillment to
external explicitly as seen in the next example.

Consider we have a function that squares its argument. And, for the
sake of argument, it consumes a lot of time doing so. From our client
code, we want to invoke it, and wait till the squared value is
available.


~~~lisp
(defun promise-with-threads ()
  (let ((p (promise))
        (stream *query-io*)
        (n (progn
             (princ "Enter a number: ")
             (read))))
    (format t "In main function...~%")
    (bt:make-thread
     #'(lambda ()
         (sleep (random 10))
         (format stream "Inside thread... fulfilling promise~%")
         (fulfill p (* n n))))
    (bt:make-thread
     #'(lambda ()
         (loop
            when (fulfilledp p)
            do (return)
            do (progn
                 (format stream "~d~%" (random 100))
                 (sleep (* 0.01 (random 100)))))))
    (format t "Inside main function, received value: ~d~%" (force p))))
~~~

And the output:

~~~lisp
LPARALLEL-USER> (promise-with-threads)
Enter a number: 19
In main function...
44
59
90
34
30
76
Inside thread... fulfilling promise
Inside main function, received value: 361
NIL
~~~

Explanation: There is nothing much in this example. We create a
promise object p, and we spawn off a thread that sleeps for some
random time and then fulfills the promise by giving it a value.

Meanwhile, in the main thread, we spawn off another thread that keeps
checking if the promise has been fulfilled or not. If not, it prints
some random number and continues checking. Once the promise has been
fulfilled, we can extract the value using `lparallel:force` in the main
thread as shown.

This shows that promises can be fulfilled by different threads while
the code that created the promise need not wait for the promise to be
fulfilled. This is especially important since, as mentioned before,
`lparallel:force` is a blocking call. We want to delay forcing the
promise until the value is actually available.

Another point to note when using promises is that once a promise has
been fulfilled, invoking force on the same object will always return
the same value. That is to say, a promise can be successfully
fulfilled only once.

For instance:

~~~lisp
(defun multiple-fulfilling ()
  (let ((p (promise)))
    (dotimes (i 10)
      (fulfill p (random 100))
      (format t "~d~%" (force p)))))
~~~

Which produces:

~~~lisp
LPARALLEL-USER> (multiple-fulfilling)
15
15
15
15
15
15
15
15
15
15
NIL
~~~

So how does a future differ from a promise?

A `lparallel:future` is simply a promise that is run in parallel, and as
such, it does not block the main thread like a default use of
`lparallel:promise` would. It is executed in its own thread (by
the lparallel library, of course).

Here is a simple example of a future:

~~~lisp
(defun test-future ()
  (let ((f (future
             (sleep (random 5))
             (print "Hello from future!"))))
    (loop
       when (fulfilledp f)
       do (return)
       do (sleep (* 0.01 (random 100)))
         (format t "~d~%" (random 100)))
    (format t "~d~%" (force f))))
~~~

And the output:

~~~lisp
LPARALLEL-USER> (test-future)
5
19
91
11
Hello from future!
NIL
~~~

Explanation: This exactly is similar to the `promise-with-threads`
example. Observe two differences, however - first of all, the
`lparallel:future` macro has a body as well. This allows the future to
fulfill itself! What this means is that as soon as the body of the
future is done executing, `lparallel:fulfilledp` will always return true
for the future object.

Secondly, the future itself is spawned off on a separate thread by the
library, so it does not interfere with the execution of the current
thread very much unlike promises as could be seen in the
promise-with-threads example (which needed an explicit thread for the
fulfilling code in order to avoid blocking the current thread).

The most interesting bit is that (even in terms of the actual theory
propounded by Dan Friedman and others), a Future is conceptually
something that fulfills a Promise. That is to say, a promise is a
contract that some value will be generated sometime in the future, and
a future is precisely that “something” that does that job.

What this means is that even when using the lparallel library, the
basic use of a future would be to fulfill a promise. This means that
hacks like promise-with-threads need not be made by the user.

Let’s take a small example to demonstrate this point (a pretty
contrived example, I must admit!).

Here’s the scenario: we want to read in a number and calculate its
square. So we offload this work to another function, and continue with
our own work. When the result is ready, we want it to be printed on
the console without any intervention from us.

Here’s how the code looks:

~~~lisp
;;; Callback example using promises and futures
(defun callback-promise-future-demo ()
  (let* ((p (promise))
         (stream *query-io*)
         (n (progn
              (princ "Enter a number: ")
              (read)))
         (f (future
              (sleep (random 10))
              (fulfill p (* n n))
              (force (future
                       (format stream "Square of ~d = ~d~%" n (force p)))))))
    (loop
       when (fulfilledp f)
       do (return)
       do (sleep (* 0.01 (random 100))))))
~~~

And the output:

~~~lisp
LPARALLEL-USER> (callback-promise-future-demo)
Enter a number: 19
Square of 19 = 361
NIL
~~~

Explanation: All right, so first off, we create a promise to hold the
squared value when it is generated. This is the p object. The input
value is stored in the local variable n.

Then we create a future object f. This future simply squares the input
value and fulfills the promise with this value. Finally, since we want
to print the output in its own time, we force an anonymous future
which simply prints the output string as shown.

Note that this is very similar to the situation in an environment like
Node, where we pass callback functions to other functions with the
understanding that the callback will be called when the invoked
function is done with its work.

Finally note that the following snippet is still fine (even if it uses
the blocking `lparallel:force` call because it’s on a separate thread):


~~~lisp
(force (future
(format stream "Square of ~d = ~d~%" n (force p))))
~~~

To summarise, the general idiom of usage is: **define objects which will
hold the results of asynchronous computations in promises, and use
futures to fulfill those promises**.

### Using cognates - parallel equivalents of Common Lisp counterparts

Cognates are arguably the raison d’etre of the lparallel
library. These constructs are what truly provide parallelism in the
lparallel. Note, however, that most (if not all) of these constructs
are built on top of futures and promises.

To put it in a nutshell, cognates are simply functions that are
intended to be the parallel equivalents of their Common Lisp
counterparts. However, there are a few extra lparallel cognates that
have no Common Lisp equivalents.

At this juncture, it is important to know that cognates come in two basic flavours:

-    Constructs for fine-grained parallelism: `defpun`, `plet`, `plet-if`, etc.
-   Explicit functions and macros for performing parallel operations -
    `pmap`, `preduce`, `psort`, `pdotimes`, etc.

In the first case we don’t have much explicit control over the
operations themselves. We mostly rely on the fact that the library
itself will optimise and parallelise the forms to whatever extent it
can. In this post, we will focus on the second category of cognates.

Take, for instance, the cognate function `lparallel:pmap` is exactly
the same as the Common Lisp equivalent, `map`, but it runs in
parallel. Let’s demonstrate that through an example.

Suppose we had a list of random strings of length varying from 3 to
10, and we wished to collect their lengths in a vector.

Let’s first set up the helper functions that will generate the random strings:

~~~lisp
(defvar *chars*
  (remove-duplicates
   (sort
    (loop for c across "The quick brown fox jumps over the lazy dog"
       when (alpha-char-p c)
       collect (char-downcase c))
    #'char<)))

(defun get-random-strings (&optional (count 100000))
  "generate random strings between lengths 3 and 10"
  (loop repeat count
     collect
       (concatenate 'string  (loop repeat (+ 3 (random 8))
                           collect (nth (random 26) *chars*)))))
~~~

And here’s how the Common Lisp map version of the solution might look like:

~~~lisp
;;; map demo
(defun test-map ()
  (map 'vector #'length (get-random-strings 100)))
~~~

And let’s have a test run:

~~~lisp
LPARALLEL-USER> (test-map)
#(7 5 10 8 7 5 3 4 4 10)
~~~

And here’s the `lparallel:pmap` equivalent:

~~~lisp
;;;pmap demo
(defun test-pmap ()
  (pmap 'vector #'length (get-random-strings 100)))
~~~

which produces:

~~~lisp
LPARALLEL-USER> (test-pmap)
#(8 7 6 7 6 4 5 6 5 7)
LPARALLEL-USER>
~~~

As you can see from the definitions of test-map and test-pmap, the
syntax of the `lparallel:map` and `lparallel:pmap` functions are exactly
the same (well, almost - `lparallel:pmap` has a few more optional
arguments).

Some useful cognate functions and macros (all of them are functions
except when marked so explicitly. Note that there are quite a few
cognates, and I have chosen a few to try and represent every category
through an example:

#### lparallel:pmap: parallel version of map.

Note that all the mapping functions (`lparallel:pmap`,
**lparallel:pmapc**,`lparallel:pmapcar`, etc.) take two special keyword
arguments
- `:size`, specifying the number of elements of the input
sequence(s) to process, and
- `:parts` which specifies the number of parallel parts to divide the
sequence(s) into.

~~~lisp
    ;;; pmap - function
    (defun test-pmap ()
      (let ((numbers (loop for i below 10
                        collect i)))
        (pmap 'vector #'(lambda (x)
                          (* x x))
              :parts (length numbers)
              numbers)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-pmap)

    #(0 1 4 9 16 25 36 49 64 81)
~~~

#### lparallel:por: parallel version of or.

The behaviour is that it returns the first non-nil element amongst its
arguments. However, due to the parallel nature of this macro, that
element varies.


~~~lisp
    ;;; por - macro
    (defun test-por ()
      (let ((a 100)
            (b 200)
            (c nil)
            (d 300))
        (por a b c d)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (dotimes (i 10)
                      (print (test-por)))

    300
    300
    100
    100
    100
    300
    100
    100
    100
    100
    NIL
~~~

In the case of the normal or operator, it would always have returned
the first non-nil element viz. 100.


#### lparallel:pdotimes: parallel version of dotimes.

Note that this macro also take an optional `:parts` argument.


~~~lisp
    ;;; pdotimes - macro
    (defun test-pdotimes ()
      (pdotimes (i 5)
        (declare (ignore i))
        (print (random 100))))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-pdotimes)

    39
    29
    81
    42
    56
    NIL
~~~

####  lparallel:pfuncall: parallel version of funcall.


~~~lisp
    ;;; pfuncall - macro
    (defun test-pfuncall ()
      (pfuncall #'* 1 2 3 4 5))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-pfuncall)

    120
~~~

####    lparallel:preduce: parallel version of reduce.

This very important function also takes two optional keyword
arguments:  `:parts` (same meaning as explained), and `:recurse`. If
`:recurse` is non-nil, it recursively applies `lparallel:preduce` to its
arguments, otherwise it default to using reduce.

~~~lisp
    ;;; preduce - function
    (defun test-preduce ()
      (let ((numbers (loop for i from 1 to 100
                        collect i)))
        (preduce #'+
                 numbers
                 :parts (length numbers)
                 :recurse t)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-preduce)

    5050
~~~

####    lparallel:premove-if-not: parallel version of remove-if-not.

This is essentially equivalent to “filter” in Functional Programming parlance.


~~~lisp
    ;;; premove-if-not
    (defun test-premove-if-not ()
      (let ((numbers (loop for i from 1 to 100
                        collect i)))
        (premove-if-not #'evenp numbers)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-premove-if-not)

    (2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54
     56 58 60 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 100)
~~~

####    lparallel:pevery: parallel version of every.


~~~lisp
    ;;; pevery - function
    (defun test-pevery ()
      (let ((numbers (loop for i from 1 to 100
                        collect i)))
        (list (pevery #'evenp numbers)
              (pevery #'integerp numbers))))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-pevery)

    (NIL T)
~~~

In this example, we are performing two checks - firstly, whether all
the numbers in the range [1,100] are even, and secondly, whether all
the numbers in the same range are integers.

#### lparallel:count: parallel version of count.

~~~lisp
    ;;; pcount - function
    (defun test-pcount ()
      (let ((chars "The quick brown fox jumps over the lazy dog"))
        (pcount #\e chars)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-pcount)

    3
~~~

####    lparallel:psort: parallel version of sort.


~~~lisp
    ;;; psort - function
    (defstruct person
      name
      age)

    (defun test-psort ()
      (let* ((names (list "Rich" "Peter" "Sybil" "Basil" "Candy" "Slava" "Olga"))
             (people (loop for name in names
                        collect (make-person :name name :age (+ (random 20) 20)))))
        (print "Before sorting...")
        (print people)
        (fresh-line)
        (print "After sorting...")
        (psort
         people
         #'(lambda (x y)
             (< (person-age x)
                (person-age y)))
         :test #'=)))
~~~

Sample run:

~~~lisp
    LPARALLEL-USER> (test-psort)

    "Before sorting..."
    (#S(PERSON :NAME "Rich" :AGE 38) #S(PERSON :NAME "Peter" :AGE 24)
     #S(PERSON :NAME "Sybil" :AGE 20) #S(PERSON :NAME "Basil" :AGE 22)
     #S(PERSON :NAME "Candy" :AGE 23) #S(PERSON :NAME "Slava" :AGE 37)
     #S(PERSON :NAME "Olga" :AGE 33))

    "After sorting..."
    (#S(PERSON :NAME "Sybil" :AGE 20) #S(PERSON :NAME "Basil" :AGE 22)
     #S(PERSON :NAME "Candy" :AGE 23) #S(PERSON :NAME "Peter" :AGE 24)
     #S(PERSON :NAME "Olga" :AGE 33) #S(PERSON :NAME "Slava" :AGE 37)
     #S(PERSON :NAME "Rich" :AGE 38))
~~~

In this example, we first define a structure of type person for
storing information about people. Then we create a list of 7 people
with randomly generated ages (between 20 and 39). Finally, we sort
them by age in non-decreasing order.

### Error handling

To see how lparallel handles error handling (hint: with
`lparallel:task-handler-bind`), please read
[https://z0ltan.wordpress.com/2016/09/10/basic-concurrency-and-parallelism-in-common-lisp-part-4b-parallelism-using-lparallel-error-handling/](https://z0ltan.wordpress.com/2016/09/10/basic-concurrency-and-parallelism-in-common-lisp-part-4b-parallelism-using-lparallel-error-handling/).


## Monitoring and controlling threads with Slime

**M-x slime-list-threads** (you can also access it through the
*slime-selector*, shortcut **t**) will list running threads by their
names, and their statuses.

The thread on the current line can be killed with **k**, or if there’s a
lot of threads to kill, several lines can be selected and **k** will kill
all the threads in the selected region.

**g** will update the thread list, but when you have a lot of threads
starting and stopping it may be too cumbersome to always press **g**, so
there’s a variable `slime-threads-update-interval`, when set to a number
X the thread list will be automatically updated each X seconds, a
reasonable value would be 0.5.

Thanks to [Slime tips](https://slime-tips.tumblr.com/).


## References

There are, of course, a lot more functions, objects, and idiomatic
ways of performing parallel computations using the lparallel
library. This post barely scratches the surface on those. However, the
general flow of operation is amply demonstrated here, and for further
reading, you may find the following resources useful:

- [The API docs hosted on Quickdoc](http://quickdocs.org/lparallel/api#package-LPARALLEL)
- [The official homepage of the lparallel library](https://lparallel.org/)
- [The Common Lisp Hyperspec](https://www.lispworks.com/documentation/HyperSpec/Front/), and, of course
- Your Common Lisp implementation’s
  manual. [For SBCL, here is a link to the official manual](https://www.lispworks.com/documentation/HyperSpec/Front/)
- [Common Lisp recipes](http://weitz.de/cl-recipes/) by the venerable Edi Weitz.

---
title: Defining Systems
---

A **system** is a collection of Lisp files that together constitute an application or a library, and that should therefore be managed as a whole. A **system definition** describes which source files make up the system, what the dependencies among them are, and the order they should be compiled and loaded in.


## ASDF

[ASDF](https://gitlab.common-lisp.net/asdf/asdf) is the standard build
system for Common Lisp. It is shipped in most Common Lisp
implementations. It includes
[UIOP](https://gitlab.common-lisp.net/asdf/asdf/blob/master/uiop/README.md),
_"the Utilities for Implementation- and OS- Portability"_. You can read
[its manual](https://common-lisp.net/project/asdf/asdf.html) and the
[tutorial and best practices](https://gitlab.common-lisp.net/asdf/asdf/blob/master/doc/best_practices.md).

<a name="example"></a>

## Simple examples

### Loading a System

The most trivial use of ASDF is by calling `(asdf:make :foobar)` (or `load-system`)
to load your library.
Then you can use it.
For instance, if it exports a function `some-fun` in its package `foobar`,
then you will be able to call it with `(foobar:some-fun ...)` or with:

~~~lisp
(in-package :foobar)
(some-fun ...)
~~~

You can also use Quicklisp:

~~~lisp
(ql:quickload :foobar)
~~~

### Testing a System

To run the tests for a system, you may use:

~~~lisp
(asdf:test-system :foobar)
~~~

The convention is that an error SHOULD be signalled if tests are unsuccessful.

### Designating a system

The proper way to designate a system in a program is with lower-case
strings, not symbols, as in:

~~~lisp
(asdf:make "foobar")
(asdf:test-system "foobar")
~~~

### Trivial System Definition

A trivial system would have a single Lisp file called `foobar.lisp`.
That file would depend on some existing libraries,
say `alexandria` for general purpose utilities,
and `trivia` for pattern-matching.
To make this system buildable using ASDF,
you create a system definition file called `foobar.asd`,
with the following contents:

~~~lisp
(defsystem "foobar"
  :depends-on ("alexandria" "trivia")
  :components ((:file "foobar")))
~~~


Note how the type `lisp` of `foobar.lisp`
is implicit in the name of the file above.
As for contents of that file, they would look like this:

~~~lisp
(defpackage :foobar
  (:use :common-lisp :alexandria :trivia)
  (:export
   #:some-function
   #:another-function
   #:call-with-foobar
   #:with-foobar))

(in-package :foobar)

(defun some-function (...)
    ...)
...
~~~

Instead of `using` multiple complete packages, you might want to just import parts of them:

~~~lisp
(defpackage :foobar
  (:use #:common-lisp)
  (:import-from #:alexandria
                #:some-function
                #:another-function))
  (:import-from #:trivia
                #:some-function
                #:another-function))
...)                
~~~


#### Using the system you defined

Assuming your system is installed under `~/common-lisp/`,
`~/quicklisp/local-projects/` or some other filesystem hierarchy
already configured for ASDF, you can load it with: `(asdf:make "foobar")`.

If your Lisp was already started when you created that file,
you may have to `(asdf:clear-configuration)` to re-process the configuration.


### Trivial Testing Definition

Even the most trivial of systems needs some tests,
if only because it will have to be modified eventually,
and you want to make sure those modifications don't break client code.
Tests are also a good way to document expected behavior.

The simplest way to write tests is to have a file `foobar-tests.lisp`
and modify the above `foobar.asd` as follows:

~~~lisp
(defsystem "foobar"
    :depends-on ("alexandria" "trivia")
    :components ((:file "foobar"))
    :in-order-to ((test-op (test-op "foobar/tests"))))

(defsystem "foobar/tests"
    :depends-on ("foobar" "fiveam")
    :components ((:file "foobar-tests"))
    :perform (test-op (o c) (symbol-call :fiveam '#:run! :foobar)))
~~~

The `:in-order-to` clause in the first system
allows you to use `(asdf:test-system :foobar)`
which will chain into `foobar/tests`.
The `:perform` clause in the second system does the testing itself.

In the test system, `fiveam` is the name of a popular test library,
and the content of the `perform` method is how to invoke this library
to run the test suite `:foobar`.
Obvious YMMV if you use a different library.

## Create a project skeleton

[cl-project](https://github.com/fukamachi/cl-project) can be used to
generate a project skeleton. It will create a default ASDF definition,
it generates a system for unit testing, etc.

Install with

    (ql:quickload :cl-project)

Create a project:

~~~lisp
(cl-project:make-project #p"lib/cl-sample/"
:author "Eitaro Fukamachi"
:email "e.arrows@gmail.com"
:license "LLGPL"
:depends-on '(:clack :cl-annot))
;-> writing /Users/fukamachi/Programs/lib/cl-sample/.gitignore
;   writing /Users/fukamachi/Programs/lib/cl-sample/README.markdown
;   writing /Users/fukamachi/Programs/lib/cl-sample/cl-sample-test.asd
;   writing /Users/fukamachi/Programs/lib/cl-sample/cl-sample.asd
;   writing /Users/fukamachi/Programs/lib/cl-sample/src/hogehoge.lisp
;   writing /Users/fukamachi/Programs/lib/cl-sample/t/hogehoge.lisp
;=> T
~~~

---
title: Using the Win32 API
---


<a name="intro"></a>

## Introduction and Scope

This chapter introduces the basics of the Win32 API, demonstrates how it can be called from Lisp, and shows how Lisp can be used to create a powerful Win32 programming environment optimized for a specific application domain. Any such Lisp environment exposing every option in the Win32 API must be at least as complex as the Win32 API. However, it is hard to imagine a given application domain needing every option of every function in the Win32 API. Extending Lisp's syntax in a domain-specific manner hides those parts of the Win32 API superfluous to the domain. (Most likely the majority of the Win32 API will be hidden.) The programmer then deals with and thinks about only those pieces of the API needed for the task at hand. Pieces of the Win32 API needed later are easily exposed at any time.

A single environment is not easily optimized for both simple and complex applications. The methods outlined in this chapter can be used to construct environments, from simple to complex, appropriate for a range of application domains.

The beginning sections of the chapter introduce concepts and ideas leading to the penultimate section, which concludes with a "Hello" program:

~~~lisp
(require "win-header")
(in-package :wh)
(export '(initialize-appendix-b))

(DefMsgHandler appendix-b-proc MsgMap ()
               (WM_PAINT
                (text-out "Hello from Lisp!" 0 0 :color (RGB 0 0 255))
                0))

(defun initialize-appendix-b ()
  (DefActiveTopWindow "Hello" 'appendix-b-proc
    :style ws_overlappedwindow :width 150 :height 200 :title "Hello Program"))
~~~

Lispworks for Windows version 4.2.7 is used. (The Personal Edition is available for free download.) No attempt is made to accommodate other Lisps and no thought is given to cross-platform compatibility. These ideas were discussed when outlining this document and the current approach decided upon because:

* It is good to publish early and publish often. Reducing the scope fits this philosophy. Other Lisps can be included at a later time. I know that [Corman Lisp](http://www.cormanlisp.com) is one that includes a lot of Win32 API capabilities. I don't have experience with other vendor's Lisps, so look at them all.
* The information presented should apply generally to other vendor's Lisps. The foreign language interface may change but Win32 remains the same.

The Cookbook welcomes contributions broadening coverage in these (and other) areas.

Anyone programming for the Win32 API should be familiar with [Programming Windows, The Definitive Guide to the Win32 API](http://www.charlespetzold.com/pw5/index.html) by Charles Petzold, or a comparable book.

The code presented here has been tested only with Lispworks for Windows 4.2.7 Professional Edition under Windows XP.


<a name="whywin32"></a>

## Why Use Lisp with Win32?

If a program can be cross-platform, it should be. The _Common_ in Common Lisp is a tribute to the cross-platform, and cross-vendor, nature of Common Lisp. The advantages of this portability are not ignored lightly. However, writing programs which take advantage of the Win32 API in a non-portable fashion is not a mandate for doing so without the power of Lisp at one's fingertips.

One concern some may have is the perception that Lisp executables are large. Microsoft operating systems are written in C and C++. The supporting libraries for C/C++ programs come with the operating system. Lisp run-time support looks large only because it does not come with the OS. It is possible to ship Lisp programs in delivery mode, especially with Lispworks for Windows which requires no royalties for this, and it is possible to ship the run-time once and then ship small, compiled programs as they become available.

If the requirement is to create small programs suitable for public download in large volumes, the fact that Lisp runtime support is not loaded on most computers is a limitation. However, Lispworks for Windows allows aggressive pruning during the delivery stage and programs can sometimes be reduced below two megabytes, which is suitable for many download situations. Situations requiring the full Lisp run-time are most likely large applications where it makes sense to deliver the runtime on CD, or via a large one-time download, and later deliver individually-compiled components which are loaded during program or component initialization.

It is my understanding that the lower limit on delivered program size is not inherent to Lisp as a language but only a result of current market demand. As market demand grows for truly small stand-alone executable programs written in Lisp, vendors will have incentive to spend time developing better pruners and shakers. (Lisp starts with a lot and removes what's not needed to produce a deliverable while C/C++ starts with a little and adds what's needed during the link phase. A freshly-run Lisp begins with a REPL, a read-eval-print loop, with the full power of Lisp available interactively and ready for use.)

Seeing the gory details of Lispwork's Foreign Language Interface, or FLI, may seem strange at first. The OS is written in C/C++, so OS calls from C/C++ are not foreign calls, just as Lisp functions called from Lisp functions are not foreign calls.


<a name="apioverview"></a>

## A (Very) Brief Overview of a Win32 Program's Life

The OS treats a Win32 GUI program as a set of subroutines, or functions.

* The first of these functions, WinMain, is called when the program is initialized.
* WinMain calls the Win32 function RegisterClass to inform the OS of the location of a callback function. Several calls may be made to RegisterClass but only one of these calls, naming one callback function, serves the primary window of the application. After RegisterClass is called, the OS knows about the newly-defined class.
* The Win32 function CreateWindowEx is called and given the class name specified on a previous RegisterClass call. Now the window exists and, depending upon the parameters passed to CreateWindowEx, is visible.
* The OS begins queueing messages relating to the newly-created window. The queue is saved for delivery to WinMain.
* WinMain accesses the message queue in a loop referred to as a _message pump_. The message pump loop calls GetMessage, TranslateMessage, and DispatchMessage.
* GetMessage retrieves one message from the queue. There is some method or dependability to the sequence in which messages are queued. A certain sequence of messages is queued when a window is created, for instance. Events, such as mouse movements, cause other messages to be queued.
* TranslateMessage translates virtual-key messages to character messages. DispatchMessage is a call to the OS requesting the OS to handle the message. The OS handles the message by calling another subroutine, or function, in the application program. The application function which is called is the function specified in a call to RegisterClass, which included a class name parameter, where that class name was specified in the call to CreateWindowEx. (Phew).
* The application function specified in the RegisterClass call takes four parameters:
    1. a handle to the window associated with the message
    2. the message id, an integer
    3. a wParam unsigned long
    4. an lParam unsigned long
* The semantics of wParam and lParam vary depending upon the message id.
* The application function contains the equivalent of a case statement, switching on the message id. There are many (hundreds of) message ids. Common ones include wm_create, sent when a window is created, wm_paint, sent when a window's contents are to be drawn, and wm_destroy, sent when a window is about to go away. Other messages are generated in response to events such as key presses, mouse movements, and mouse button clicks. There are windows messages related to displaying video from attached cameras, capturing or playing sound files, dialing telephones, and much more. There are thousands of these messages in the OS but any given program normally deals with a small subset of them. When a message function receives a message with which it does not deal explicitly, the message is passed to a Win32 default function.
*   When a menu command or other event causes the program to enter code which calls the Win32 function PostQuitMessage, the message pump returns zero from GetMessage, which is the cue to exit the message pump loop. WinMain then exits and the program ends.


<a name="unicode"></a>

## Windows Character Systems and Lisp

Some Microsoft operating systems use a single-byte, ASCII, character set and others use a double-byte, Unicode, character set. Use

~~~lisp
(defun external-format ()
  (if (string= (software-type) "Windows NT")
      :unicode
    :ascii)
~~~

to determine which format is in use. Win32 functions taking or returning characters or strings come in two flavors: 1) those ending in A for ASCII characters and 2) those ending in W for wide Unicode characters. This external-format function is useful primarily when calling `fli:with-foreign-string`, part of the Lispworks foreign function interface. When defining Win32 functions in the Foreign Function Interface, or FLI, the presence of the keyword :dbcs indicates that the function has both a single-byte and a double-byte version. When :dbcs is present, Lispworks appends an "A" to the function name in single-byte Windows 95 and a "W" in double-byte Windows NT/2000/XP. (I wrote and tested the example program (see Appendix A) under Windows XP.) Without :dbcs, Lispworks leaves the foreign function name unchanged.

One FLI definition for the Win32 TextOut function is:

~~~lisp
(fli:define-foreign-function
    (TextOut "TextOut" :dbcs :calling-convention :stdcall)
    ((HDC (:unsigned :long)) (nXStart (:unsigned :int)) (nYStart (:unsigned :int))
     (lpString :pointer) (cbString (:unsigned :int)))
  :result-type (:unsigned :long))
~~~

which is equivalent to:

~~~lisp
(fli:define-foreign-function
    (TextOut "TextOutW" :calling-convention :stdcall)
    ((HDC (:unsigned :long)) (nXStart (:unsigned :int)) (nYStart (:unsigned :int))
     (lpString :pointer) (cbString (:unsigned :int)))
  :result-type (:unsigned :long))
~~~


under NT/2000/XP (the second example would use "TextOutA" under 95).

To demonstrate this, let's use a simple FLI definition which is easy to call interactively for testing purposes. We are trying only to see if a given Win32 function is known to the OS. In the following REPL interaction, the return result is important only when the Lisp restart handler is invoked. When the restart handler is not invoked, the Win32 function was found, loaded and called. Trying to call a Win32 function which the FLI cannot find results in an invocation of the restart handler. (The correct define-foreign-function definition for textout can be found in [Appendix A](#appendixa)

~~~lisp
CL-USER 9 > (fli:define-foreign-function
                (TextOut-1 "TextOut" :dbcs :calling-convention :stdcall)
                () :result-type :int)
TEXTOUT-1

CL-USER 10 > (textout-1)
1

CL-USER 11 > (fli:define-foreign-function
                 (TextOut-2 "TextOut" :dbcs :calling-convention :stdcall)
                 () :result-type :int)
TEXTOUT-2

CL-USER 12 > (textout-2)
0
~~~


The `TextOut` function was found both times. This shows that a given Win32 function can be named in more than one FLI definition. This technique is sometimes useful when more than one Lisp datatype can satisfy the requirements for a parameter of the Win32 function.

~~~lisp
CL-USER 13 > (fli:define-foreign-function
                 (TextOut-3 "TextOutW" :dbcs :calling-convention :stdcall)
                 () :result-type :int)

TEXTOUT-3

CL-USER 14 > (textout-3)

Error: Foreign function TEXTOUT-3 trying to call to unresolved
external function "TextOutWW".

1 (abort) Return to level 0.
2 Return to top-level loop.
3 Return from multiprocessing.

Type :b for backtrace, :c  to proceed,  or :? for other
options

CL-USER 15 : 1 > :top

CL-USER 16 > (fli:define-foreign-function
                 (TextOut-4 "TextOutW" :calling-convention :stdcall)
                 () :result-type :int)

TEXTOUT-4

CL-USER 17 > (textout-4)
1

CL-USER 18 > (fli:define-foreign-function
                 (TextOut-5 "TextOutA" :calling-convention :stdcall)
                 () :result-type :int)
TEXTOUT-5

CL-USER 19 > (textout-5)
0

CL-USER 20 >
~~~


I elided a warning Lispworks gives after multiple definitions of a foreign function when the previous definition differs in its use of the :dbcs keyword from the current definition's use. After CL-USER 14, Lispworks complains about "TextOutWW", which shows that using the :dbcs keyword causes Lispworks to append a 'W' to the foreign function's name, although the existence of the foreign function itself is not verified until an actual call is made. TextOut-5 verifies the existence of the Win32 function TextOutA, which is the ASCII version of TextOut.

I have seen strange character sets in titles and other places and have been able to resolve those problems by ensuring I made consistent use of the :dbcs and/or W/A declarations.

Edi Weitz asked if the :dbcs keyword decides at compile time or at run time which function to call, the ...A or the ...W. I wrote the following program:

~~~lisp
(in-package :cl-user)

(fli:define-foreign-function
    (MessageBox "MessageBox" :dbcs :calling-convention :stdcall)
    ((hwnd (:unsigned :long)) (text (:unsigned :long)) (title (:unsigned :long)) (flags (:unsigned :long)))
  :result-type (:unsigned :long))

(defun external-format ()
  (if (string= (software-type) "Windows NT")
      :unicode
      :ascii))

(defun display-format-used ()
  (fli:with-foreign-string
      (unicode-p u-ec u-bc :external-format (external-format)) "Unicode"
      (fli:with-foreign-string
	  (ascii-p a-ec a-bc :external-format (external-format)) "Ascii"
	  (fli:with-foreign-string
              (title-p t-ec t-bc :external-format (external-format)) "External Format"
              (if (eq (external-format) :unicode)
                  (messagebox 0 (fli:pointer-address unicode-p)
                              (fli:pointer-address title-p) 0)
                  (messagebox 0 (fli:pointer-address ascii-p)
                              (fli:pointer-address title-p) 0))))))

(compile 'external-format)
(compile 'display-format-used)

(deliver 'display-format-used "dbcs-run" 5)
(quit)

; We then have dbcs-run.exe.  When run on Windows XP, dbcs-run pops up a messagebox
; displaying "Unicode".  The same dbcs-run.exe file, ftp'd to a Macintosh running OS 9 with
; Virtual PC running Windows 98, pops up a message box displaying "Ascii".
~~~


<a name="fli"></a>

## FLI - The Foreign Language Interface - Translating C Header Files to Lisp

When calling the Win32 API from C/C++, header files provided with the compiler are `#include`d in the program. The header files contain the definitions of constants, structures, and functions comprising the API. These definitions must be available to the Lisp program. I find it most straightforward to do this conversion by hand. Although there are automated methods, doing it manually does not take long on a per-function basis.

In C/C++, the `#define`s exist in the preprocessor. Only those `define`s used by the program are included in the object code. With Lisp, the `defconstants` are all loaded into the Lisp image, whether or not they are subsequently used. I do not know a clean solution for this issue.

In the meantime, I make a base, or core, win-header.lisp and use other .lisp files, grouped by functionality, for less-frequently-used definitions, loading those .lisp files when I need them.<a name="fli-types"></a>


### FLI Data Types

The Win32 C/C++ header files include many typedefs for OS-specific data types, including HINSTANCE, HANDLE, HMENU, LPCTSTR, and more. Regarding Lisp, these essentially boil down to signed or unsigned, long or char, and singleton or array, or C structures composed of those types. (Int seems to be the same as long.)

Lisp does not know or care about the difference between an HINSTANCE and an HMENU. They both are simply 32-bit values. Lisp pays attention to these values at two different points in time: 1) when moving Lisp data to a foreign field and 2) when moving the foreign data to Lisp. Lispworks attempts coercion at those points and conditions result when incorrect attempts are made to do conversions like stuffing a negative value into an unsigned field. If more hints about type are given to Lisp, such as declaring a foreign field to be of type :pointer, Lisp will complain when trying to stuff zero into the pointer. That is not handy if one is trying to pass a null pointer to the OS. Thus, I find it easier to call most parameters long, although I bend that rule from time to time.

Lispworks FLI pointers are actually a Lisp structure containing an address retrieved, or unboxed, by fli:pointer-address. When passing a pointer value to the OS, for example when passing the address of a `RECT` to `GetClientRect`, there are two steps that need to happen: 1) allocate the foreign structure and 2) pass the address of that allocated structure to the OS. Most of the time these allocations are best handled with `fli:with-dynamic-foreign-objects` enclosing calls to `fli:allocate-dynamic-foreign-object` because one doesn't have to worry about deallocations. I pass the address of the allocated structure using `fli:pointer-address` (unboxing the pointer value) and define the field in the foreign function's parameter list as an unsigned long.

The FLI allows things to be defined such that Lispworks will try automatic coercion (unboxing). Try defining the parameter type as :pointer. However, Lispworks complains when trying to pass a NULL pointer, although I did not try creating a FLI pointer with address zero. The approach I chose, calling pointers unsigned longs, is clear to me and works well in both directions (OS->Lisp, Lisp->OS). This may simply be a result of my current lack of complete understanding and there may be a better way.

On occasion is it helpful to define C arrays inside C structures, in particular in `sPAINTSTRUCT`. This works but I don't like my current method of obtaining the address of structure members or array entries. I find myself counting byte offsets by hand and using something like:

~~~lisp
(defun interior-copy (to-struct-ptr byte-offset src-ptr)
  (let ((ptr (fli:make-pointer
              :address (fli:pointer-address to-struct-ptr :type :char))))
    (fli:incf-pointer ptr byte-offset)
    (wcscpy (fli:pointer-address ptr) (fli:pointer-address src-ptr))))
~~~


where wcscpy, the wide-character version of strcpy, is defined through the FLI. I hope there's a better way to do this and that someone quickly teaches me. I haven't worked enough with different OSes and Lispworks to know the best way to choose strcpy vs. wcscpy, other than to use (software-type) to decide which to call. (Or use (external-format), defined in Appendix A.)

Although the data types defined to Lisp are kept a minimum, it is very useful for documentation purposes to mimic the typedef names used in the C/C++ header files. Thus `fli:define-c-typedef` is used to define BOOL, DWORD, HANDLE, HDC, and other similar Win32 data types.

Many OS-specific constants must be made available to the Lisp program:

~~~lisp
(defconstant CW_USEDEFAULT       #x80000000)
(defconstant IDC_ARROW                32512)
(defconstant SW_SHOW                      5)
(defconstant WM_CLOSE            #x00000010)
(defconstant WM_DESTROY          #x00000002)
~~~


These constants are given by name, without values, in the MSDN documentation. The Lisp program needs not only the name but also the value. An easy way to find the necessary values is to grep through the VC98/Include directory. Visual Studio contains a "find in files" function on its toolbar which allows this kind of search. Kenny Tilton says, "What I did was grab any VC++ project that builds (the NeHe OpenGL site is full of VC++ projects (see OpenGL tutorials in sidebar to left of the page at [http:///nehe.gamedev.net](http://nehe.gamedev.net)) which built without a problem for me) and then right-click on the symbol I was curious about. (Of > course first you have to find a reference <g>.) VC++ then offers 'find definition' and will jump right to a header entry for a function or constant or macro or whatever."<a name="fli-structures"></a></g>

### FLI Data Structures

I usually define the structure and a typedef for it:

~~~lisp
; PAINTSTRUCT
(fli:define-c-struct sPAINTSTRUCT
    (HDC hdc)
  (fErase bool)
  (rcPaint-x uint)
  (rcPaint-y uint)
  (rcPaint-width uint)
  (rcPaint-height uint)
  (fRestore bool)
  (fIncUpdate bool)
  (rgbReserved (:c-array wBYTE 32)))
(fli:define-c-typedef PAINTSTRUCT sPAINTSTRUCT)
~~~

and then can do something like:

~~~lisp
(fli:with-dynamic-foreign-objects ()
  (let ((ps-ptr (fli:allocate-dynamic-foreign-object :type 'paintstruct)))
    (format t "~&Pointer value: ~a" (fli:pointer-address ps-ptr))))
~~~

although I'm not clear on why the typedef is valuable. Lisp is not C and in Lisp the typedef does not save me from typing `struct sPAINTSTRUCT`, for example. I think the typedefs are superfluous and I probably will stop using them.<a name="fli-functions"></a>


### FLI Functions

It is very easy to define OS calls in the FLI. I start with the API definition in the OS documentation. If Visual C++ is available, the MSDN documentation is probably loaded on the machine. The documentation is available on the [MSDN website](http://msdn.microsoft.com). I go to the Win32 documentation page for the desired function and do a simple translation:

~~~lisp
; LoadCursor
(fli:define-foreign-function
    (LoadCursor "LoadCursor" :dbcs :calling-convention :stdcall)
    ((hInstance handle) (param ulong))
  :result-type handle)
~~~


All the Win32 calls I've seen so far are `:calling-convention :stdcall`. If I know the function includes a text parameter, I include the :dbcs keyword. If I don't know, I try it without :dbcs. The actual function called in this example is `LoadCursorA` or `LoadCursorW`.


<a name="callbacks"></a>

## Callbacks from Windows to Lisp

Once the message pump is up and going, the OS delivers the messages by calling a Lisp function repeatedly. Lisp functions callable from the foreign environment can be defined in the following manner:

~~~lisp
; WndProc -- Window procedure for the window we will create
(fli:define-foreign-callable
    (wndproc :result-type :long :calling-convention :stdcall)
    ((hwnd hwnd) (msg ulong)
     (wparam ulong) (lparam ulong))
  (case msg
    (#.WM_PAINT (wndproc-paint hwnd msg wparam lparam))
    #+console (#.WM_DESTROY (PostQuitMessage 0) 0)
    (t (DefWindowProc hwnd msg wparam lparam))))
~~~


This wndproc function is the message dispatcher. The OS calls wndproc once for every message sent to the program. Wndproc is responsible for understanding the message and calling the appropriate function.

The #. reader macro returns the value of `WM_PAINT` and `WM_DESTROY` at compile-time, allowing `case` to work. #+console means "include the next form only if :console is a member of *features*".

The example Win32 Lisp program in Appendix A may be run either from the Lispworks IDE or from console mode, such as ILISP in Emacs. If `PostQuitMessage` is called from the IDE, the IDE shuts down. If `PostQuitMessage` is not called in console mode, the Win32 window does not close.


<a name="startup"></a>

## Starting the Program

Multiprocessing is always running under the Lispworks IDE but may or may not be running using ILISP under Emacs. Using multiprocessing is great because one can peek and poke at the program and its variables, provide new or redefined functions which take effect immediately, and even make Win32 API calls, all while the program is running and the window is visible with all its buttons and menus active.

Using multiprocessing has not proven so nice for me under ILISP. I love the ILISP and Emacs environment. The Lispworks IDE is very nice, and I keep a copy of it going for certain tasks such as finding online manuals and using debug tools such as the inspector. For editing and most running, though, I prefer Emacs and ILISP. However, I have not learned how to view multiple processes under ILISP, nor do I know how to switch between them. When I use multiprocessing with ILISP, it appears to me that any thread with a condition grabs *standard-output* and *standard-input*. I don't know how to switch back to the other thread. This is enough of a problem that I don't use multiprocessing under ILISP and when I need or want the interactive debug capabilities possible with multiprocessing, or need multiprocessing in any form, I switch to the Lispworks IDE.

When running under the IDE, Lispworks provides the message pump. When running under Emacs/ILISP (or in console mode, as would happen in a delivered application), the Lisp program itself must provide the message pump.

Thus in the example program in Appendix A, the function `create-toplevel-window` ensures multiprocessing is running when in console mode. The function `create-toplevel-window-run` performs the message pump operation in console mode but not otherwise.

The program in Appendix A makes a call to register-class when the file is loaded. The call needs to be made only once and so I make the call at the top-level:

~~~lisp
(defvar *reg-class-atom* (register-class))
~~~


`create-toplevel-window-run` then only needs to call `CreateWindowEx` and optionally start the message pump.


<a name="repl"></a>

## The Lisp REPL and Win32 Development

When a Win32 application is running from within the Lispworks IDE, one is able to enter Lisp forms at the IDE's REPL prompt. One can view any variable, redefine any function, and make calls to `SendMessage` or any other Win32 function that doesn't require context from the OS, such as being within a WM_PAINT. If one redefines the function called when a button is clicked, the next click of the button gets the new function. The Lispworks debug tools are available. Other Lisp programs can be run simultaneously. Individual functions within the running Win32 program can be called from the REPL. Functions can be traced and untraced, advice can be added or removed, and CLOS classes can be redefined on the fly with Lisp guaranteeing that the slot additions or deletions happen in an orderly fashion within the running program.

Lisp is designed to allow programs to run for years at a time, with careful management, and to allow the programs to be maintained, with bugs fixed, new functions defined, and CLOS objects redefined, during that time.


<a name="capi"></a>

## Making Direct Win32 Calls from CAPI

Lispworks includes CAPI, a cross-platform API for GUI program development. CAPI is powerful and easy to use. For true cross-platform capability, it is important to stay with 100%-pure CAPI.

However, even in a pure Win32 environment it is reasonable to want to use CAPI's features quickly to generate advanced GUI programs without having to recreate every wheel. It is possible to use Win32-specific features from within a CAPI program.

~~~lisp
(defclass image-pane (output-pane) ()
  (:default-initargs
   :display-callback 'draw-image))

...

(let ((pane-1 (make-instance 'image-pane))
      ...
      (contain (make-instance 'column-layout
                              :description (list pane-1 other-pane))
        :best-width 640
        :best-height 480)))

...

(defun draw-image (pane x y width height)
  (let ((hwnd (capi:simple-pane-handle pane)))
    ;; This returns the actual Win32 window handle
    ;; Now call CreateWindowEx with hwnd as the new window's parent
    ;; The Win32-defined window then covers the
    ;; CAPI window. After destroying or hiding the Win32 window, the
    ;; CAPI window is revealed.
    ;; Be careful not to create the window if it already is created.

    ....
~~~

Note the connections from image-pane to pane-1 and from image-pane to draw-image, and that pane-1 is contained in the CAPI window. Draw-image gets called when it is time to ... well, when it is time to draw the image!

Certainly calls to Win32 functions which don't require handles to windows or other interaction with the CAPI environment work just fine.

Many good programs use primarily local variables. If one wishes to use the multiprocessing environment to operate upon the program from the Lispworks IDE while the Windows program is running, it is important to have access to symbols and variables for the window handles and other Windows resources. If `SendMessage` is to be called from the REPL, a valid hwnd must be available. A way to have the hwnd available is to do a setf from within a function to which the OS passes the hwnd. This may be used as a debug-only technique or left as a permanent part of the program.


<a name="cinterface"></a>

## Interfacing to C

The OS makes many of the Win32 functions available always. Other functions, for example the `avicap32` video functions, exist in DLLs which must be explicitly loaded. Third-party or custom-built DLLs also require explicit loading.

Make a def file, such as avicap32.def, named after the desired DLL:

~~~
exports capCreateCaptureWindow=capCreateCaptureWindowW
exports capGetDriverDescription=capGetDriverDescriptionW
~~~


and in Lisp

~~~lisp
;; capCreateCaptureWindow
(fli:define-foreign-function
    (capCreateCaptureWindow "capCreateCaptureWindowW")
    ((lpszWindowName :pointer) (dwStyle fli-dword) (x :int) (y :int)
     (nWidth :int) (nHeight :int) (HWND fli-hwnd) (nID :int))
  :result-type (:unsigned :long)
  :module :avicap32
  :documentation "Opens a video capture window.")

(fli:register-module "avicap32")
~~~


Windows created using functions in DLLs can be given a CAPI window as a parent, as previously shown. The def file may or may not be required, depending upon what functions are desired. Maybe :dbcs should be used here, eliminating the need for the hard-coded 'W' in the foreign function name.


<a name="raiiandgc"></a>

## RAII and GC

A common C++ idiom is "Resource Acquisition Is Initialization", in which a C++ object acquires an operating system resource, perhaps an open file, in the constructor and releases the resource in the object's destructor. These objects may have dynamic or indefinite extent.

Objects with dynamic extent are declared local at the beginning of a C++ function and the object's destructor is called when the function returns and the object goes out of scope. The corresponding Lisp idiom is the use of a `with-...` macro. The macro is responsible for acquiring the resource and releasing it under an unwind-protect.

In C++, objects with indefinite extent must have their destructor called explicitly, with `delete` or `delete []`. The destructor tears down the object, first releasing any acquired resources via explicitly-programmed C++ code, then releasing the object's memory via compiler-generated code as the destructor exits.

Lisp is garbage collected, which means that Lisp is responsible for freeing the object's memory. However, that may not happen for a very long time after the last reference to the object has disappeared. The garbage collector runs only as memory fills or when it is explicitly called. If an object holds an acquired resource, almost always there is a proper time to release the resource and not releasing it at that time leads to resource exhaustion.

Lisp is not responsible for acquired resources, such as window handles, which the programmer acquired with explicit Lisp code. The programmer must define a function, something like `(defun release-resources...`, and call the release function at the point where the destructor would have been called in C++. After the release function returns and there are no references to the object, Lisp will free the object's memory during a future garbage collection.

Another issue with the Win32/Lisp environment concerns the GC, which is free to move Lisp data. One cannot give the OS the address of Lisp data which may be moved by the GC. Any data given to the OS should be allocated through the FLI, which is responsible for making the data immovable.


<a name="com"></a>

## COM

COM is widely used in Windows programming. Lispworks for Windows includes a _COM/Automation User Guide and Reference Manual_ and the associated functions. I have not played with COM under Lisp and only note the availability of the manuals and functions. Actually, I did require com and automation and called the com:midl form, which loaded a huge series of IDL files, amazing in breadth and extent. I didn't actually make things happen with COM, though.


<a name="power"></a>

## Beginning to Use the Power of Lisp

My first thought, when I finally completed my demo program, was "That looks like any other Win32 program." There was nothing Lispy about it. It was just Win32 code, programmable in any language. Different languages are good for different problem sets. When I think of Perl, I think of text. I think of numbers along with Fortran. When I think of Lisp, I think of defining my own language. Macros are one of the tools used to define embedded languages within Lisp and are part of what makes Lisp the programmable programming language.

Win32 API programming cries out for new languages. It is a very powerful and flexible API but in a given application context, only certain subsets are used and they are used in repetitive fashions. This does not mean that the APIs should be redefined, were that possible. What works for one application may not work for the next. There probably are some language extensions that will be used in nearly all Win32 programs. Other extensions will apply only to certain applications.

One beauty of Lisp is that the programmer can define a new extension at any time. See [the Common Lisp Cookbook's chapter on macros](macros.html). I also recommend Paul Graham's [On Lisp](http://paulgraham.com/books.html) for learning to write macros and a whole lot more.

When writing code, notice when the same pattern is typed over and over. Then think, sooner rather than later, "it's time for a macro or a function." Notice the repetitive coding even when you're writing macros. Macros can be built upon macros, and macros can generate macros.

Knowing whether to choose a macro or a function is partly a function of code bloat. Macros are evaluated in place and cause new code to be created, where functions do not. The advantage of macros is that they can create closures, capturing variable values present in the environment when the macro is expanded, thus eliminating much of the need for simple objects. If the macro creates `defvar` or `defparameter` forms, other functions the macro creates can use those vars and parameters, although closures can also be used for that purpose. The use of defvars allows reference to those defvars at the REPL in a multiprocessing environment while the Win32 program has an open window and is executing. Whether defvars or closures are used, absolutely zero store-and-retrieve infrastructure is needed.

If macros are not used in this way, then functions that would have been created by the macro must have some form of object look-up code to retrieve window handles, captions, and other resources specific to the object in question.

Be aware that functions can be declaimed inline and can be passed as parameters, while macros cannot.

Many of the macros defined in [On Lisp](http://www.paulgraham.com/books.html) are very useful in Win32 programming. I use the symbol creation macros extensively.

One set of needed macros make using the Foreign Language Interface easier, more compact, and more readable. I have a macro `with-foreign-strings` which takes a list of pointer-name/string pairs and creates a nested series of `(fli:with-foreign-string...` calls, including creation of the element-count and byte-count parameters with unique, predictable names for each string. My `setf-foreign-slot-values`, and `with-foreign-slot-values` macros also make for more compact and readable code.

Another set of macros is useful for defining the Windows message handler functions. I prefer to create a CLOS class for windows messages and let the CLOS method dispatcher find the proper function for each message. This allows message handlers to be inherited and more-specific handlers defined for selected messages in the derived class. CLOS handles this nicely. I have a `DefMsgHandler` macro that calls RegisterClass, defines the CLOS method for each message to be handled, takes care of necessary housekeeping in ubiquitous functions such as `WM_PAINT`, and allows easy definition of function bodies to be executed for each desired type of message. Other macros are useful for defining pushbuttons, edit boxes, list views, and other Win32 controls.

Look at [Appendix B](#appendixb). Compare it to [Appendix A](#appendixa). Of course, an extended version of the header-file portion of Appendix A is used in Appendix B but not shown there. All but a little of the program in Appendix A has been reduced to a library which is invoked in Appendix B. All of the application-specific information is contained in the very small program in Appendix B.

The program in [Appendix C](#appendixc) uses these macros to define a window including radio buttons, pushbuttons, a check box, text drawn on the background window, and a listview with columns.


<a name="conclusion"></a>

### Conclusion

The example code presented in the text and in appendices A-C places an emphasis on staying in Lisp and accessing the Win32 API from there. Paul Tarvydas has code in [Appendix D](#appendixd) which demonstrates cooperation and interaction between C and Lisp. In Paul's well-documented example, a C dll is used to drive the message loop. The Lisp callback function's address is placed from within Lisp into a variable in the dll.

Lisp provides an interactive and rich programming environment. The judicious use of macros to create language extensions in Lisp concentrates application-specific information into small local areas of the overall system. This simplifies the effort of understanding the application, increases the reliability of the application, reduces maintenance time, and increases the reliability of maintenance changes. Lisp is designed to make this easy.

* * *

<a name="appendixa"></a>

## Appendix A: "Hello, Lisp" Program #1

Here is a Win32 Lisp program that opens a GUI window displaying "Hello, Lisp!". A detailed discussion of program specifics follows the listing. The program listing contains necessary lines from the header files which are `#included` in a C/C++ program.

It may be advantageous to open a separate window with the program source code visible while reading the text.

~~~lisp
{% include code/w32-appendix-a.lisp %}
~~~

![](AppendixA.jpg)

* * *

<a name="appendixb"></a>

## Appendix B: "Hello, Lisp!" Program #2

~~~lisp
{% include code/w32-appendix-b.lisp %}
~~~

![](AppendixB.jpg)

* * *

<a name="appendixc"></a>

### Appendix C: Program #3

~~~lisp
{% include code/w32-appendix-c.lisp %}
~~~

![](AppendixC.jpg)

* * *

<a name="appendixd"></a>

### Appendix D: Paul Tarvydas's Example

Here's an example that creates a windows class (in C) and gets invoked and
handled from LWW.  It is similar to the "Hello" example in Petzhold, except
that it hooks to the LWW mainloop instead of creating its own.  Probably it
ain't as pretty as it might be, due to my rustiness with Win32 (and my lack
of patience with it :-).

To use:

1) create a DevStudio Win32 DLL project called "wintest"
2) put the wintest.c file into the project
3) copy the run.lisp file into the project directory (so that the Debug
directory is a subdirectory)
4) Build the C project.
5) Set Project>>Settings>>Debug>>Executable for debug session to point
to the
lispworksxxx.exe.
6) Run the project - this should bring up lispworks.
7) Open run.lisp, compile and load it.
8) In the listener, type "(run)".
9) You should then see a window with "hello" in the middle of it.

The example window class is built and initialized in C (called from the lisp
mainline).  The windows callbacks to this window are handled in lisp (eg. the
WM_PAINT message) - windows has been given a pointer to a lisp function
(Lisp_WndProc) and has been told to use it for callbacks.  The lisp code
makes direct Win32 calls that display the "hello" text.  Lisp uses FLI
foreign functions and foreign variables to set this up.  [If one were doing
this on a real project, a less contrived flow of control would be chosen, but
this one appears to exercise the FLI calls that you were asking about].

[I welcome comments from anyone, re. style, simplification, etc.]

Paul Tarvydas
tarvydas at spamoff-attcanada dotca

~~~c
{% include code/w32-appendix-d.c %}
~~~

~~~lisp
{% include code/w32-appendix-d.lisp %}
~~~

---
title: Debugging
---

You entered this new world of Lisp and now wonder: how can we debug
what's going on ?  How is it more interactive than in other platforms
?  What does bring the interactive debugger apart from stacktraces ?

## Print debugging

Well of course we can use the famous technique of "print
debugging". Let's just recap a few print functions.

`print` works, it prints a READable representation of its argument,
which means what is `print`ed can be `read` back in by the Lisp
reader.

`princ` focuses on an *aesthetic* representation.

`format t "~a" …)`, with the *aesthetic* directive, prints a string (in `t`, the standard output
stream) and returns nil, whereas `format nil …` doesn't print anything
and returns a string. With many format controls we can print several
variables at once.

## Logging

Logging is a good evolution from print debugging ;)

[log4cl](https://github.com/sharplispers/log4cl/) is the popular,
de-facto logging library but it isn't the only one. Download it:

~~~lisp
(ql:quickload :log4cl)
~~~

and let's have a dummy variable:

~~~lisp
(defvar *foo* '(:a :b :c))
~~~

We can use log4cl with its `log` nickname, then it is as simple to use as:

~~~lisp
(log:info *foo*)
;; <INFO> [13:36:49] cl-user () - *FOO*: (:A :B :C)
~~~

We can interleave strings and expressions, with or without `format`
control strings:

~~~lisp
(log:info "foo is " *foo*)
;; <INFO> [13:37:22] cl-user () - foo is *FOO*: (:A :B :C)
(log:info "foo is ~{~a~}" *foo*)
;; <INFO> [13:39:05] cl-user () - foo is ABC
~~~

With its companion library `log4slime`, we can interactively change
the log level:

- globally,
- per package,
- per function,
- and by CLOS methods and CLOS hierarchy (before and after methods).

It is very handy, when we have a lot of output, to turn off the
logging of functions or packages we know to work, and thus narrowing
our search to the right area. We can even save this configuration and
re-use it in another image, be it on another machine.

We can do all this through commands, keyboard shortcuts and also through a
menu or mouse clicks.

!["changing the log level with log4slime"](https://github.com/sharplispers/log4cl/raw/master/images/screenshot-15.png)

We invite you to read log4cl's readme.

## Using the powerful REPL

Part of the joy of Lisp is the excellent REPL. Its existence usually
delays the need to use other debugging tools, if it doesn't annihilate
them for the usual routine.

As soon as we define a function, we can try it in the REPL. In Slime,
compile a function with `C-c C-c` (the whole buffer with `C-c C-k`),
switch to the REPL with `C-c C-z` and try it. Eventually enter the
package you are working on with `(in-package :your-package)`.

The feedback is immediate. There is no need to recompile everything,
nor to restart any process, nor to create a main function and define
command line arguments for use in the shell (we can do this later on
when needed).

We usually need to create some data to test our function(s). This is a
subsequent art of the REPL existence and it may be a new discipline
for newcomers. A trick is to write the test data alongside your
functions but inside a `#+nil` declaration so that only you can
manually compile them:

~~~lisp
#+nil
(progn
   (defvar *test-data* nil)
   (setf *test-data* (make-instance 'foo …)))
~~~

When you load this file, `*test-data*` won't exist, but you can
manually create it with a `C-c C-c` away.

We can define tests functions like this.

Some do similarly inside `#| … |#` comments.

All that being said, keep in mind to write unit tests when time comes ;)


## Inspect and describe

These two commands share the same goal, printing a description of an
object, `inspect` being the interactive one.

~~~
(inspect *foo*)

The object is a proper list of length 3.
0. 0: :A
1. 1: :B

2. 2: :C
> q
~~~

We can also, in editors that support it, right-click on any object in
the REPL and `inspect` them. We are presented a screen where we can
dive deep inside the data structure and even change it.

Let's have a quick look with a more interesting structure, an object:

~~~lisp
(defclass foo ()
    ((a :accessor foo-a :initform '(:a :b :c))
     (b :accessor foo-b :initform :b)))
;; #<STANDARD-CLASS FOO>
(make-instance 'foo)
;; #<FOO {100F2B6183}>
~~~

We right-click on the `#<FOO` object and choose "inspect". We are
presented an interactive pane (in Slime):

```
#<FOO {100F2B6183}>
--------------------
Class: #<STANDARD-CLASS FOO>
--------------------
 Group slots by inheritance [ ]
 Sort slots alphabetically  [X]

All Slots:
[ ]  A = (:A :B :C)
[ ]  B = :B

[set value]  [make unbound]
```

When we click or press enter on the line of slot A, we inspect it further:

```
#<CONS {100F5E2A07}>
--------------------
A proper list:
0: :A
1: :B
2: :C
```

## The interactive debugger

Whenever an exceptional situation happens (see
[error handling](error_handling.html)), the interactive debugger pops
up.

It presents the error message, available actions (*restarts*),
and the backtrace. A few remarks:

- the restarts are programmable, we can create our owns,
- in Slime, press `v` on a stacktrace to be redirected to the source
  file at the right line,
- hit enter on a frame for more details,
- we can explore the functionality with the menu that should appear
  in our editor. See the "break" section below for a few
  more commands (eval in frame, etc).

Usually your compiler will optimize things out and this will reduce
the amount of information available to the debugger. For example
sometimes we can't see intermediate variables of computations. We can
change the optimization choices with:

~~~lisp
(declaim (optimize (speed 0) (space 0) (debug 3)))
~~~

and recompile our code.


## Trace

[trace](http://www.xach.com/clhs?q=trace) allows us to see when a
function was called, what arguments it received, and the value it
returned.

~~~lisp
(defun factorial (n)
  (if (plusp n)
    (* n (factorial (1- n)))
    1))
~~~

~~~lisp
(trace factorial)

(factorial 2)
  0: (FACTORIAL 3)
    1: (FACTORIAL 2)
      2: (FACTORIAL 1)
        3: (FACTORIAL 0)
        3: FACTORIAL returned 1
      2: FACTORIAL returned 1
    1: FACTORIAL returned 2
  0: FACTORIAL returned 6
6

(untrace factorial)
~~~

To untrace all functions, just evaluate `(untrace)`.

In Slime we also have the shortcut `C-c M-t` to trace or untrace a
function.

If you don't see recursive calls, that may be because of the
compiler's optimizations. Try this before defining the function to be
traced:

~~~lisp
(declaim (optimize (debug 3)))
~~~

The output is printed to `*trace-output*` (see the CLHS).

In Slime, we also have an interactive trace dialog with ``M-x
slime-trace-dialog`` bound to `C-c T`.


### Tracing method invocation

In SBCL, we can use `(trace foo :methods t)` to trace the execution order of method combination (before, after, around methods). For example:

~~~lisp
(trace foo :methods t)

(foo 2.0d0)
  0: (FOO 2.0d0)
    1: ((SB-PCL::COMBINED-METHOD FOO) 2.0d0)
      2: ((METHOD FOO (FLOAT)) 2.0d0)
        3: ((METHOD FOO (T)) 2.0d0)
        3: (METHOD FOO (T)) returned 3
      2: (METHOD FOO (FLOAT)) returned 9
      2: ((METHOD FOO :AFTER (DOUBLE-FLOAT)) 2.0d0)
      2: (METHOD FOO :AFTER (DOUBLE-FLOAT)) returned DOUBLE
    1: (SB-PCL::COMBINED-METHOD FOO) returned 9
  0: FOO returned 9
9
~~~

See the [CLOS](clos.html) section for a tad more information.

## Step

[step](http://www.xach.com/clhs?q=step) is an interactive command with
similar scope than `trace`. This:

~~~lisp
(step (factorial 2))
~~~

gives an interactive pane with the available restarts:

```
Evaluating call:
  (FACTORIAL 2)
With arguments:
  2
   [Condition of type SB-EXT:STEP-FORM-CONDITION]

Restarts:
 0: [STEP-CONTINUE] Resume normal execution
 1: [STEP-OUT] Resume stepping after returning from this function
 2: [STEP-NEXT] Step over call
 3: [STEP-INTO] Step into call
 4: [RETRY] Retry SLIME REPL evaluation request.
 5: [*ABORT] Return to SLIME's top level.
 --more--

Backtrace:
  0: ((LAMBDA ()))
  1: (SB-INT:SIMPLE-EVAL-IN-LEXENV (LET ((SB-IMPL::*STEP-OUT* :MAYBE)) (UNWIND-PROTECT (SB-IMPL::WITH-STEPPING-ENABLED #))) #S(SB-KERNEL:LEXENV :FUNS NIL :VARS NIL :BLOCKS NIL :TAGS NIL :TYPE-RESTRICTIONS ..
  2: (SB-INT:SIMPLE-EVAL-IN-LEXENV (STEP (FACTORIAL 2)) #<NULL-LEXENV>)
  3: (EVAL (STEP (FACTORIAL 2)))
```

Stepping is useful, however it may be a sign that you need to simplify your function.

## Break

A call to [break](http://www.xach.com/clhs?q=break) makes the program
enter the debugger, from which we can inspect the call stack.


### Breakpoints in Slime

Look at the `SLDB` menu, it shows navigation keys and available
actions. Of which:

- `e` (*sldb-eval-in-frame*) prompts for an expression and evaluates
  it in the selected frame. This is how we can explore our
  intermediate variables.
- `d` is similar with the addition of pretty printing the result.

Once we are in a frame and detect a suspicious behavior, we can even
re-compile a function at runtime and resume the program execution from
where it stopped (using the "step-continue" restart).


## Advise and watch

*advise* and
[watch](http://www.xach.com/clhs?q=watch) are available in some
implementations, like CCL
([advise](https://ccl.clozure.com/manual/chapter4.3.html#Advising) and
[watch](https://ccl.clozure.com/manual/chapter4.12.html#watched-objects))
and [LispWorks](http://www.lispworks.com/). They do exist in
SBCL but are not exported. `advise` allows to modify a function without changing its
source, or to do something before or after its execution, like CLOS'
method combination (before, after around methods).

`watch` will signal a condition when a thread attempts to write to an
object being watched. It can be coupled with the display of the
watched objects in a GUI.

There is a [cl-advice](https://bitbucket.org/budden/budden-tools/src/default/cl-advice/?at=default) non-published library defining a portability layer.

## Unit tests

Last but not least, automatic testing of functions in isolation might
be what you're looking for ! See the [testing](testing.html) section and a list of
[test frameworks and libraries](https://github.com/CodyReichert/awesome-cl#unit-testing).


## Remote debugging

Here's how to debug a running application on another machine.

The steps involved are to start a Swank server on the remote machine, create an
ssh tunnel, and connect to the Swank server from our editor. Then we
can browse and evaluate code of the running instance transparently.

Let's define a function that prints forever.

If needed, import the dependencies first:

~~~lisp
(ql:quickload '(:swank :bordeaux-threads))
~~~


~~~lisp
;; a little common lisp swank demo
;; while this program is running, you can connect to it from another terminal or machine
;; and change the definition of doprint to print something else out!

(require :swank)
(require :bordeaux-threads)

(defparameter *counter* 0)

(defun dostuff ()
  (format t "hello world ~a!~%" *counter*))

(defun runner ()
  (bt:make-thread (lambda ()
                    (swank:create-server :port 4006)))
  (format t "we are past go!~%")
  (loop while t do
       (sleep 5)
       (dostuff)
       (incf *counter*)))

(runner)
~~~

On the server, we can run it with

    sbcl --load demo.lisp

we do port forwarding on our development machine:

    ssh -L4006:127.0.0.1:4006 username@example.com

this will securely forward port 4006 on the server at example.com to
our local computer's port 4006 (swanks only accepts connections from
localhost).

We connect to the running swank with `M-x slime-connect`, typing in
port 4006.

We can write new code:

~~~lisp
(defun dostuff ()
  (format t "goodbye world ~a!~%" *counter*))
(setf *counter* 0)
~~~

and eval it as usual with `C-c C-c` or `M-x slime-eval-region` for instance. The output should change.

That's how Ron Garret debugged the Deep Space 1 spacecraft from the earth
in 1999:

> we were able to debug and fix a race condition that had not shown up during ground testing. (Debugging a program running on a $100M piece of hardware that is 100 million miles away is an interesting experience. Having a read-eval-print loop running on the spacecraft proved invaluable in finding and fixing the problem.


## References

- ["How to understand and use Common Lisp"](https://successful-lisp.blogspot.com/p/httpsdrive.html), chap. 30, David Lamkins (book download from author's site)
- [Malisper: debugging Lisp series](https://malisper.me/debugging-lisp-part-1-recompilation/)
- [Two Wrongs: debugging Common Lisp in Slime](https://two-wrongs.com/debugging-common-lisp-in-slime.html)
- [Slime documentation: connecting to a remote Lisp](https://common-lisp.net/project/slime/doc/html/Connecting-to-a-remote-lisp.html#Connecting-to-a-remote-lisp)
- [cvberrycom: remotely modifying a running Lisp program using Swank](http://cvberry.com/tech_writings/howtos/remotely_modifying_a_running_program_using_swank.html)
- [Ron Garret: Lisping at the JPL](http://www.flownet.com/gat/jpl-lisp.html#1994-1999%20-%20Remote%20Agent)
- [the Remote Agent experiment: debugging code from 60 million miles away (youtube)](https://www.youtube.com/watch?v=_gZK0tW8EhQ&feature=youtu.be&t=4175) (["AMA" on reddit](https://www.reddit.com/r/lisp/comments/a7156w/lisp_and_the_remote_agent/))

---
title: Performance Tuning and Tips
---

Many Common Lisp implementations translate the source code into assembly
language, so the performance is really good compared with some other
interpreted languages.

However, sometimes we just want the program to be faster. This chapter
introduces some techniques to squeeze the CPU power out.

## Finding Bottlenecks

### Acquiring Execution Time

The macro [`time`][time] is very useful for finding out bottlenecks. It takes
a form, evaluates it and prints timing information in
[`*trace-output*`][trace-output], as shown below:

~~~lisp
* (defun collect (start end)
    "Collect numbers [start, end] as list."
    (loop for i from start to end
          collect i))

* (time (collect 1 10))

Evaluation took:
  0.000 seconds of real time
  0.000001 seconds of total run time (0.000001 user, 0.000000 system)
  100.00% CPU
  3,800 processor cycles
  0 bytes consed
~~~

By using `time` macro it is fairly easy to find out which part of your program
takes too much time.

Please note that the timing information provided here is not guaranteed to be
reliable enough for marketing comparisons. It should only be used for tuning
purpose, as demonstrated in this chapter.

### Checking Assembly Code

The function [`disassemble`][disassemble] takes a function and prints the
compiled code of it to `*standard-output*`. For example:

~~~lisp
* (defun plus (a b)
    (+ a b))
PLUS

* (disassemble 'plus)
; disassembly for PLUS
; Size: 37 bytes. Origin: #x52B8063B
; 3B:       498B5D60         MOV RBX, [R13+96]                ; no-arg-parsing entry point
                                                              ; thread.binding-stack-pointer
; 3F:       48895DF8         MOV [RBP-8], RBX
; 43:       498BD0           MOV RDX, R8
; 46:       488BFE           MOV RDI, RSI
; 49:       FF1425B0001052   CALL QWORD PTR [#x521000B0]      ; GENERIC-+
; 50:       488B75E8         MOV RSI, [RBP-24]
; 54:       4C8B45F0         MOV R8, [RBP-16]
; 58:       488BE5           MOV RSP, RBP
; 5B:       F8               CLC
; 5C:       5D               POP RBP
; 5D:       C3               RET
; 5E:       CC0F             BREAK 15                         ; Invalid argument count trap
~~~

The code above was evaluated in SBCL. In some other implementations such as
CLISP, `disassembly` might return something different:

~~~lisp
* (defun plus (a b)
    (+ a b))
PLUS

* (disassemble 'plus)
Disassembly of function PLUS
2 required arguments
0 optional arguments
No rest parameter
No keyword parameters
4 byte-code instructions:
0     (LOAD&PUSH 2)
1     (LOAD&PUSH 2)
2     (CALLSR 2 55)                       ; +
5     (SKIP&RET 3)
NIL
~~~

It is because SBCL compiles the Lisp code into machine code, while CLISP does
not.

## Using Declare Expression

The [*declare expression*][declare] can be used to provide hints for compilers
to perform various optimization. Please note that these hints are
implementation-dependent. Some implementations such as SBCL support this
feature, and you may refer to their own documentation for detailed
information. Here only some basic techniques mentioned in CLHS are introduced.

In general, declare expressions can occur only at the beginning of the bodies
of certain forms, or immediately after a documentation string if the context
allows. Also, the content of a declare expression is restricted to limited
forms. Here we introduce some of them that are related to performance tuning.

Please keep in mind that these optimization skills introduced in this section
are strongly connected to the Lisp implementation selected. Always check their
documentation before using `declare`!

### Speed and Safety

Lisp allows you to specify several quality properties for the compiler using
the declaration [`optimize`][optimize]. Each quality may be assigned a value
from 0 to 3, with 0 being "totally unimportant" and 3 being "extremely
important".

The most significant qualities might be `safety` and `speed`.

By default, Lisp considers code safety to be much more important than
speed. But you may adjust the weight for more aggressive optimization.

~~~lisp
* (defun max-original (a b)
    (max a b))
MAX-ORIGINAL

* (disassemble 'max-original)
; disassembly for MAX-ORIGINAL
; Size: 144 bytes. Origin: #x52D450EF
; 7A7:       8D46F1           lea eax, [rsi-15]               ; no-arg-parsing entry point
; 7AA:       A801             test al, 1
; 7AC:       750E             jne L0
; 7AE:       3C0A             cmp al, 10
; 7B0:       740A             jeq L0
; 7B2:       A80F             test al, 15
; 7B4:       7576             jne L5
; 7B6:       807EF11D         cmp byte ptr [rsi-15], 29
; 7BA:       7770             jnbe L5
; 7BC: L0:   8D43F1           lea eax, [rbx-15]
; 7BF:       A801             test al, 1
; 7C1:       750E             jne L1
; 7C3:       3C0A             cmp al, 10
; 7C5:       740A             jeq L1
; 7C7:       A80F             test al, 15
; 7C9:       755A             jne L4
; 7CB:       807BF11D         cmp byte ptr [rbx-15], 29
; 7CF:       7754             jnbe L4
; 7D1: L1:   488BD3           mov rdx, rbx
; 7D4:       488BFE           mov rdi, rsi
; 7D7:       B9C1030020       mov ecx, 536871873              ; generic->
; 7DC:       FFD1             call rcx
; 7DE:       488B75F0         mov rsi, [rbp-16]
; 7E2:       488B5DF8         mov rbx, [rbp-8]
; 7E6:       7E09             jle L3
; 7E8:       488BD3           mov rdx, rbx
; 7EB: L2:   488BE5           mov rsp, rbp
; 7EE:       F8               clc
; 7EF:       5D               pop rbp
; 7F0:       C3               ret
; 7F1: L3:   4C8BCB           mov r9, rbx
; 7F4:       4C894DE8         mov [rbp-24], r9
; 7F8:       4C8BC6           mov r8, rsi
; 7FB:       4C8945E0         mov [rbp-32], r8
; 7FF:       488BD3           mov rdx, rbx
; 802:       488BFE           mov rdi, rsi
; 805:       B929040020       mov ecx, 536871977              ; generic-=
; 80A:       FFD1             call rcx
; 80C:       4C8B45E0         mov r8, [rbp-32]
; 810:       4C8B4DE8         mov r9, [rbp-24]
; 814:       488B75F0         mov rsi, [rbp-16]
; 818:       488B5DF8         mov rbx, [rbp-8]
; 81C:       498BD0           mov rdx, r8
; 81F:       490F44D1         cmoveq rdx, r9
; 823:       EBC6             jmp L2
; 825: L4:   CC0A             break 10                        ; error trap
; 827:       04               byte #X04
; 828:       13               byte #X13                       ; OBJECT-NOT-REAL-ERROR
; 829:       FE9B01           byte #XFE, #X9B, #X01           ; RBX
; 82C: L5:   CC0A             break 10                        ; error trap
; 82E:       04               byte #X04
; 82F:       13               byte #X13                       ; OBJECT-NOT-REAL-ERROR
; 830:       FE1B03           byte #XFE, #X1B, #X03           ; RSI
; 833:       CC0A             break 10                        ; error trap
; 835:       02               byte #X02
; 836:       19               byte #X19                       ; INVALID-ARG-COUNT-ERROR
; 837:       9A               byte #X9A                       ; RCX

* (defun max-with-speed-3 (a b)
    (declare (optimize (speed 3) (safety 0)))
    (max a b))
MAX-WITH-SPEED-3

* (disassemble 'max-with-speed-3)
; disassembly for MAX-WITH-SPEED-3
; Size: 92 bytes. Origin: #x52D452C3
; 3B:       48895DE0         mov [rbp-32], rbx                ; no-arg-parsing entry point
; 3F:       488945E8         mov [rbp-24], rax
; 43:       488BD0           mov rdx, rax
; 46:       488BFB           mov rdi, rbx
; 49:       B9C1030020       mov ecx, 536871873               ; generic->
; 4E:       FFD1             call rcx
; 50:       488B45E8         mov rax, [rbp-24]
; 54:       488B5DE0         mov rbx, [rbp-32]
; 58:       7E0C             jle L1
; 5A:       4C8BC0           mov r8, rax
; 5D: L0:   498BD0           mov rdx, r8
; 60:       488BE5           mov rsp, rbp
; 63:       F8               clc
; 64:       5D               pop rbp
; 65:       C3               ret
; 66: L1:   488945E8         mov [rbp-24], rax
; 6A:       488BF0           mov rsi, rax
; 6D:       488975F0         mov [rbp-16], rsi
; 71:       4C8BC3           mov r8, rbx
; 74:       4C8945F8         mov [rbp-8], r8
; 78:       488BD0           mov rdx, rax
; 7B:       488BFB           mov rdi, rbx
; 7E:       B929040020       mov ecx, 536871977               ; generic-=
; 83:       FFD1             call rcx
; 85:       488B45E8         mov rax, [rbp-24]
; 89:       488B75F0         mov rsi, [rbp-16]
; 8D:       4C8B45F8         mov r8, [rbp-8]
; 91:       4C0F44C6         cmoveq r8, rsi
; 95:       EBC6             jmp L0
~~~

As you can see, the generated assembly code is much shorter (92 bytes
VS 144).  The compiler was able to perform optimizations. Yet we
can do better by declaring types.


### Type Hints

As mentioned in the [*Type System*][sec:type] chapter, Lisp has a relatively
powerful type system. You may provide type hints so that the compiler may
reduce the size of the generated code.

~~~lisp
* (defun max-with-type (a b)
    (declare (optimize (speed 3) (safety 0)))
    (declare (type integer a b))
    (max a b))
MAX-WITH-TYPE

* (disassemble 'max-with-type)
; disassembly for MAX-WITH-TYPE
; Size: 42 bytes. Origin: #x52D48A23
; 1B:       488BF7           mov rsi, rdi                     ; no-arg-parsing entry point
; 1E:       488975F0         mov [rbp-16], rsi
; 22:       488BD8           mov rbx, rax
; 25:       48895DF8         mov [rbp-8], rbx
; 29:       488BD0           mov rdx, rax
; 2C:       B98C030020       mov ecx, 536871820               ; generic-<
; 31:       FFD1             call rcx
; 33:       488B75F0         mov rsi, [rbp-16]
; 37:       488B5DF8         mov rbx, [rbp-8]
; 3B:       480F4CDE         cmovl rbx, rsi
; 3F:       488BD3           mov rdx, rbx
; 42:       488BE5           mov rsp, rbp
; 45:       F8               clc
; 46:       5D               pop rbp
; 47:       C3               ret
~~~

The size of generated assembly code shrunk to about 1/3 of the size. What
about speed?

~~~lisp
* (time (dotimes (i 10000) (max-original 100 200)))
Evaluation took:
  0.000 seconds of real time
  0.000107 seconds of total run time (0.000088 user, 0.000019 system)
  100.00% CPU
  361,088 processor cycles
  0 bytes consed

* (time (dotimes (i 10000) (max-with-type 100 200)))
Evaluation took:
  0.000 seconds of real time
  0.000044 seconds of total run time (0.000036 user, 0.000008 system)
  100.00% CPU
  146,960 processor cycles
  0 bytes consed
~~~

You see, by specifying type hints, our code runs much faster!

But wait...What happens if we declare wrong types? The answer is: it depends.

For example, SBCL treats type declarations in a [special way][sbcl-type]. It
performs different levels of type checking according to the safety level. If
safety level is set to 0, no type checking will be performed. Thus a wrong
type specifier might cause a lot of damage.

### More on Type Declaration with `declaim`

If you try to evaluate a `declare` form in the top level, you might get the
following error:

~~~lisp
Execution of a form compiled with errors.
Form:
  (DECLARE (SPEED 3))
Compile-time error:
  There is no function named DECLARE.  References to DECLARE in some contexts
(like starts of blocks) are unevaluated expressions, but here the expression is
being evaluated, which invokes undefined behaviour.
   [Condition of type SB-INT:COMPILED-PROGRAM-ERROR]
~~~

This is because type declarations have [scopes][declare-scope]. In the
examples above, we have seen type declarations applied to a function.

During development it is usually useful to raise the importance of safety in
order to find out potential problems as soon as possible. On the contrary,
speed might be more important after deployment. However, it might be too
verbose to specify declaration expression for each single function.

The macro [`declaim`][declaim] provides such possibility. It can be used as a
top level form in a file and the declarations will be made at compile-time.

~~~lisp
* (declaim (optimize (speed 0) (safety 3)))
NIL

* (defun max-original (a b)
    (max a b))
MAX-ORIGINAL

* (disassemble 'max-original)
; disassembly for MAX-ORIGINAL
; Size: 181 bytes. Origin: #x52D47D9C
...

* (declaim (optimize (speed 3) (safety 3)))
NIL

* (defun max-original (a b)
    (max a b))
MAX-ORIGINAL

* (disassemble 'max-original)
; disassembly for MAX-ORIGINAL
; Size: 142 bytes. Origin: #x52D4815D
~~~

Please note that `declaim` works in **compile-time** of a file. It is mostly
used to make some declares local to that file. And it is unspecified whether
or not the compile-time side-effects of a declaim persist after the file has
been compiled.


### Declaring function types
Another useful declaration is a `ftype` declaration which establishes
the relationship between the function argument types and the return value type.
If the type of passed arguments matches the declared types, the return value type
is expected to match the declared one. Because of that, a function can have more
than one `ftype` declaration associated with it. A `ftype` declaration restricts
the type of the argument every time the function is called. It has the following form:

~~~lisp
 (declare (ftype (function (arg1 arg2 ...) return-value) function-name1))
~~~~

If the function returns `nil`, its return type is `null`.
This declaration does not put any restriction on the types of arguments by itself.
It only takes effect if the provided arguments have the specified types -- otherwise
no error is signaled and declaration has no effect. For example,
the following declamation states that if the argument to the function `square`
is a `fixnum`, the value of the function will also be a `fixnum`:

~~~lisp
(declaim (ftype (function (fixnum) fixnum) square))
(defun square (x) (* x x))
~~~~
If we provide it with the argument which is not declared to be of type `fixnum`,
no optimization will take place:

~~~lisp
(defun do-some-arithmetic (x)
  (the fixnum (+ x (square x))))
~~~~

Now let's try to optimize the speed. The compiler will state that there is type uncertainty:

~~~lisp
(defun do-some-arithmetic (x)
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (the fixnum (+ x (square x))))

; compiling (DEFUN DO-SOME-ARITHMETIC ...)

; file: /tmp/slimeRzDh1R
 in: DEFUN DO-SOME-ARITHMETIC
;     (+ TEST-FRAMEWORK::X (TEST-FRAMEWORK::SQUARE TEST-FRAMEWORK::X))
;
; note: forced to do GENERIC-+ (cost 10)
;       unable to do inline fixnum arithmetic (cost 2) because:
;       The first argument is a NUMBER, not a FIXNUM.
;       unable to do inline (signed-byte 64) arithmetic (cost 5) because:
;       The first argument is a NUMBER, not a (SIGNED-BYTE 64).
;       etc.
;
; compilation unit finished
;   printed 1 note


      (disassemble 'do-some-arithmetic)
; disassembly for DO-SOME-ARITHMETIC
; Size: 53 bytes. Origin: #x52CD1D1A
; 1A:       488945F8         MOV [RBP-8], RAX                 ; no-arg-parsing entry point
; 1E:       488BD0           MOV RDX, RAX
; 21:       4883EC10         SUB RSP, 16
; 25:       B902000000       MOV ECX, 2
; 2A:       48892C24         MOV [RSP], RBP
; 2E:       488BEC           MOV RBP, RSP
; 31:       E8C2737CFD       CALL #x504990F8                  ; #<FDEFN SQUARE>
; 36:       480F42E3         CMOVB RSP, RBX
; 3A:       488B45F8         MOV RAX, [RBP-8]
; 3E:       488BFA           MOV RDI, RDX
; 41:       488BD0           MOV RDX, RAX
; 44:       E807EE42FF       CALL #x52100B50                  ; GENERIC-+
; 49:       488BE5           MOV RSP, RBP
; 4C:       F8               CLC
; 4D:       5D               POP RBP
; 4E:       C3               RET
NIL
~~~~


Now we can add a type declaration for `x`, so the compiler can assume
that the expression `(square x)` is a `fixnum`, and use the fixnum-specific `+`:

~~~lisp
(defun do-some-arithmetic (x)
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (declare (type fixnum x))
  (the fixnum (+ x (square x))))

       (disassemble 'do-some-arithmetic)

; disassembly for DO-SOME-ARITHMETIC
; Size: 48 bytes. Origin: #x52C084DA
; 4DA:       488945F8         MOV [RBP-8], RAX                ; no-arg-parsing entry point
; 4DE:       4883EC10         SUB RSP, 16
; 4E2:       488BD0           MOV RDX, RAX
; 4E5:       B902000000       MOV ECX, 2
; 4EA:       48892C24         MOV [RSP], RBP
; 4EE:       488BEC           MOV RBP, RSP
; 4F1:       E8020C89FD       CALL #x504990F8                 ; #<FDEFN SQUARE>
; 4F6:       480F42E3         CMOVB RSP, RBX
; 4FA:       488B45F8         MOV RAX, [RBP-8]
; 4FE:       4801D0           ADD RAX, RDX
; 501:       488BD0           MOV RDX, RAX
; 504:       488BE5           MOV RSP, RBP
; 507:       F8               CLC
; 508:       5D               POP RBP
; 509:       C3               RET
NIL
~~~~

### Code Inline

The declaration [`inline`][inline] replaces function calls with function body,
if the compiler supports it. It will save the cost of function calls but will
potentially increase the code size. The best situation to use `inline` might
be those small but frequently used functions. The following snippet shows how
to encourage and prohibit code inline.

~~~lisp
;; The globally defined function DISPATCH should be open-coded,
;; if the implementation supports inlining, unless a NOTINLINE
;; declaration overrides this effect.
(declaim (inline dispatch))
(defun dispatch (x) (funcall (get (car x) 'dispatch) x))

;; Here is an example where inlining would be encouraged.
;; Because function DISPATCH was defined as INLINE, the code
;; inlining will be encouraged by default.
(defun use-dispatch-inline-by-default ()
  (dispatch (read-command)))

;; Here is an example where inlining would be prohibited.
;; The NOTINLINE here only affects this function.
(defun use-dispatch-with-declare-notinline  ()
  (declare (notinline dispatch))
  (dispatch (read-command)))

;; Here is an example where inlining would be prohibited.
;; The NOTINLINE here affects all following code.
(declaim (notinline dispatch))
(defun use-dispatch-with-declaim-noinline ()
  (dispatch (read-command)))

;; Inlining would be encouraged because you specified it.
;; The INLINE here only affects this function.
(defun use-dispatch-with-inline ()
  (declare (inline dispatch))
  (dispatch (read-command)))
~~~

Please note that when the inlined functions change, all the callers must be
re-compiled.

## Optimizing Generic Functions

### Using Static Dispatch

Generic functions provide much convenience and flexibility during
development. However, the flexibility comes with cost: generic methods are
much slower than trivial functions. The performance cost becomes a burden
especially when the flexibility is not needed.

The package [`inlined-generic-function`][inlined-generic-function] provides
functions to convert generic functions to static dispatch, moving the dispatch
cost to compile-time. You just need to define generic function as a
`inlined-generic-function`.

**Caution**

This package is declared as experimental thus is not recommended to be used in
a serious software production. Use it at your own risk!

~~~lisp
* (defgeneric plus (a b)
    (:generic-function-class inlined-generic-function))
#<INLINED-GENERIC-FUNCTION HELLO::PLUS (2)>

* (defmethod plus ((a fixnum) (b fixnum))
    (+ a b))
#<INLINED-METHOD HELLO::PLUS (FIXNUM FIXNUM) {10056D7513}>

* (defun func-using-plus (a b)
    (plus a b))
FUNC-USING-PLUS

* (defun func-using-plus-inline (a b)
    (declare (inline plus))
    (plus a b))
FUNC-USING-PLUS-INLINE

* (time
   (dotimes (i 100000)
     (func-using-plus 100 200)))
Evaluation took:
  0.018 seconds of real time
  0.017819 seconds of total run time (0.017800 user, 0.000019 system)
  100.00% CPU
  3 lambdas converted
  71,132,440 processor cycles
  6,586,240 bytes consed

* (time
   (dotimes (i 100000)
     (func-using-plus-inline 100 200)))
Evaluation took:
  0.001 seconds of real time
  0.000326 seconds of total run time (0.000326 user, 0.000000 system)
  0.00% CPU
  1,301,040 processor cycles
  0 bytes consed
~~~

The inlining is not enabled by default because once inlined, changes made to
methods will not be reflected.

It can be enabled globally by adding `:inline-generic-function` flag in
[`*features*`][*features*].

~~~lisp
* (push :inline-generic-function *features*)
(:INLINE-GENERIC-FUNCTION :SLYNK :CLOSER-MOP :CL-FAD :BORDEAUX-THREADS
:THREAD-SUPPORT :CL-PPCRE ALEXANDRIA.0.DEV::SEQUENCE-EMPTYP :QUICKLISP
:QUICKLISP-SUPPORT-HTTPS :SB-BSD-SOCKETS-ADDRINFO :ASDF3.3 :ASDF3.2 :ASDF3.1
:ASDF3 :ASDF2 :ASDF :OS-UNIX :NON-BASE-CHARS-EXIST-P :ASDF-UNICODE :ROS.INIT
:X86-64 :64-BIT :64-BIT-REGISTERS :ALIEN-CALLBACKS :ANSI-CL :AVX2
:C-STACK-IS-CONTROL-STACK :CALL-SYMBOL :COMMON-LISP :COMPACT-INSTANCE-HEADER
:COMPARE-AND-SWAP-VOPS :CYCLE-COUNTER :ELF :FP-AND-PC-STANDARD-SAVE ..)
~~~

When this feature is present, all inlinable generic functions are inlined
unless it is declared `notinline`.

[time]: http://www.lispworks.com/documentation/lw51/CLHS/Body/m_time.htm
[trace-output]: http://www.lispworks.com/documentation/lw71/CLHS/Body/v_debug_.htm#STtrace-outputST
[disassemble]: http://www.lispworks.com/documentation/lw60/CLHS/Body/f_disass.htm
[inlined-generic-function]: https://github.com/guicho271828/inlined-generic-function
[sec:type]: #type
[declare]: http://www.lispworks.com/documentation/lw71/CLHS/Body/s_declar.htm
[declare-scope]: http://www.lispworks.com/documentation/lw71/CLHS/Body/03_cd.htm
[optimize]: http://www.lispworks.com/documentation/lw71/CLHS/Body/d_optimi.htm
[sbcl-type]: http://sbcl.org/manual/index.html#Handling-of-Types
[declaim]: http://www.lispworks.com/documentation/lw71/CLHS/Body/m_declai.htm
[inline]: http://www.lispworks.com/documentation/lw51/CLHS/Body/d_inline.htm
[*features*]: http://www.lispworks.com/documentation/lw71/CLHS/Body/v_featur.htm

---
title: Scripting. Command line arguments. Executables.
---

Using a program from a REPL is fine and well, but if we want to
distribute our program easily, we'll want to build an executable.

Lisp implementations differ in their processes, but they all create
**self-contained executables**, for the architecture they are built on. The
final user doesn't need to install a Lisp implementation, he can run
the software right away.

**Start-up times** are near to zero, specially with SBCL and CCL.

Binaries **size** are large-ish. They include the whole Lisp
including its libraries, the names of all symbols, information about
argument lists to functions, the compiler, the debugger, source code
location information, and more.

Note that we can similarly build self-contained executables for **web apps**.


# Building a self-contained executable

## With SBCL

How to build (self-contained) executables is implementation-specific (see
below Buildapp and Rowsell). With SBCL, as says
[its documentation](http://www.sbcl.org/manual/index.html#Function-sb_002dext_003asave_002dlisp_002dand_002ddie),
it is a matter of:

~~~lisp
(sb-ext:save-lisp-and-die #P"path/name-of-executable" :toplevel #'my-app:main-function :executable t)
~~~

`sb-ext` is an SBCL extension to run external processes.  See other
[SBCL extensions](http://www.sbcl.org/manual/index.html#Extensions)
(many of them are made implementation-portable in other libraries).

`:executable  t`  tells  to  build  an  executable  instead  of  an
image. We  could build an  image to save  the state of  our current
Lisp image, to come back working with it later. Specially useful if
we made a lot of work that is computing intensive.

If you try to run this in Slime, you'll get an error about threads running:

> Cannot save core with multiple threads running.

Run the command from a simple SBCL repl.

I suppose your project has Quicklisp dependencies. You must then:

* ensure Quicklisp is installed and loaded at Lisp startup (you
  completed Quicklisp installation)
* `load` the project's .asd
* install dependencies
* build the executable.

That gives:

~~~lisp
(load "my-app.asd")
(ql:quickload :my-app)
(sb-ext:save-lisp-and-die #p"my-app-binary" :toplevel #'my-app:main :executable t)
~~~

From the command line, or from a Makefile, use `--load` and `--eval`:

```
build:
	sbcl --load my-app.asd \
	     --eval '(ql:quickload :my-app)' \
         --eval "(sb-ext:save-lisp-and-die #p\"my-app\" :toplevel #'my-app:main :executable t)"
```

## With ASDF

Now that we've seen the basics, we need a portable method. Since its
version 3.1, ASDF allows to do that. It introduces the [`make` command](https://common-lisp.net/project/asdf/asdf.html#Convenience-Functions),
that reads parameters from the .asd. Add this to your .asd declaration:

~~~
:build-operation "program-op" ;; leave as is
:build-pathname "<binary-name>"
:entry-point "<my-package:main-function>"
~~~

and call `asdf:make :my-package`.

So, in a Makefile:

~~~lisp
LISP ?= sbcl

build:
    $(LISP) --load my-app.asd \
    	--eval '(ql:quickload :my-app)' \
		--eval '(asdf:make :my-app)' \
		--eval '(quit)'
~~~


## With Roswell or Buildapp

[Roswell](https://roswell.github.io), an implementation manager and much
more, also has the `ros build` command, that should work for many
implementations.

We can also make our app installable with Roswell by a `ros install
my-app`. See its documentation.

We'll finish with a word on
[Buildapp](http://www.xach.com/lisp/buildapp/), a battle-tested and
still popular "application for SBCL or CCL that configures and saves
an executable Common Lisp image".

Example usage:

~~~lisp
buildapp --output myapp \
         --asdf-path . \
         --asdf-tree ~/quicklisp/dists \
         --load-system my-app \
         --entry my-app:main
~~~

Many applications use it (for example,
[pgloader](https://github.com/dimitri/pgloader)),  it is available on
Debian: `apt install buildapp`, but you shouldn't need it now with asdf:make or Roswell.


## For web apps

We can similarly build a self-contained executable for our web-app. It
would thus contain a web server and would be able to run on the
command line:

    $ ./my-web-app
    Hunchentoot server is started.
    Listening on localhost:9003.

Note that this runs the production webserver, not a development one,
so we can run the binary on our VPS right away and access the app from
outside.

We have one thing to take care of, it is to find and put the thread of
the running web server on the foreground. In our `main` function, we
can do something like this:

~~~lisp
(defun main ()
  (start-app :port 9003) ;; our start-app, for example clack:clack-up
  ;; let the webserver run.
  ;; warning: hardcoded "hunchentoot".
  (handler-case (bt:join-thread (find-if (lambda (th)
                                            (search "hunchentoot" (bt:thread-name th)))
                                         (bt:all-threads)))
    ;; Catch a user's C-c
    (#+sbcl sb-sys:interactive-interrupt
      #+ccl  ccl:interrupt-signal-condition
      #+clisp system::simple-interrupt-condition
      #+ecl ext:interactive-interrupt
      #+allegro excl:interrupt-signal
      () (progn
           (format *error-output* "Aborting.~&")
           (clack:stop *server*)
           (uiop:quit)))
    (error (c) (format t "Woops, an unknown error occured:~&~a~&" c))))
~~~

We used the `bordeaux-threads` library (`(ql:quickload
"bordeaux-threads")`, alias `bt`) and `uiop`, which is part of ASDF so
already loaded, in order to exit in a portable way (`uiop:quit`, with
an optional return code, instead of `sb-ext:quit`).


## Size and startup times of executables per implementation

SBCL isn't the only Lisp implementation.
[ECL](https://gitlab.com/embeddable-common-lisp/ecl/), Embeddable
Common Lisp, transpiles Lisp programs to C.  That creates a smaller
executable.

According to
[this reddit source](https://www.reddit.com/r/lisp/comments/46k530/tackling_the_eternal_problem_of_lisp_image_size/), ECL produces indeed the smallest executables of all,
an order of magnitude smaller than SBCL, but with a longer startup time.

CCL's binaries seem to be as fast as SBCL and nearly half the size.

```
| program size | implementation |  CPU | startup time |
|--------------+----------------+------+--------------|
|           28 | /bin/true      |  15% |        .0004 |
|         1005 | ecl            | 115% |        .5093 |
|        48151 | sbcl           |  91% |        .0064 |
|        27054 | ccl            |  93% |        .0060 |
|        10162 | clisp          |  96% |        .0170 |
|         4901 | ecl.big        | 113% |        .8223 |
|        70413 | sbcl.big       |  93% |        .0073 |
|        41713 | ccl.big        |  95% |        .0094 |
|        19948 | clisp.big      |  97% |        .0259 |
```

## Building a smaller binary with SBCL's core compression

Building with SBCL's core compression can dramatically reduce your
application binary's size. In our case, we passed from 120MB to 23MB,
for a loss of a dozen milliseconds of start-up time, which was still
under 50ms!

Your SBCL must be built with core compression, see the documentation: [http://www.sbcl.org/manual/#Saving-a-Core-Image](http://www.sbcl.org/manual/#Saving-a-Core-Image)

Is it the case ?

~~~lisp
(find :sb-core-compression *features*)
:SB-CORE-COMPRESSION
~~~

Yes, it is the case with this SBCL installed from Debian.

<!-- In case you want to build from scratch, you can use `./make.sh --fancy`. -->

**With SBCL**

In SBCL, we would give an argument to `save-lisp-and-die`, where
`:compression`

> may be an integer from -1 to 9, corresponding to zlib compression levels, or t (which is equivalent to the default compression level, -1).

We experienced a 1MB difference between levels -1 and 9.

**With ASDF**

However, we prefer to do this with ASDF (or rather, UIOP). Add this in your .asd:

~~~lisp
#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))
~~~

**With Deploy**

Also, the [Deploy](https://github.com/Shinmera/deploy/) library can be used
to build a fully standalone application. It will use compression if available.

Deploy is specifically geared towards applications with foreign
library dependencies. It collects all the foreign shared libraries of
dependencies, such as libssl.so in the `bin` subdirectory.

And voilà !


# Parsing command line arguments

SBCL stores the command line arguments into `sb-ext:*posix-argv*`.

But that variable name differs from implementations, so we may want a
library to handle the differences for us.

We also want to parse the arguments.

A quick look at the
[awesome-cl#scripting](https://github.com/CodyReichert/awesome-cl#scripting)
list and we'll do that with the
[unix-opts](https://github.com/mrkkrp/unix-opts) library.

    (ql:quickload "unix-opts")

We can call it with its `opts` alias (nickname).

As often work happens in two phases:

* declaring the options our app accepts, their optional argument, defining their type
  (string, integer,…), long and short names, and the required ones,
* parsing them (and handling missing or malformed parameters).


## Declaring arguments

We define the arguments with `opts:define-opts`:

~~~lisp
(opts:define-opts
    (:name :help
           :description "print this help text"
           :short #\h
           :long "help")
    (:name :nb
           :description "here we want a number argument"
           :short #\n
           :long "nb"
           :arg-parser #'parse-integer) ;; <- takes an argument
    (:name :info
           :description "info"
           :short #\i
           :long "info"))
~~~

Here `parse-integer` is a built-in CL function.

Example output on the command line (auto-generated help text):

~~~
$ my-app -h
my-app. Usage:

Available options:
  -h, --help               print this help text
  -n, --nb ARG             here we want a number argument
  -i, --info               info
~~~


## Parsing

We parse and get the arguments with `opts:get-opts`, which returns two
values: the list of valid options and the remaining free arguments. We
then must use `multiple-value-bind` to assign both into variables:

~~~lisp
  (multiple-value-bind (options free-args)
      ;; There is no error handling yet.
      (opts:get-opts)
      ...
~~~

We can test this by giving a list of strings to `get-opts`:

~~~lisp
(multiple-value-bind (options free-args)
                   (opts:get-opts '("hello" "-h" "-n" "1"))
                 (format t "Options: ~a~&" options)
                 (format t "free args: ~a~&" free-args))
Options: (HELP T NB-RESULTS 1)
free args: (hello)
NIL
~~~

If we  put an unknown option,  we get into the  debugger. We'll see
error handling in a moment.

So `options` is a
[property list](https://lispcookbook.github.io/cl-cookbook/data-structures.html#plist). We
use `getf` and `setf` with plists, so that's how we do our
logic. Below we print the help with `opts:describe` and then `exit`
(in a portable way).

~~~lisp
  (multiple-value-bind (options free-args)
      (opts:get-opts)

    (if (getf options :help)
        (progn
          (opts:describe
           :prefix "You're in my-app. Usage:"
           :args "[keywords]") ;; to replace "ARG" in "--nb ARG"
          (opts:exit))) ;; <= optional return status.
    (if (getf options :nb)
       ...)
~~~

For a full example, see its
[official example](https://github.com/mrkkrp/unix-opts/blob/master/example/example.lisp)
and
[cl-torrents' tutorial](https://vindarel.github.io/cl-torrents/tutorial.html).

The  example in  the unix-opts  repository suggests  a macro  to do
slightly better. Now to error handling.


### Handling malformed or missing arguments

There are 4 situations that unix-opts doesn't handle, but signals
conditions for us to take care of:

* for an unknown argument: an `unknown-option` condition is signaled
* also `missing-arg`
* `arg-parser-failed` when, for example, it expected an integer but got text
* `missing-required-option`

So, we must create simple functions to handle those conditions, and
surround the parsing of the options with an `handler-bind`:

~~~lisp
  (multiple-value-bind (options free-args)
      (handler-bind ((opts:unknown-option #'unknown-option) ;; the condition / our function
                     (opts:missing-arg #'missing-arg)
                     (opts:arg-parser-failed #'arg-parser-failed)
                     (opts:missing-required-option))
         (opts:get-opts))
    …
    ;; use "options" and "free-args"
~~~

Here we suppose we want one function to handle each case, but it could
be a simple one. They take the condition as argument.

~~~lisp
(defun handle-arg-parser-condition (condition)
  (format t "Problem while parsing option ~s: ~a .~%" (opts:option condition) ;; reader to get the option from the condition.
                                                       condition)
  (opts:describe) ;; print help
  (opts:exit)) ;; portable exit
~~~

For more about condition handling, see [error and condition handling](error_handling.html).

### Catching a C-c termination signal

Let's build a simple binary, run it, try a `C-c` and read the stacktrace:

~~~
$ ./my-app
sleep…
^C
debugger invoked on a SB-SYS:INTERACTIVE-INTERRUPT in thread   <== condition name
#<THREAD "main thread" RUNNING {1003156A03}>:
  Interactive interrupt at #x7FFFF6C6C170.

Type HELP for debugger help, or (SB-EXT:EXIT) to exit from SBCL.

restarts (invokable by number or by possibly-abbreviated name):
  0: [CONTINUE     ] Return from SB-UNIX:SIGINT.               <== it was a SIGINT indeed
  1: [RETRY-REQUEST] Retry the same request.
~~~

The signaled condition is named after our implementation:
`sb-sys:interactive-interrupt`. We just have to surround our
application code with a `handler-case`:

~~~lisp
(handler-case
    (run-my-app free-args)
  (sb-sys:interactive-interrupt () (progn
                                     (format *error-output* "Abort.~&")
                                     (opts:exit))))
~~~

This code only for SBCL though. We know about
[trivial-signal](https://github.com/guicho271828/trivial-signal/),
but we were not satisfied with our test yet. So we can use something
like this:

~~~lisp
(handler-case
    (run-my-app free-args)
  (#+sbcl sb-sys:interactive-interrupt
   #+ccl  ccl:interrupt-signal-condition
   #+clisp system::simple-interrupt-condition
   #+ecl ext:interactive-interrupt
   #+allegro excl:interrupt-signal
   ()
   (opts:exit)))
~~~

here `#+` includes the line at compile time depending on
the  implementation.  There's  also `#-`.  What `#+` does is to look for
symbols in the `*features*` list.  We can also combine symbols with
`and`, `or` and `not`.

# Continuous delivery of executables

We can make a Continuous Integration system (Travis CI, Gitlab CI,…)
build binaries for us at every commit, or at every tag pushed or at
whichever other policy.

See [Continuous Integration](testing.html#continuous-integration).

# Credit

* [cl-torrents' tutorial](https://vindarel.github.io/cl-torrents/tutorial.html)
* [lisp-journey/web-dev](https://lisp-journey.gitlab.io/web-dev/)

---
title: Testing
---

So you want to easily test the code you're writing ? The following
recipes cover how to write automated tests and see their code
coverage. We also give pointers to plug those in modern continuous
integration services like Travis CI and Coveralls.

We will use an established and well-designed regression testing
framework called [Prove](https://github.com/fukamachi/prove). It is
not the only possibility though,
[FiveAM](http://quickdocs.org/fiveam/api) is a popular one (see
[this blogpost](http://turtleware.eu/posts/Tutorial-Working-with-FiveAM.html) for an
introduction). We prefer
`Prove` for its doc and its **extensible reporters** (it has different
report styles and we can extend them).


<a name="install"></a>

## Testing with Prove

### Install and load

`Prove` is in Quicklisp:

~~~lisp
(ql:quickload :prove)
~~~

This command installs `prove` if necessary, and loads it.

<a name="writetest"></a>

### Write a test file

~~~lisp
(in-package :cl-user)
(defpackage my-test
  (:use :cl
        :prove))
(in-package :my-test)

(subtest "Showing off Prove"
  (ok (not (find 4 '(1 2 3))))
  (is 4 4)
  (isnt 1 #\1))

~~~

Prove's API contains the following testing functions: `ok`, `is`,
`isnt`, `is-values`, `is-type`, `like` (for regexps), `is-print`
(checks the standard output), `is-error`, `is-expand`, `pass`, `fail`,
`skip`, `subtest`.


### Run a test file

~~~lisp
(prove:run #P"myapp/tests/my-test.lisp")
(prove:run #P"myapp/tests/my-test.lisp" :reporter :list)
~~~

We get an output like:

<img src="assets/prove-report.png"
     style="max-width: 800px"/>

### Run one test

You can directly run one test by compiling it. With Slime, use the
usual `C-c C-c`.


### More about Prove

`Prove` can also:

* be run on **Travis CI**,
* **colorize** the output,
* report **tests duration**,
* change the default test function,
* set a threshold for slow tests,
* invoke the **CL debugger** whenever getting an error during running tests,
* integrate with **ASDF** so than we can execute `(asdf:test-system)` or
  `(prove:run)` in the REPL (such configuration is provided by
  [cl-project](https://github.com/fukamachi/cl-project), by the same
  author).

See [Prove's documentation](https://github.com/fukamachi/prove) !


## Interactively fixing unit tests

Common Lisp is interactive by nature (or so are most implementations),
and testing frameworks make use of it. It is possible to ask the
framework to open the debugger on a failing test, so that we can
inspect the stacktrace and go to the erroneous line instantly, fix it
and re-run the test from where it left off, by choosing the suggested
*restart*.

With Prove, set `prove:*debug-on-error*` to `t`.

Below is a short screencast showing all this in action (with FiveAM):

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.video/videos/embed/c0c82209-feaa-444d-962f-afa25745bfc0" frameborder="0" allowfullscreen></iframe>

Note that in the debugger:

- `<enter>` on a backtrace shows more of it
- `v` on a backtrace goes to the corresponding line or function.
- see more options with the menu.


## Code coverage

A code coverage tool produces a visual output that allows to see what
parts of our code were tested or not:


![](assets/coverage.png "source: https://www.snellman.net/blog/archive/2007-05-03-code-coverage-tool-for-sbcl.html")


### Generating an html test coverage output

SBCL comes with a built-in module to do code coverage analysis:
[sb-cover](http://www.sbcl.org/manual/index.html#sb_002dcover).

Coverage reports are only generated for code compiled using
`compile-file` with the value of the `sb-cover:store-coverage-data`
optimization quality set to 3.

~~~lisp
;;; Load SB-COVER
(require :sb-cover)

;;; Turn on generation of code coverage instrumentation in the compiler
(declaim (optimize sb-cover:store-coverage-data))

;;; Load some code, ensuring that it's recompiled with the new optimization
;;; policy.
(asdf:oos 'asdf:load-op :cl-ppcre-test :force t)

;;; Run the test suite.
(prove:run :yoursystem-test)
~~~

Produce a coverage report, set the output directory:

~~~lisp
(sb-cover:report "coverage/")
~~~

Finally, turn off instrumentation:

~~~lisp
(declaim (optimize (sb-cover:store-coverage-data 0)))
~~~

You can open your browser at
`../yourproject/t/coverage/cover-index.html` to see the report like
the capture above or like
[this code coverage of cl-ppcre](https://www.snellman.net/sbcl/cover/cl-ppcre-report-3/cover-index.html).


## Continuous Integration

### Travis CI and Coveralls

[Travis](https://travis-ci.org/) is a service for running unit tests
in the cloud and [Coveralls](https://coveralls.io/) shows you the
evolution of coverage over time, and also tells you what a pull
request will do to coverage.

Thanks to `cl-travis` we can easily test our program against one or many
Lisp implementations (ABCL, Allegro CL, SBCL, CMUCL, CCL and
ECL). `cl-coveralls` helps to post our coverage to the service. It
supports SBCL and Clozure CL with Travis CI and Circle CI.

We refer you to the lengthy and illustrated explanations of the
["continuous integration" page on lisp-lang.org](http://lisp-lang.org/learn/continuous-integration).

You'll find many example projects using them in the links above, but
if you want a quick overview of what it looks like:

- Lucerne on [Coveralls](https://coveralls.io/github/eudoxia0/lucerne)
  and [Travis](https://travis-ci.org/eudoxia0/lucerne).

### Gitlab CI

[Gitlab CI](https://docs.gitlab.com/ce/ci/README.html) is part of
Gitlab and is available on [Gitlab.com](https://gitlab.com/), for
public and private repositories. Let's see straight away a simple
`.gitlab-ci.yml`:

~~~
image: daewok/lisp-devel

before_script:
  - apt-get update -qy
  - apt-get install -y git-core
  - git clone https://github.com/foo/bar ~/quicklisp/local-projects/

test:
  script:
    - make test
~~~

Gitlab CI is based on Docker. With `image` we tell it to use the
[daewok/lisp-devel](https://hub.docker.com/r/daewok/lisp-devel/)
one. It includes SBCL, ECL, CCL and ABCL, and Quicklisp is installed
in the home (`/home/lisp/`), so we can `quickload` packages right
away. If you're interested it also has a more bare bones option. Gitlab will load the
image, clone our project and put us at the project root with
administrative rights to run the rest of the commands.

`test` is a "job" we define, `script` is a
recognized keywords that takes a list of commands to run.

Suppose we must install dependencies before running our tests:
`before_script` will run before each job. Here we clone a library
where Quicklisp can find it, and for doing so we must install git
(Docker images are usually pretty bare bones).

We can try locally ourselves. If we already installed [Docker](https://docs.docker.com/) and
started its daemon (`sudo service docker start`), we can do:

    docker run --rm -it -v /path/to/local/code:/usr/local/share/common-lisp/source daewok/lisp-devel:latest bash

This will download the lisp image (±400Mo), mount some local code in
the image where indicated, and drop us in bash. Now we can try a `make
test`.

To show you a more complete example:

~~~
image: daewok/lisp-devel

stages:
  - test
  - build

before_script:
  - apt-get update -qy
  - apt-get install -y git-core
  - git clone https://github.com/foo/bar ~/quicklisp/local-projects/

test:
  stage: test
  script:
    - make test

build:
  stage: build
  only:
    - tags
  script:
    - make build
  artifacts:
    paths:
      - some-file-name
~~~

Here we defined two `stages` (see
[environments](https://docs.gitlab.com/ce/ci/environments.html)),
"test" and "build", defined to run one after another. A "build" stage
will start only if the "test" one succeeds.

"build" is asked to run `only` when a
new tag is pushed, not at every commit. When it succeeds, it will make
the files listed in `artifacts`'s `paths` available for download. We can
download them from Gitlab's Pipelines UI, or with an url. This one will download
the file "some-file-name" from the latest "build" job:

    https://gitlab.com/username/project-name/-/jobs/artifacts/master/raw/some-file-name?job=build

When the pipelines pass, you will see:

![](assets/img-ci-build.png)


You now have a ready to use Gitlab CI.

---
title: Database Access and Persistence
---

The
[Database section on the Awesome-cl list](https://github.com/CodyReichert/awesome-cl#database)
is a resource listing popular libraries to work with different kind of
databases. We can group them roughly in four categories:

- wrappers to one database engine (cl-sqlite, postmodern, cl-redis,…),
- interfaces to several DB engines (clsql, sxql,…),
- persistent object databases (bknr.datastore (see chap. 21 of "Common Lisp Recipes"), ubiquitous,…),
- [Object Relational Mappers](https://en.wikipedia.org/wiki/Object-relational_mapping) (Mito),

and other DB-related tools (pgloader).

We'll begin with an overview of Mito. If you must work with an
existing DB, you might want to have a look at cl-dbi and clsql. If you
don't need a SQL database and want automatic persistence of Lisp
objects, you also have a choice of libraries.


# The Mito ORM and SxQL

Mito is in Quicklisp:

~~~lisp
(ql:quickload :mito)
~~~

## Overview

[Mito](https://github.com/fukamachi/mito) is "an ORM for Common Lisp
with migrations, relationships and PostgreSQL support".

- it **supports MySQL, PostgreSQL and SQLite3**,
- when defining a model, it adds an `id` (serial primary key),
  `created_at` and `updated_at` fields by default like Ruby's
  ActiveRecord or Django,
- handles DB **migrations** for the supported backends,
- permits DB **schema versioning**,
- is tested under SBCL and CCL.

As an ORM, it allows to write class definitions, to specify
relationships, and provides functions to query the database.  For
custom queries, it relies on
[SxQL](https://github.com/fukamachi/sxql), an SQL generator that
provides the same interface for several backends.

Working with Mito generally involves these steps:

- connecting to the DB
- writing [CLOS](clos.html) classes to define models
- running migrations to create or alter tables
- creating objects, saving same in the DB,

and iterating.

## Connecting to a DB

Mito provides the function `connect-toplevel` to establish a
connection to RDBMs:

~~~lisp
(mito:connect-toplevel :mysql :database-name "myapp" :username "fukamachi" :password "c0mon-1isp")
~~~

The driver type can be of `:mysql`, `:sqlite3` and `:postgres`.

With sqlite you don't need the username and password:

~~~lisp
(connect-toplevel :sqlite3 :database-name "myapp")
~~~

As usual, you need to create the MySQL or PostgreSQL database beforehand.
Refer to their documentation.

Connecting sets `mito:*connection*` to the new connection and returns it.

Disconnect with `disconnect-toplevel`.

=> you might make good use of a wrapper function:

~~~lisp
(defun connect ()
  "Connect to the DB."
  (connect-toplevel :sqlite3 :database-name "myapp"))
~~~

## Models

### Defining models

In Mito, you can define a class which corresponds to a database table
by specifying `(:metaclass mito:dao-table-class)`:

~~~lisp
(defclass user ()
  ((name :col-type (:varchar 64)
         :initarg :name
         :accessor user-name)
   (email :col-type (or (:varchar 128) :null)
          :initarg :email
          :accessor user-email))
  (:metaclass mito:dao-table-class))
~~~

Note that the class automatically adds some slots: a primary key named `id`
if there's no primary keys, `created_at` and `updated_at` for
recording timestamps. To disable these behaviors, specify `:auto-pk
nil` or `:record-timestamps nil` to the defclass forms.

You can inspect the new class:

~~~lisp
(mito.class:table-column-slots (find-class 'user))
;=> (#<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS MITO.DAO.MIXIN::ID>
;    #<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS COMMON-LISP-USER::NAME>
;    #<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS COMMON-LISP-USER::EMAIL>
;    #<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS MITO.DAO.MIXIN::CREATED-AT>
;    #<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS MITO.DAO.MIXIN::UPDATED-AT>)
~~~

The class inherits `mito:dao-class` implicitly.

~~~lisp
(find-class 'user)
;=> #<MITO.DAO.TABLE:DAO-TABLE-CLASS COMMON-LISP-USER::USER>

(c2mop:class-direct-superclasses *)
;=> (#<STANDARD-CLASS MITO.DAO.TABLE:DAO-CLASS>)
~~~

This may be useful when you define methods which can be applied for
all table classes.

For more information on using the Common Lisp Object System, see the
[clos](clos.html) page.


### Creating the tables

After defining the models, you must create the tables:

~~~lisp
(mito:ensure-table-exists 'user)
~~~

So a helper function:

~~~lisp
(defun ensure-tables ()
  (mapcar #'mito:ensure-table-exists '('user 'foo 'bar)))
~~~


See
[Mito's documentation](https://github.com/fukamachi/mito#generating-table-definitions)
for a couple more ways.

When you alter the model you'll need to run a DB migration, see the next section.

### Fields

#### Fields types

Field types are:

`(:varchar <integer>)` ,

`:serial`, `:bigserial`, `:integer`, `:bigint`, `:unsigned`,

`:timestamp`, `:timestamptz`,

`:bytea`,

#### Optional fields

Use `(or <real type> :null)`:

~~~lisp
   (email :col-type (or (:varchar 128) :null)
          :initarg :email
          :accessor user-email))
~~~


#### Field constraints

`:unique-keys` can be used like so:

~~~lisp
(defclass user ()
  ((name :col-type (:varchar 64)
         :initarg :name
         :accessor user-name)
   (email :col-type (:varchar 128)
          :initarg :email
          :accessor user-email))
  (:metaclass mito:dao-table-class)
  (:unique-keys email))
~~~

We already saw `:primary-key`.

You can change the table name with `:table-name`.

### Relationships

You can define a relationship by specifying  a foreign class with `:col-type`:

~~~lisp
(defclass tweet ()
  ((status :col-type :text
           :initarg :status
           :accessor tweet-status)
   ;; This slot refers to USER class
   (user :col-type user
         :initarg :user
         :accessor tweet-user))
  (:metaclass mito:dao-table-class))

(table-definition (find-class 'tweet))
;=> (#<SXQL-STATEMENT: CREATE TABLE tweet (
;        id BIGSERIAL NOT NULL PRIMARY KEY,
;        status TEXT NOT NULL,
;        user_id BIGINT NOT NULL,
;        created_at TIMESTAMP,
;        updated_at TIMESTAMP
;    )>)
~~~

Now you can create or retrieve a `TWEET` by a `USER` object, not a `USER-ID`.

~~~lisp
(defvar *user* (mito:create-dao 'user :name "Eitaro Fukamachi"))
(mito:create-dao 'tweet :user *user*)

(mito:find-dao 'tweet :user *user*)
~~~

Mito doesn't add foreign key constraints for referring tables.

#### One-to-one

A one-to-one relationship is simply represented with a simple foreign
key on a slot (as `:col-type user` in the `tweet` class). Besides, we
can add a unicity constraint, as with `(:unique-keys email)`.

#### One-to-many, many-to-one

The tweet example above shows a one-to-many relationship between a user and
his tweets: a user can write many tweets, and a tweet belongs to only
one user.

The relationship is defined with a foreign key on the "many" side
linking back to the "one" side. Here the `tweet` class defines a
`user` foreign key, so a tweet can only have one user. You didn't need
to edit the `user` class.

A many-to-one relationship is actually the contrary of a one-to-many.
You have to put the foreign key on the appropriate side.

#### Many-to-many

A many-to-many relationship needs an intermediate table, which will be
the "many" side for the two tables it is the intermediary of.

And, thanks to the join table, we can store more information about the relationship.

Let's define a `book` class:

~~~lisp
(defclass book ()
    ((title
       :col-type (:varchar 128)
       :initarg :title
       :accessor title)
     (ean
       :col-type (or (:varchar 128) :null)
       :initarg :ean
       :accessor ean))
    (:metaclass mito:dao-table-class))
~~~

A user can have many books, and a book (as the title, not the physical
copy) is likely to be in many people's library. Here's the
intermediate class:

~~~lisp
(defclass user-books ()
    ((user
      :col-type user
      :initarg :user)
    (book
      :col-type book
      :initarg :book))
    (:metaclass mito:dao-table-class))
~~~

Each time we want to add a book to a user's collection (say in
a `add-book` function), we create a new `user-books` object.

But someone may very well own many copies of one book. This is an
information we can store in the join table:

~~~lisp
(defclass user-books ()
    ((user
      :col-type user
      :initarg :user)
    (book
      :col-type book
      :initarg :book)
    ;; Set the quantity, 1 by default:
    (quantity
      :col-type :integer
      :initarg :quantity
      :initform 1
      :accessor quantity))
    (:metaclass mito:dao-table-class))
~~~


### Inheritance and mixin

A subclass of DAO-CLASS is allowed to be inherited. This may be useful
when you need classes which have similar columns:

~~~lisp
(defclass user ()
  ((name :col-type (:varchar 64)
         :initarg :name
         :accessor user-name)
   (email :col-type (:varchar 128)
          :initarg :email
          :accessor user-email))
  (:metaclass mito:dao-table-class)
  (:unique-keys email))

(defclass temporary-user (user)
  ((registered-at :col-type :timestamp
                  :initarg :registered-at
                  :accessor temporary-user-registered-at))
  (:metaclass mito:dao-table-class))

(mito:table-definition 'temporary-user)
;=> (#<SXQL-STATEMENT: CREATE TABLE temporary_user (
;        id BIGSERIAL NOT NULL PRIMARY KEY,
;        name VARCHAR(64) NOT NULL,
;        email VARCHAR(128) NOT NULL,
;        registered_at TIMESTAMP NOT NULL,
;        created_at TIMESTAMP,
;        updated_at TIMESTAMP,
;        UNIQUE (email)
;    )>)
~~~

If you need a 'template' for tables which doesn't related to any
database tables, you can use `DAO-TABLE-MIXIN`. Below the `has-email`
class will not create a table.

~~~lisp
(defclass has-email ()
  ((email :col-type (:varchar 128)
          :initarg :email
          :accessor object-email))
  (:metaclass mito:dao-table-mixin)
  (:unique-keys email))
;=> #<MITO.DAO.MIXIN:DAO-TABLE-MIXIN COMMON-LISP-USER::HAS-EMAIL>

(defclass user (has-email)
  ((name :col-type (:varchar 64)
         :initarg :name
         :accessor user-name))
  (:metaclass mito:dao-table-class))
;=> #<MITO.DAO.TABLE:DAO-TABLE-CLASS COMMON-LISP-USER::USER>

(mito:table-definition 'user)
;=> (#<SXQL-STATEMENT: CREATE TABLE user (
;       id BIGSERIAL NOT NULL PRIMARY KEY,
;       name VARCHAR(64) NOT NULL,
;       email VARCHAR(128) NOT NULL,
;       created_at TIMESTAMP,
;       updated_at TIMESTAMP,
;       UNIQUE (email)
;   )>)
~~~

See more examples of use in [mito-auth](https://github.com/fukamachi/mito-auth/).


### Troubleshooting

#### "Cannot CHANGE-CLASS objects into CLASS metaobjects."

If you get the following error message:

~~~
Cannot CHANGE-CLASS objects into CLASS metaobjects.
   [Condition of type SB-PCL::METAOBJECT-INITIALIZATION-VIOLATION]
See also:
  The Art of the Metaobject Protocol, CLASS [:initialization]
~~~

it is certainly because you first wrote a class definition and *then*
added the Mito metaclass and tried to evaluate the class definition
again.

If this happens, you must remove the class definition from the current package:

~~~lisp
(setf (find-class 'foo) nil)
~~~

or, with the Slime inspector, click on the class and find the "remove" button.

More info [here](https://stackoverflow.com/questions/38811931/how-to-change-classs-metaclass).

## Migrations

First create the tables if needed:

~~~lisp
(ensure-table-exists 'user)
~~~

then alter the tables, if needed:

~~~lisp
(mito:migrate-table 'user)
~~~

You can check the SQL generated code with `migration-expressions
'class`. For example, we create the `user` table:

~~~lisp
(ensure-table-exists 'user)
;-> ;; CREATE TABLE IF NOT EXISTS "user" (
;       "id" BIGSERIAL NOT NULL PRIMARY KEY,
;       "name" VARCHAR(64) NOT NULL,
;       "email" VARCHAR(128),
;       "created_at" TIMESTAMP,
;       "updated_at" TIMESTAMP
;   ) () [0 rows] | MITO.DAO:ENSURE-TABLE-EXISTS
~~~

There are no changes from the previous user definition:

~~~lisp
(mito:migration-expressions 'user)
;=> NIL
~~~

Now let's add a unique `email` field:

~~~lisp
(defclass user ()
  ((name :col-type (:varchar 64)
         :initarg :name
         :accessor user-name)
   (email :col-type (:varchar 128)
          :initarg :email
          :accessor user-email))
  (:metaclass mito:dao-table-class)
  (:unique-keys email))
~~~

The migration will run the following code:

~~~lisp
(mito:migration-expressions 'user)
;=> (#<SXQL-STATEMENT: ALTER TABLE user ALTER COLUMN email TYPE character varying(128), ALTER COLUMN email SET NOT NULL>
;    #<SXQL-STATEMENT: CREATE UNIQUE INDEX unique_user_email ON user (email)>)
~~~

so let's apply it:

~~~lisp
(mito:migrate-table 'user)
;-> ;; ALTER TABLE "user" ALTER COLUMN "email" TYPE character varying(128), ALTER COLUMN "email" SET NOT NULL () [0 rows] | MITO.MIGRATION.TABLE:MIGRATE-TABLE
;   ;; CREATE UNIQUE INDEX "unique_user_email" ON "user" ("email") () [0 rows] | MITO.MIGRATION.TABLE:MIGRATE-TABLE
;-> (#<SXQL-STATEMENT: ALTER TABLE user ALTER COLUMN email TYPE character varying(128), ALTER COLUMN email SET NOT NULL>
;    #<SXQL-STATEMENT: CREATE UNIQUE INDEX unique_user_email ON user (email)>)
~~~


## Queries

### Creating objects

We can create user objects with the regular `make-instance`:

~~~lisp
(defvar me
  (make-instance 'user :name "Eitaro Fukamachi" :email "e.arrows@gmail.com"))
;=> USER
~~~

To save it in DB, use `insert-dao`:

~~~lisp
(mito:insert-dao me)
;-> ;; INSERT INTO `user` (`name`, `email`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?) ("Eitaro Fukamachi", "e.arrows@gmail.com", "2016-02-04T19:55:16.365543Z", "2016-02-04T19:55:16.365543Z") [0 rows] | MITO.DAO:INSERT-DAO
;=> #<USER {10053C4453}>
~~~

Do the two steps above at once:

~~~lisp
(mito:create-dao 'user :name "Eitaro Fukamachi" :email "e.arrows@gmail.com")
~~~

You should not export the `user` class and create objects outside of
its package (it is good practice anyway to keep all database-related
operations in say a `models` package and file). You should instead use
a helper function:

~~~lisp
(defun make-user (&key name)
  (make-instance 'user :name name))
~~~

### Updating fields

~~~lisp
(setf (slot-value me 'name) "nitro_idiot")
;=> "nitro_idiot"
~~~

and save it:

~~~lisp
(mito:save-dao me)
~~~

### Deleting

~~~lisp
(mito:delete-dao me)
;-> ;; DELETE FROM `user` WHERE (`id` = ?) (1) [0 rows] | MITO.DAO:DELETE-DAO

;; or:
(mito:delete-by-values 'user :id 1)
;-> ;; DELETE FROM `user` WHERE (`id` = ?) (1) [0 rows] | MITO.DAO:DELETE-DAO
~~~

### Get the primary key value

~~~lisp
(mito:object-id me)
;=> 1
~~~

### Count

~~~lisp
(mito:count-dao 'user)
;=> 1
~~~

### Find one

~~~lisp
(mito:find-dao 'user :id 1)
;-> ;; SELECT * FROM `user` WHERE (`id` = ?) LIMIT 1 (1) [1 row] | MITO.DB:RETRIEVE-BY-SQL
;=> #<USER {10077C6073}>
~~~

So here's a possibility of generic helpers to find an object by a given key:

~~~lisp
(defgeneric find-user (key-name key-value)
  (:documentation "Retrieves an user from the data base by one of the unique
keys."))

(defmethod find-user ((key-name (eql :id)) (key-value integer))
  (mito:find-dao 'user key-value))

(defmethod find-user ((key-name (eql :name)) (key-value string))
  (first (mito:select-dao 'user
                          (sxql:where (:= :name key-value)))))
~~~

### Find all

Use the macro `select-dao`.

Get a list of all users:

~~~lisp
(mito:select-dao 'user)
;(#<USER {10077C6073}>)
;#<SXQL-STATEMENT: SELECT * FROM user>
~~~


### Find by relationship

As seen above:

~~~lisp
(mito:find-dao 'tweet :user *user*)
~~~

### Custom queries

It is with `select-dao` that you can write more precise queries by
giving it [SxQL](https://github.com/fukamachi/sxql) statements.

Example:

~~~lisp
(select-dao 'tweet
    (where (:like :status "%Japan%")))
~~~

another:

~~~lisp
(select (:id :name :sex)
  (from (:as :person :p))
  (where (:and (:>= :age 18)
               (:< :age 65)))
  (order-by (:desc :age)))
~~~

You can compose your queries with regular Lisp code:

~~~lisp
(defun find-tweets (&key user)
  (select-dao 'tweet
    (when user
      (where (:= :user user)))
    (order-by :object-created)))
~~~

`select-dao` is a macro that expands to the right thing©.


#### Clauses

See the [SxQL documentation](https://github.com/fukamachi/sxql#sql-clauses).

Examples:

~~~lisp
(select-dao 'foo
  (where (:and (:> :age 20) (:<= :age 65))))
~~~

~~~lisp
(order-by :age (:desc :id))
~~~

~~~lisp
(group-by :sex)
~~~

~~~lisp
(having (:>= (:sum :hoge) 88))
~~~

~~~lisp
(limit 0 10)
~~~

and `join`s, etc.


#### Operators

~~~lisp
:not
:is-null, :not-null
:asc, :desc
:distinct
:=, :!=
:<, :>, :<= :>=
:a<, :a>
:as
:in, :not-in
:like
:and, :or
:+, :-, :* :/ :%
:raw
~~~


## Triggers

Since `insert-dao`, `update-dao` and `delete-dao` are defined as generic
functions, you can define `:before`, `:after` or `:around` methods to those, like regular [method combination](clos.html#qualifiers-and-method-combination).

~~~lisp
(defmethod mito:insert-dao :before ((object user))
  (format t "~&Adding ~S...~%" (user-name object)))

(mito:create-dao 'user :name "Eitaro Fukamachi" :email "e.arrows@gmail.com")
;-> Adding "Eitaro Fukamachi"...
;   ;; INSERT INTO "user" ("name", "email", "created_at", "updated_at") VALUES (?, ?, ?, ?) ("Eitaro Fukamachi", "e.arrows@gmail.com", "2016-02-16 21:13:47", "2016-02-16 21:13:47") [0 rows] | MITO.DAO:INSERT-DAO
;=> #<USER {100835FB33}>
~~~

## Inflation/Deflation

Inflation/Deflation is a function to convert values between Mito and RDBMS.

~~~lisp
(defclass user-report ()
  ((title :col-type (:varchar 100)
          :initarg :title
          :accessor report-title)
   (body :col-type :text
         :initarg :body
         :initform ""
         :accessor report-body)
   (reported-at :col-type :timestamp
                :initarg :reported-at
                :initform (local-time:now)
                :accessor report-reported-at
                :inflate #'local-time:universal-to-timestamp
                :deflate #'local-time:timestamp-to-universal))
  (:metaclass mito:dao-table-class))
~~~

## Eager loading

One of the pains in the neck to use ORMs is the "N+1 query" problem.

~~~lisp
;; BAD EXAMPLE

(use-package '(:mito :sxql))

(defvar *tweets-contain-japan*
  (select-dao 'tweet
    (where (:like :status "%Japan%"))))

;; Getting names of tweeted users.
(mapcar (lambda (tweet)
          (user-name (tweet-user tweet)))
        *tweets-contain-japan*)
~~~

This example sends a query to retrieve a user like "SELECT * FROM user
WHERE id = ?" at each iteration.

To prevent this performance issue, add `includes` to the above query
which only sends a single WHERE IN query instead of N queries:

~~~lisp
;; GOOD EXAMPLE with eager loading

(use-package '(:mito :sxql))

(defvar *tweets-contain-japan*
  (select-dao 'tweet
    (includes 'user)
    (where (:like :status "%Japan%"))))
;-> ;; SELECT * FROM `tweet` WHERE (`status` LIKE ?) ("%Japan%") [3 row] | MITO.DB:RETRIEVE-BY-SQL
;-> ;; SELECT * FROM `user` WHERE (`id` IN (?, ?, ?)) (1, 3, 12) [3 row] | MITO.DB:RETRIEVE-BY-SQL
;=> (#<TWEET {1003513EC3}> #<TWEET {1007BABEF3}> #<TWEET {1007BB9D63}>)

;; No additional SQLs will be executed.
(tweet-user (first *))
;=> #<USER {100361E813}>
~~~

## Schema versioning

~~~
$ ros install mito
$ mito
Usage: mito command [option...]

Commands:
    generate-migrations
    migrate

Options:
    -t, --type DRIVER-TYPE          DBI driver type (one of "mysql", "postgres" or "sqlite3")
    -d, --database DATABASE-NAME    Database name to use
    -u, --username USERNAME         Username for RDBMS
    -p, --password PASSWORD         Password for RDBMS
    -s, --system SYSTEM             ASDF system to load (several -s's allowed)
    -D, --directory DIRECTORY       Directory path to keep migration SQL files (default: "/Users/nitro_idiot/Programs/lib/mito/db/")
    --dry-run                       List SQL expressions to migrate
~~~

## Introspection

Mito provides some functions for introspection.

We can access the information of **columns** with the functions in
`(mito.class.column:...)`:

- `table-column-[class, name, info, not-null-p,...]`
- `primary-key-p`

and likewise for **tables** with `(mito.class.table:...)`.

Given we get a list of slots of our class:

~~~lisp
(ql:quickload "closer-mop")

(closer-mop:class-direct-slots (find-class 'user))
;; (#<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS NAME>
;;  #<MITO.DAO.COLUMN:DAO-TABLE-COLUMN-CLASS EMAIL>)

(defparameter user-slots *)
~~~

We can answer the following questions:

### What is the type of this column ?

~~~lisp
(mito.class.column:table-column-type (first user-slots))
;; (:VARCHAR 64)
~~~

### Is this column nullable ?

~~~lisp
(mito.class.column:table-column-not-null-p
  (first user-slots))
;; T
(mito.class.column:table-column-not-null-p
  (second user-slots))
;; NIL
~~~


## Testing

We don't want to test DB operations against the production one. We
need to create a temporary DB before each test.

The macro below creates a temporary DB with a random name, creates the
tables, runs the code and connects back to the original DB connection.

~~~lisp
(defpackage my-test.utils
  (:use :cl)
  (:import-from :my.models
                :*db*
                :*db-name*
                :connect
                :ensure-tables-exist
                :migrate-all)
  (:export :with-empty-db))

(in-package my-test.utils)

(defun random-string (length)
  ;; thanks 40ants/hacrm.
  (let ((chars "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))
    (coerce (loop repeat length
                  collect (aref chars (random (length chars))))
            'string)))

(defmacro with-empty-db (&body body)
  "Run `body` with a new temporary DB."
  `(let* ((*random-state* (make-random-state t))
          (prefix (concatenate 'string
                               (random-string 8)
                               "/"))
          ;; Save our current DB connection.
          (connection (when (mito.connection:connected-p)
                        mito:*connection*)))
     (uiop:with-temporary-file (:pathname name :prefix prefix)
       ;; Bind our *db-name* to a new name, so as to create a new DB.
       (let* ((*db-name* name))
         ;; Always re-connect to our real DB even in case of error in body.
         (unwind-protect
           (progn
             ;; our functions to connect to the DB, create the tables and run the migrations.
             (connect)
             (ensure-tables-exist)
             (migrate-all)
             ,@body)

           (setf mito:*connection* connection))))))
~~~

Use it like this:

~~~lisp
(prove:subtest "Creation in a temporary DB."
  (with-empty-db
    (let ((user (make-user :name "Cookbook")))
      (save-user user)

      (prove:is (name user)
                "Cookbook"
                "Test username in a temp DB."))))
;; Creation in a temporary DB
;;  CREATE TABLE "user" (
;;       id BIGSERIAL NOT NULL PRIMARY KEY,
;;       name VARCHAR(64) NOT NULL,
;;       email VARCHAR(128) NOT NULL,
;;       created_at TIMESTAMP,
;;       updated_at TIMESTAMP,
;;       UNIQUE (email)
;; ) () [0 rows] | MITO.DB:EXECUTE-SQL
;; ✓ Test username in a temp DB.
~~~

## See also

- [exploring an existing (PostgreSQL) database with postmodern](https://sites.google.com/site/sabraonthehill/postmodern-examples/exploring-a-database)

- [mito-attachment](https://github.com/fukamachi/mito-attachment)
- [mito-auth](https://github.com/fukamachi/mito-auth)
- [can](https://github.com/fukamachi/can/) a role-based access right control library
- an advanced ["defmodel" macro](drafts/defmodel.lisp.html).

<!-- # todo: Generating models for an existing DB -->

---
title: Web development
---


For web development as for any other task, one can leverage Common
Lisp's advantages: the unmatched REPL and exception handling system,
performance, the ability to build a self-contained executable,
stability, good threads story, strong typing, etc. We can, say, define
a new route and try it right away, there is no need to restart any
running server. We can change and compile *one function at a time*
(the usual `C-c C-c` in Slime) and try it. The feedback is
immediate. We can choose the degree of interactivity: the web server
can catch exceptions and fire the interactive debugger, or print lisp
backtraces on the browser, or display a 404 error page and print logs
on standard output. The ability to build self-contained executables eases
deployment tremendously (compared to, for example, npm-based apps), in
that we just copy the executable to a server and run it.

We'll present here some established web frameworks and other common
libraries to help you getting started in developing a web
application. We do *not* aim to be exhaustive nor to replace the
upstream documentation. Your feedback and contributions are
appreciated.


<!-- form creation, form validation -->

<!-- Javascript -->

# Overview

[Hunchentoot][hunchentoot] and [Clack][clack] are two projects that
you'll often hear about.

Hunchentoot is

> a web server and at the same time a toolkit for building dynamic websites. As a stand-alone web server, Hunchentoot is capable of HTTP/1.1 chunking (both directions), persistent connections (keep-alive), and SSL. It provides facilities like automatic session handling (with and without cookies), logging, customizable error handling, and easy access to GET and POST parameters sent by the client.

It is a software written by Edi Weitz ("Common Lisp Recipes",
`cl-ppcre` and [much more](https://edicl.github.io/)), it's used and
proven solid. One can achieve a lot with it, but sometimes with more
friction than with a traditional web framework. For example,
dispatching a route by the HTTP method is a bit convoluted, one must
write a function for the `:uri` parameter that does the check, when it
is a built-in keyword in other frameworks like Caveman.

Clack is

> a web application environment for Common Lisp inspired by Python's WSGI and Ruby's Rack.

Also written by a prolific lisper
([E. Fukamachi](https://github.com/fukamachi/)), it actually uses
Hunchentoot by default as the server, but thanks to its pluggable
architecture one can use another web server, like the asynchronous
[Woo](https://github.com/fukamachi/woo), built on the
[libev](http://software.schmorp.de/pkg/libev.html) event loop, maybe
"the fastest web server written in any programming language".

We'll cite also [Wookie](https://github.com/orthecreedence/wookie), an asynchronous HTTP server, and its
companion library
[cl-async](https://github.com/orthecreedence/cl-async), for general
purpose, non-blocking programming in Common Lisp, built on libuv, the
backend library in Node.js.

Clack being more recent and less documented, and Hunchentoot a
de-facto standard, we'll concentrate on the latter for this
recipe. Your contributions are of course welcome.

Web frameworks build upon web servers and can provide facilities for
common activities in web development, like a templating system, access
to a database, session management, or facilities to build a REST api.

Some web frameworks include:

- [Caveman][caveman], by E. Fukamachi. It provides, out of the box,
database management, a templating engine (Djula), a project skeleton
generator, a routing system à la Flask or Sinatra, deployment options
(mod_lisp or FastCGI), support for Roswell on the command line, etc.
- [Radiance][radiance], by [Shinmera](https://github.com/Shinmera)
  (Qtools, Portacle, lquery, …), is a web application environment,
  more general than usual web frameworks. It lets us write and tie
  websites and applications together, easing their deployment as a
  whole. It has thorough [documentation](https://shirakumo.github.io/radiance/), a [tutorial](https://github.com/Shirakumo/radiance-tutorial), [modules](https://github.com/Shirakumo/radiance-contribs), [pre-written applications](https://github.com/Shirakumo?utf8=%E2%9C%93&q=radiance&type=&language=) such as [an image board](https://github.com/Shirakumo/purplish) or a [blogging platform](https://github.com/Shirakumo/reader), and more.
  For example websites, see
  [https://shinmera.com/](https://shinmera.com/),
  [reader.tymoon.eu](https://reader.tymoon.eu/) and [events.tymoon.eu](https://events.tymoon.eu/).
- [Snooze][snooze], by João Távora (Sly, Emacs' Yasnippet, Eglot, …),
  is "an URL router designed around REST web services". It is
  different because in Snooze, routes are just functions and HTTP
  conditions are just Lisp conditions.
- [cl-rest-server][cl-rest-server] is a library for writing REST web
  APIs. It features validation with schemas, annotations for logging,
  caching, permissions or authentication, documentation via OpenAPI (Swagger),
  etc.
- last but not least, [Weblocks][weblocks] is a venerable Common Lisp
  web framework that permits to write ajax-based dynamic web
  applications without writing any JavaScript, nor writing some lisp
  that would transpile to JavaScript. It is seeing an extensive
  rewrite and update since 2017. We present it in more details below.

For a full list of libraries for the web, please see the [awesome-cl
list
#network-and-internet](https://github.com/CodyReichert/awesome-cl#network-and-internet)
and [Cliki](https://www.cliki.net/Web). If you are looking for a
featureful static site generator, see
[Coleslaw](https://github.com/coleslaw-org/coleslaw).


# Installation

Let's install the libraries we'll use:

~~~lisp
(ql:quickload '("hunchentoot" "caveman" "spinneret" "djula"))
~~~

To try Weblocks, please see its documentation. The Weblocks in
Quicklisp is not yet, as of writing, the one we are interested in.

We'll start by serving local files and we'll run more than one local
server in the running image.

# Simple webserver

## Serve local files

### Hunchentoot

Create and start a webserver like this:

~~~lisp
(defvar *acceptor* (make-instance 'hunchentoot:easy-acceptor :port 4242))
(hunchentoot:start *acceptor*)
~~~

We create an instance of `easy-acceptor` on port 4242 and we start
it. We can now access [http://127.0.0.1:4242/](http://127.0.0.1:4242/). You should get a welcome
screen with a link to the documentation and logs to the console.

By default, Hunchentoot serves the files from the `www/` directory in
its source tree. Thus, if you go to the source of
`easy-acceptor` (`M-.` in Slime), which is probably
`~/quicklisp/dists/quicklisp/software/hunchentoot-v1.2.38/`, you'll
find the `root/` directory. It contains:

- an `errors/` directory, with the error templates `404.html` and `500.html`,
- an `img/` directory,
- an `index.html` file.

To serve another directory, we give the option `document-root` to
`easy-acceptor`. We can also set the slot with its accessor:

~~~lisp
(setf (hunchentoot:acceptor-document-root *acceptor*) #p"path/to/www")
~~~

Let's create our `index.html` first. Put this in a new
`www/index.html` at the current directory (of the lisp repl):

~~~html
<html>
  <head>
    <title>Hello!</title>
  </head>
  <body>
    <h1>Hello local server!</h1>
    <p>
    We just served our own files.
    </p>
  </body>
</html>
~~~

Let's start a new acceptor on a new port:

~~~lisp
(defvar *my-acceptor* (make-instance 'hunchentoot:easy-acceptor :port 4444
                                   :document-root #p"www/"))
(hunchentoot:start *my-acceptor*)
~~~

go to [http://127.0.0.1:4444/](http://127.0.0.1:4444/) and see the difference.

Note that we just created another *acceptor* on a different port on
the same lisp image. This is already pretty cool.


# Access your server from the internet

## Hunchentoot

With Hunchentoot we have nothing to do, we can see the server from the
internet right away.

If you evaluate this on your VPS:

    (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 4242))

You can see it right away on your server's IP.

Stop it with `(hunchentoot:stop *)`.

# Routing

## Simple routes

### Hunchentoot

To bind an existing function to a route, we create a "prefix dispatch"
that we push onto the `*dispatch-table*` list:

~~~lisp
(defun hello ()
   (format nil "Hello, it works!"))

(push
  (hunchentoot:create-prefix-dispatcher "/hello.html" #'hello)
  hunchentoot:*dispatch-table*)
~~~

To create a route with a regexp, we use `create-regex-dispatcher`, where
the url-as-regexp can be a string, an s-expression or a cl-ppcre scanner.

If you didn't yet, create an acceptor and start the server:

~~~lisp
(defvar *server* (make-instance 'hunchentoot:easy-acceptor :port 4242))
(hunchentoot:start *server*)
~~~

and access it on [http://localhost:4242/hello.html](http://localhost:4242/hello.html).

We can see logs on the REPL:

```
127.0.0.1 - [2018-10-27 23:50:09] "get / http/1.1" 200 393 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0"
127.0.0.1 - [2018-10-27 23:50:10] "get /img/made-with-lisp-logo.jpg http/1.1" 200 12583 "http://localhost:4242/" "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0"
127.0.0.1 - [2018-10-27 23:50:10] "get /favicon.ico http/1.1" 200 1406 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0"
127.0.0.1 - [2018-10-27 23:50:19] "get /hello.html http/1.1" 200 20 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0"
```

---

[define-easy-handler](https://edicl.github.io/hunchentoot/#define-easy-handler) allows to create a function and to bind it to an uri at once.

Its form follows

    define-easy-handler (function-name :uri <uri> …) (lambda list parameters)

where `<uri>` can be a string or a function.

Example:

~~~lisp
(hunchentoot:define-easy-handler (say-yo :uri "/yo") (name)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "Hey~@[ ~A~]!" name))
~~~

Visit it at [p://localhost:4242/yo](http://localhost:4242/yo) and add parameters on the url:
[http://localhost:4242/yo?name=Alice](http://localhost:4242/yo?name=Alice).

Just a thought… we didn't explicitly ask Hunchentoot to add this
route to our first acceptor of the port 4242. Let's try another acceptor (see
previous section), on port 4444: [http://localhost:4444/yo?name=Bob](http://localhost:4444/yo?name=Bob) It
works too ! In fact, `define-easy-handler` accepts an `acceptor-names`
parameter:

> acceptor-names (which is evaluated) can be a list of symbols which means that the handler will only be returned by DISPATCH-EASY-HANDLERS in acceptors which have one of these names (see ACCEPTOR-NAME). acceptor-names can also be the symbol T which means that the handler will be returned by DISPATCH-EASY-HANDLERS in every acceptor.


So, `define-easy-handler` has the following signature:

    define-easy-handler (function-name &key uri acceptor-names default-request-type) (lambda list parameters)

It also has a `default-parameter-type` which we'll use in a minute to get url parameters.

There are also keys to know for the lambda list. Please see the documentation.


### Easy-routes (Hunchentoot)

[easy-routes](https://github.com/mmontone/easy-routes) is a route
handling extension on top of Hunchentoot. It provides:

- dispatch based on HTTP method (otherwise cumbersome in Hunchentoot)
- arguments extraction from the url path
- and decorators.

To use it, don't create a server with `hunchentoot:easy-acceptor` but
with `easy-routes:routes-acceptor`:

~~~lisp
(setf *server* (make-instance 'easy-routes:routes-acceptor))
~~~

Then define a route like this:

~~~lisp
(easy-routes:defroute name ("/foo/:x" :method :get) (y &get z)
    (format nil "x: ~a y: ~y z: ~a" x y z))
~~~

Here, `:x` captures the path parameter and binds it to the `x`
variable into the route body. `y` and `&get z` define url parameters,
and we can have `&post` parameters to extract from the HTTP request
body.

These parameters can take an `:init-form` and `:parameter-type`
options as in `define-easy-handler`.

**Decorators** are functions that are executed before the route body. They
should call the `next` parameter function to continue executing the
decoration chain and the route body finally. Examples:

~~~lisp
(defun @auth (next)
  (let ((*user* (hunchentoot:session-value 'user)))
    (if (not *user*)
	(hunchentoot:redirect "/login")
	(funcall next))))

(defun @html (next)
  (setf (hunchentoot:content-type*) "text/html")
  (funcall next))

(defun @json (next)
  (setf (hunchentoot:content-type*) "application/json")
  (funcall next))
(defun @db (next)
  (postmodern:with-connection *db-spec*
    (funcall next)))
~~~

See `easy-routes`' readme for more.

### Caveman

[Caveman](caveman) provides two ways to
define a route: the `defroute` macro and the `@route` pythonic
*annotation*:

~~~lisp
(defroute "/welcome" (&key (|name| "Guest"))
  (format nil "Welcome, ~A" |name|))

@route GET "/welcome"
(lambda (&key (|name| "Guest"))
  (format nil "Welcome, ~A" |name|))
~~~

A route with an url parameter (note `:name` in the url):

~~~lisp
(defroute "/hello/:name" (&key name)
  (format nil "Hello, ~A" name))
~~~

It is also possible to define "wildcards" parameters. It works with
the `splat` key:

~~~lisp
(defroute "/say/*/to/*" (&key splat)
  ; matches /say/hello/to/world
  (format nil "~A" splat))
;=> (hello world)
~~~

We must enable regexps with `:regexp t`:

~~~lisp
(defroute ("/hello/([\\w]+)" :regexp t) (&key captures)
  (format nil "Hello, ~A!" (first captures)))
~~~


## Accessing GET and POST parameters

### Hunchentoot

First of all, note that we can access query parameters anytime with

~~~lisp
(hunchentoot:parameter "my-param")
~~~

It acts on the default `*request*` object which is passed to all handlers.

There is also `get-parameter` and `post-parameter`.


Earlier we saw some key parameters to `define-easy-handler`. We now
introduce `default-parameter-type`.

We defined the following handler:

~~~lisp
(hunchentoot:define-easy-handler (say-yo :uri "/yo") (name)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "Hey~@[ ~A~]!" name))
~~~

The variable `name` is a string by default. Let's check it out:

~~~lisp
(hunchentoot:define-easy-handler (say-yo :uri "/yo") (name)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "Hey~@[ ~A~] you are of type ~a" name (type-of name)))
~~~

Going to [http://localhost:4242/yo?name=Alice](http://localhost:4242/yo?name=Alice) returns

    Hey Alice you are of type (SIMPLE-ARRAY CHARACTER (5))

To automatically bind it to another type, we use `default-parameter-type`. It can be
one of those simple types:

* `'string` (default),
* `'integer`,
* `'character` (accepting strings of length 1 only, otherwise it is nil)
* or `'boolean`

or a compound list:

- `'(:list <type>)`
- `'(:array <type>)`
- `'(:hash-table <type>)`

where `<type>` is a simple type.


<!-- ## Sessions -->

<!-- todo ? -->

<!-- ## Cookies -->

# Error handling

In all frameworks, we can choose the level of interactivity. The web
framework can return a 404 page and print output on the repl, it can
catch errors and invoke the interactive lisp debugger, or it can show
the lisp backtrace on the html page.

## Hunchentoot

The global variables to set are `*catch-errors-p*`,
`*show-lisp-errors-p*` and `*show-lisp-backtraces-p*`.

Hunchentoot also defines condition classes.

See the documentation: [https://edicl.github.io/hunchentoot/#conditions](https://edicl.github.io/hunchentoot/#conditions).


## Clack

Clack users might make a good use of plugins, like the clack-errors middleware: [https://github.com/CodyReichert/awesome-cl#clack-plugins](https://github.com/CodyReichert/awesome-cl#clack-plugins).

<img src="https://camo.githubusercontent.com/17dd6e0a7a916c8118f0134a94404f6757bee9dc/68747470733a2f2f7261772e6769746875622e636f6d2f6575646f786961302f636c61636b2d6572726f72732f6d61737465722f73637265656e73686f742d6465762e706e67" width="800"/>

# Weblocks - solving the "JavaScript problem"©

[Weblocks][weblocks] is a widgets-based and
server-based framework with a built-in ajax update mechanism. It
allows to write dynamic web applications *without the need to write
JavaScript or to write lisp code that would transpile to JavaScript*.

![](http://40ants.com/weblocks/_images/quickstart-check-task.gif)

Weblocks is an old framework developed by Slava Akhmechet, Stephen
Compall and Leslie Polzer. After nine calm years, it is seeing a very
active update, refactoring and rewrite effort by Alexander Artemenko.

It was initially based on continuations (they were removed to date)
and thus a lispy cousin of Smalltalk's
[Seaside](https://en.wikipedia.org/wiki/Seaside_(software)). We can
also relate it to Haskell's Haste, OCaml's Eliom,
Elixir's Phoenix LiveView and others.

The [Ultralisp](http://ultralisp.org/) website is an example Weblocks
website in production known in the CL community.

---

Weblock's unit of work is the *widget*. They look like a class definition:

~~~lisp
(defwidget task ()
        ((title
          :initarg :title
          :accessor title)
         (done
          :initarg :done
          :initform nil
          :accessor done)))
~~~

Then all we have to do is to define the `render` method for this widget:

~~~lisp
(defmethod render ((task task))
        "Render a task."
        (with-html
              (:span (if (done task)
                         (with-html
                               (:s (title task)))
                       (title task)))))
~~~

It uses the Spinneret template engine by default, but we can bind any
other one of our choice.

To trigger an ajax event, we write lambdas in full Common Lisp:

~~~lisp
...
(with-html
          (:p (:input :type "checkbox"
            :checked (done task)
            :onclick (make-js-action
                      (lambda (&key &allow-other-keys)
                        (toggle task))))
...
~~~

The function `make-js-action` creates a simple javascript function
that calls the lisp one on the server, and automatically refreshes the
HTML of the widgets that need it. In our example, it re-renders one
task only.

Is it appealing ? Carry on this quickstart guide here: [http://40ants.com/weblocks/quickstart.html](http://40ants.com/weblocks/quickstart.html).


# Templates

## Djula - HTML markup

[Djula](https://github.com/mmontone/djula) is a port of Python's
Django template engine to Common Lisp. It has [excellent documentation](https://mmontone.github.io/djula/doc/build/html/index.html).

Caveman uses it by default, but otherwise it is not difficult to
setup. We must declare where our templates are with something like

~~~lisp
(djula:add-template-directory (asdf:system-relative-pathname "webapp" "templates/"))
~~~


A Djula template looks like this, no surprises (forgive the antislash
in `\%`, this is a Jekyll limitation):

```
{\% extends "base.html" \%}
{\% block title %}Memberlist{\% endblock \%}
{\% block content \%}
  <ul>
  {\% for user in users \%}
    <li><a href="{{ user.url }}">{{ user.username }}</a></li>
  {\% endfor \%}
  </ul>
{\% endblock \%}
```

Djula compiles the templates before rendering them.

It is, along with its companion
[access](https://github.com/AccelerationNet/access/) library, one of
the most downloaded libraries of Quicklisp.

## Spinneret - lispy templates

[Spinneret](https://github.com/ruricolist/spinneret) is a "lispy"
HTML5 generator. It looks like this:

~~~lisp
(with-page (:title "Home page")
     (:header
      (:h1 "Home page"))
     (:section
      ("~A, here is *your* shopping list: " *user-name*)
      (:ol (dolist (item *shopping-list*)
             (:li (1+ (random 10)) item))))
     (:footer ("Last login: ~A" *last-login*)))
~~~

The author finds it is easier to compose the HTML in separate
functions and macros than with the more famous cl-who. But it
has more features under it sleeves:

- it warns on invalid tags and attributes
- it can automatically number headers, given their depth
- it pretty prints html per default, with control over line breaks
- it understands embedded markdown
- it can tell where in the document a generator function is (see `get-html-tag`)


# Connecting to a database

Please see the [databases section](databases.html). The Mito ORM
supports SQLite3, PostgreSQL, MySQL, it has migrations and db schema
versioning, etc.

In Caveman, a database connection is alive during the Lisp session and is
reused in each HTTP requests.

## Checking a user is logged-in

A framework will provide a way to work with sessions. We'll create a
little macro to wrap our routes to check if the user is logged in.

In Caveman, `*session*` is a hash table that represents the session's
data. Here are our login and logout functions:

~~~lisp
(defun login (user)
  "Log the user into the session"
  (setf (gethash :user *session*) user))

(defun logout ()
  "Log the user out of the session."
  (setf (gethash :user *session*) nil))
~~~

We define a simple predicate:

~~~lisp
(defun logged-in-p ()
  (gethash :user cm:*session*))
~~~

and we define our `with-logged-in` macro:

~~~lisp
(defmacro with-logged-in (&body body)
  `(if (logged-in-p)
       (progn ,@body)
       (render #p"login.html"
               '(:message "Please log-in to access this page."))))
~~~

If the user isn't logged in, there will nothing in the session store,
and we render the login page. When all is well, we execute the macro's
body. We use it like this:

~~~lisp
(defroute "/account/logout" ()
  "Show the log-out page, only if the user is logged in."
  (with-logged-in
    (logout)
    (render #p"logout.html")))

(defroute ("/account/review" :method :get) ()
  (with-logged-in
    (render #p"review.html"
            (list :review (get-review (gethash :user *session*))))))
~~~

and so on.


## Encrypting passwords

In this recipe we use the de-facto standard
[Ironclad](https://github.com/froydnj/ironclad) cryptographic toolkit
and the [Babel](https://github.com/cl-babel/babel) charset
encoding/decoding library.

This snippet creates the password hash that should be stored in your
database. Note that Ironclad expects a byte-vector, not a string.

~~~lisp
(defun password-hash (password)
  (ironclad:pbkdf2-hash-password-to-combined-string
   (babel:string-to-octets password)))
~~~

`pbkdf2` is defined in [RFC2898](https://tools.ietf.org/html/rfc2898).
It uses a pseudorandom function to derive a secure encryption key
based on the password.

The following function checks if a user is active and verifies the
entered password. It returns the user-id if active and verified and
nil in all other cases even if an error occurs. Adapt it to your
application.

~~~lisp
(defun check-user-password (user password)
  (handler-case
      (let* ((data (my-get-user-data user))
             (hash (my-get-user-hash data))
             (active (my-get-user-active data)))
        (when (and active (ironclad:pbkdf2-check-password (babel:string-to-octets password)
                                                          hash))
          (my-get-user-id data)))
    (condition () nil)))
~~~

And the following is an example on how to set the password on the
database. Note that we use `(password-hash password)` to save the
password. The rest is specific to the web framework and to the DB
library.

~~~lisp
(defun set-password (user password)
  (with-connection (db)
    (execute
     (make-statement :update :web_user
                     (set= :hash (password-hash password))
                     (make-clause :where
                                  (make-op := (if (integerp user)
                                                  :id_user
                                                  :email)
                                           user))))))
~~~

*Credit: `/u/arvid` on [/r/learnlisp](https://www.reddit.com/r/learnlisp/comments/begcf9/can_someone_give_me_an_eli5_on_hiw_to_encrypt_and/)*.

# Building

## Building a self-contained executable

As for all Common Lisp applications, we can bundle our web app in one
single executable, including the assets. It makes deployment very
easy: copy it to your server and run it.

```
$ ./my-web-app
Hunchentoot server is started.
Listening on localhost:9003.
```

See this recipe on [scripting#for-web-apps](scripting.html#for-web-apps).


## Continuous delivery with Travis CI or Gitlab CI

Please see the section on [testing#continuous-integration](testing.html#continuous-integration).


## Multi-platform delivery with Electron

[Ceramic](https://ceramic.github.io/) makes all the work for us.

It is as simple as this:

~~~lisp
;; Load Ceramic and our app
(ql:quickload '(:ceramic :our-app))

;; Ensure Ceramic is set up
(ceramic:setup)
(ceramic:interactive)

;; Start our app (here based on the Lucerne framework)
(lucerne:start our-app.views:app :port 8000)

;; Open a browser window to it
(defvar window (ceramic:make-window :url "http://localhost:8000/"))

;; start Ceramic
(ceramic:show-window window)
~~~

and we can ship this on Linux, Mac and Windows.

There is more:

> Ceramic applications are compiled down to native code, ensuring both performance and enabling you to deliver closed-source, commercial applications.

Thus, no need to minify our JS.

# Deployment

## Deploying manually

We can start our executable in a shell and send it to the background (`C-z bg`), or run it inside a `tmux` session. These are not the best but hey, it works©.


## Daemonizing, restarting in case of crashes, handling logs with Systemd

This is actually a system-specific task. See how to do that on your system.

Most GNU/Linux distros now come with Systemd, so here's a little example.

Deploying an app with Systemd is as simple as writing a configuration file:

```
$ emacs -nw /etc/systemd/system/my-app.service
[Unit]
Description=stupid simple example

[Service]
WorkingDirectory=/path/to/your/app
ExecStart=/usr/local/bin/sthg sthg
Type=simple
Restart=always
RestartSec=10
```

Then we have a command to start it:

    sudo systemctl start my-app.service

a command to check its status:

    systemctl status my-app.service


and Systemd can handle **logging** (we write to stdout or stderr, it writes logs):

    journalctl -f -u my-app.service


and it handles crashes and **restarts the app**:

    Restart=always

and it can **start the app after a reboot**:

    [Install]
    WantedBy=basic.target

to enable it:

    sudo systemctl enable my-app.service


## With Docker

There are several Docker images for Common
Lisp. For example:

- [40ants/base-lisp-image](https://github.com/40ants/base-lisp-image)
is based on Ubuntu LTS and includes SBCL, CCL, Quicklisp, Qlot and
Roswell.
- [container-lisp/s2i-lisp](https://github.com/container-lisp/s2i-lisp)
is CentOs based and contains the source for building a Quicklisp based
Common Lisp application as a reproducible docker image using OpenShift's
source-to-image.


## With Guix

[GNU Guix](https://www.gnu.org/software/guix/) is a transactional
package manager, that can be installed on top of an existing OS, and a
whole distro that supports declarative system configuration. It allows
to ship self-contained tarballs, which also contain system
dependencies. For an example, see the [Next browser](https://github.com/atlas-engineer/next/).


## Deploying on Heroku and other services

See [heroku-buildpack-common-lisp](https://gitlab.com/duncan-bayne/heroku-buildpack-common-lisp) and the [Awesome CL#deploy](https://github.com/CodyReichert/awesome-cl#deployment) section for interface libraries for Kubernetes, OpenShift, AWS, etc.


## Monitoring

See [Prometheus.cl](https://github.com/deadtrickster/prometheus.cl)
for a Grafana dashboard for SBCL and Hunchentoot metrics (memory,
threads, requests per second,…).

## Connecting to a remote Lisp image

This this section: [debugging#remote-debugging](debugging.html#remote-debugging).

## Hot reload

This is an example from [Quickutil](https://github.com/stylewarning/quickutil/blob/master/quickutil-server/). It is actually an automated version of the precedent section.

It has a Makefile target:

```lisp
hot_deploy:
	$(call $(LISP), \
		(ql:quickload :quickutil-server) (ql:quickload :swank-client), \
		(swank-client:with-slime-connection (conn "localhost" $(SWANK_PORT)) \
			(swank-client:slime-eval (quote (handler-bind ((error (function continue))) \
				(ql:quickload :quickutil-utilities) (ql:quickload :quickutil-server) \
				(funcall (symbol-function (intern "STOP" :quickutil-server))) \
				(funcall (symbol-function (intern "START" :quickutil-server)) $(start_args)))) conn)) \
		$($(LISP)-quit))
```

It has to be run on the server (a simple fabfile command can call this
through ssh). Beforehand, a `fab update` has run `git pull` on the
server, so new code is present but not running. It connects to the
local swank server, loads the new code, stops and starts the app in a
row.


# Credits

- [https://lisp-journey.gitlab.io/web-dev/](https://lisp-journey.gitlab.io/web-dev/)

[hunchentoot]: https://edicl.github.io/hunchentoot
[clack]: https://github.com/fukamachi/clack
[caveman]: https://github.com/fukamachi/caveman
[radiance]: https://github.com/Shirakumo/radiance
[snooze]: https://github.com/joaotavora/snooze
[cl-rest-server]: https://github.com/mmontone/cl-rest-server
[weblocks]: https://github.com/40ants/weblocks

---
title: Web Scraping
---

The set of tools to do web scraping in Common Lisp is pretty complete
and pleasant. In this short tutorial we'll see how to make http
requests, parse html, extract content and do asynchronous requests.

Our simple task will be to extract the list of links on the CL
Cookbook's index page and check if they are reachable.

We'll use the following libraries:

- [Dexador](https://github.com/fukamachi/dexador) - an HTTP client
  (that aims at replacing the venerable Drakma),
- [Plump](https://shinmera.github.io/plump/) - a markup parser, that works on malformed HTML,
- [Lquery](https://shinmera.github.io/lquery/) - a DOM manipulation
  library, to extract content from our Plump result,
- [lparallel](https://lparallel.org/pmap-family/) -  a library for parallel programming (read more in the [process section](process.html)).

Before starting let's install those libraries with Quicklisp:

~~~lisp
(ql:quickload '(:dexador :plump :lquery :lparallel))
~~~

## HTTP Requests

Easy things first. Install Dexador. Then we use the `get` function:

~~~lisp
(defvar *url* "https://lispcookbook.github.io/cl-cookbook/")
(defvar *request* (dex:get *url*))
~~~

This returns a list of values: the whole page content, the return code
(200), the response headers, the uri and the stream.

```
"<!DOCTYPE html>
 <html lang=\"en\">
  <head>
    <title>Home &ndash; the Common Lisp Cookbook</title>
    […]
    "
200
#<HASH-TABLE :TEST EQUAL :COUNT 19 {1008BF3043}>
#<QURI.URI.HTTP:URI-HTTPS https://lispcookbook.github.io/cl-cookbook/>
#<CL+SSL::SSL-STREAM for #<FD-STREAM for "socket 192.168.0.23:34897, peer: 151.101.120.133:443" {100781C133}>>

```

Remember, in Slime we can inspect the objects with a right-click on
them.

## Parsing and extracting content with CSS selectors

We'll use `lquery` to parse the html and extract the
content.

- [https://shinmera.github.io/lquery/](https://shinmera.github.io/lquery/)

We first need to parse the html into an internal data structure. Use
`(lquery:$ (initialize <html>))`:

~~~lisp
(defvar *parsed-content* (lquery:$ (initialize *request*)))
;; => #<PLUMP-DOM:ROOT {1009EE5FE3}>
~~~

lquery uses [plump](https://shinmera.github.io/plump/) internally.

Now we'll extract the links with CSS selectors.

__Note__: to find out what should be the CSS selector of the element
I'm interested in, I right click on an element in the browser and I
choose "Inspect element". This opens up the inspector of my browser's
web dev tool and I can study the page structure.

So the links I want to extract are in a page with an `id` of value
"content", and they are in regular list elements (`li`).

Let's try something:

~~~lisp
(lquery:$ *parsed-content* "#content li")
;; => #(#<PLUMP-DOM:ELEMENT li {100B3263A3}> #<PLUMP-DOM:ELEMENT li {100B3263E3}>
;;  #<PLUMP-DOM:ELEMENT li {100B326423}> #<PLUMP-DOM:ELEMENT li {100B326463}>
;;  #<PLUMP-DOM:ELEMENT li {100B3264A3}> #<PLUMP-DOM:ELEMENT li {100B3264E3}>
;;  #<PLUMP-DOM:ELEMENT li {100B326523}> #<PLUMP-DOM:ELEMENT li {100B326563}>
;;  #<PLUMP-DOM:ELEMENT li {100B3265A3}> #<PLUMP-DOM:ELEMENT li {100B3265E3}>
;;  #<PLUMP-DOM:ELEMENT li {100B326623}> #<PLUMP-DOM:ELEMENT li {100B326663}>
;;  […]
~~~

Wow it works ! We get here a vector of plump elements.

I'd like to easily check what those elements are. To see the entire
html, we can end our lquery line with `(serialize)`:

~~~lisp
(lquery:$  *parsed-content* "#content li" (serialize))
#("<li><a href=\"license.html\">License</a></li>"
  "<li><a href=\"getting-started.html\">Getting started</a></li>"
  "<li><a href=\"editor-support.html\">Editor support</a></li>"
  […]
~~~

And to see their *textual* content (the user-visible text inside the
html), we can use `(text)` instead:

~~~lisp
(lquery:$  *parsed-content* "#content" (text))
#("License" "Editor support" "Strings" "Dates and Times" "Hash Tables"
  "Pattern Matching / Regular Expressions" "Functions" "Loop" "Input/Output"
  "Files and Directories" "Packages" "Macros and Backquote"
  "CLOS (the Common Lisp Object System)" "Sockets" "Interfacing with your OS"
  "Foreign Function Interfaces" "Threads" "Defining Systems"
  […]
  "Pascal Costanza’s Highly Opinionated Guide to Lisp"
  "Loving Lisp - the Savy Programmer’s Secret Weapon by Mark Watson"
  "FranzInc, a company selling Common Lisp and Graph Database solutions.")
~~~

All right, so we see we are manipulating what we want. Now to get their
`href`, a quick look at lquery's doc and we'll use `(attr
"some-name")`:


~~~lisp
(lquery:$  *parsed-content* "#content li a" (attr :href))
;; => #("license.html" "editor-support.html" "strings.html" "dates_and_times.html"
;;  "hashes.html" "pattern_matching.html" "functions.html" "loop.html" "io.html"
;;  "files.html" "packages.html" "macros.html"
;;  "/cl-cookbook/clos-tutorial/index.html" "os.html" "ffi.html"
;;  "process.html" "systems.html" "win32.html" "testing.html" "misc.html"
;;  […]
;;  "http://www.nicklevine.org/declarative/lectures/"
;;  "http://www.p-cos.net/lisp/guide.html" "https://leanpub.com/lovinglisp/"
;;  "https://franz.com/")
~~~

*Note*: using `(serialize)` after `attr` leads to an error.

Nice, we now have the list (well, a vector) of links of the
page. We'll now write an async program to check and validate they are
reachable.

External resources:

- [CSS selectors by example](https://codingsec.net/2016/12/select-specific-text-css-using-selectors/)

## Async requests

In this example we'll take the list of url from above and we'll check
if they are reachable. We want to do this asynchronously, but to see
the benefits we'll first do it synchronously !

We need a bit of filtering first to exclude the email addresses (maybe
that was doable in the CSS selector ?).

We put the vector of urls in a variable:

~~~lisp
(defvar *urls* (lquery:$  *parsed-content* "#content li a" (attr :href)))
~~~

We remove the elements that start with "mailto:": (a quick look at the
[strings](strings.html) page will help)

~~~lisp
(remove-if (lambda (it) (string= it "mailto:" :start1 0 :end1 (length "mailto:"))) *urls*)
;; => #("license.html" "editor-support.html" "strings.html" "dates_and_times.html"
;;  […]
;;  "process.html" "systems.html" "win32.html" "testing.html" "misc.html"
;;  "license.html" "http://lisp-lang.org/"
;;  "https://github.com/CodyReichert/awesome-cl"
;;  "http://www.lispworks.com/documentation/HyperSpec/Front/index.htm"
;;  […]
;;  "https://franz.com/")
~~~

Actually before writing the `remove-if` (which works on any sequence,
including vectors) I tested with a `(map 'vector …)` to see that the
results where indeed `nil` or `t`.

As a side note, there is a handy `starts-with` function in
[cl-strings](https://github.com/diogoalexandrefranco/cl-strings/),
available in Quicklisp. So we could do:

~~~lisp
(map 'vector (lambda (it) (cl-strings:starts-with it "mailto:")) *urls*)
~~~

it also has an option to ignore or respect the case.

While we're at it, we'll only consider links starting with "http", in
order not to write too much stuff irrelevant to web scraping:

~~~lisp
(remove-if-not (lambda (it) (string= it "http" :start1 0 :end1 (length "http"))) *) ;; note the remove-if-NOT
~~~

All right, we put this result in another variable:

~~~lisp
(defvar *filtered-urls* *)
~~~

and now to the real work. For every url, we want to request it and
check that its return code is 200. We have to ignore certain
errors. Indeed, a request can timeout, be redirected (we don't want
that) or return an error code.

To be in real conditions we'll add a link that times out in our list:

~~~lisp
(setf (aref *filtered-urls* 0) "http://lisp.org")  ;; too bad indeed
~~~

We'll take the simple approach to ignore errors and return `nil` in
that case. If all goes well, we return the return code, that should be
200.

As we saw at the beginning, `dex:get` returns many values, including
the return code. We'll catch only this one with `nth-value` (instead
of all of them with `multiple-value-bind`) and we'll use
`ignore-errors`, that returns nil in case of an error. We could also
use `handler-case` and catch specific error types (see examples in
dexador's documentation) or (better yet ?) use `handler-bind` to catch
any `condition`.

(*ignore-errors has the caveat that when there's an error, we can not
return the element it comes from. We'll get to our ends though.*)


~~~lisp
(map 'vector (lambda (it)
  (ignore-errors
    (nth-value 1 (dex:get it))))
  *filtered-urls*)
~~~

we get:

```
#(NIL 200 200 200 200 200 200 200 200 200 200 NIL 200 200 200 200 200 200 200
  200 200 200 200)
```

it works, but *it took a very long time*. How much time precisely ?
with `(time …)`:

```
Evaluation took:
  21.554 seconds of real time
  0.188000 seconds of total run time (0.172000 user, 0.016000 system)
  0.87% CPU
  55,912,081,589 processor cycles
  9,279,664 bytes consed
```

21 seconds ! Obviously this synchronous method isn't efficient. We
wait 10 seconds for links that time out. It's time to write and
measure and async version.

After installing `lparallel` and looking at
[its documentation](https://lparallel.org/), we see that the parallel
map [pmap](https://lparallel.org/pmap-family/) seems to be what we
want. And it's only a one word edit. Let's try:

~~~lisp
(time (lparallel:pmap 'vector
  (lambda (it)
    (ignore-errors (let ((status (nth-value 1 (dex:get it)))) status)))
  *filtered-urls*)
;;  Evaluation took:
;;  11.584 seconds of real time
;;  0.156000 seconds of total run time (0.136000 user, 0.020000 system)
;;  1.35% CPU
;;  30,050,475,879 processor cycles
;;  7,241,616 bytes consed
;;
;;#(NIL 200 200 200 200 200 200 200 200 200 200 NIL 200 200 200 200 200 200 200
;;  200 200 200 200)
~~~

Bingo. It still takes more than 10 seconds because we wait 10 seconds
for one request that times out. But otherwise it proceeds all the http
requests in parallel and so it is much faster.

Shall we get the urls that aren't reachable, remove them from our list
and measure the execution time in the sync and async cases ?

What we do is: instead of returning only the return code, we check it
is valid and we return the url:

~~~lisp
... (if (and status (= 200 status)) it) ...
(defvar *valid-urls* *)
~~~

we get a vector of urls with a couple of `nil`s: indeed, I thought I
would have only one unreachable url but I discovered another
one. Hopefully I have pushed a fix before you try this tutorial.

But what are they ? We saw the status codes but not the urls :S We
have a vector with all the urls and another with the valid ones. We'll
simply treat them as sets and compute their difference. This will show
us the bad ones. We must transform our vectors to lists for that.

~~~lisp
(set-difference (coerce *filtered-urls* 'list)
                (coerce *valid-urls* 'list))
;; => ("http://lisp-lang.org/" "http://www.psg.com/~dlamkins/sl/cover.html")
~~~

Gotcha !

BTW it takes 8.280 seconds of real time to me to check the list of
valid urls synchronously, and 2.857 seconds async.

Have fun doing web scraping in CL !


More helpful libraries:

- we could use [VCR](https://github.com/tsikov/vcr), a store and
  replay utility to set up repeatable tests or to speed up a bit our
  experiments in the REPL.
- [cl-async](https://github.com/orthecreedence/cl-async),
  [carrier](https://github.com/orthecreedence/carrier) and others
  network, parallelism and concurrency libraries to see on the
  [awesome-cl](https://github.com/CodyReichert/awesome-cl) list,
  [Cliki](http://www.cliki.net/) or
  [Quickdocs](http://quickdocs.org/search?q=web).

---
title: WebSockets
---

The Common Lisp ecosystem boasts a few approaches to building WebSocket servers.
First, there is the excellent
[Hunchensocket](https://github.com/joaotavora/hunchensocket) that is written as
an extension to [Hunchentoot](https://edicl.github.io/hunchentoot/), the classic
web server for Common Lisp.  I have used both and I find them to be wonderful.

Today, however, you will be using the equally excellent
[websocket-driver](https://github.com/fukamachi/websocket-driver) to build a WebSocket server with
[Clack](https://github.com/fukamachi/clack). The Common Lisp web development community has expressed a
slight preference for the Clack ecosystem because Clack provides a uniform interface to
a variety of backends, including Hunchentoot. That is, with Clack, you can pick and choose the
backend you prefer.

In what follows, you will build a simple chat server and connect to it from a
web browser. The tutorial is written so that you can enter the code into your
REPL as you go, but in case you miss something, the full code listing can be found at the end.

As a first step, you should load the needed libraries via quicklisp:

~~~lisp

(ql:quickload '(clack websocket-driver alexandria))

~~~


## The websocket-driver Concept

In websocket-driver, a WebSocket connection is an instance of the `ws` class,
which exposes an event-driven API. You register event handlers by passing your
WebSocket instance as the second argument to a method called `on`. For example,
calling `(on :message my-websocket #'some-message-handler)` would invoke
`some-message-handler` whenever a new message arrives.

The `websocket-driver` API provides handlers for the following events:

- `:open`: When a connection is opened. Expects a handler with zero arguments.
- `:message` When a message arrives. Expects a handler with one argument, the message received.
- `:close` When a connection closes. Expects a handler with two keyword args, a
  "code" and a "reason" for the dropped connection.
- `:error` When some kind of protocol level error occurs. Expects a handler with
  one argument, the error message.

For the purposes of your chat server, you will want to handle three cases: when
a new user arrives to the channel, when a user sends a message to the channel,
and when a user leaves.

## Defining Handlers for Chat Server Logic

In this section you will define the functions that your event handlers will
eventually call. These are helper functions that manage the chat server logic.
You will define the WebSocket server in the next section.

First, when a user connects to the server, you need to give that user a nickname
so that other users know whose chats belong to whom. You will also need a data
structure to map individual WebSocket connections to nicknames:

~~~lisp

;; make a hash table to map connections to nicknames
(defvar *connections* (make-hash-table))

;; and assign a random nickname to a user upon connection
(defun handle-new-connection (con)
  (setf (gethash con *connections*)
        (format nil "user-~a" (random 100000))))

~~~

Next, when a user sends a chat to the room, the rest of the room should be
notified. The message that the server receives is prepended with the nickname of
the user who sent it.

~~~lisp

(defun broadcast-to-room (connection message)
  (let ((message (format nil "~a: ~a"
                         (gethash connection *connections*)
                         message)))
    (loop :for con :being :the :hash-key :of *connections* :do
          (websocket-driver:send con message))))
~~~

Finally, when a user leaves the channel, by closing the browser tab or
navigating away, the room should be notified of that change, and the user's
connection should be dropped from the `*connections*` table.

~~~lisp
(defun handle-close-connection (connection)
  (let ((message (format nil " .... ~a has left."
                         (gethash connection *connections*))))
    (remhash connection *connections*)
    (loop :for con :being :the :hash-key :of *connections* :do
          (websocket-driver:send con message))))
~~~

## Defining A Server

Using Clack, a server is started by passing a function to `clack:clackup`. You
will define a function called `chat-server` that you will start by
calling `(clack:clackup #'chat-server :port 12345)`.

A Clack server function accepts a single plist as its argument. That plist
contains environment information about a request and is provided by the system.
Your chat server will not make use of that environment, but if you want to learn
more you can check out Clack's documentation.

When a browser connects to your server, a websocket will be instantiated and
handlers will be defined on it for each of the the events you want to support.
A WebSocket "handshake" will then be sent back to the browser, indicating
that the connection has been made. Here's how it works:

~~~lisp
(defun chat-server (env)
  (let ((ws (websocket-driver:make-server env)))

    (websocket-driver:on :open ws
                         (lambda () (handle-new-connection ws)))

    (websocket-driver:on :message ws
                         (lambda (msg) (broadcast-to-room ws msg)))

    (websocket-driver:on :close ws
                         (lambda (&key code reason)
                           (declare (ignore code reason))
                           (handle-close-connection ws)))

    (lambda (responder)
      (declare (ignore responder))
      (websocket-driver:start-connection ws)))) ; send the handshake

~~~

You may now start your server, running on port `12345`:

~~~lisp
;; keep the handler around so that you can stop your server later on

(defvar *chat-handler* (clack:clackup #'chat-server :port 12345))
~~~


## A Quick HTML Chat Client

So now you need a way to talk to your server. Using Clack, define a simple
application that serves a web page to display and send chats.  First the web page:

~~~lisp

(defvar *html*
  "<!doctype html>

<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
  <title>LISP-CHAT</title>
</head>

<body>
    <ul id=\"chat-echo-area\">
    </ul>
    <div style=\"position:fixed; bottom:0;\">
        <input id=\"chat-input\" placeholder=\"say something\" >
    </div>
    <script>
     window.onload = function () {
         const inputField = document.getElementById(\"chat-input\");

         function receivedMessage(msg) {
             let li = document.createElement(\"li\");
             li.textContent = msg.data;
             document.getElementById(\"chat-echo-area\").appendChild(li);
         }

         const ws = new WebSocket(\"ws://localhost:12345/chat\");
         ws.addEventListener('message', receivedMessage);

         inputField.addEventListener(\"keyup\", (evt) => {
             if (evt.key === \"Enter\") {
                 ws.send(evt.target.value);
                 evt.target.value = \"\";
             }
         });
     };

    </script>
</body>
</html>
")


(defun client-server (env)
    (declare (ignore env))
    `(200 (:content-type "text/html")
          (,*html*)))

~~~

You might prefer to put the HTML into a file, as escaping quotes is kind of annoying.
Keeping the page data in a `defvar` was simpler for the purposes of this
tutorial.

You can see that the `client-server` function just serves the HTML content. Go
ahead and start it, this time on port `8080`:

~~~lisp
(defvar *client-handler* (clack:clackup #'client-server :port 8080))
~~~

## Check it out!

Now open up two browser tabs and point them to `http://localhost:8080` and you
should see your chat app!

<img src="https://raw.githubusercontent.com/thegoofist/resource-dump/master/lisp-chat.gif"
     width="470" height="247"/>

## All The Code

~~~lisp
(ql:quickload '(clack websocket-driver alexandria))

(defvar *connections* (make-hash-table))

(defun handle-new-connection (con)
  (setf (gethash con *connections*)
        (format nil "user-~a" (random 100000))))

(defun broadcast-to-room (connection message)
  (let ((message (format nil "~a: ~a"
                         (gethash connection *connections*)
                         message)))
    (loop :for con :being :the :hash-key :of *connections* :do
          (websocket-driver:send con message))))

(defun handle-close-connection (connection)
  (let ((message (format nil " .... ~a has left."
                         (gethash connection *connections*))))
    (remhash connection *connections*)
    (loop :for con :being :the :hash-key :of *connections* :do
          (websocket-driver:send con message))))

(defun chat-server (env)
  (let ((ws (websocket-driver:make-server env)))
    (websocket-driver:on :open ws
                         (lambda () (handle-new-connection ws)))

    (websocket-driver:on :message ws
                         (lambda (msg) (broadcast-to-room ws msg)))

    (websocket-driver:on :close ws
                         (lambda (&key code reason)
                           (declare (ignore code reason))
                           (handle-close-connection ws)))
    (lambda (responder)
      (declare (ignore responder))
      (websocket-driver:start-connection ws))))

(defvar *html*
  "<!doctype html>

<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
  <title>LISP-CHAT</title>
</head>

<body>
    <ul id=\"chat-echo-area\">
    </ul>
    <div style=\"position:fixed; bottom:0;\">
        <input id=\"chat-input\" placeholder=\"say something\" >
    </div>
    <script>
     window.onload = function () {
         const inputField = document.getElementById(\"chat-input\");

         function receivedMessage(msg) {
             let li = document.createElement(\"li\");
             li.textContent = msg.data;
             document.getElementById(\"chat-echo-area\").appendChild(li);
         }

         const ws = new WebSocket(\"ws://localhost:12345/\");
         ws.addEventListener('message', receivedMessage);

         inputField.addEventListener(\"keyup\", (evt) => {
             if (evt.key === \"Enter\") {
                 ws.send(evt.target.value);
                 evt.target.value = \"\";
             }
         });
     };

    </script>
</body>
</html>
")

(defun client-server (env)
  (declare (ignore env))
  `(200 (:content-type "text/html")
     (,*html*)))

(defvar *chat-handler* (clack:clackup #'chat-server :port 12345))
(defvar *client-handler* (clack:clackup #'client-server :port 8080))
~~~

---
title: Common Lisp for the 21st century
---

[CL21](http://cl21.org/) is an experimental project redesigning Common
Lisp. It makes some common things that were tedious to do much easier
and brings some new welcome features:

* more functional programming facilities,
* more object oriented, generic functions.
* new syntax (for regular expressions, hash-tables, string-interpolation,…),
* symbols organized into several packages,
* written in pure Common Lisp.

CL21 is a bit disruptive in the sense that it redefines usual symbols
(specially for the generic functions). Nevertheless, in doubt, *you can always use a regular CL symbol* by accessing it in the `cl:` package. You might want to check
other related projects, that go beyond
[Alexandria](https://common-lisp.net/project/alexandria) and stay
"good citizens" of the CL world:

- [rutils](https://github.com/vseloved/rutils/blob/master/docs/ann-rutils.md) -
  a comprehensive and all-encompassing suite of syntactic utilities to
  support modern day-to-day Common Lisp development. It adds readtable
  literal syntax for shorter lambdas, hash-tables and vectors
  (<code>#`…`</code>, `#h` and `#{…}`, `#v`), some generic functions
  (whose name start with `generic-`),
  [shortcuts](https://github.com/vseloved/rutils/blob/master/core/abbr.lisp)
  for standard operators, and many helpful functions.
- [Serapeum](https://github.com/TBRSS/serapeum) is a set of utilities
  beyond Alexandria, as a supplement. It defines a lot of helper
  functions (see its
  [reference](https://github.com/TBRSS/serapeum/blob/master/REFERENCE.md))
  for macros, data structures (including more functions for trees,
  queues), sequences (`partition`, `keep`,…), strings, definitions
  (`defalias`,…),… no new reader macros here.


## Motivation

From [http://cl21.org/](http://cl21.org/) :

> Dear Common Lispers,
>
> Common Lisp has the most expressive power of any modern language. It has first class functions with lexical closures, an object system with multiple-dispatch and a metaobject protocol, true macros, and more. It is ANSI standardized and has numerous high-performance implementations, many of which are free software.
>
> In spite of this, it has not had much success (at least in Japan). Its community is very small compared to languages like Ruby and most young Lispers are hacking with Clojure.
>
> Why? Common Lisp is much faster than them. Ruby has no macros and even Clojure doesn't have reader macros. Why then?
>
> Because these languages are well-designed and work for most people for most purposes. These languages are easy to use and the speed isn't an issue.
>
> Is Common Lisp sufficiently well-designed? I don't think so. You use different functions to do the same thing to different data types (elt, aref, nth). You have long names for commonly used macros (destructuring-bind, multiple-value-bind). There is no consistency in argument order (getf and gethash). To put it simply, the language is time-consuming to learn.
>
> Given this, how can programmers coming from other languages believe Common Lisp is the most expressive one?
>
> Fortunately in Common Lisp we can improve the interface with abstractions such as functions, macros, and reader macros. If you believe our language is the most expressive and powerful language, then let's justify that belief.
>
> We should consider the future of the language, not only for ourselves but for the next generation.


## Install and use

CL21 is in Quicklisp.

To get its latest version, do:

~~~lisp
(ql-dist:install-dist "http://dists.cl21.org/cl21.txt")
(ql:quickload :cl21)
~~~

Use:

~~~lisp
(in-package :cl21-user)
(defpackage myapp (:use :cl21))
(in-package :myapp)
~~~


## Features

Please bear in mind that the following is only a summary of CL21
features. To be sure not to miss anything, you should read CL21's
[wiki](https://github.com/cl21/cl21/wiki/Language-Difference-between-CL21-and-Common-Lisp),
its automated
[list of major changes from standard CL](https://github.com/cl21/cl21/blob/master/CHANGES_AUTO.markdown)
and better yet, its [sources](https://github.com/cl21/cl21/tree/master/).

That said, go include CL21 in your new project and enjoy, it's worth it !

### Functional programming

#### Shorter lambda

`lm` is a macro for creating an anonymous function:


~~~lisp
(lm (x) (typep x 'cons))
;=> #<FUNCTION (LAMBDA (X)) {1008F50DAB}>

(map (lm (x) (+ 2 x)) '(1 2 3))
;=> (3 4 5)
~~~

`^` is a reader macro which will be expanded to `lm`.

~~~lisp
^(typep % 'cons)
;=> #<FUNCTION (LAMBDA (%1 &REST #:G1156 &AUX ...)) {10092490CB}>

(map ^(+ 2 %) '(1 2 3))
;=> (3 4 5)
~~~

Unused arguments will be ignored automatically.

~~~lisp
(map ^(random 10) (iota 10))
;=> (6 9 5 1 3 5 4 0 7 4)
~~~

`%n` designates the nth argument (1-based). `%` is a synonym for `%1`.

~~~lisp
(sort '(6 9 5 1 3 5 4 0 7 4) ^(< %1 %2))
;=> (0 1 3 4 4 5 5 6 7 9)
~~~

#### `function` and `compose`

`function` is a special operator for getting a function value from a given form.

If a symbol is given, `function` returns a function value of it.

~~~lisp
(function integerp)
;=> #<FUNCTION INTEGERP>
~~~

If a form which starts with `compose`, `and`, `or` or `not`, `function` returns a composed function.

~~~lisp
(function (compose - *))
<=> (compose (function -) (function *))

(function (and integerp evenp))
<=> (conjoin (function integerp) (function evenp))

(function (or oddp zerop))
<=> (disjoin (function oddp) (function zerop))

(function (not zerop))
<=> (complement (function zerop))
~~~

`#'` is a reader macro for `function`.

~~~lisp
#'(compose - *)
#'(and integerp evenp)
#'(or oddp zerop)
#'(not zerop)
#'(and integerp (or oddp zerop))
~~~

#### Currying

CL21 gets new symbols: `curry` and `rcurry`. See also
[functions](functions.html#currying-functions).

#### Lazy sequences

Lazy sequences in CL21
([src](https://github.com/cl21/cl21/blob/master/src/stdlib/lazy.lisp))
use the new
[abstract classes (wiki)](https://github.com/cl21/cl21/wiki/Abstract-Classes).

~~~lisp
(use-package :cl21.lazy)

(defun fib-seq ()
  (labels ((rec (a b)
             (lazy-sequence (cons a (rec b (+ a b))))))
    (rec 0 1)))

(take 20 (fib-seq))
;=> (0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181)

(take 3 (drop-while (lambda (x) (< x 500)) (fib-seq)))
;=> (610 987 1597)
~~~

#### Immutable data structures

This actually is not included in CL21 but may be worth the addition in
this section. For immutable data structures, see the
[Fset](https://github.com/slburson/fset) library (in Quicklisp).


### Generic functions

There are several generic functions which have the same name to CL's normal functions.

* getf
* equalp
* emptyp
* coerce

~~~lisp
(defvar *hash* #H(:name "Eitaro Fukamachi" :living "Japan"))

(getf *hash* :name)
;=> "Eitaro Fukamachi"

(coerce *hash* 'plist)
;=> (:LIVING "Japan" :NAME "Eitaro Fukamachi")
~~~

You can define these methods for your own classes.

There are also new functions:

* `append`
* `flatten`
* `elt`: Returns the element at position INDEX of SEQUENCE or signals
a ABSTRACT-METHOD-UNIMPLMENETED error if the sequence method is not
implemented for the class of SEQUENCE.
* `emptyp`
* `equalp`
* `split`, `split-if`
* `drop`, `drop-while`
* `take`, `take-while`
* `join`
* `length`
* `keep`, `keep-if`, `nkeep`, `nkeep-if`
* `partition`, `partition-if`

### Mapping

In Common Lisp, functions which have a name starts with "map" are higher-order functions take a function and a sequence.

It is same to CL21, but in CL21, "map" functions always return a value. This aims to clarify the roles of "iteration" and "mapping".

For that reason, CL21 doesn't have CL's `mapc` and `mapl`. `maphash` exists, but it returns a new hash table.

~~~lisp
(maphash (lm (k v)
           (cons k (1+ v)))
         #H(:a 1 :b 2))
;=> #H(:B 3 :A 2)
~~~

`map` is similar to Common Lisp's `mapcar` except it can take any kind of sequences, not only list.

~~~lisp
(map #'- '(1 2 3 4))
;=> (-1 -2 -3 -4)

(map #'- #(1 2 3 4))
;=> #(-1 -2 -3 -4)
~~~

CL21 doesn't have `mapcar`. Use `map` instead.

Filter is provided with `keep`, `keep-if` (instead of
`remove-if[-not]`) and `nkeep[-if]`.

### Iteration

Common Lisp has simple iteration facilities: `dolist`, `dotimes` and `dolist`.

In addition, CL21 provides another one: `doeach`.

`doeach` is similar to `dolist`, but it can be used with any kind of sequences and hash-table.

~~~lisp
(doeach (x '("al" "bob" "joe"))
  (when (> (length x) 2)
    (princ #"${x}\n")))
;-> bob
;   joe

(doeach ((key value) #H('a 2 'b 3))
  (when (> value 2)
    (print key)))
;=> B
~~~

Destructuring binding form can be placed at the variable position.

~~~lisp
(doeach ((x y) '((1 2) (2 3) (3 4)))
  (print (+ x y)))
;-> 3
;   5
;   7
~~~

CL21 also gets a `while` keyword.

### New data types

New data types were added.

* proper-list
* plist
* alist
* octet
* file-associated-stream
* character-designator
* function-designator
* file-position-designator
* list-designator
* package-designator
* stream-designator
* string-designator

Most of these are imported from [trivial-types](https://github.com/m2ym/trivial-types).

### String

A double quote character is a macro character which represents a string.

~~~lisp
"Hello, World!"
;=> "Hello, World!"
~~~

A backslash followed by some characters will be treated special.

~~~lisp
"Hello\nWorld!"
;=> "Hello
;   World!"
~~~

#### String interpolation

`#"` is similar to `"`, but it allows interpolation within strings.

If `${...}` or `@{...}` is seen, it will be replaced by the result value of an expression (or the last expression) inside of it.

~~~lisp
#"1 + 1 = ${(+ 1 1)}"
;=> "1 + 1 = 2"
~~~

### Hash table

CL21 provides a notation for hash-tables.

~~~lisp
#H(:name "Eitaro Fukamachi" :living "Japan")
;=> #H(:LIVING "Japan" :NAME "Eitaro Fukamachi")
~~~

Note this always creates a hash-table whose test function is `EQUAL`. If you want to create it with another test function, a function `hash-table` is also available.

~~~lisp
(hash-table 'eq :name "Eitaro Fukamachi")
;=> #H(:NAME "Eitaro Fukamachi")
; instead of
; (defvar *hash* (make-hash-table))
; (setf (gethash :name *hash*) "Eitaro Fukamachi")
~~~

Accessing an element is also done with `getf` (instead of `gethash`):

~~~lisp
(getf *hash* :name)
~~~

Looping over a hash-table:

~~~lisp
(doeach ((key val) *hash*)
  (when (< (length key) 2)
    (princ #"${x}\n")))
~~~

Transform a hash-table into a plist:

~~~lisp
(coerce *hash* 'plist)
~~~

### Vector

`#(...)` is a notation for vectors. Unlike in Common Lisp, its elements will be evaluated and it creates an adjustable vector.

~~~lisp
(let ((a 1)
      (b 2)
      (c 3))
  #(a b c))
;=> #(1 2 3)
~~~

~~~lisp
(defvar *vec* #(0))

(push 1 *vec*)
;=> #(1 0)

(push-back 3 *vec*)
;=> #(1 0 3)

(pop *vec*)
;=> 1
~~~

### Regular Expressions

This new regexp reader macro uses the new
["Syntax"](https://github.com/cl21/cl21/wiki/Language-Difference-between-CL21-and-Common-Lisp#syntax)
extension.

~~~lisp
(use-package :cl21.re)

(#/^(\d{4})-(\d{2})-(\d{2})$/ "2014-01-23")

(re-replace #/a/ig "Eitaro Fukamachi" "α")
~~~

### Running external programs (cl21.process)

With `run-process`:

~~~lisp
(use-package :cl21.process)

(run-process '("ls" "-l" "/Users"))
;-> total 0
;   drwxrwxrwt    5 root         wheel   170 Nov  1 18:00 Shared
;   drwxr-xr-x+ 174 nitro_idiot  staff  5916 Mar  5 21:41 nitro_idiot
;=> #<PROCESS /bin/sh -c ls -l /Users (76468) EXITED 0>
~~~

or the <code class="highlighter-rouge">#`</code> reader macro:

~~~
#`ls -l /Users`
;=> "total 0
;   drwxrwxrwt    5 root         wheel   170 Nov  1 18:00 Shared
;   drwxr-xr-x+ 174 nitro_idiot  staff  5916 Mar  5 21:41 nitro_idiot
;   "
;   ""
;   0
~~~

### Naming of constants

All constant variables were renamed to the name added "+" signs before and after.

~~~lisp
+pi+
;=> 3.141592653589793d0

+array-rank-limit+
;=> 65529
~~~

### CL21 Standard Library

CL21 Standard Library is a set of libraries that are distributed with CL21. It is intended to offer a wide range of facilities.

We are working on increasing the number of it. Currently, the following packages are provided.

* cl21.re
* cl21.process
* cl21.os
* cl21.lazy
* cl21.abbr

---
title: Miscellaneous
---


<a name="opt"></a>

## Re-using complex data structures

Sometimes you want your functions to behave in a 'functional' way, i.e. return [fresh](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_f.htm#fresh) results without side effects, sometimes you want them to re-use and modify existing data in a destructive way - consider the difference between [`append`](http://www.lispworks.com/documentation/HyperSpec/Body/f_append.htm) and [`nconc`](http://www.lispworks.com/documentation/HyperSpec/Body/f_nconc.htm) for an example.

Well, you can have your cake and eat it too, by using optional (or keyword) parameters. Here's an example: Let's assume you're writing a function `complex-matrix-stuff` which takes two matrices `m1` and `m2` as its arguments and computes and returns a resulting matrix the size of which depends on `m1` and `m2`, i.e. for a fresh result you'll need an empty matrix which'll be created by, say, `(make-appropriate-result-matrix-for m1 m2)`.

The classical textbook way to implement this function will more or less look like this:

~~~lisp
(defun complex-matrix-stuff (m1 m2)
  (let ((result (make-appropriate-result-matrix-for m1 m2)))
    ;; ... compute storing the results in RESULT
    result))
~~~

And you'll use it like this:

~~~lisp
(setq some-matrix (complex-matrix-stuff A B))
~~~

But why not write it like so:

~~~lisp
(defun complex-matrix-stuff (m1 m2
                             &optional
                             (result
                              (make-appropriate-result-matrix-for m1 m2)))
  ;; ... compute storing the results in RESULT
  result)
~~~

Now you have it both ways. You can still "make up results" on the fly as in:

~~~lisp
(setq some-matrix (complex-matrix-stuff A B))
~~~

But you can also (destructively) re-use previously allocated matrices:

~~~lisp
(complex-matrix-stuff A B some-appropriate-matrix-I-built-before)
~~~

Or use your function like this:

~~~lisp
(setq some-other-matrix
      (complex-matrix-stuff A B some-appropriate-matrix-I-built-before))
~~~

in which case you'll end up with:

~~~lisp
* (eq some-other-matrix some-appropriate-matrix-I-built-before)

T
~~~


<a name="adjust"></a>

## Using `ADJUST-ARRAY` instead of consing up new sequences with `SUBSEQ`

Most CL functions operating on sequences will accept `start` and `end` keywords so you can make them operate on a sub-sequence without actually creating it, i.e. instead of

~~~lisp
(count #\a (subseq long-string from to))
~~~

you should of course use

~~~lisp
(count #\a long-string :start from :end to)
~~~

which'll yield the same result but not create an unnecessary intermediate sub-sequence.

However, sometimes it looks like you can't avoid creating new data. Consider a hash table the keys of which are strings. If the key you're looking for is a sub-string of another string you'll most likely end up with

~~~lisp
(gethash (subseq original-string from to)
         hash-table)
~~~

But you don't have to. You can create _one_ [displaced](http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_d.htm#displaced_array) string and reuse it multiple times with [`adjust-array`](http://www.lispworks.com/documentation/HyperSpec/Body/f_adjust.htm):

~~~lisp
(let ((substring (make-array 0
                             :element-type 'character
                             :displaced-to ""
                             :displaced-index-offset 0)))
  ;; more code
  (gethash
   (adjust-array substring (- to from)
                 :displaced-to original-string
                 :displaced-index-offset from)
   hash-table)
  ;; even more code
  )
~~~